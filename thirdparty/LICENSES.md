Third-Party Licenses
====================
This is a list of third-party libraries bundled with Arcmancer and their
respective licenses.


* [eigen3-hdf5](https://github.com/garrison/eigen3-hdf5), by James R. Garrison   
   License Name: MIT License  
   License File: [LICENSE](./include/eigen3-hdf5/LICENSE)  
   Files List:  
    1. `thirdparty/include/eigen3-hdf5/*`


* [Eigen](http://eigen.tuxfamily.org/)  
   License Name: Mozilla Public License 2.0  
   License File: [MPL-2.0.txt](./share/licenses/MPL-2.0.txt)  
   Files List:
    1. `thirdparty/include/Eigen/*`
    2. `thirdparty/include/unsupported/Eigen/*`


* [pybind11](https://github.com/pybind/pybind11/), by Wenzel Jakob  
   License File: [LICENSE](../pyarcmancer/pybind11/LICENSE)
   Files List:
    1. `pyarcmancer/pybind11/*`


* [spdlog](https://github.com/gabime/spdlog), by Gabi Melman  
   License Name: MIT License  
   License File: [LICENSE](./include/spdlog/LICENSE)
   Files List:
    1. `thirdparty/include/spdlog/*`
