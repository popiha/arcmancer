#pragma once

#include <algorithm>
#include <functional>
#include <vector>
#include <type_traits>

#include "arcmancer/geometry.h"
#include "arcmancer/hdf5_utils.h"
#include "arcmancer/log.h"

namespace arcmancer {

// Image plane generation

/** \brief Image plane initial conditions and image computation.
 *
 *  
 *  This class represents the apparent plane through which all rays contributing 
 *  to an image pass.
 *  It generates the initial conditions for the arriving light rays and allows 
 *  performing computations based on them to form an image.
 *
 *
 *  Currently supports two projections: a plane of large physical extent through 
 *  which the rays pass perpendicularly and an incomplete perspective projection.
 *  
 * 
 *  Only defined for Lorentzian spacetimes.
 *
 * \tparam MetricSpaceType the type of the underlying manifold.
 * \tparam DataType type data to compute
 *
 * \ingroup radiation
 *
 */
template <typename MetricSpaceType, typename DataType>
class ImagePlane {
    static_assert(MetricSpaceType::is_lorentzian,
                  "Only defined for Lorentzian spacetimes!");

public:

    /** \brief Initial conditions at a pixel.
     * \ingroup overaligned
     */
    struct ImagePoint {
        // Add an aligned new just to be sure, even though these should only be
        // allocated in the containers.
        EIGEN_MAKE_ALIGNED_OPERATOR_NEW

        size_t ix, iy; 
        ///< indices of the pixel
        
        double x, y;
        ///< coordinates of the pixel in the coordinate system of the image 
        /// plane
        
        TangentVector<MetricSpaceType> ray_tangent;
        ///< Tangent vector of the incoming ray
        
        LorentzFrame<MetricSpaceType> frame;
        ///< The local observer frame corresponding at this point on the image
        /// plane. The ray travels in the z-direction.

        ImagePoint(size_t ix, size_t iy, double x, double y,
                   const TangentVector<MetricSpaceType> &ray_tangent,
                   const LorentzFrame<MetricSpaceType> &frame)
        : ix(ix), iy(iy), x(x), y(y), ray_tangent(ray_tangent), frame(frame) {}
    };

    // struct for representing computed values on the image grid
    /** \brief Computed data at a pixel.
     */
    struct ImageDataPoint {
        // Depending on the `Data` type, this may also be overaligned
        EIGEN_MAKE_ALIGNED_OPERATOR_NEW

        size_t ix, iy;
        ///< indices of the pixel
        double x, y;
        ///< coordinates of the pixel in the coordinate system of the image 
        /// plane
        DataType data;
        ///< arbitrary computed data.

        ImageDataPoint(size_t ix, size_t iy, double x, double y,
                       const DataType &data)
        : ix(ix), iy(iy), x(x), y(y), data(data) {}
    };

    /** \brief Shape of the image plane
     */
    struct PlaneShape {
        size_t xbins, ybins; 
        ///< number of pixels
        
        /**  \brief Size in the projection dependent coordinate system
         * plane extends from -span to span,
         * and the pixel edges match the plane edges.
         */
        double xspan, yspan;
        double dx, dy, pixel_area; ///< pixel size

        PlaneShape(size_t xbins, size_t ybins, double xspan, double yspan)
        : xbins(xbins), ybins(ybins), xspan(xspan), yspan(yspan) {
            dx = 2 * xspan / xbins;
            dy = 2 * yspan / ybins;
            pixel_area = dx * dy;
        }
    };

    /** \brief Projection types
     */
    enum class Projection {
        /** \brief A large plane with parallel transported basis.
         *
         *  This projection approximates a far-away observer with a plane
         *  constructed in Riemann normal coordinates, with the original basis
         *  parallel transported to each pixel.
         *  The x,y coordinates correspond to physical distances from the
         *  origin.
         */
        ParallelPlane,
        /** \brief A simple perspective projection.
         *
         *  In this projection the rays of light meet at the observer position
         *  and appear to pass through a plane located at z=-1.
         *  The x,y coordinates of the image plane correspond to the positions
         *  where the rays intersect this apparent plane.
         *
         */
        Perspective
    };

    // The image plane normal points in the e_z direction of the frame,
    // x,y axes and the horizontal and vertical polarization in the e_x and e_y
    // directions.
    /** \brief Construct from a LorentzFrame.
     *
     *
     * \param shape the shape of the image plane.
     * \param projection the projection to use.
     * \param frame the local Lorentz frame at the origin. 
     *      The image plane extends along the x,y-axes of this frame, with rays
     *      arriving along the z-axis.
     */
    ImagePlane(const PlaneShape &shape, Projection projection,
               const LorentzFrame<MetricSpaceType> &frame)
    : shape_(shape), projection_(projection)  {
        
        // create the rows
        // XXX: this is somewhat inconvenient, but using nested std::vectors as
        // the containers should interface nicely with pybind, which compensates
        // for this.
        points_.resize(shape_.ybins);
        data_points_.resize(shape_.ybins);

        switch (projection) {
            case Projection::ParallelPlane:
                parallel_plane(frame);
                break;
            case Projection::Perspective:
                perspective(frame);
                break;
        }
    }
    
    /** \brief Construct from a LorentzFrame with the default projection
     *
     *  Projection defaults to `Projection::ParallelPlane`.
     *
     *
     * \param shape the shape of the image plane.
     * \param frame the local Lorentz frame at the origin. 
     *      The image plane extends along the x,y-axes of this frame, with rays
     *      arriving along the z-axis.
     */
    ImagePlane(const PlaneShape &shape, 
               const LorentzFrame<MetricSpaceType> &frame)
    : ImagePlane(shape, Projection::ParallelPlane, frame) {}
    
    ImagePlane(const PlaneShape &shape, Projection projection,
               const TangentVector<MetricSpaceType> &obs_u,
               const TangentVector<MetricSpaceType> &approximate_z,
               const TangentVector<MetricSpaceType> &approximate_x) :
        ImagePlane(shape, projection, obs_u, approximate_z, approximate_x,
                LorentzFrame<MetricSpaceType>::Handedness::RIGHT_HANDED) {}

    ImagePlane(const PlaneShape &shape,
               const TangentVector<MetricSpaceType> &obs_u,
               const TangentVector<MetricSpaceType> &approximate_z,
               const TangentVector<MetricSpaceType> &approximate_x)
    : ImagePlane(shape, Projection::ParallelPlane, obs_u, approximate_z,
                 approximate_x,
                 LorentzFrame<MetricSpaceType>::Handedness::RIGHT_HANDED) {}

    ImagePlane(const PlaneShape &shape, Projection projection,
               const TangentVector<MetricSpaceType> &obs_u,
               const TangentVector<MetricSpaceType> &approximate_z,
               const TangentVector<MetricSpaceType> &approximate_x,
               const typename LorentzFrame<MetricSpaceType>::Handedness &h)
    : ImagePlane(shape, projection, {obs_u, approximate_z, approximate_x, h}) {}

    /** \brief Construct from another ImagePlane
     *
     *  Allows copying the constructed image plane while changing the DataType.
     *
     */
    template <typename T>
    ImagePlane(const ImagePlane<MetricSpaceType, T> &other)
    : shape_(other.shape()),
      projection_(other.projection()),
      points_(other.points()) {

        data_points_.resize(shape_.ybins);
    }

    const PlaneShape &shape() const { return shape_; }
    ///< Get the shape of the image plane

    Projection projection() { return projection_; }
    ///< Get the projection used

    using PointContainer =
        std::vector<std::vector<ImagePoint, aligned_allocator<ImagePoint>>>;
    ///< Container type for image plane points.

    /** \brief Get the ImagePoint container for the image plane
     *  
     * The points are stored as nested `std::vector`s with matrix indexing.
     *
     */
    const PointContainer &points() const {
        return points_;
    }

    // XXX: Since `ImageDataPoint` is not necessarily overaligned, could
    // make the allocator conditional. But the possible extra cost of
    // aligned allocation should be minimal anyway.
    using DataPointContainer = std::vector<
        std::vector<ImageDataPoint, aligned_allocator<ImageDataPoint>>>;
    ///< Container type for data points.

    /** \brief Get the computed data points
     *
     * The points are stored as nested lists, indexed as [iy][ix], i.e.
     * each inner list is a row of the image.
     *
     * The inner lists are empty if compute has not been called.
     *
     */
    const DataPointContainer &data_points() const {
        //TODO: should maybe throw if compute hasn't been called

        return data_points_;
    }

    /** \brief Run a given computation on all pixels.
     *
     *  The results are available through the `data_points` and
     * `scalar_data_array`methods after the computation is done.
     *
     *  The computation is parallelized if compiled with OpenMP,
     *  and progress is reported in the console log at the info level.
     *
     *  \param f the computation to run on each pixel: a function of signature
     * `T(const ImagePoint&)` where `T` is a type convertible to `DataType`.
     *
     */
    template <typename F>
    void compute(const F &f) {
        static_assert(
            std::is_convertible<
                std::result_of_t<F(const ImagePoint &)>, DataType>::value,
            "Return type of f is not convertible to DataType!");

        // if already computed, clear old data
        // XXX: this may cause problems if data is cleared while a reference
        // to the data is held through the data_points() getter.
        // If that turns out to be the case, data_points() could be changed to
        // give a copy instead
        if(data_points_[0].size() != 0) {
            data_points_.clear();
            data_points_.resize(shape_.ybins);
        }

        size_t rows_done = 0;
        size_t report_interval = shape_.ybins/10;

        if (report_interval == 0) report_interval = 1;

// Use OpenMP
// Dynamic scheduling should work well since in general the inner loop is fairly
// expensive, but different rows can have significantly different costs.
#ifdef _OPENMP
#pragma omp parallel for schedule(dynamic)
#endif
        for (size_t iy = 0; iy < shape_.ybins; ++iy) {
            data_points_[iy].reserve(shape_.xbins);
            for (size_t ix = 0; ix < shape_.xbins; ++ix) {
                const ImagePoint &ip = points_[iy][ix];
                // The parallel loop is over the iy index, so this is safe
                data_points_[iy].emplace_back(ip.ix, ip.iy, ip.x, ip.y, f(ip));
            }

#ifdef _OPENMP
#pragma omp critical(arcmancer_image_progress)
#endif
            {
                ++rows_done;
                if(rows_done % report_interval == 0) {
                    Log::console()->info("ImagePlane.compute: {:3.0f}% done", 
                        static_cast<double>(rows_done)/shape_.ybins*100);    
                }
            }
        }
    }

    /** \brief Get an array of computed scalar data.
     *
     *  Useful for extracting data for furhter processing or for writing to file
     *  using \c EigenHDF5.
     *
     *  The data is stored in an `Eigen::Array` with shape `(ybins, xbins)`,
     *  i.e. the row index is first.
     *  The stored data must be of a scalar type supported by Eigen.
     *
     *  \param converter a function extracting scalar data of type \c D from an
     *      \c ImageDataPoint. By default extracts the \c data member.
     *
     *  \tparam D a scalar datatype supported by `Eigen::Array` to extract.
     *
     *  \return An array of data extracted from the computed points.
     */
    template <typename D = DataType>
    Eigen::Array<D, Eigen::Dynamic, Eigen::Dynamic> scalar_data_array(
        std::function<D(const ImageDataPoint &)> converter =
            [](const ImageDataPoint &p) -> D { return p.data; }) const {

        Eigen::Array<D, Eigen::Dynamic, Eigen::Dynamic> data(shape_.ybins,
                                                             shape_.xbins);

        for (const auto &row : data_points_) {
            for (const ImageDataPoint &x : row) {
                data(x.iy, x.ix) = converter(x);
            }
        }

        return data;
    }

    /**
     *  Overload of the above for template argument deduction from lambdas.
     * 
     */
    template <typename F>
    auto scalar_data_array(const F converter) const {

        using D = std::result_of_t<F(const ImageDataPoint &)>;
        return scalar_data_array(
            std::function<D(const ImageDataPoint &)>(converter));
    }

    //TODO: accept more general locations than H5File
    /** \brief Write metadata to a HDF5 location.
     *
     *  Adds attributes containing the image plane shape and projection to
     *  the given \p h5obj.
     *  The attributes are named as the corresponding members of \c PlaneShape,
     *  e.g. \c xbins.
     */
    void write_plane_attributes(const H5::H5File &h5obj) const {

        EigenHDF5::save_scalar_attribute(h5obj, "xbins", shape_.xbins);
        EigenHDF5::save_scalar_attribute(h5obj, "ybins", shape_.ybins);
        EigenHDF5::save_scalar_attribute(h5obj, "xspan", shape_.xspan);
        EigenHDF5::save_scalar_attribute(h5obj, "yspan", shape_.yspan);
        EigenHDF5::save_scalar_attribute(h5obj, "dx", shape_.dx);
        EigenHDF5::save_scalar_attribute(h5obj, "dy", shape_.dy);
        EigenHDF5::save_scalar_attribute(h5obj, "pixel_area",
                                         shape_.pixel_area);
        EigenHDF5::save_scalar_attribute(h5obj, "projection",
                                         static_cast<int>(projection_));
    }

    // XXX: This can be removed, as does not really offer much over directly 
    // using the hdf5/EigenHDF5 libraries with the above methods.
    /** \brief Save the image data in a HDF5 file.
     *
     *  \param computation existing output data from a compute() call
     *  \param filename The HDF5 file where the computation should be
     *  stored. NOTE: The file will be silently overwritten.
     *  \param converter Optional converter taking ImageDataPoint
     *  to a plain D to be stored. Default behaviour is to store DataType.
     *  \param description Optional metadata description of the type of
     *  data saved.
     *
     */
    template <typename D = DataType>
    void save_data(std::string filename,
                   std::string description = std::string(),
                   std::function<D(const ImageDataPoint &)> converter =
                       [](const ImageDataPoint &p) -> D { return p.data; }) {

        // transform vector data into a matrix and save the matrix.
        // naturally T must be both a valid type for Eigen, and also
        // something Eigen-hdf5 supports. use convention where x-pixels
        // are along columns (numpy compatible)

        Log::log()->info("save_computation: Opening hdf5 file {}", filename);
        H5::H5File file(filename, H5F_ACC_TRUNC);

        // add image plane metadata
        Log::log()->info("Saving IP metadata");

        write_plane_attributes(file);

        // create & save into DataSet, then add metadata
        EigenHDF5::save(file, "/data", scalar_data_array(converter));

        Log::log()->info("Saving data metadata");
        auto ds = file.openDataSet("/data");
        EigenHDF5::save_scalar_attribute(ds, "description", description);

        file.close();
    }

private:
    PlaneShape shape_;
    Projection projection_;

    PointContainer points_;
    DataPointContainer data_points_;

    // Construct a plane of parallel transported rays
    // xspan, yspan are the physical dimensions of the plane
    void parallel_plane(
        const LorentzFrame<MetricSpaceType> &frame) {
        Log::log()->info("parallel_plane: Generating image plane...");

#ifdef _OPENMP
#pragma omp parallel for schedule(dynamic)
#endif
        for (size_t iy = 0; iy < shape_.ybins; ++iy) {
            for (size_t ix = 0; ix < shape_.xbins; ++ix) {
                // pixel center position
                double x = -shape_.xspan + (ix + 0.5) * shape_.dx;
                double y = -shape_.yspan + (iy + 0.5) * shape_.dy;

                // parallel transport the local frame to the pixel location
                Geodesic<MetricSpaceType, LorentzFrame<MetricSpaceType>> geo(
                    {frame, {0, x, y, 0}}, frame);
                geo.compute(1);
                auto transported_frame = geo.back().transported();

                // construct ray
                auto ray_tangent =
                    transported_frame.make_lightlike(transported_frame.e_z());

                // The parallel loop is over the iy index, so this shouldn't
                // have
                // any problems
                points_[iy].push_back(
                    {ix, iy, x, y, ray_tangent, transported_frame});
            }
        }
        
        Log::log()->info("parallel_plane: Done");
    }
    // image plane using perspective projection
    // xspan, yspan are the dimensions of the apparent image plane that is at
    // unit
    // distance from the observer
    // x,y are coordinates in the apparent plane, correspond to angles in some
    // way
    // the polarization vectors are along the apparent plane
    void perspective(
        const LorentzFrame<MetricSpaceType> &frame) {

        // TODO: need to abstract these loops away from the projection
        // implementations if we want to add more projections
        for (size_t iy = 0; iy < shape_.ybins; ++iy) {
            for (size_t ix = 0; ix < shape_.xbins; ++ix) {
                double x = -shape_.xspan + (ix + 0.5) * shape_.dx;
                double y = -shape_.yspan + (iy + 0.5) * shape_.dy;
                TangentVector<MetricSpaceType> ray_dir(frame, {0, -x, -y, 1});

                points_[iy].push_back(
                    {ix, iy, x, y, frame.make_lightlike(ray_dir), frame});
            }
        }
    }
};



} // namespace arcmancer
