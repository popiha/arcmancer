#pragma once

#include <cmath>
#include <stdexcept>
#include <string>
#include <ostream>
#include <iterator>

#include <Eigen/Core>
#include <Eigen/Dense>
#include <unsupported/Eigen/CXX11/Tensor>

#include <boost/math/constants/constants.hpp>

#include "arcmancer/log.h"

namespace arcmancer {


/** \brief  Mathematical constants
 * \ingroup utils
 */
namespace cnst = boost::math::double_constants;


/** \brief Proper mathematical sign function
 * \ingroup utils
 */
template <typename T>
inline
int sign(T x) {
    if (x == 0) {
        return 0;
    }

    return (std::signbit(x) ? -1 : 1);
}

/** \brief Check if value is within given range \c [low,high]
 *  \param val a value.
 *  \param low lower limit of the range.
 *  \param high upper limit of the range.
 *  \return True if value is in the specified range, false otherwise.
 *
 *  \ingroup utils
 */
template <typename T>
inline bool value_in_range(const T &val, const T &low, const T &high) {
    return !(val < low) && !(high < val);
}

/** \copybrief value_in_range
 *
 * \ingroup utils
 */
template <typename T>
inline bool value_in_range(const T &val, const std::pair<T, T> &interval) {
    return value_in_range(val, interval.first, interval.second);
}

/** \brief Cap a real value to an interval
 *  
 *  \return True if capped
 *  \ingroup utils
 */
inline
bool cap_value(double &value, std::pair<double,double> interval) {
    assert(interval.first <= interval.second);
    if (value < interval.first) {
        value = interval.first;
        return true;
    }
    if (value > interval.second) {
        value = interval.second;
        return true;
    }
    return false;
}

/** \copybrief cap_value
 *
 */
inline
bool cap_value(double &value, double first, double second) {
    return cap_value(value, {first, second});
}


//
// Numerical differentiation

/** \brief Order of finite differences
 *
 *  Used to specify the order of the finite differences used in the 
 *  `differentiate()` function.
 * 
 * \ingroup utils
 */
enum class DifferenceOrder { order_2, order_4, order_6 };

// TODO: Maybe add numerical derivative for general f: R^n -> R^m since
// it's easy-ish with Eigen facilities

/** \brief Numerically compute the derivative of a single variable function
 *
 *  Computes the derivative of func: \c ScalarType -> \c OutputType using
 *  central differences of the specified order.
 *
 *  \param func the function to differentiate.
 *  \param point the point where the derivative is evaluated at.
 *  \param step_size size of the difference step.
 *  \param order order of the finite difference expansion.
 *
 *  \tparam ScalarType the scalar type of the argument to \c func.
 *  \tparam OutputType the return value type of \c func. 
 *          Can be for example a vector type, but needs to support arithmetic
 *          operations.
 * 
 * \ingroup utils
 */
template <typename ScalarType, typename OutputType>
inline
OutputType differentiate(typename std::function<OutputType(ScalarType)> func,
                         ScalarType point, ScalarType step_size, 
                         DifferenceOrder order = DifferenceOrder::order_2) {

    // use central differences
    const ScalarType x = point;
    const ScalarType dx1 = step_size;

    const OutputType m1 = (1./2.0)*(func(x + dx1) - func(x - dx1));

    // O(dx^2)
    if (order == DifferenceOrder::order_2) {
        return (1.0/dx1) * m1;
    }

    // O(dx^4)
    const ScalarType dx2 = 2. * dx1;
    const OutputType m2 = (1/4.)*(func(x + dx2) - func(x - dx2)) ;

    const ScalarType three_dx1  = 3. * dx1;

    if (order == DifferenceOrder::order_4) {
        return (4. * m1 - m2)/three_dx1;
    }

    // O(dx^6)
    const ScalarType dx3 = 3. * dx1;
    const OutputType m3 = (1./6) * (func(x + dx3) - func(x - dx3));

    const OutputType fifteen_m1 = 15. * m1;
    const OutputType six_m2     =  6. * m2;
    const ScalarType ten_dx1    = 10. * dx1;

    return ((fifteen_m1 - six_m2) + m3) / ten_dx1;
}

/** \brief Numerically compute the directional derivative
 * \f$ \frac{f(x + h) - f(x-h)}{\||h\||} \f$.
 *
 * \param func The function \f$f:\mathbb{R}^n\rightarrow\mathbb{R}^m\f$
 * to differentiate.
 * \param point The point at which the derivative is evaluated at.
 * \param h The vector towards which to evalute the derivative. Use 
 * \f$h_i = \epsilon\delta_{ik}\f$ to compute the \f$k\f$'th partial
 * derivative with stepsize \f$\epsilon\f$.
 *
 * \tparam InputType Type of the domain of the function. Should be an
 * Eigen::Tensor.
 * \tparam OutputType Type of the codomain of the function. Can in
 * principle be any type supporting substraction and division by a
 * double.
 *
 * \ingroup utils
 */
template <typename InputType, typename OutputType>
inline
OutputType differentiate_n(typename std::function<OutputType(InputType)> func,
        InputType point, InputType h) {

    const Eigen::Tensor<double, 0> hsqnorm = h.square().sum();
    const double hnorm = std::sqrt(hsqnorm());

    return ( func(point+h) - func(point-h) ) / ( 2 * hnorm );
}

/** \brief Cast an enum value into the underlying type.
 *
 * \param e An enum value
 * \return The corresponding value of the underlying type.
 *
 * \ingroup utils
 */
template <typename E>
constexpr typename std::underlying_type<E>::type enum_to_underlying(E e) noexcept {
    return static_cast<typename std::underlying_type<E>::type>(e);
}

/** \brief A stream manipulator that skips \c n entries of type \c T from an input stream.
 *
 * \tparam n Number of entries to skip
 * \tparam T Type of entries to skip
 * \tparam Char Passed to std::basic_istream
 * \tparam Traits Passed to std::basic_istream
 *
 * \ingroup utils
 */
template<size_t n, typename T, typename Char, typename Traits>
inline std::basic_istream<Char, Traits>& skip(std::basic_istream<Char, Traits>& stream) {

    for (size_t i = 0; i < n; i++) {
        T unused;
        stream >> unused;
    }

    return stream;
}

/** \brief Pretty print a std::array to a stream.
 * \ingroup utils
 */
template <class T, std::size_t N>
inline std::ostream& operator<<(std::ostream& o, const std::array<T, N>& arr) {
    o << "[ ";
    copy(arr.cbegin(), arr.cend(), std::ostream_iterator<T>(o, " "));
    o << "]";
    return o;
}

/** \brief Numerically stable solution of a real quadratic equation
 * \f$ ax^2 + bx + c = 0.
 */
inline std::array<double, 2> solve_quadratic(double a, double b, double c) {
    // TODO: Add sanity checks -- but exceptions are very costly.
    double det = b*b - 4*a*c;
    if (det < 0) {
        Log::log()->warn("solve_quadratic: {}x^2 + {}x + {} = 0 has imaginary roots."
                "returning double root instead.", a, b, c);
        det = 0;
    }

    // short circuit for zero b required, since c/q is a division by
    // zero otherwise
    if (b == 0) {
        return { -std::sqrt(det) / (2*a), std::sqrt(det) / (2*a) };

    }

    const double q  = -0.5*(b + sign(b)*std::sqrt(det));
    return {q/a, c/q};
}


} // namespace arcmancer
