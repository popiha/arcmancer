#pragma once

#include <iostream>

#include <Eigen/Dense>

#include <H5Cpp.h>
#include <eigen3-hdf5/eigen3-hdf5.hpp>

#include "arcmancer/log.h"

// the eigen3-hdf5-library is missing some essentials, which we add by
// hand
namespace EigenHDF5 {
    using Log = arcmancer::Log;

// bool specialization
template <> struct DatatypeSpecialization<bool> {
    static inline const H5::DataType *get(void) {
        return &H5::PredType::NATIVE_INT;
    }
};

template <typename T>
void load_scalar(const H5::H5File &file, const std::string dataset_name,
                 T &scalar) {
    Log::log()->debug("load_scalar: loading dataset {}", dataset_name);

    const H5::DataType *const datatype = DatatypeSpecialization<T>::get();
    H5::DataSet dataset = file.openDataSet(dataset_name);
    assert(*datatype == dataset.getDataType());
    dataset.read(&scalar, *datatype);

    Log::log()->debug("load_scalar: read value {}", scalar);
}

template <typename T>
void save_scalar(H5::H5File &file, const std::string dataset_name, T scalar) {
    Log::log()->debug("save_scalar: saving value {} to dataset {}", scalar , dataset_name);

    const H5::DataType *const datatype = DatatypeSpecialization<T>::get();
    H5::DataSpace dataspace(H5S_SCALAR);
    H5::DataSet dataset =
        file.createDataSet(dataset_name, *datatype, dataspace);
    dataset.write(&scalar, *datatype);
}

template <typename T>
void load_scalar_attribute(const LocationType &h5obj, const std::string &name,
                           T &value) {
    const H5::DataType *const datatype = DatatypeSpecialization<T>::get();
    H5::Attribute att = h5obj.openAttribute(name);
    att.read(*datatype, &value);
}

template <>
inline void load_scalar_attribute(const LocationType &h5obj,
                                  const std::string &name, std::string &value) {

    H5::Attribute att = h5obj.openAttribute(name);
    size_t size = att.getInMemDataSize();
    char *buf = new char[size+1];
    buf[size] = 0;
    load_scalar_attribute(h5obj, name, buf);
    value.assign(buf);
}

// Explicit overrides for old HDF5 versions where H5File is neither a
// Object or Location
template <typename T>
void load_scalar_attribute(const H5::H5File &h5file, const std::string &name,
                           T &value) {
    const H5::DataType *const datatype = DatatypeSpecialization<T>::get();
    const auto obj = h5file.openGroup("/");
    H5::Attribute att = obj.openAttribute(name);
    att.read(*datatype, &value);
}

template <>
inline void load_scalar_attribute(const H5::H5File &h5file,
                                  const std::string &name, std::string &value) {
    const auto obj = h5file.openGroup("/");
    H5::Attribute att = obj.openAttribute(name);
    size_t size = att.getInMemDataSize();
    char *buf = new char[size+1];
    buf[size] = 0;
    load_scalar_attribute(obj, name, buf);
    value.assign(buf);
}

template <typename T>
void save_scalar_attribute (const H5::H5File &h5file, const std::string &name, const T &value)
{
    const H5::DataType * const datatype = DatatypeSpecialization<T>::get();
    H5::DataSpace dataspace(H5S_SCALAR);
    const auto h5obj = h5file.openGroup("/");
    H5::Attribute att = h5obj.createAttribute(name, *datatype, dataspace);
    att.write(*datatype, &value);
}

template <>
inline void save_scalar_attribute (const H5::H5File &h5file, const std::string &name, const std::string &value)
{
    save_scalar_attribute(h5file, name, value.c_str());
}

}
