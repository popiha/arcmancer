#pragma once

#include <cmath>

#include <boost/math/constants/constants.hpp>

//
// TODO: The unit system could still be improved, now basically just a list of 
// values.

namespace arcmancer {
/** \brief Physical constants and units
 * 
 * The constants are given in the base units of the unit system, e.g. masses
 * are in kg (grams) and velocities in m/s (cm/s) for the SI (cgs) quantities.
 *
 * \ingroup utils
 */
namespace units {
// Natural constants and unit conversions
// Sources:
//
// For most constants:
// NIST CODATA 2014 values (see: <https://physics.nist.gov/cuu/Constants/bibliography.html>)
//
// For astronomical unit (and consequently parsec), IAU 2012 Resolution B2
// (see: <https://www.iau.org/static/resolutions/IAU2012_English.pdf>)
//
// For Solar mass (together with CODATA G), radius, luminosity & temperature:
// IAU Resolution B3 (2015): https://arxiv.org/abs/1510.07674
using boost::math::double_constants::pi;

//
// Dimensionless constants

constexpr double fine_structure_constant = 7.2973525664e-3;
///< Fine structure constant

constexpr double N_A = 6.022140857e+23;
///< Avogadro's number

//
// Physical constants in SI units

constexpr double c_SI = 299792458.0;
///< Speed of light in vacuum in SI units

constexpr double G_SI = 6.67408e-11;
///< Gravitational constant in SI units

constexpr double k_SI = 1.38064852e-23;
///< Boltzmann constant in SI units

constexpr double sigma_SB_SI = 5.670367e-8;
///< Stefan-Boltzmann constant in SI units
constexpr double a_rad_SI = 7.56572335e-16;
///< Radiation density constant in SI units

constexpr double h_SI = 6.626070040e-34;
///< Planck's constant in SI units
constexpr double hbar_SI = 1.054571800e-34;
///< Reduced Planck's constant in SI units

constexpr double epsilon0_SI = 8.854187817620389e-12;
///< Vacuum permittivity in SI units
constexpr double mu0_SI = 4e-7 * pi;
///< Vacuum permeability in SI units

constexpr double e_SI = 1.6021766208e-19;
///< Elementary charge in SI units

// electron, proton mass, atomic mass unit
constexpr double m_e_SI = 9.10938356e-31;
///< Electron mass in SI units
constexpr double m_p_SI = 1.672621898e-27;
///< Proton mass in SI units
constexpr double u_SI = 1.660539040e-27;
///< Unified atomic mass unit in SI units
constexpr double eV_SI = 1.6021766208e-19;
///< Electron volt in SI units


constexpr double au_SI = 1.49597870700e11;
///< Astronomical unit in SI units
constexpr double pc_SI = 648000/pi * au_SI;
///< Parsec in SI units
//
constexpr double M_sun_SI = 1.98848e30;
///< Solar mass in SI units
constexpr double R_sun_SI = 6.957e8;
///< Solar radius in SI units
constexpr double L_sun_SI = 3.828e26;
///< Solar luminosity in SI units
constexpr double T_sun_SI = 5.772e3;
///< Solar temperature in cgs units


//
// Physical constants in cgs units.

/// Speed of light in vacuum in cgs units
constexpr double c_cgs            = 2.99792458e10;    //  cm s-1

/// Gravitational constant in cgs units
constexpr double G_cgs            = 6.67408e-8;    //  cm3 g-1 s-2

/// Boltzmann constant in cgs units
constexpr double k_cgs            = 1.38064852e-16;   //  erg K-1

/// Stefan-Boltzmann constant in cgs units
constexpr double sigma_SB_cgs     = 5.670367e-5;    //  erg cm-2 K-4 s-1
/// Radiation density constant in cgs units
constexpr double a_rad_cgs        = 7.56572335e-15;   //  erg cm-3 K-4

/// Planck's constant in cgs units
constexpr double h_cgs            = 6.626070040e-27;   //  erg s
/// Reduced Planck's constant in cgs units
constexpr double hbar_cgs         = 1.054571800e-27;   //  erg s


/// Elementary charge in cgs units
constexpr double e_cgs            = 4.8032068e-10;   //  esu

/// Electron mass in cgs units
constexpr double m_e_cgs          = 9.1093897e-28;   //  g
/// Proton mass in cgs units
constexpr double m_p_cgs          = 1.6726231e-24;   //  g
/// Unified atomic mass unit in cgs units
constexpr double u_cgs          = 1.660539040e-24;   //  g
/// Electron volt in cgs units
constexpr double eV_cgs           = 1.6021766208e-12;   //  erg


/// Astronomical unit in cgs units
constexpr double au_cgs    = 1.49597870700e13;    // cm
/// Parsec in cgs units
constexpr double pc_cgs = 648000/pi * au_cgs; // cm

/// Solar mass in cgs units
constexpr double M_sun_cgs = 1.98848e33;    // g
/// Solar radius in cgs units
constexpr double R_sun_cgs = 6.957e10;    // cm
/// Solar luminosity in cgs units
constexpr double L_sun_cgs = 3.828e33;    // erg s-1
/// Solar temperature in cgs units
constexpr double T_sun_cgs = 5.772e3;    // K

}
} // namespace arcmancer::units
