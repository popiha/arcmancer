#pragma once

#include <cmath>
#include <string>

#include "arcmancer/geometry/metric_space.h"

namespace arcmancer {
/** \brief Classes related to the n-dimensional Euclidean spaces
 *  \ingroup manifolds
 */
namespace euclidean {

using namespace std::literals::string_literals; // for ""s suffix operator

/** \brief Euclidean n-dimensional space
 *
 * Represents the n-dimensional Euclidean (flat) space.
 *
 * Implements a single chart, the Cartesian chart.
 *
 *  \ingroup manifolds
 */

template <long dim>
class EuclideanSpace : public MetricSpace<EuclideanSpace<dim>, dim, OtherSignature> {
public:
    std::string name() const { return "Euclidean "s +
        std::to_string(dim) + "-dimensional space" ; }

    using BaseType = MetricSpace<EuclideanSpace<dim>, dim, OtherSignature>;
    const typename BaseType::ChartType cartesian = {*this, "Cartesian coordinates"s};
    ///< Standard cartesian coordinates


    /** \brief Constructor
     */
    EuclideanSpace() {
        BaseType::add_chart(cartesian, &EuclideanSpace::cartesian_metric,
                  &EuclideanSpace::cartesian_metric_derivatives);

        BaseType::complete_chart_graph();
    }

private:
    EigenRk2Tensor<dim> cartesian_metric(const EigenRk1Tensor<dim>& /* x */) const {
        EigenRk2Tensor<dim> ret;
        ret.setZero();
        for (long i = 0; i < dim; i++) {
            ret(i,i) = 1;
        }

        return ret;
    }

    EigenRk3Tensor<dim> cartesian_metric_derivatives(
        const EigenRk1Tensor<dim>& /* x */) const {

        EigenRk3Tensor<dim> ret;
        ret.setZero();
        return ret;
    }
};

}
}
