#pragma once

#include <cmath>
#include <string>

#include "arcmancer/geometry/metric_space.h"

namespace arcmancer {
/** \brief Classes related to the 2-sphere manifold
 *  \ingroup manifolds
 */
namespace two_sphere {

using namespace std::literals::string_literals; // for ""s suffix operator

/** \brief Unit 2-sphere
 *
 *  A simple example of a non-spacetime manifold.
 *  Simply the surface of a unit sphere embedded in Euclidean space.
 *
 *  Implements two charts using standard spherical coordinates, rotated 90
 *  degrees w.r.t. each other in order to cover the whole sphere.
 *
 *  \ingroup manifolds
 */
class TwoSphere : public MetricSpace<TwoSphere, 2, OtherSignature> {
public:
    std::string name() const { return "2-sphere"s; }

    const ChartType spherical = {*this, "Spherical coordinates"s};
    ///< Standard \f$(\theta, \phi)\f$ spherical coordinates

    const ChartType rotated_spherical = {*this,
                                         "Rotated spherical coordinates"s};
    ///< Standard \f$(\theta, \phi)\f$ spherical coordinates rotated 90 degrees
    ///  with respect to the \c spherical chart

    /** \brief Constructor
     */
    TwoSphere() {
        add_chart(spherical, &TwoSphere::spherical_metric,
                  &TwoSphere::spherical_metric_derivatives);

        add_chart(rotated_spherical, &TwoSphere::spherical_metric,
                  &TwoSphere::spherical_metric_derivatives);

        add_transition(spherical, rotated_spherical,
                       &TwoSphere::standard_spherical_to_rotated,
                       &TwoSphere::standard_spherical_to_rotated_J);

        add_transition(rotated_spherical, spherical,
                       &TwoSphere::rotated_spherical_to_standard);

        complete_chart_graph();
    }

private:
    EigenRk2Tensor<2> spherical_metric(const EigenRk1Tensor<2> &x) const {
        const double th = x(0);

        EigenRk2Tensor<2> ret;
        ret.setValues({{1, 0}, {0, std::pow(std::sin(th), 2)}});

        return ret;
    }

    EigenRk3Tensor<2> spherical_metric_derivatives(
        const EigenRk1Tensor<2> &x) const {

        const double th = x(0);

        EigenRk3Tensor<2> ret;
        ret.setZero();
        ret(1, 1, 0) = std::sin(2 * th);
        return ret;
    }

    EigenRk1Tensor<2> standard_spherical_to_rotated(
        const EigenRk1Tensor<2> &X) const {
        using namespace std;

        // Rotate 90 degrees around x-axis
        const double sth = sin(X(0));
        const double x = sth * cos(X(1)), y = cos(X(0)), z = -sth * sin(X(1));

        EigenRk1Tensor<2> ret;
        ret.setValues({acos(z), atan2(y, x)});
        return ret;
    }

    EigenRk1Tensor<2> rotated_spherical_to_standard(
        const EigenRk1Tensor<2> &X) const {
        using namespace std;

        // Rotate -90 degrees around x-axis
        const double sth = sin(X(0));
        const double x = sth * cos(X(1)), y = -cos(X(0)), z = sth * sin(X(1));

        EigenRk1Tensor<2> ret;
        ret.setValues({acos(z), atan2(y, x)});
        return ret;
    }

    ArcMatrix<2> standard_spherical_to_rotated_J(
        const EigenRk1Tensor<2> &X) const {
        using namespace std;
        const double sth = sin(X(0)), cth = cos(X(0)), sphi = sin(X(1)),
                     cphi = cos(X(1));

        const double sth2 = sth * sth, sphi2 = sphi * sphi;

        const double rt_term = sqrt(1 - sth2 * sphi2);
        ArcMatrix<2> J;
        // clang-format off
        J << cth * sphi / rt_term     , cphi * sth / rt_term,
             cphi / (sphi2 * sth2 - 1), sth * cth * sphi / (1 - sphi2 * sth2);
        // clang-format on
        return J;
    }
};
} // namespace two_sphere
} // namespace arcmancer
