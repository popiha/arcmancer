#pragma once

#include <array>
#include <cmath>
#include <functional>
#include <string>
#include <tuple>
#include <type_traits>

#include "arcmancer/general_utils.h"
#include "arcmancer/geometry/metric_space.h"
#include "arcmancer/other_spaces/immersion.h"
#include "arcmancer/other_spaces/tensor_space.h"

namespace arcmancer {

/** \brief Classes and functions related to immersed velocity-spaces.
 *
 * \ingroup manifolds
 */
namespace velocity_space {

/// The type of a physical velocity space: A 3-dimensional immersed
/// subspace of a 4-dimensional MetricSpace.
template <class MetricSpaceType>
using VelocitySpaceType =
    immersion::Immersion<tensor_space::TensorSpace<MetricSpaceType, Cnt>, 3,
                         OtherSignature>;

/// The type of the Jacobian matrix of the immersion for a velocity space.
template <class MetricSpaceType>
using JacobianType = typename VelocitySpaceType<MetricSpaceType>::JacobianType;
/// The type of the tensor of second derivatives of the immersion for a velocity space.
template <class MetricSpaceType>
using JacobianDerivativeType =
    typename VelocitySpaceType<MetricSpaceType>::JacobianDerivativeType;

/// The type of the immersion function for a velocity space.
template <class MetricSpaceType>
using ImmersionFuncType =
    typename VelocitySpaceType<MetricSpaceType>::ImmersionFuncType;
/// The type of the Jacobian map of the immersion function for a velocity space.
template <class MetricSpaceType>
using JacobianFuncType =
    typename VelocitySpaceType<MetricSpaceType>::JacobianFuncType;
/// The type of the second derivative map of the immersion function for a velocity space.
template <class MetricSpaceType>
using JacobianFuncDerivativeType =
    typename VelocitySpaceType<MetricSpaceType>::JacobianFuncDerivativeType;

/// A convenience definition for a tuple containing the immersion map,
/// and the Jacobian and the second derivative map of the immersion map.
template <class MetricSpaceType>
using ImmersionTupleType =
    std::tuple<ImmersionFuncType<MetricSpaceType>,
               JacobianFuncType<MetricSpaceType>,
               JacobianFuncDerivativeType<MetricSpaceType>>;

namespace internal {

template <class MetricSpaceType>
std::array<double, 2> compute_bc(const EigenRk2Tensor<4> &gM,
                                 const EigenRk1Tensor<3> &x) {
    const double b =
        (2 / gM(0, 0)) * (gM(0, 1) * x(0) + gM(0, 2) * x(1) + gM(0, 3) * x(2));
    double c = 0;
    for (size_t i = 1; i < 4; i++) {
        for (size_t j = 1; j < 4; j++) {
            c += gM(i, j) * x(i - 1) * x(j - 1);
        }
    }
    c -= MetricSpaceType::Signature::timelike_sign;
    c /= gM(0, 0);

    return {b, c};
}

template <class MetricSpaceType>
inline EigenRk1Tensor<4> immersion(const EigenRk2Tensor<4> &gM,
                                   const EigenRk1Tensor<3> &x) {
    const auto bc = compute_bc<MetricSpaceType>(gM, x);

    std::array<double, 2> sol = solve_quadratic(1, bc[0], bc[1]);

    // XXX: Bit of a hack, since apparently near black holes the velocity
    // immersion can be double valued, at least in some base coordinates
    const auto minmax = std::minmax(sol[0], sol[1]);
    const double v0 = minmax.first > 0 ? minmax.first : minmax.second;

    Log::log()->trace("immersion: Possible v0 values {} {}, picked {}",
            sol[0], sol[1], v0);

    EigenRk1Tensor<4> ret;
    ret.setValues({v0, x(0), x(1), x(2)});
    return ret;
}

template <class MetricSpaceType>
inline JacobianType<MetricSpaceType> jacobian(const EigenRk2Tensor<4> &gM,
                                              const EigenRk1Tensor<3> &x) {
    const auto bc = compute_bc<MetricSpaceType>(gM, x);
    const double b = bc[0], c = bc[1];

    JacobianType<MetricSpaceType> ret;
    ret.setZero();

    const double q = b * b - 4 * c;

    for (size_t j = 1; j < 4; j++) {
        // \partial b / \partial v^j
        const double db_dvj = 2 / gM(0, 0) * gM(0, j);

        // \partial c / \partial v^j
        double dc_dvj = 0;

        for (size_t i = 1; i < 4; i++) {
            dc_dvj += 2 * gM(j, i) * x(i - 1) / gM(0, 0);
        }

        ret(0, j - 1) =
            0.5 * (-db_dvj + 1 / std::sqrt(q) * (b * db_dvj - 2 * dc_dvj));
        ret(j, j - 1) = 1.0;
    }

    return ret;
}

template <class MetricSpaceType>
inline JacobianDerivativeType<MetricSpaceType> jacobian_derivative(
    const EigenRk2Tensor<4> &gM, const EigenRk1Tensor<3> &x) {

    const auto bc = compute_bc<MetricSpaceType>(gM, x);
    const double b = bc[0], c = bc[1];

    JacobianDerivativeType<MetricSpaceType> ret;
    ret.setZero();

    const double q = b * b - 4 * c;

    for (size_t j = 1; j < 4; j++) {
        // \partial b / \partial v^j
        const double db_dvj = 2 / gM(0, 0) * gM(0, j);

        // \partial c / \partial v^j
        double dc_dvj = 0;

        for (size_t i = 1; i < 4; i++) {
            dc_dvj += 2 * gM(j, i) * x(i - 1) / gM(0, 0);
        }

        for (size_t k = 1; k < 4; k++) {
            // \partial b / \partial v^k
            const double db_dvk = 2 / gM(0, 0) * gM(0, k);

            // \partial^2 c / (\partial v^k \partial v^j)
            double ddc = 2 * gM(j, k) / gM(0, 0);

            // \partial c / \partial v^k
            double dc_dvk = 0;

            for (size_t i = 1; i < 4; i++) {
                dc_dvk += 2 * gM(k, i) * x(i - 1) / gM(0, 0);
            }

            ret(0, j - 1, k - 1) =
                0.5 * (-1 * std::pow(q, -3. / 2) * (b * db_dvk - 2 * dc_dvk) *
                           (b * db_dvj - 2 * dc_dvj) +
                       1 / std::sqrt(q) * (db_dvk * db_dvj - 2 * ddc));
        }
    }

    return ret;
}

} // namespace internal

/** \brief A convenience wrapper class for creating immersed velocity
 * spaces.
 *
 * This class is meant to be used for constructing immersed
 * 3-dimensional velocity spaces on a 4-dimensional spacetime. It
 * packages the necessary functions (the immersion map and it's two
 * first derivatives) together, ready to be supplied to the constructor
 * of a suitable \c Immersion.
 *
 * An example of how to construct a velocity space on a given
 * MetricSpaceType.
 * \code
    using VSpaceType = velocity_space::VelocitySpaceType<MetricSpaceType>;
    using VSpaceImmersionType = velocity_space::VelocitySpaceImmersion<MetricSpaceType>;

    tensor_space::TensorSpace<MetricSpaceType, Cnt> TM(chart, point);

    velocity_space::VelocitySpaceImmersion<MetricSpaceType> vimmersion(TM.canonical);
    VSpaceType VSpace(TM.canonical,
            std::bind(&VSpaceImmersionType::immersion           , &vimmersion , std::placeholders::_1) ,
            std::bind(&VSpaceImmersionType::jacobian            , &vimmersion , std::placeholders::_1) ,
            std::bind(&VSpaceImmersionType::jacobian_derivative , &vimmersion , std::placeholders::_1)
            );
 * \endcode
 *
 * \ingroup manifolds
 * \ingroup geometry
 */
template <class MetricSpaceType>
class VelocitySpaceImmersion {
public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW

    /** \brief Construct a velocity space immersion from a given \c
     * Chart.
     */
    VelocitySpaceImmersion(
        const Chart<tensor_space::TensorSpace<MetricSpaceType, Cnt>>
            &codomain_chart)
    : metric_components_(codomain_chart.M.composite_metric_components()) {}

    EigenRk1Tensor<4> immersion(const EigenRk1Tensor<3> &x) {
        return internal::immersion<MetricSpaceType>(metric_components_, x);
    }

    JacobianType<MetricSpaceType> jacobian(const EigenRk1Tensor<3> &x) {
        return internal::jacobian<MetricSpaceType>(metric_components_, x);
    }

    JacobianDerivativeType<MetricSpaceType> jacobian_derivative(
        const EigenRk1Tensor<3> &x) {
        return internal::jacobian_derivative<MetricSpaceType>(metric_components_, x);
    }

private:
    EigenRk2Tensor<4> metric_components_;
};

/** \brief Send a free vector to a point on a velocity space manifold.
 *
 * Logs a warning if the 0-component does not match the immersion.
 *
 * \param M The velocity space manifold.
 * \param t A contravariant tensor to transform into a point on M.
 * \return A ManifoldPoint on M representing \c t.
 *
 * \ingroup manifolds
 * \ingroup geometry
 */
template <class MetricSpaceType>
inline std::enable_if_t<MetricSpaceType::is_lorentzian,
                        ManifoldPoint<VelocitySpaceType<MetricSpaceType>>>
send_to_manifold(const VelocitySpaceType<MetricSpaceType> &M,
                 const Tensor<MetricSpaceType, Cnt> &t) {

    // first make into a point of the underlying TensorSpace
    auto pt = M.codomain_chart().M.tensor_to_point(t);
    EigenRk1Tensor<4> comps = pt.coordinates(M.codomain_chart());
    EigenRk1Tensor<3> proj_comps;
    proj_comps.setValues({comps(1), comps(2), comps(3)});

    auto gM = M.codomain_chart().M.composite_metric_components();

    // recompute v0 through the immersion
    auto immersed_comps = internal::immersion<MetricSpaceType>(gM, proj_comps);

    if (std::fabs(immersed_comps(0)-comps(0)) > M.configuration.pointwise.tolerance.absolute) {
        Log::log()->warn(
            "VelocitySpace::send_to_manifold: component 0 "
            "of t\n{}\nnot same (within {}) as component 0 of immersed t:\n{}\n "
            "Difference vector immersed-original:\n{}\n",
            comps, M.configuration.pointwise.tolerance.absolute, immersed_comps, immersed_comps-comps);
    }

    return make_point(M.canonical, proj_comps);
}

} // namespace velocity_space
} // namespace arcmancer
