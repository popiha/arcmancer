#pragma once

#include <array>
#include <cmath>
#include <sstream>
#include <stdexcept>
#include <string>

#include <boost/mpl/for_each.hpp>

#include "arcmancer/geometry/types.h"
#include "arcmancer/geometry/metric_space.h"
#include "arcmancer/geometry/meta.h"

namespace arcmancer {

namespace tensor_space {

namespace internal {
template <typename T>
struct to_long {
    using type = long;
};

inline constexpr long cpowl(long base, size_t exp) {
    long ret = 1;
    for (size_t i = 0; i < exp; i++) {
        ret *= base;
    }
    return ret;
}

} // namespace internal

using namespace std::literals::string_literals; // for ""s suffix operator

/** \brief The space of rank (r,s) tensors at a point.
 *
 *  The space \f$\mathcal{T}^r_{s,p}M\f$ of rank (r,s) tensors at a
 *  point \f$p\in M\f$, considered as a manifold. The metric is
 *  inherited from that of the underlying manifold in a natural way.
 *
 *  There is one chart available, the canonical chart of a tensor space
 *  at a point, which depends on the chosen coordinate chart.
 *
 *  \ingroup manifolds
 */
template <typename MetricSpaceType, typename... Indices>
class TensorSpace
: public MetricSpace<TensorSpace<MetricSpaceType, Indices...>,
                     internal::cpowl(MetricSpaceType::D, sizeof...(Indices)),
                     OtherSignature
                     // typename MetricSpaceType::Signature
                     > {
private:
    using ThisType = TensorSpace<MetricSpaceType, Indices...>;
    using ParentType =
        MetricSpace<TensorSpace<MetricSpaceType, Indices...>,
                    internal::cpowl(MetricSpaceType::D, sizeof...(Indices)),
                    OtherSignature
                    // typename MetricSpaceType::Signature
                    >;

    static constexpr long base_dim = MetricSpaceType::D;
    static constexpr long dim = ParentType::D;
    static constexpr size_t rank = sizeof...(Indices);

    const Chart<MetricSpaceType> &chart_;
    ManifoldPoint<MetricSpaceType> base_point_;
    const EigenRk2Tensor<base_dim> g_;
    const EigenRk2Tensor<base_dim> gi_;

    EigenRk2Tensor<dim> composite_metric_;

public:
    using StandardComponentType = Eigen::TensorFixedSize<double,
               typename ::arcmancer::internal::sizes_helper<rank, base_dim>::sizes>;

    std::string name() const {
        using index_seq =
            typename ::arcmancer::internal::index_int_sequence<Indices...>::type;

        std::ostringstream ss;

        ss << "TensorSpace<";
        mpl::for_each<index_seq>([&ss](int index_integer) {
            if (index_integer > 0) {
                ss << " " << Cnt::repr;
            } else if (index_integer < 0) {
                ss << " " << Cov::repr;
            }
        });
        ss << "> at point: " << base_point_;
        return ss.str();
    }

    /// The canonical coordinates \f$(x, v^{a_1\cdots a_r}_{b_1\cdots b_s})\f$
    const typename ParentType::ChartType canonical = {*this,
                                                    "Canonical coordinates"s};

    /** \brief Constructor
     */
    TensorSpace(const Chart<MetricSpaceType> &chart,
                const ManifoldPoint<MetricSpaceType> &base_point)
    : chart_(chart),
      base_point_(base_point),
      g_(base_point.M().metric_components(base_point, chart)),
      gi_(base_point.M().inverse_metric_components(base_point, chart)) {

        ParentType::add_chart(
            canonical,
            &ThisType::canonical_metric,
            &ThisType::canonical_metric_derivatives);

        ParentType::complete_chart_graph();

        // construct the composite metric
        using index_seq =
            typename ::arcmancer::internal::index_int_sequence<Indices...>::type;

        for (size_t i = 0; i < dim; i++) {
            for (size_t j = i; j < dim; j++) {

                auto a_arr = index_array(i);
                auto b_arr = index_array(j);
                double val = 1;
                auto op = [this, &a_arr, &b_arr, &val](int index_pos) {

                    if (index_pos < 0) {
                        // Cov
                        val *= gi_(a_arr[-index_pos-1], b_arr[-index_pos-1]);
                    }
                    else {
                        // Cnt
                        val *= g_(a_arr[index_pos-1], b_arr[index_pos-1]);
                    }
                };

                mpl::for_each<index_seq>(op);

                composite_metric_(j, i) = composite_metric_(i, j) = val;
            }
        }
    }

    /** \brief Convert an index to a concatenated tensor to an array of
     * indexes into the unconcatenated tensor.
     */
    constexpr std::array<long, rank> index_array(long long_index) const {
        std::array<long, rank> ret = {};
        for (size_t i = 0; i < rank; i++) {
            ret[i] =
                ((long_index / internal::cpowl(base_dim, i)) % base_dim);
        }
        return ret;
    }

    /** \brief Convert a standard component representation into
     * concatenated representation.
     */
    EigenRk1Tensor<dim> concatenate(const StandardComponentType &standard) const {
        Eigen::array<Eigen::DenseIndex, 1> reshape_dim({ dim } );

        EigenRk1Tensor<dim> concatenated;
        concatenated = standard.reshape(reshape_dim);

        return concatenated;
    }

    /** \brief Convert a concatenated component representation into
     * standard representation.
     */
    StandardComponentType unconcatenate(const EigenRk1Tensor<dim> &concatenated) const {
        StandardComponentType standard;
        standard = concatenated.reshape(standard.dimensions());

        return standard;
    }

    /** \brief Get a reference to the chart of the underlying manifold
     * in terms of which this TensorSpace is defined.
     */
    const Chart<MetricSpaceType>& underlying_chart() const {
        return chart_;
    }

    /** \brief Convert a freestanding \c Tensor into a point of this
     * manifold.
     */
    ManifoldPoint<ThisType> tensor_to_point(
            const Tensor<MetricSpaceType, Indices...> &t) const {
        if (t.point() != base_point()) {
            std::stringstream ss;
            ss << "tensor_to_point: Tensor defined at a different base point:\n"
                << t.point() << "\n"
                << "for:\n" << name() << "\n";
            throw std::domain_error(ss.str());
        }

        auto comps = t.components(underlying_chart());

        EigenRk1Tensor<dim> concatenated;
        Eigen::array<Eigen::DenseIndex, 1> reshape_dim({ dim } );
        concatenated = comps.reshape(reshape_dim);

        return make_point(canonical, concatenate(comps));
    }

    /** \brief Convert a point on this manifold to a \c Tensor.
     */
    Tensor<MetricSpaceType, Indices...> point_to_tensor(
            const ManifoldPoint<ThisType> &point) const {

        // unconcatenate the components
        EigenRk1Tensor<dim> coords = point.coordinates(canonical);

        return {base_point(), underlying_chart(), unconcatenate(coords)};
    }

    /** \brief Get a copy of the manifold base point where this tensor
     * space sits at.
     */
    ManifoldPoint<MetricSpaceType> base_point() const {
        return base_point_;
    }

    /** \brief Get a copy of the constant composite metric components of
     * this space, in the canonical chart.
     */
    EigenRk2Tensor<dim> composite_metric_components() const {
        return composite_metric_;
    }

private:
    EigenRk2Tensor<dim> canonical_metric(
        const EigenRk1Tensor<dim> & /* x */) const {
        return composite_metric_;
    }

    EigenRk3Tensor<dim> canonical_metric_derivatives(
        const EigenRk1Tensor<dim> & /* x */) const {

        EigenRk3Tensor<dim> ret;
        ret.setZero();
        return ret;
    }
};
} // namespace tensor_space
} // namespace arcmancer
