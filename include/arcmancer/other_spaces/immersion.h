#pragma once

#include <array>
#include <cmath>
#include <string>

#include "arcmancer/geometry/metric_space.h"

namespace arcmancer {
/** \brief Classes related to immersed manifolds.
 *
 * \ingroup manifolds
 */
namespace immersion {

using namespace std::literals::string_literals; // for ""s suffix operator

/** \brief An immersed submanifold
 *
 * Creates a (semi-)Riemannian submanifold \f$S\f$ of another manifold
 * \f$M\f$ (the ambient manifold), given as a \c MetricSpace, using an
 * immersion function \f$i:S\rightarrow M\f$. Here naturally 
 * $\f$\dim(S) < \dim(M)\f$. In addition to the
 * immersion function, the user needs to supply the first and second
 * derivatives $\fJ:TS\rightarrow TM\f$ and \f$J:TTS\rightarrow TTM\f$
 * of the immersion function.
 *
 * Using these functions, the \c Immersion class computes the pullback
 * metric
 * \f$(g_S)_{ab} = (g_M)_{cd,f} J^c_a J^d_b\f$
 * as well as its derivatives
 * \f$(g_S)_{ab,e} = (g_M)_{cd,f} J^c_a J^d_b J^f_e + (g_M)_{cd}
 * (J^c_{a,e} J^d_b + J^c_a J^d_{b,e})\f$.
 *
 * Currently \c Immersion can only be defined in terms of a single fixed
 * chart on the ambient manifold \f$M\f$. Likewise, there is only a
 * single chart available (the canonical chart) on the resulting \c
 * Immersion object.
 *
 *  \ingroup manifolds
 *  \ingroup geometry
 */
template <class MetricSpaceType, long D_, typename Signature>
class Immersion : public MetricSpace<Immersion<MetricSpaceType, D_, Signature>, D_, Signature> {
    static_assert(D_ <= MetricSpaceType::D, "Immersion N->M must have dim(N) <= dim(M)");

public:
    using ThisType = Immersion<MetricSpaceType, D_, Signature>;
    using ParentType = MetricSpace<ThisType, D_, Signature>;

    /// Dimension dim(M) of the ambient manifold M.
    static constexpr long D_amb = MetricSpaceType::D;

    using ImmersionFuncType
        = std::function<EigenRk1Tensor<D_amb>(const EigenRk1Tensor<D_>&)>;
    using JacobianType = Eigen::TensorFixedSize<double, Eigen::Sizes<D_amb, D_>>;
    using JacobianDerivativeType = Eigen::TensorFixedSize<double, Eigen::Sizes<D_amb, D_, D_>>;
    using JacobianFuncType
        = std::function<JacobianType(const EigenRk1Tensor<D_>&)>;
    using JacobianFuncDerivativeType
        = std::function<JacobianDerivativeType(const EigenRk1Tensor<D_>&)>;


    std::string name() const { return std::to_string(D_) + "-dim immersed submanifold of "s + chart_.M.name(); }

    // TODO: What to do about the possibility of multiple charts?
    //const typename ParentType::ChartType canonical = {*this, "Canonical chart"s};
    const Chart<ThisType> canonical = {*this, "Immersion chart"s};

    /** \brief Constructs an \c Immersion using a given immersion map
     * and its first two derivatives.
     *
     * \param A Chart on the ambient manifold in terms of which the
     * immersion is defined.
     * \param immersion_function A function \f$S\rightarrow M\f$, where
     * \f$\dim(S) < dim(M)\f$.
     * \param immersion_jac The first derivative (Jacobian) of the immersion map.
     * \param immersion_jac_der The derivative of the Jacobian.
     *
     * \return An immersed \c MetricSpace
     */
    Immersion(const Chart<MetricSpaceType> &chart,
            const ImmersionFuncType &immersion_function,
            const JacobianFuncType &immersion_jac,
            const JacobianFuncDerivativeType &immersion_jac_der)
        : chart_(chart),
        j_(immersion_function),
        jjac_(immersion_jac),
        jjacder_(immersion_jac_der) {

        ParentType::add_chart(canonical,
                &ThisType::pullback_metric,
                &ThisType::pullback_metric_derivatives);

        ParentType::complete_chart_graph();
    }

    /** \brief Access the chart used on the codomain (ambient) manifold.
     */
    const Chart<MetricSpaceType>& codomain_chart() const { return chart_; }

    /** \brief Apply the immersion map to a point.
     *
     * Given a point \f$x\f$ on the immersed submanifold \f$S\f$, computes
     * the corresponding point \f$i(x)\f$ on the ambient manifold.
     *
     * \param pt A point on the submanifold.
     *
     * \return The immersed point on the ambient manifold.
     */
    ManifoldPoint<MetricSpaceType> immerse(const ManifoldPoint<ThisType> &pt) const {
        return make_point(chart_, j_(pt.coordinates(canonical)));
    }


private:
    const Chart<MetricSpaceType> &chart_;
    ImmersionFuncType j_;
    JacobianFuncType jjac_;
    JacobianFuncDerivativeType jjacder_;


    EigenRk2Tensor<D_> pullback_metric(const EigenRk1Tensor<D_> &x) const {
        auto amb_point = make_point(chart_, j_(x));

        EigenRk2Tensor<D_amb> gM = chart_.M.metric_components(amb_point, chart_);

        JacobianType jac = jjac_(x);


        EigenRk2Tensor<D_> ret;
        ret.setZero();

        ret = gM.contract(
                    jac, Eigen::array<IndexPair, 1>{ IndexPair(0, 0) }
                ).contract(
                    jac, Eigen::array<IndexPair, 1>{ IndexPair(0, 0) }
                );

        return ret;
    }

    EigenRk3Tensor<D_> pullback_metric_derivatives(const EigenRk1Tensor<D_> &x) const {
        auto amb_point = make_point(chart_, j_(x));

        EigenRk2Tensor<D_amb> gM = chart_.M.metric_components(amb_point, chart_);
        EigenRk3Tensor<D_amb> gMder = chart_.M.metric_derivatives(amb_point, chart_);

        JacobianType jac = jjac_(x);
        JacobianDerivativeType jac_der = jjacder_(x);

        EigenRk3Tensor<D_> ret;
        ret.setZero();

        // 1st term: (((gM_{cd,f} J^c_a) J^d_b) J^f_e), e is derivative index
        ret = gMder.contract(
                    jac, Eigen::array<IndexPair, 1>{ IndexPair(0, 0) }
                ).contract(
                    jac, Eigen::array<IndexPair, 1>{ IndexPair(0, 0) }
                ).contract(
                    jac, Eigen::array<IndexPair, 1>{ IndexPair(0, 0) }
                );


        // 2nd term:  ((gM_{cd} J^c_{a,e}) J^d_b)
        // XXX: Note that the indexes below turn out as {a,e,b}, so need
        // to shuffle the last two
        ret += gM.contract(
                    jac_der, Eigen::array<IndexPair, 1>{ IndexPair(0, 0) }
                ).contract(
                    jac, Eigen::array<IndexPair, 1>{ IndexPair(0, 0) }
                ).shuffle(Eigen::array<Eigen::Index,3> {0,2,1});


        // 3rd term: ((gM_{cd} J^c_a) J^d_{b,e})
        ret += gM.contract(
                    jac, Eigen::array<IndexPair, 1>{ IndexPair(0, 0) }
                ).contract(
                    jac_der, Eigen::array<IndexPair, 1>{ IndexPair(0, 0) }
                );

        return ret;
    }


};



}
}
