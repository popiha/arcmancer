#pragma once

#include "arcmancer/geometry/types.h"

namespace arcmancer {

/** \brief An abstract base class for surfaces in a MetricSpace.
 *
 *  The surface is defined by S(x) == 0, 
 *  with S some function on the background manifold.
 *  Additionally, the surface defines the velocity of a comoving observer,
 *  which is used to compute impact angles and redshifts.
 *
 *  To define a surface, override at least the pure virtual methods.
 *  Other virtual methods may be optionally overridden for additional 
 *  functionality.
 *
 *  \c ParametrizedCurve and \c Geodesic can solve intersection points
 *  with these surfaces.
 *  Note that the function should be fairly well behaved for this to work 
 *  reliably.
 *
 *  \ingroup geometry
 */
template <typename MetricSpaceType>
class Surface {
public:
    using PointType = typename MetricSpaceType::PointType;

    /** \brief Constructor
     * Surfaces can only be created when we already have an existing
     * metric space
     */
    Surface(const MetricSpaceType& M) : M_(&M) {}

    /** \brief The value of the defining function S at a point
     */
    virtual double value(const PointType&) const = 0;

    /** \brief The gradient of the defining function dS
     */
    virtual CotangentVector<MetricSpaceType> gradient(
        const PointType&) const = 0;

    /** \brief The four-velocity of an observer on the surface
     */
    virtual TangentVector<MetricSpaceType> observer(const PointType&) const = 0;

    /** \brief Whether the surface should be ignored here instead of terminating
     *         a curve
     *
     *         Can be overridden to define holes in the surface.
     */
    virtual bool ignore_at(const PointType&) const { return false; }

    /** \brief Are two points on different sides of the surface
     */
    bool points_cross_surface(const PointType& p1, const PointType& p2) const {
        // check sign; include 0 to include cases where one of the
        // points is on the surface
        return value(p1) * value(p2) <= 0;
    }

    /** \brief Get the underlying \c MetricSpace
     */
    const MetricSpaceType& M() const { return *M_; }

    virtual ~Surface() {}

private:
    const MetricSpaceType* M_;
};

} // namespace arcmancer
