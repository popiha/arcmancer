#pragma once

#include <cmath>
#include <initializer_list>
#include <stdexcept>
#include <string>
#include <utility>

#include <Eigen/Core>

#include "arcmancer/geometry/types.h"

namespace arcmancer {


/** \brief Dot product of vectors.
 *  
 *  Computes \f$ a^\mu b_\mu \f$, raising and lowering indices as necessary.
 *  \param a,b vectors
 *  \return the dot product of the vectors.
 *  
 *  \ingroup geometry 
 */
template<typename MetricSpaceType>
inline double dot_product(const TangentVector<MetricSpaceType> &a, 
                          const TangentVector<MetricSpaceType> &b) {
    //using Eigen matrices for these computations increases performance
    //somewhat compared to using tensor operations
    a.assert_at_same_point(b);
    const auto& M = a.M();
    const auto& chart = M.choose_better_chart(a.point(), a.chart(), b.chart());
    const auto a_comp = a.components(chart);
    const auto b_comp = b.components(chart);
    auto g = M.metric_components(a.point(),chart);

    constexpr long D = MetricSpaceType::D;
    return Eigen::Map<const ArcRowVector<D>>(a_comp.data()) 
           * Eigen::Map<const ArcMatrix<D>>(g.data())
           * Eigen::Map<const ArcVector<D>>(b_comp.data());
    //return a.template contract_prod<0, 0>(b.template lower_index<0>());
}

/** \copybrief dot_product
 *
 * \copydetails dot_product
 *  \ingroup geometry 
 */
template<typename MetricSpaceType>
inline double dot_product(const CotangentVector<MetricSpaceType> &a, 
                          const CotangentVector<MetricSpaceType> &b) {
    a.assert_at_same_point(b);
    const auto& M = a.M();
    const auto& chart = M.choose_better_chart(a.point(), a.chart(), b.chart());
    const auto a_comp = a.components(chart);
    const auto b_comp = b.components(chart);
    auto g_inv = M.inverse_metric_components(a.point(),chart);

    constexpr long D = MetricSpaceType::D;
    return Eigen::Map<const ArcRowVector<D>>(a_comp.data()) 
           * Eigen::Map<const ArcMatrix<D>>(g_inv.data())
           * Eigen::Map<const ArcVector<D>>(b_comp.data());
    //return a.template contract_prod<0, 0>(b.template raise_index<0>());
}


/** \copybrief dot_product
 *
 * \copydetails dot_product
 *  \ingroup geometry 
 */
template<typename MetricSpaceType>
inline double dot_product(const CotangentVector<MetricSpaceType> &a, 
                          const TangentVector<MetricSpaceType> &b) {
    
    a.assert_at_same_point(b);
    const auto& M = a.M();
    const auto& chart = M.choose_better_chart(a.point(), a.chart(), b.chart());
    const auto a_comp = a.components(chart);
    const auto b_comp = b.components(chart);

    constexpr long D = MetricSpaceType::D;
    return Eigen::Map<const ArcRowVector<D>>(a_comp.data()) 
           * Eigen::Map<const ArcVector<D>>(b_comp.data());
    //return a.template contract_prod<0, 0>(b);
}

/** \copybrief dot_product
 *
 * \copydetails dot_product
 *  \ingroup geometry 
 */
template<typename MetricSpaceType>
inline double dot_product(const TangentVector<MetricSpaceType> &a, 
                          const CotangentVector<MetricSpaceType> &b) {
    return dot_product(b,a);
}


/** \brief Screen projected dot product
 *
 * Computes the dot product of \p a and \p b projected orthogonal to
 * an observers four velocity \p u and a null vector \p k.
 *
 * \p u needs to be normalized.
 *
 * Only defined in Lorentzian spacetimes.
 *
 * Equivalent to 
 * \code
 * dot_product(screen_project(u, k, a), screen_project(u, k, b)); 
 * \endcode
 *
 * \param u a normalized timelike vector, the observer four-velocity
 * \param k a null vector
 * \param a,b two vectors
 *
 * \return the dot product of \p a and \p b projected to the plane orthogonal to
 * \p u and \p k.
 *
 * \see screen_project, dot_product
 * \ingroup geometry 
 */
template<typename MetricSpaceType>
inline double screen_inner_product(const TangentVector<MetricSpaceType> &u,
                                   const TangentVector<MetricSpaceType> &k, 
                                   const TangentVector<MetricSpaceType> &a, 
                                   const TangentVector<MetricSpaceType> &b) {

    static_assert(MetricSpaceType::is_lorentzian,
                  "Only defined for Lorentzian spacetimes!");

    constexpr int s = MetricSpaceType::Signature::timelike_sign;

    //Optimize the large amount of dot products by writing them out by hand
    u.assert_at_same_point(k);
    a.assert_at_same_point(b);
    u.assert_at_same_point(a);
    const auto& M = u.M();
    //Typically all the vectors are given in the same chart, so this works fine.
    //The dot product shouldn't be particularly sensitive to the chart used
    //anyway.
    const auto& chart = M.choose_better_chart(u.point(), u.chart(), k.chart());
    const auto g_comp = M.metric_components(u.point(),chart);
    const auto u_comp = u.components(chart);
    const auto k_comp = k.components(chart);
    const auto a_comp = a.components(chart);
    const auto b_comp = b.components(chart);
    const Eigen::Map<const ArcVector<4>> u_vec(u_comp.data());
    const Eigen::Map<const ArcVector<4>> k_vec(k_comp.data());
    const Eigen::Map<const ArcVector<4>> a_vec(a_comp.data());
    const Eigen::Map<const ArcVector<4>> b_vec(b_comp.data());
    const Eigen::Map<const ArcMatrix<4>> g(g_comp.data());

    const ArcVector<4> eK = s * k_vec/(u_vec.transpose() * g * k_vec) - u_vec;
    const double a_b = a_vec.transpose() * g * b_vec;
    const double u_a = u_vec.transpose() * g * a_vec;
    const double u_b = u_vec.transpose() * g * b_vec;
    const double eK_a = eK.transpose() * g * a_vec;
    const double eK_b = eK.transpose() * g * b_vec;
    
    return a_b - s * u_a * u_b + s * eK_a * eK_b;
}

/** \brief Project a 4-vector to a 2d plane.
 *
 *  Project the vector \p a to a plane orthogonal to the observers four-velocity
 *  \p u and a null vector \p k.
 *
 *  \p u needs to be normalized.
 *
 *  Only defined in Lorentzian spacetimes.
 *
 *  \param u a normalized timelike vector.
 *  \param k a null vector.
 *  \param a the vector to be projected.
 *
 *  \return \p a projected orthogonal to \p u and \p k.
 *
 *  \ingroup geometry 
 */
template<typename MetricSpaceType>
inline TangentVector<MetricSpaceType> 
screen_project(const TangentVector<MetricSpaceType> &u, 
               const TangentVector<MetricSpaceType> &k,
               const TangentVector<MetricSpaceType> &a) {
    
    static_assert(MetricSpaceType::is_lorentzian,
                  "Only defined for Lorentzian spacetimes!");

    //TODO: optimize like project_orthogonal, check for u timelikeness
    //XXX: u seems to be assumed to be normalized, this assumption can be
    //dropped if u^a u_a is computed anyway for timelikeness check

    constexpr int s = MetricSpaceType::Signature::timelike_sign;

    // construct vector e_K
    const double u_k = dot_product(u, k);
    const auto eK = s * k/u_k - u;

    // compute the inner products
    const double u_a = dot_product(u, a);
    const double eK_a = dot_product(eK, a);

    const auto retval = a - s * u_a * u + s * eK_a * eK;
    return retval;
}

/** \brief Find the likeness of a vector.
 *  
 *  In Lorentzian spacetimes,
 *  check whether the vector \c v is timelike, spacelike or lightlike (null).
 *
 *  \see VectorLikeness
 *  \ingroup geometry 
 */
template <typename MetricSpaceType, typename Ind>
inline std::enable_if_t<MetricSpaceType::is_lorentzian, VectorLikeness>
vector_likeness(const Tensor<MetricSpaceType, Ind> &v) {

    double sqnorm = dot_product(v, v);

    // check lightlikeness to absolute tolerance, using propagation
    // tolerances (since this is most relevant for vectors usually)
    if (std::fabs(sqnorm) < v.M().configuration.pointwise.tolerance.absolute) {
        return VectorLikeness::Lightlike;
    }

    if (sqnorm * MetricSpaceType::Signature::timelike_sign > 0) {
        return VectorLikeness::Timelike;
    }

    if (sqnorm * MetricSpaceType::Signature::timelike_sign < 0) {
        return VectorLikeness::Spacelike;
    }

    return VectorLikeness::Undefined;
}

/** \brief Find the likeness of a vector.
 *
 * Version for non-Lorenzian manifolds, always returns
 * VectorLikeness::Undefined.
 *
 *  \see VectorLikeness
 *  \ingroup geometry 
 */
template <typename MetricSpaceType, typename Ind>
inline std::enable_if_t<!MetricSpaceType::is_lorentzian, VectorLikeness>
vector_likeness(const Tensor<MetricSpaceType, Ind> &) {

    return VectorLikeness::Undefined;
}

/** \brief Normalize a tangent vector.
 *  
 *  \param u the vector to normalize.
 *
 *  \return Vector pointing along \c u with norm \f$ \pm 1 \f$, 
 *  equal to \f$ \hat{u}^\mu = u^\mu / \sqrt{|u^\nu u_\nu|} \f$.
 *  \ingroup geometry 
 */
template<typename MetricSpaceType>
inline TangentVector<MetricSpaceType> 
normalized(const TangentVector<MetricSpaceType> &u) {
    return u/std::sqrt(std::fabs(dot_product(u,u)));
}


/** \brief Normalize a cotangent vector.
 *  
 *  \param w the vector to normalize.
 *
 *  \return Vector pointing along \c w with norm \f$ \pm 1 \f$, 
 *  equal to \f$ \hat{w}^\mu = w^\mu / \sqrt{|w^\nu w_\nu|} \f$.
 *  \ingroup geometry 
 */
template<typename MetricSpaceType>
inline CotangentVector<MetricSpaceType> 
normalized(const CotangentVector<MetricSpaceType> &w) {
    return w/std::sqrt(std::fabs(dot_product(w,w)));
}


/** \brief Generalization of the ordinary cross product.
 *
 *  Computes the cross product of the spatial parts of \p a and \p b,
 *  as seen by an observer with four-velocity along \p u.
 *  The resulting vector is \f$ c^\mu = (0, \vec{c}) \f$ in the observers frame
 *  and satisfies \f$ \vec{c} = \vec{a} \times \vec{b} \f$.
 *  It is defined as
 *  \f$ c^\mu = -{{\epsilon_\alpha}^\mu}_{\beta\gamma} 
 *             u^\alpha a^\beta b^\gamma \f$,
 *  where \f$ \epsilon_{\alpha\beta\gamma\delta} \f$ is the Levi-Civita tensor.
 *
 *  Only defined for four-dimensional spacetimes.
 *
 *  \param u a timelike vector along the four-velocity of the observer, 
 *  normalized internally.
 *
 *  \param a,b Vectors whose cross product is computed
 *
 *  \return The spatial cross product of \p a and \p b defined above.
 *
 *  \ingroup geometry 
 */
template<typename MetricSpaceType>
inline TangentVector<MetricSpaceType> 
spatial_cross_product(const TangentVector<MetricSpaceType> &u,
                      const TangentVector<MetricSpaceType> &a,
                      const TangentVector<MetricSpaceType> &b) {

    static_assert(MetricSpaceType::is_lorentzian,
                  "Only defined for Lorentzian spacetimes");

    const MetricSpaceType &M = u.M();
    const ManifoldPoint<MetricSpaceType> &p = u.point();

    // TODO: If this needs to be optimized further, should combine the 
    // generation of levi-civita components and contractions into a single loop
    // here.
    // Now most of the terms in the contractions are 0, which leads to wasted
    // work.
    return MetricSpaceType::Signature::spacelike_sign *
           (M.levi_civita_tensor(p)
                .template contract_prod<0, 0>(u)
                .template contract_prod<1, 0>(a)
                .template contract_prod<1, 0>(b)
                .template raise_index<0>() /
            std::sqrt(std::fabs(dot_product(u, u))));
}


/** \brief Project a vector orthogonal to another.
 * 
 *  The projection of a vector \f$ b^\mu \f$ orthogonal to a vector 
 *  \f$ a^\mu \f$ is defined as 
 *  \f$ b_\perp^\mu 
 *      = b^\mu - a^\mu \frac{a^\alpha b_\alpha}{a^\beta a_\beta} \f$,
 *  and satisfies by definition \f$  b_\perp^\mu a_\mu = 0\f$.
 *  
 *  The vector \p a must be non-null, \f$ a^\mu a_\mu \neq 0 \f$.
 *
 *  \param a a non-null vector.
 *  \param b vector to be projected.
 *
 *  \return \p b projected orthogonal to \p a.
 *  \ingroup geometry 
 */
template<typename MetricSpaceType>
inline TangentVector<MetricSpaceType> 
project_orthogonal(const TangentVector<MetricSpaceType> &a,
                   const TangentVector<MetricSpaceType> &b){

    //optimize the repeated dot products
    a.assert_at_same_point(b);
    const auto& M = a.M();
    const auto& chart = M.choose_better_chart(a.point(), a.chart(), b.chart());
    const auto a_comp = a.components(chart);
    const auto b_comp = b.components(chart);
    const auto g_comp = M.metric_components(a.point(),chart);
    
    constexpr long D = MetricSpaceType::D;
    
    const Eigen::Map<const ArcVector<D>> a_vec(a_comp.data());
    const Eigen::Map<const ArcVector<D>> b_vec(b_comp.data());
    const Eigen::Map<const ArcMatrix<D>> g(g_comp.data());
    const double a_dot_a = a_vec.transpose() * g * a_vec;
    const double a_dot_b = a_vec.transpose() * g * b_vec;
    
    //Not defined for null vectors since they are orthogonal to themselves
    if(std::fabs(a_dot_a) < M.configuration.pointwise.tolerance.absolute){
        throw std::domain_error(
            "Can't project orthogonal to a lightlike vector");
    }

    return b - a*(a_dot_b/a_dot_a);
}

/** \brief Compute spacelike angle between vectors.
 *  
 *  Computes the angle between the spatial parts of \p a and \p b, as seen by
 *  an observer with four-velocity along \p u.
 *
 *  Only defined for four-dimensional spacetimes.
 *
 *  \param u a timelike vector along the four-velocity of the observer.
 *  \param a,b two vectors
 *
 *  \return The angle between the spatial parts of \p a and \p b.
 *  \ingroup geometry 
 */
template<typename MetricSpaceType>
inline std::enable_if_t<MetricSpaceType::is_lorentzian, double>
vector_angle(const TangentVector<MetricSpaceType> &u,
             const TangentVector<MetricSpaceType> &a,
             const TangentVector<MetricSpaceType> &b){

    //optimize the repeated dot products
    a.assert_at_same_point(b);
    u.assert_at_same_point(b);
    const auto& M = a.M();
    const auto& chart = M.choose_better_chart(a.point(), a.chart(), b.chart());
    const auto a_comp = a.components(chart);
    const auto b_comp = b.components(chart);
    const auto u_comp = u.components(chart);
    const auto g_comp = M.metric_components(a.point(),chart);
    
    const Eigen::Map<const ArcVector<4>> a_vec(a_comp.data());
    const Eigen::Map<const ArcVector<4>> b_vec(b_comp.data());
    const Eigen::Map<const ArcVector<4>> u_vec(u_comp.data());
    const Eigen::Map<const ArcMatrix<4>> g(g_comp.data());
    
    const double u_dot_u = u_vec.transpose() * g * u_vec;

    //check timelikedness
    if (MetricSpaceType::Signature::timelike_sign * u_dot_u <
        M.configuration.pointwise.tolerance.absolute) {
        throw std::domain_error(
            "u needs to be timelike, u^a u_a = " + std::to_string(u_dot_u) +
            ", expected " +
            std::to_string(MetricSpaceType::Signature::timelike_sign));
    }
    //Normalized spatial components.
    //Repeats the project_orthogonal logic to avoid creation of unnecessary
    //objects.
    const double a_dot_u = a_vec.transpose() * g * u_vec;
    const double b_dot_u = b_vec.transpose() * g * u_vec;
    const ArcVector<4> a_spatial = a_vec - u_vec*(a_dot_u/u_dot_u); 
    const ArcVector<4> b_spatial = b_vec - u_vec*(b_dot_u/u_dot_u);
    //square norms
    const double a_spatial2 = a_spatial.transpose() * g * a_spatial;
    const double b_spatial2 = b_spatial.transpose() * g * b_spatial;
    
    const double a_dot_b = (a_spatial.transpose() * g * b_spatial);

    return std::acos(MetricSpaceType::Signature::spacelike_sign * a_dot_b /
                     std::sqrt(std::fabs(a_spatial2 * b_spatial2)));
}


/** 
 *
 * A stub function for metric spaces that are not spacetimes.
 * Only for simplifying the implementation of \c AffineCurve, likely to be 
 * removed in  the future.
 *
 * Always returns 0.
 * \ingroup geometry 
 */
// TODO: figure out how to handle other dimensions
template<typename MetricSpaceType>
inline std::enable_if_t<!MetricSpaceType::is_lorentzian, double>
vector_angle(const TangentVector<MetricSpaceType> &,
             const TangentVector<MetricSpaceType> &,
             const TangentVector<MetricSpaceType> &) {
   
    // Need to return something so that terminations work in AffineCurve
    return 0;
}

//
// Conversions between rank 1/2 EigenTensors and vectors/matrices

/** \brief Convert \c EigenRk1Tensor to \c ArcVector
 * \tparam D The dimension
 * \param t Rank 1 tensor to convert
 * \return An ArcVector containing the tensor data
 * \ingroup geometry
 */
template<long D>
inline ArcVector<D> eigen_tensor_to_vector(const EigenRk1Tensor<D> &t) {
    return Eigen::Map<const ArcVector<D>>(t.data());
}

/** \brief Convert \c EigenRk2Tensor to \c ArcMatrix
 * \tparam D The dimension
 * \param t Rank 2 tensor to convert
 * \return An ArcMatrix containing the tensor data
 * \ingroup geometry
 */
template<long D>
inline ArcMatrix<D> eigen_tensor_to_matrix(const EigenRk2Tensor<D> &t) {
    return Eigen::Map<const ArcMatrix<D>>(t.data());
}

/** \brief Convert \c ArcVector to \c EigenRk1Tensor
 * \tparam D The dimension
 * \param v ArcVector to convert
 * \return An EigenRk1Tensor containing the vector data
 * \ingroup geometry
 */
template<long D>
inline EigenRk1Tensor<D> eigen_vector_to_tensor(const ArcVector<D> &v) {
    //const_cast needed to remove const qualifier from the data pointer
    //The data is immediately copied to a new Eigen::Tensor, so it shouldn't
    //cause any problems.
    return Eigen::TensorMap<Eigen::Tensor<double,1>>(
                const_cast<double*>(v.data()),v.size());
}

/** \brief Convert \c ArcRowVector to \c EigenRk1Tensor
 * \tparam D The dimension
 * \param v ArcRowVector to convert
 * \return An EigenRk1Tensor containing the vector data
 * \ingroup geometry
 */
template<int D>
inline EigenRk1Tensor<D> eigen_vector_to_tensor(const ArcRowVector<D> &v) {
    return Eigen::TensorMap<Eigen::Tensor<double,1>>(
                const_cast<double*>(v.data()),v.size());
}

/** \brief Convert \c ArcMatrix to \c EigenRk2Tensor
 * \tparam D The dimension
 * \param m ArcMatrix to convert
 * \return An EigenRk2Tensor containing the vector data
 * \ingroup geometry
 */
template<int D>
inline EigenRk2Tensor<D> eigen_matrix_to_tensor(const ArcMatrix<D> &m) {
    return Eigen::TensorMap<Eigen::Tensor<double,2>>(
                const_cast<double*>(m.data()), m.rows(), m.cols());
}

// Inverse condition number = min(singular values) / max(singular
// values).
// TODO: Since TensorBase and EigenBase have _no_ relation, it is
// currently easier to do this with overloads
/** \brief Compute the inverse condition number of a matrix.
 * \tparam Derived Passed to Eigen::MatrixBase
 * \param mat Input matrix
 * \return \f$ min(\Sigma) / max(\Sigma)\f$, where \f$\Sigma\f$ is a
 *          vector of the singular values of \c mat.
 */
template <typename Derived>
inline double inv_condition_number(const Eigen::MatrixBase<Derived>& mat) {
    const auto sing_vals = mat.jacobiSvd().singularValues();
    const double inverse_ratio = sing_vals(sing_vals.size()-1) / sing_vals(0);
    return inverse_ratio;
}

/** \brief Compute the inverse condition number of a rank 2 tensor.
 * \tparam D Dimension
 * \param tensor Input rank 2 tensor
 * \return \f$ min(\Sigma) / max(\Sigma)\f$, where \f$\Sigma\f$ is a
 *          vector of the singular values of \c tensor.
 */
template<long D>
inline double inv_condition_number(const EigenRk2Tensor<D> &tensor) {

    // transform into a matrix
    const Eigen::Map<const ArcMatrix<D>> mat(tensor.data());
    return inv_condition_number(mat);
}

/** \brief A convenience function for creating ManifoldPoints.
 *
 * \param chart Which chart to use. Also determines the MetricSpace.
 * \param coordinates The coordinates for the point in the given chart.
 * \ingroup geometry
 */
template <typename MetricSpaceType>
inline constexpr ManifoldPoint<MetricSpaceType> make_point(
    const Chart<MetricSpaceType> &chart,
    const EigenRk1Tensor<MetricSpaceType::D> &coordinates) {
    return ManifoldPoint<MetricSpaceType>(chart, coordinates);
}

/** \copydoc make_point
 */
template <typename MetricSpaceType>
inline constexpr ManifoldPoint<MetricSpaceType> make_point(
    const Chart<MetricSpaceType> &chart,
    EigenRk1Tensor<MetricSpaceType::D> &&coordinates) {
    return ManifoldPoint<MetricSpaceType>(chart,
            std::forward<EigenRk1Tensor<MetricSpaceType::D>>(coordinates));
}

/** \copydoc make_point
 */
template <typename MetricSpaceType>
inline constexpr ManifoldPoint<MetricSpaceType> make_point(
    const Chart<MetricSpaceType> &chart,
    std::initializer_list<double> coordinates) {
    return ManifoldPoint<MetricSpaceType>(chart, coordinates);
}

/** \brief A convenience function for creating fixed size Eigen tensors.
 */
template <long D>
inline constexpr EigenRk1Tensor<D> make_EigenRk1Tensor(
        std::initializer_list<double> components) {
    EigenRk1Tensor<D> t;
    t.setValues(components);
    return t;
}

template <long D>
inline constexpr EigenRk2Tensor<D> make_EigenRk2Tensor(
        std::initializer_list<std::initializer_list<double>> components) {
    EigenRk2Tensor<D> t;
    t.setValues(components);
    return t;
}

template <long D>
inline constexpr EigenRk3Tensor<D> make_EigenRk3Tensor(
        std::initializer_list<std::initializer_list<std::initializer_list<double>>> components) {
    EigenRk3Tensor<D> t;
    t.setValues(components);
    return t;
}

template <long D>
inline constexpr EigenRk4Tensor<D> make_EigenRk4Tensor(
        std::initializer_list<std::initializer_list<std::initializer_list<std::initializer_list<double>>>> components) {
    EigenRk4Tensor<D> t;
    t.setValues(components);
    return t;
}

} // namespace arcmancer
