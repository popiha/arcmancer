#pragma once

#include <ostream>
#include <sstream>
#include <string>

#include "arcmancer/geometry/meta.h"
#include "arcmancer/geometry/types.h"

namespace arcmancer {

namespace internal {
// Placeholder class for when no parallel transport is needed
// The ParametrizedPoint implementation contains special cases for this class,
// so only some of the methods need to be defined.
template <typename MetricSpaceType>
class EmptyTransport {
public:
    static const size_t component_count = 0;
    using ContainerType = Eigen::TensorFixedSize<double, Eigen::Sizes<0>>;

    EmptyTransport() {}

    EmptyTransport(const typename MetricSpaceType::PointType &,
                   const typename MetricSpaceType::ChartType &,
                   const ContainerType &) {}

    ContainerType components(const Chart<MetricSpaceType> &) const {
        return ContainerType{};
    }

    ContainerType transport_derivatives(
        const Chart<MetricSpaceType> &,
        const EigenRk2Tensor<MetricSpaceType::D> &) const {
        return ContainerType{};
    }
};

template <typename MetricSpaceType>
std::ostream &operator<<(std::ostream &os,
                         const EmptyTransport<MetricSpaceType> &) {
    return os;
}

} // namespace internal


/** \brief A single point on a parametrized curve.
 *
 *  This class together with ParametrizedCurve forms the core of Arcmancer's 
 *  support for curves on manifolds.
 *  It wraps together the tangent vector of the curve and optionally an object
 *  to be transported along the curve, and handles conversion between the
 *  components of provided by these objects and the internal representation used
 *  in ParametrizedCurve's integration routine.
 *
 *  Support for a wide range of curves and transported objects is provided by
 *  allowing user specified forces and templatization of the transported object 
 *  type.
 *  The curve equation of motion is determined by the \c force_function
 *  of type \c ParametrizedPoint::ForceFunction.
 *  The equation of motion of the curve is 
 *  \f[
 *       u^\mu \nabla_\mu u^\nu = f^\nu, 
 *  \f]
 *  where \f$ u^\mu \f$ is the tangent vector of the curve and \f$ f^\nu \f$ is 
 *  the vector returned by \c force_function.
 *  The \c force_function is passed through \c ParametrizedCurve, see also its
 *  documentation.
 *  
 *  
 *  \c TransportType is the type of the object transported along the curve.
 *  It can be for example a Tensor, in which case the tensor is parallel 
 *  transported along the curve, 
 *  but an object of any type satisfying the following requirements can be used.
 *
 * ### Requirements on Custom `TransportType`s
 *
 *  \c TransportType must
 *
 *  * Define a public static member \c component_count, which gives the total
 *    number of components.
 *
 *  * Define a public member typedef \c ContainerType.
 *    \c ContainerType must be a \c Eigen::TensorFixedSize of \c double with
 *    \c component_count components.
 *    The shape of the container can be set freely.
 *
 *  * Optionally define a public member typedef \c ExtraDataType.
 *    An object of this type is passed along the curve, and can be used attach
 *    arbitrary additional data and functions to be used for example in the
 *    equation of motion of the object.
 *    Objects of this type are copied to each point on a curve,
 *    so \c ExtraDataType should be cheap to copy, for example a pointer.
 *
 *  * Define a constructor with one of the following signatures:
 *
 *      - If \c ExtraDataType is not defined
 *        \code
 *          TransportType(const ManifoldPoint<MetricSpaceType> &point,
 *                      const Chart<MetricSpaceType> &chart,
 *                      const ContainerType &components);
 *        \endcode
 *        which constructs the object at \c point with \c components in the
 *        given \c chart.
 *
 *      - If \c ExtraDataType is defined
 *        \code
 *          TransportType(const ManifoldPoint<MetricSpaceType> &point,
 *                      const Chart<MetricSpaceType> &chart,
 *                      const ContainerType &components,
 *                      ExtraDataType extra_data);
 *        \endcode
 *        which constructs the object at \c point with \c components in the
 *        given \c chart and also takes the additional data.
 *
 *  * Define the public methods
 *    \code
 *      const PointType& point() const;
 *      ContainerType components(const Chart<MetricSpaceType> &chart) const;
 *    \endcode
 *    which return the underlying point and the components of the object.
 *
 *  * If \c ExtraDataType is defined, define a public method
 *    \code
 *      ExtraDataType extra_data() const;
 *    \endcode
 *    which gives access to the extra data.
 *
 *  * Define a public method \c transport_derivatives returning the components
 *    of \f$ \frac{\mathrm{d} T}{\mathrm{d} \lambda} \f$, where
 *    \f$ T \f$ is the transported object.
 *
 *    The \c transport_derivatives 
 *    method must have one of the following signatures
 *    \code
 *      ContainerType transport_derivatives(const Chart<MetricSpaceType> &chart,
 *          const EigenRk2Tensor<MetricSpaceType::D> &contracted_gamma) const;
 *
 *      ContainerType transport_derivatives(const Chart<MetricSpaceType> &chart,
 *          const EigenRk2Tensor<MetricSpaceType::D> &contracted_gamma,
 *          const TangentVector<MetricSpacetype> &tangent) const;
 *
 *      ContainerType transport_derivatives(const Chart<MetricSpaceType> &chart,
 *          const EigenRk2Tensor<MetricSpaceType::D> &contracted_gamma,
 *          const TangentVector<MetricSpacetype> &tangent,
 *          const TangentVector<MetricSpacetype> &force) const;
 *
 *    \endcode
 *    Here \c contracted_gamma are the component of the Christoffel symbols 
 *    contracted with the tangent vector in the given chart,
 *    i.e. \f$ {\Gamma^\alpha}_{\beta\gamma} u^\gamma\f$,
 *    \c tangent is the tangent vector of the curve and \c force is the 
 *    external force or acceleration applied on the curve.
 *
 *  * Define
 *    \code
 *      std::ostream &operator<<(std::ostream &,
 *                               const TransportType&);
 *    \endcode
 *    used for printing a description of the object.
 *
 *
 *  \tparam MetricSpaceType the type of the underlying manifold.
 *  \tparam TransportType the type of the transported object.
 *          Optional, defaults to a special placeholder type.
 *
 *  \see ParametrizedCurve
 *
 *  \ingroup geometry
 *  \ingroup overaligned
 */
template <typename MetricSpaceType,
          typename TransportType = internal::EmptyTransport<MetricSpaceType>>
class ParametrizedPoint {

    // For use in function bodies, can't use in enable_ifs since they need to
    // depend on a template type parameter
    static const bool no_transport =
        std::is_same<TransportType,
                     internal::EmptyTransport<MetricSpaceType>>::value;

public:

    EIGEN_MAKE_ALIGNED_OPERATOR_NEW

    using ChartType = Chart<MetricSpaceType>;
    ///< Convenience alias.

    using TangentType = TangentVector<MetricSpaceType>;
    ///< Convenience alias.

    using PointType = ManifoldPoint<MetricSpaceType>;
    ///< Convenience alias.
    
    using ForceFunction = std::function<TangentType(const TangentType &)>;
    ///< For passing a right hand side to the equation of motion.

    static constexpr size_t component_count =
        1                                 // curve parameter
        + PointType::component_count
        + TangentType::component_count
        + TransportType::component_count; 
    ///< Total number of components.

    using ContainerType =
        Eigen::TensorFixedSize<double, Eigen::Sizes<component_count>>;
    ///< Container used for combined components.

    //
    // Get the extra data type for TransportType

    using TransportExtraDataType =
        typename internal::get_extra_data_type<TransportType>::type;
    ///< TransportType::ExtraDataType if defined, otherwise boost::mpl::void_


    /** \brief Constructor when TransportType has been specified.
     *
     *  Construct a ParametrizedPoint with the given curve parameter, tangent
     *  and transported objects.
     *
     *  Both objects must be at the same point,
     *  \code tangent.point() == transported.point() \endcode is true.
     *
     *  \param curve_parameter the value of the curve's parameter at this
     *         point.
     *
     *  \param tangent the tangent vector of the curve.
     *  \param transported the object transported along the curve.
     *
     */
    template <typename T = TransportType,
              typename = std::enable_if_t<!std::is_same<
                  T, internal::EmptyTransport<MetricSpaceType>>::value>>
    ParametrizedPoint(double curve_parameter, const TangentType &tangent,
                      const TransportType &transported)
    : M_(&tangent.point().M()),
      curve_parameter_(curve_parameter),
      tangent_(tangent),
      transported_(transported) {
        // The objects need to be at the same point
        if (tangent_.point() != transported_.point()) {
            std::ostringstream oss;
            oss << "tangent and transported at different points:\n"
                << tangent_.point() << std::endl
                << transported_.point() << std::endl
                << " in coordinates of tangent:\n"
                << transported.point().coordinates(tangent_.chart()) << std::endl;
            throw std::runtime_error(oss.str());
        }
    }

    /** \brief Constructor when TransportType has been omitted.
     *
     *  Construct a ParametrizedPoint with the given curve parameter and
     * tangent.
     *
     *  \param curve_parameter the value of the curve's parameter at this
     *         point.
     *
     *  \param tangent the tangent vector of the curve.
     */
    template <typename T = TransportType,
              typename = std::enable_if_t<std::is_same<
                  T, internal::EmptyTransport<MetricSpaceType>>::value>>
    ParametrizedPoint(double curve_parameter, const TangentType &tangent)
    : M_(&tangent.point().M()),
      curve_parameter_(curve_parameter),
      tangent_(tangent) {}

    //
    // Getters for the private members

    /** \brief Get the curve parameter of the point.
     *
     */
    double curve_parameter() const { return curve_parameter_; }

    /** \brief Get the point on the manifold at this point in the curve.
     *
     */
    const PointType &point() const { return tangent_.point(); }

    /** \brief Get the tangent vector of the curve.
     *
     */
    const TangentType &tangent() const { return tangent_; }

    /** \brief Get the object transported along the curve.
     *
     *  Not available if \c TransportType has been omitted.
     */
    template <typename T = TransportType,
              typename = std::enable_if_t<!std::is_same<
                  T, internal::EmptyTransport<MetricSpaceType>>::value>>
    const TransportType &transported() const {
        return transported_;
    }

    /** \brief Get the associated manifold object.
     *
     */
    const MetricSpaceType &M() const { return *M_; }

    friend std::ostream &operator<<(std::ostream &os,
                                    const ParametrizedPoint &ap) {
        os << "ParametrizedPoint with curve_parameter = " << ap.curve_parameter_
           << std::endl;
        os << "At point: " << ap.point() << std::endl;
        os << "Tangent vector: " << std::endl << ap.tangent_ << std::endl;

        if (!no_transport) {
            os << "Transported object: " << std::endl
               << ap.transported_ << std::endl;
        }

        return os;
    }

    //
    // Methods needed for ParametrizedCurve integration
    // Really only intended for use in the ParametrizedCurve implementation

    /** \brief Constructor used in ParametrizedCurve implementation.
     *
     *  Construct from the combined components of the ParametrizedPoint.
     *
     *  Used when \c TransportType has been given.
     *
     *  Intended for internal use.
     */
    template <typename T = TransportType,
              typename = std::enable_if_t<!std::is_same<
                  T, internal::EmptyTransport<MetricSpaceType>>::value>>
    ParametrizedPoint(const ChartType &chart, const ContainerType &components,
                      const TransportExtraDataType &transported_extra)
    : M_(&chart.M),
      curve_parameter_(components(0)),

      tangent_({chart, components.slice(Eigen::array<size_t, 1>{1},
                                        Eigen::array<size_t, 1>{
                                            PointType::component_count})},
               chart,
               components.slice(
                   Eigen::array<size_t, 1>{1 + PointType::component_count},
                   Eigen::array<size_t, 1>{TangentType::component_count})),

      transported_(select_constructor<TransportType>(
          tangent_.point(), chart,
          components
              .slice(Eigen::array<size_t, 1>{1 + PointType::component_count +
                                             tangent_.component_count},
                     Eigen::array<size_t, 1>{TransportType::component_count}
                     // XXX: the following probably requires ContainerType to
                     // be an Eigen::TensorFixedSize
                     )
              .reshape(typename TransportType::ContainerType::Dimensions{}),
          transported_extra)) {}

    /** \brief Constructor used in ParametrizedCurve implementation.
     *
     *  Construct from the combined components of the ParametrizedPoint.
     *
     *  Used when \c TransportType has been omitted.
     *
     *  Intended for internal use.
     */
    // Needs an extra template parameter to have different signature than the
    // previous definition for template deduction. Arguments need to be the same
    // to avoid leaking this complexity to ParametrizedCurve implementation
    template <typename T = TransportType, typename = void,
              typename = std::enable_if_t<std::is_same<
                  T, internal::EmptyTransport<MetricSpaceType>>::value>>
    ParametrizedPoint(const ChartType &chart, const ContainerType &components,
                      const TransportExtraDataType &)
    : M_(&chart.M),
      curve_parameter_(components(0)),

      tangent_({chart, components.slice(Eigen::array<size_t, 1>{1},
                                        Eigen::array<size_t, 1>{
                                            PointType::component_count})},
               chart,
               components.slice(
                   Eigen::array<size_t, 1>{1 + PointType::component_count},
                   Eigen::array<size_t, 1>{TangentType::component_count})) {}

    /** \brief Combined components.
     *
     *  Concatenates all the components into a one dimensional container.
     *
     *  Used in ParametrizedCurve implementation.
     *
     *  Intended for internal use.
     */
    ContainerType components(const ChartType &chart) const {
        ContainerType ret;
        ret(0) = curve_parameter_;
        size_t current_offset = 1;

        ret.slice(Eigen::array<size_t, 1>{current_offset},
                  Eigen::array<size_t, 1>{PointType::component_count}) =
            point().coordinates(chart);
        current_offset += PointType::component_count;

        ret.slice(Eigen::array<size_t, 1>{current_offset},
                  Eigen::array<size_t, 1>{tangent_.component_count}) =
            tangent_.components(chart);
        current_offset += tangent_.component_count;

        if (!no_transport) {
            ret.slice(Eigen::array<size_t, 1>{current_offset},
                      Eigen::array<size_t, 1>{transported_.component_count}) =
                transported_.components(chart).reshape(
                    Eigen::array<size_t, 1>{transported_.component_count});
        }

        return ret;
    }

    /** \brief Combined derivatives.
     *
     *  Concatenates all the \c transport_derivatives into a one dimensional
     *  container.
     *  Also adds the external force provided via \c ParametrizedCurve.
     *
     *  Used in ParametrizedCurve implementation.
     *
     *  Intended for internal use.
     */
    ContainerType transport_derivatives(const ChartType &chart,
                                        const ForceFunction &force_function)
    const {

        typename TangentType::ContainerType tangent_components =
            tangent_.components(chart);

        EigenRk2Tensor<MetricSpaceType::D> contracted_Gamma =
            M_->christoffel_symbols_contracted(point(), chart,
                                               tangent_components);

        ContainerType ret;
        ret(0) = 1; // dlambda/dlambda
        size_t current_offset = 1;

        // dx/dlambda = tangent vector
        ret.slice(Eigen::array<size_t, 1>{current_offset},
                  Eigen::array<size_t, 1>{PointType::component_count}) =
            tangent_components;
        current_offset += PointType::component_count;

        if (force_function) {
            const auto force = force_function(tangent_);

            ret.slice(Eigen::array<size_t, 1>{current_offset},
                      Eigen::array<size_t, 1>{tangent_.component_count}) =
                tangent_.transport_derivatives(chart, contracted_Gamma) +
                force.components(chart);

            current_offset += tangent_.component_count;

            if (!no_transport) {
                ret.slice(
                    Eigen::array<size_t, 1>{current_offset},
                    Eigen::array<size_t, 1>{transported_.component_count}) =
                    transported_transport_derivs(
                        &TransportType::transport_derivatives, chart,
                        contracted_Gamma, force)
                        .reshape(Eigen::array<size_t, 1>{
                            transported_.component_count});
            }
        } else {

            ret.slice(Eigen::array<size_t, 1>{current_offset},
                      Eigen::array<size_t, 1>{tangent_.component_count}) =
                tangent_.transport_derivatives(chart, contracted_Gamma);
            
            current_offset += tangent_.component_count;

            if (!no_transport) {
                ret.slice(
                    Eigen::array<size_t, 1>{current_offset},
                    Eigen::array<size_t, 1>{transported_.component_count}) =
                    transported_transport_derivs(
                        &TransportType::transport_derivatives, chart,
                        contracted_Gamma)
                        .reshape(Eigen::array<size_t, 1>{
                            transported_.component_count});
            }
        }

        return ret;
    }

    // expose the extra data members of TransportType if
    // applicable


    /** \brief Get the extra data of \c transported.
     *
     *  This version is used when \c TransportType::ExtraDataType is not
     *  defined,
     *  and returns an empty object.
     *
     *  Exists to simplify ParametrizedCurve implementation.
     */
    template <typename T = TransportType>
    std::enable_if_t<!internal::has_extra_data_type<T>::value,
                     TransportExtraDataType>
    transported_extra() const {
        return {};
    }

    /** \copybrief transported_extra
     *
     *  Returns \c transport.extra_data().
     */
    template <typename T = TransportType>
    std::enable_if_t<internal::has_extra_data_type<T>::value,
                     TransportExtraDataType>
    transported_extra() const {
        return transported_.extra_data();
    }

private:
    const MetricSpaceType *M_;
    double curve_parameter_;
    TangentType tangent_;
    TransportType transported_;

    //
    // Choose the correct constructor signature to call for the transport types
    // XXX: was used also for TangentType, now the templatization is somewhat 
    // too generic. Needs templates for SFINAE anyway, although it might be 
    // possible to avoid using it with some cleverness.
    template <typename T>
    static T select_constructor(const PointType &point, const ChartType &chart,
                                const typename T::ContainerType &components,
                                const typename T::ExtraDataType &extra_data) {

        return T(point, chart, components, extra_data);
    }

    template <typename T, typename = std::enable_if_t<
                              !internal::has_extra_data_type<T>::value, void>>
    static T select_constructor(const PointType &point, const ChartType &chart,
                                const typename T::ContainerType &components,
                                const mpl::void_ &) {

        return T(point, chart, components);
    }

    //
    // Choose between the different TransportType::transport_derivatives call
    // signatures by using the method type as an overload tag.

    // The call signature of the Tensor like transport_derivatives function
    using StandardTransportDerivSignature =
        typename TransportType::ContainerType (TransportType::*)(
            const ChartType &,
            const EigenRk2Tensor<MetricSpaceType::D> &) const;
    
    // signature for transport_derivatives taking also the tangent
    using TransportDerivWTangentSignature =
        typename TransportType::ContainerType (TransportType::*)(
            const ChartType &, const EigenRk2Tensor<MetricSpaceType::D> &,
            const TangentType &) const;
    
    // signature for transport_derivatives taking also the acceleration/force
    using TransportDerivWForceSignature =
        typename TransportType::ContainerType (TransportType::*)(
            const ChartType &, const EigenRk2Tensor<MetricSpaceType::D> &,
            const TangentType &, const TangentType &) const;
    
    // The following works since template class methods are instantiated only 
    // when called, so the invalid bodies of methods that are not called don't
    // matter.

    typename TransportType::ContainerType transported_transport_derivs(
        StandardTransportDerivSignature,
        const ChartType &chart,
        const EigenRk2Tensor<MetricSpaceType::D> &contracted_Gamma) const {

        return transported_.transport_derivatives(chart, contracted_Gamma);
    }

    typename TransportType::ContainerType transported_transport_derivs(
        TransportDerivWTangentSignature,
        const ChartType &chart,
        const EigenRk2Tensor<MetricSpaceType::D> &contracted_Gamma) const {

        return transported_.transport_derivatives(chart, contracted_Gamma,
                                                  tangent_);
    }
    
    typename TransportType::ContainerType transported_transport_derivs(
        TransportDerivWForceSignature,
        const ChartType &chart,
        const EigenRk2Tensor<MetricSpaceType::D> &contracted_Gamma) const {

        return transported_.transport_derivatives(
            chart, contracted_Gamma, tangent_, {point(), chart, {0, 0, 0, 0}});
    }
    
    // Need separate overloads which accept the force vector
    typename TransportType::ContainerType transported_transport_derivs(
        StandardTransportDerivSignature,
        const ChartType &chart,
        const EigenRk2Tensor<MetricSpaceType::D> &contracted_Gamma,
        const TangentType &) const {

        return transported_.transport_derivatives(chart, contracted_Gamma);
    }

    typename TransportType::ContainerType transported_transport_derivs(
        TransportDerivWTangentSignature,
        const ChartType &chart,
        const EigenRk2Tensor<MetricSpaceType::D> &contracted_Gamma,
        const TangentType &) const {

        return transported_.transport_derivatives(chart, contracted_Gamma,
                                                  tangent_);
    }
    
    typename TransportType::ContainerType transported_transport_derivs(
        TransportDerivWForceSignature,
        const ChartType &chart,
        const EigenRk2Tensor<MetricSpaceType::D> &contracted_Gamma,
        const TangentType & force) const {

        return transported_.transport_derivatives(
            chart, contracted_Gamma, tangent_, force);
    }

    // Catch all cases to give a clear error message instead of the standard 
    // overload resolution failure messages.
    template <typename F>
    typename TransportType::ContainerType transported_transport_derivs(
        F, const ChartType &,
        const EigenRk2Tensor<MetricSpaceType::D> &) const {
        static_assert(std::is_same<F, void>::value,
                      "TransportType has no transport_derivatives method with "
                      "a supported signature!");
    }

    template <typename F>
    typename TransportType::ContainerType transported_transport_derivs(
        F, const ChartType &, const EigenRk2Tensor<MetricSpaceType::D> &,
        const TangentType &) const {
        static_assert(std::is_same<F, void>::value,
                      "TransportType has no transport_derivatives method with "
                      "a supported signature!");
    }
};


} // namespace arcmancer
