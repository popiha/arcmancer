#pragma once

#include <algorithm>
#include <cmath>
#include <functional>
#include <limits>
#include <map>
#include <ostream>
#include <sstream>
#include <stdexcept>
#include <string>
#include <utility>

#include <Eigen/Dense>
#include <unsupported/Eigen/CXX11/Tensor>
#include <unsupported/Eigen/CXX11/TensorSymmetry>

#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/dijkstra_shortest_paths.hpp>

#include <spdlog/fmt/ostr.h>

#include "arcmancer/configuration.h"
#include "arcmancer/general_utils.h"
#include "arcmancer/geometry/tensor.h"
#include "arcmancer/geometry/types.h"
#include "arcmancer/geometry/utils.h"

namespace arcmancer {

// Tag structs representing the metric signature

/** \brief Represents the (+ - - -) metric signature
 *
 *  A tag type for use as the \c Signature parameter of \c MetricSpace.
 *  \ingroup manifolds
 */
struct LorentzianTimelikeSignature {
    static const int timelike_sign = 1;
    static const int spacelike_sign = -1;
};

/** \brief Represents the (- + + +) metric signature
 *
 *  A tag type for use as the \c Signature parameter of \c MetricSpace.
 *  \ingroup manifolds
 */
struct LorentzianSpacelikeSignature {
    static const int timelike_sign = -1;
    static const int spacelike_sign = 1;
};

/** \brief Represents a generic non-Lorentzian signature.
 *
 *  A tag type for use as the \c Signature parameter of \c MetricSpace.
 *  \ingroup manifolds
 */
struct OtherSignature {};

/** \brief A base class for different metric space geometries.
 *
 *  The central mathematical structure in Arcmancer is the (pseudo-) Riemannian
 *  manifold on which all the other geometrical objects, such as curves and
 *  tensors are defined.
 *  This class is the abstract base class for all classes that describe such a
 *  manifold, and can only be instantiated as a part of a deriving class.
 *  These concrete implementations will be referred to as concrete \c
 *  MetricSpaces.
 *  In Arcmancer,
 *  manifolds of arbitrary dimension are supported, but the most complete set of
 *  features is only provided for Lorentzian spacetimes.
 *
 *  Most classes that describe geometrical objects have a template parameter
 *  \c MetricSpaceType that makes this dependency explicit on the type level.
 *  Valid values of this parameter are classes that are concrete \c
 *  MetricSpaces.
 *
 * ### Implementing a concrete `MetricSpace`
 *
 *  \c MetricSpace is implemented using CRTP
 *  (curiously recurring template pattern) to allow propagating the type-level
 *  dependencies of the different geometrical objects, such as points.
 *
 *  The definition of a concrete \c MetricSpace named \c Derived starts like
 *  this:
 *  \code
 *      class Derived: public MetricSpace<Derived, D, Signature> {
 *  \endcode
 *  Here \c D > 1 is the dimension of the manifold described by \c Derived,
 *  a positive integer.
 *  \c Signature is a type describing the signature convention used for the
 *  metric tensor.
 *  For \c D == 4 it can be one of \c LorentzianTimelikeSignature,
 *  \c LorentzianSpacelikeSignature or \c OtherSignature,
 *  otherwise only \c OtherSignature is supported.
 *  This signature information is used whenever the computation depends on the
 *  signature convention, such as when computing angles between spacelike
 *  vectors, allowing Arcmancer to support both of the common conventions used
 *  in general relativity.
 *
 *  Next, define a method
 *  \code std::string name() const; \endcode
 *  that returns a string describing the manifold.
 *
 *  The manifold described by \c Derived may have one or more coordinate charts.
 *  For each chart, define a member describing the chart, for example
 *  \code
 *      const ChartType cartesian = {*this, "Cartesian coordinates"};
 *  \endcode
 *  If \c M is an instance of \c Derived, the charts can be referred to with the
 *  convenient syntax `M.cartesian`.
 *
 *  For each chart, define functions for the metric components
 *  and the derivatives of the metric, e.g.
 *  \code
 *      EigenRk2Tensor<D>
 *      metric_components_cartesian(const EigenRk1Tensor<D> &x) const;
 *
 *      EigenRk3Tensor<D>
 *      metric_derivatives_cartesian(const EigenRk1Tensor<D> &x) const;
 *  \endcode
 *  Here \c x contains the coordinates of the point where components are to be
 *  calculated.
 *  These functions can be either member functions of \c Derived, or anything
 *  that can be converted to a `std::function`, more specifically the component
 *  function can be either of type \c MetricComponentType or
 *  \c MemberMetricComponentType, and the derivative function either
 *  \c MetricDerivativeType or \c MemberMetricDerivativeType.
 *  These functions are then paired with the charts by calling in the
 *  constructor of \c Derived either
 *  \code
 *      add_chart(cartesian,
 *                metric_components_cartesian,
 *                metric_derivatives_cartesian);
 *  \endcode
 *  or
 *  \code
 *      add_chart(cartesian,
 *                &Derived::metric_components_cartesian,
 *                &Derived::metric_derivatives_cartesian);
 *  \endcode
 *  depending on whether the functions are defined as member functions or not.
 *  The indices of the metric derivatives are ordered as in
 *  \f$ g_{\mu\nu,\alpha} \f$.
 *
 *  If multiple charts are defined, the transition functions and jacobians for
 *  the transitions between the charts must be defined.
 *  A transition function from \c chart1 to \c chart2 takes the coordinates in
 *  \c chart 1 and returns the corresponding coordinates in \c chart2.
 *  The possible signatures are given by \c TransitionFunctionType and
 *  \c MemberTransitionFunctionType.
 *  The jacobians take likewise the coordinates in \c chart1 and return the
 *  jacobian matrix for the transition, i.e.
 *  \f[ {J^\mu}_\nu = \frac{\partial y^\mu}{\partial x^\nu}, \f]
 *  where \f$ x^\nu \f$ are the coordinates in \c chart1 and \f$ y^\mu \f$
 *  the coordinates of \c chart2.
 *  The first index corresponds to the rows of the matrix, and the second to
 *  columns.
 *  As with the transition functions, the possible signatures are given by
 *  \c JacobianFunctionType and \c MemberTransitionFunctionType.
 *
 *  The transitions between charts need to be added by calling 
 *  `add_transition()` in the constructor, e.g.
 *  \code
 *      add_transition(chart1, chart2,
 *                     &Derived::transition12,
 *                     &Derived::jacobian12);
 *
 *      add_transition(chart2, chart1,
 *                     &Derived::transition21);
 *  \endcode
 *  It is enough to define the minimum amount of transition functions so that
 *  the bidirectional graph of transition functions is connected.
 *  Likewise, it is enough to define the jacobians in one direction.
 *  The missing transition functions and jacobians can be generated
 *  automatically by calling \c complete_chart_graph() at the end of the
 *  constructor.
 *  The missing functions are generated by composing the transition
 *  functions and by inverting and multiplying the jacobian matrices.
 *  It is naturally also possible to define all the transition functions by
 *  hand.
 *
 *  Due to its implementation details, the base `MetricSpace` object cannot be
 *  copied or moved. Deriving classes need to define their own copy constructors
 *  if they are required.
 *
 *  For complete examples see e.g. the implementations of 
 *  \c minkowski::MinkowskiSpacetime and \c kerr::KerrSpacetime.
 *
 *
 *  \tparam Derived the class derived from MetricSpace, a concrete \c
 *                  MetricSpace.
 *
 *  \tparam D_ the number of dimensions of the manifold.
 *             The value is available through the static member \c D.
 *
 *  \tparam Signature_ a type describing the signature of the metric.
 *                     Valid values are \c LorentzianTimelikeSignature,
 *                     \c LorentzianSpacelikeSignature,
 *                     \c OtherSignature.
 *                     If \c D_ != 4, only \c OtherSignature is valid.
 *                     Available as the member type \c Signature.
 *
 *  \ingroup manifolds
 *  \ingroup geometry
 */
template <typename Derived, long D_ = 4,
          typename Signature_ = LorentzianTimelikeSignature>
class MetricSpace {
    static_assert(D_ > 1, "Must have D > 1!");

    static_assert(
        std::is_same<Signature_, LorentzianTimelikeSignature>::value ||
            std::is_same<Signature_, LorentzianSpacelikeSignature>::value ||
            std::is_same<Signature_, OtherSignature>::value,
        "Invalid signature type! Must be one of "
        "LorentzianTimelikeSignature, LorentzianSpacelikeSignature, "
        "OtherSignature");

public:
    // These type aliases are available in the deriving classes also
    using ChartType = Chart<Derived>;         ///< Convenience alias for charts
    using PointType = ManifoldPoint<Derived>; ///< Convenience alias for points

    static constexpr long D = D_; ///< The dimension of the manifold

    using Signature = Signature_; ///< The signature of the metric

    static constexpr bool is_lorentzian =
        std::is_same<Signature, LorentzianTimelikeSignature>::value ||
        std::is_same<Signature, LorentzianSpacelikeSignature>::value;
    ///< Whether the metric has a Lorentzian signature

    static_assert(!(is_lorentzian && D != 4),
                  "Lorentzian signatures are only valid with D == 4");

    // Types of the different functions.
    // metric functions take the coordinate values directly in the corresponding
    // chart
    using MetricComponentType =
        std::function<EigenRk2Tensor<D>(const EigenRk1Tensor<D> &)>;
    ///< Non-member functions returning the metric components

    using MetricDerivativeType =
        std::function<EigenRk3Tensor<D>(const EigenRk1Tensor<D> &)>;
    ///< Non-member functions returning the metric derivatives

    // transition functions take the coordinates in the original chart
    using TransitionFunctionType =
        std::function<EigenRk1Tensor<D>(const EigenRk1Tensor<D> &)>;
    ///< Non-member functions giving a mapping between coordinates in two charts

    using JacobianFunctionType =
        std::function<ArcMatrix<D>(const EigenRk1Tensor<D> &)>;
    ///< Non-member functions giving the jacobian of a transition function

    // The same as member functions of Derived
    using MemberMetricComponentType =
        EigenRk2Tensor<D> (Derived::*)(const EigenRk1Tensor<D> &) const;
    ///< Member functions returning the metric components

    using MemberMetricDerivativeType =
        EigenRk3Tensor<D> (Derived::*)(const EigenRk1Tensor<D> &) const;
    ///< Member functions returning the metric derivatives

    using MemberTransitionFunctionType =
        EigenRk1Tensor<D> (Derived::*)(const EigenRk1Tensor<D> &) const;
    ///< Member functions giving a mapping between coordinates in two charts

    using MemberJacobianFunctionType =
        ArcMatrix<D> (Derived::*)(const EigenRk1Tensor<D> &) const;
    ///< Member functions giving the jacobian of a transition function

    /** \brief Adjustable configuration used for calculations on \c Derived.
     *
     *  Initially set to default values from configuration.h.
     *  The configuration is shared for all the computations done with the
     *  same instance of \c Derived.
     */
    configuration::Configuration configuration;

private:
    // containers for storing the functions on different charts
    std::map<const ChartType *, MetricComponentType>
        metric_component_functions_;

    std::map<const ChartType *, MetricDerivativeType>
        metric_derivative_functions_;

    std::map<std::pair<const ChartType *, const ChartType *>,
             TransitionFunctionType>
        transition_functions_;

    std::map<std::pair<const ChartType *, const ChartType *>,
             JacobianFunctionType>
        jacobian_functions_;

    // Pointer to the chart that is to be used instead of the best chart
    // Is null if the chart isn't fixed
    const ChartType *fixed_chart_ = nullptr;

    // bind a member function pointer to the instance of Derived
    template <typename T, typename... Args>
    auto bind_mem_fn(T (Derived::*f)(Args...) const) const {
        return [this, f](Args... args) -> T {
            return (static_cast<const Derived *>(this)->*f)(args...);
        };
    }

protected:
    // MetricSpace should not be constructed on its own, only as a base class
    MetricSpace() = default;

    // Explicitly delete copy and move constructor, since the
    // pointer trickery used here may break otherwise
    MetricSpace(const MetricSpace&) = delete;
    MetricSpace(MetricSpace&&) = delete;


    /** \brief Pair a chart with metric functions
     *
     *  Call in the constructor of \c Derived to associate the metric functions
     *  with the given \c chart.
     *
     *  \param chart a coordinate chart.
     *  \param metric_component_function a function giving the components of
     *      the metric tensor at the given coordinates in \c chart.
     *
     *  \param metric_derivative_function a function giving the partial
     *      derivatives of
     *      the metric tensor at the given coordinates in \c chart.
     *
     */
    void add_chart(const ChartType &chart,
                   MetricComponentType metric_component_function,
                   MetricDerivativeType metric_derivative_function) {

        metric_component_functions_[&chart] = metric_component_function;
        metric_derivative_functions_[&chart] = metric_derivative_function;
    }

    /** \brief Define a transition between two charts
     *
     *  Call in the constructor of \c Derived to associate
     *  \c transition_function and \c jacobian_function with the transition
     *  from \c from_chart to \c to_chart.
     *
     *  \param from_chart, to_chart coordinate charts.
     *
     *  \param transition_function a function mapping coordinates in
     *      \c from_chart to coordinates in \c to_chart
     *
     *  \param jacobian_function a function returning the jacobian matrix of the
     *      transition function
     *
     */
    void add_transition(const ChartType &from_chart, const ChartType &to_chart,
                        TransitionFunctionType transition_function,
                        JacobianFunctionType jacobian_function) {

        transition_functions_[std::make_pair(&from_chart, &to_chart)] =
            transition_function;

        jacobian_functions_[std::make_pair(&from_chart, &to_chart)] =
            jacobian_function;
    }

    /** \copybrief add_transition
     *
     *  A version for when the jacobian can be generated from already defined
     *  jacobians.
     *
     *  \param from_chart, to_chart coordinate charts.
     *
     *  \param transition_function a function mapping coordinates in
     *      \c from_chart to coordinates in \c to_chart
     *
     */
    void add_transition(const ChartType &from_chart, const ChartType &to_chart,
                        TransitionFunctionType transition_function) {

        transition_functions_[std::make_pair(&from_chart, &to_chart)] =
            transition_function;
    }

    /** \copybrief add_chart
     *
     *  A version for member functions.
     *
     *  \see add_chart
     */
    void add_chart(const ChartType &chart,
                   MemberMetricComponentType metric_component_function,
                   MemberMetricDerivativeType metric_derivative_function) {

        add_chart(chart, bind_mem_fn(metric_component_function),
                  bind_mem_fn(metric_derivative_function));
    }

    /** \copybrief add_transition
     *
     *  A version for member functions.
     *
     *  \see add_transition
     */
    void add_transition(const ChartType &from_chart, const ChartType &to_chart,
                        MemberTransitionFunctionType transition_function,
                        MemberJacobianFunctionType jacobian_function) {

        add_transition(from_chart, to_chart, bind_mem_fn(transition_function),
                       bind_mem_fn(jacobian_function));
    }

    /** \copybrief add_transition
     *
     *  A version for member functions.
     *
     *  \see add_transition
     */
    void add_transition(const ChartType &from_chart, const ChartType &to_chart,
                        MemberTransitionFunctionType transition_function) {

        add_transition(from_chart, to_chart, bind_mem_fn(transition_function));
    }

    /** \brief Check that the chart graph is complete
     *
     *  Throws an exception if a transition function or jacobian is missing.
     */
    void validate_transitions() {
        for (const auto &kv1 : metric_component_functions_) {
            for (const auto &kv2 : metric_component_functions_) {
                if (kv1.first != kv2.first &&
                    transition_functions_.count(
                        std::make_pair(kv1.first, kv2.first)) != 1 &&
                    jacobian_functions_.count(
                        std::make_pair(kv1.first, kv2.first)) != 1) {
                    std::stringstream ss;
                    ss << "Missing transition/jacobian between charts "
                       << kv1.first << " and " << kv2.first;

                    // Log the exception to make debugging easier
                    // XXX: should probably add these to all the other
                    // exceptions also.
                    Log::log()->error("validate_transitions: {}", ss.str());
                    throw std::runtime_error(ss.str());
                }
            }
        }
    }

    /** \brief Check that the chart has been added to the set of charts
     *
     */
    bool is_valid_chart(const ChartType &chart) {
        return metric_component_functions_.count(&chart) == 1;
    }

    /** \brief Generate missing transition functions and jacobians.
     *
     *  Call this function at the end of the \c Derived constructor to
     *  automatically generate missing transition functions if possible.
     */
    void complete_chart_graph() {
        complete_transition_graph();
        complete_jacobian_graph();

        // Check that everything worked out.
        // This should never fail, but bugs in the automatic generation are
        // possible
        validate_transitions();
    }

public:
    /** \brief Get a string describing the manifold
     *
     */
    std::string name() const {
        return static_cast<const Derived *>(this)->name();
    }

    /** \brief Convenience function for creating points
     *  through the Spacetime itself
     *
     *  \param chart a coordinate chart.
     *  \param coordinates the coordinates of the point.
     *
     */
    PointType create_point(const ChartType &chart,
                           const EigenRk1Tensor<D> &coordinates) const {
        return PointType(chart, coordinates);
    }

    /** \copydoc create_point
     */
    PointType create_point(const ChartType &chart,
                           std::initializer_list<double> coordinates) const {
        return PointType(chart, coordinates);
    }

    /** \brief Set the fixed chart to be used for computations
     *
     *  By default all computations attempt to use the best available chart.
     *  Determining the best chart can be computationally expensive, so
     *  using a fixed chart is usually faster.
     *
     *  \param chart the coordinate chart to use.
     */
    void use_fixed_chart(const ChartType &chart) {
        if (!is_valid_chart(chart)) {
            throw std::runtime_error("Invalid chart");
        }
        fixed_chart_ = &chart;
    }

    /** \brief Use the best available chart for computations.
     *
     *  Revert to the default mode of operation after calling
     *  \c use_fixed_chart().
     */
    void use_best_chart() { fixed_chart_ = nullptr; }

    /** \brief Whether multiple charts are currently in use.
     *
     */
    bool uses_multiple_charts() const {
        return (transition_functions_.size() != 0) && (fixed_chart_ == nullptr);
    }

    /** \brief Evaluate the transition function between charts
     *
     *  Maps the given coordinates from \c from_chart to \c to_chart
     *
     *  \param from_chart, to_chart coordinate charts.
     *  \param coordinates the coordinates in \c from_chart
     *
     *  \return the coordinates in \c to_chart
     */
    EigenRk1Tensor<D> transition_function(
        const ChartType &from_chart, const ChartType &to_chart,
        const EigenRk1Tensor<D> &coordinates) const {
        return transition_functions_.at(std::make_pair(&from_chart, &to_chart))(
            coordinates);
    }

    /** \brief Get the jacobian matrix for a transition
     *
     *  Computes the jacobian for the transition from \c from_chart to
     *  \c to_chart at the point \c x.
     *
     *  \param x the point where the jacobian is evaluated.
     *  \param from_chart,to_chart coordinate charts.
     *
     *  \return the jacobian matrix
     *
     */
    ArcMatrix<D> jacobian(const PointType &x, const ChartType &from_chart,
                          const ChartType &to_chart) const {
        return jacobian_functions_.at(std::make_pair(&from_chart, &to_chart))(
            x.coordinates(from_chart));
    }

    /** \brief Get the components of the metric tensor
     *
     *  Computes the metric components in \c chart at the point \c x.
     *
     *  \param x the point where the metric is computed
     *  \param chart the chart in which the components are expressed
     *
     */
    EigenRk2Tensor<D> metric_components(const PointType &x,
                                        const ChartType &chart) const {
        return metric_component_functions_.at(&chart)(x.coordinates(chart));
    }

    /** \brief Get the derivatives of the metric
     *
     *  Computes the partial derivatives
     *  \f$ g_{\mu\nu,\alpha} = \partial_\alpha g_{\mu\nu}\f$
     *  at \c x in the given \c chart.
     *
     *  \param x the point where the derivatives are computed
     *  \param chart the chart in which the components are expressed
     *
     *  \return The derivatives of the metric \f$ g_{\mu\nu,\alpha}\f$.
     *
     */
    EigenRk3Tensor<D> metric_derivatives(const PointType &x,
                                         const ChartType &chart) const {
        return metric_derivative_functions_.at(&chart)(x.coordinates(chart));
    }

    /** \brief Numerically approximate the metric derivatives
     *
     *  Like \c metric_derivatives, but computes the derivatives using finite
     *  differences.
     *
     *  Useful for instance for debugging.
     *
     *
     *  \param x the point where the derivatives are computed
     *  \param chart the chart in which the components are expressed
     *  \param order the order of the finite difference approximation.
     *
     *  \return Numerical approximation of the derivatives of the metric
     *  \f$ g_{\mu\nu,\alpha}\f$.
     *
     *  \see DifferenceOrder, differentiate
     *
     */
    EigenRk3Tensor<D> metric_derivatives_num(
        const PointType &x, const ChartType &chart,
        DifferenceOrder order = DifferenceOrder::order_2) const {
        EigenRk3Tensor<D> ret;
        ret.setZero();

        // get base point in desired chart
        EigenRk1Tensor<D> x0 = x.coordinates(chart);

        // compute g_ab,i = d g_ab(x)/dx^i for each i
        for (size_t i = 0; i < D; i++) {

            std::function<EigenRk2Tensor<D>(double)> f_xi =
                [&](double xi) -> EigenRk2Tensor<D> {
                EigenRk1Tensor<D> xnew = x0;
                xnew(i) = xi;
                PointType p(chart, xnew);
                return this->metric_components(p, chart);
            };

            EigenRk2Tensor<D> gab_i = differentiate<double, EigenRk2Tensor<D>>(
                f_xi, x0(i), std::sqrt(std::numeric_limits<double>::epsilon()),
                order);

            ret.chip(i, 2) = gab_i;
        }

        return ret;
    }

    /** \brief Get the components of the inverse metric tensor
     *
     *  Computes the inverse metric components in \c chart at the point \c x.
     *  The components are computed by inverting the matrix of metric
     *  components.
     *
     *  \param x the point where the inverse metric is computed
     *  \param chart the chart in which the components are expressed
     *
     */
    EigenRk2Tensor<D> inverse_metric_components(const PointType &x,
                                                const ChartType &chart) const {
        // Convert the Eigen tensor to a matrix to compute the inverse
        EigenRk2Tensor<D> g = metric_components(x, chart);
        // XXX: This depends on the internal storage order of the Eigen
        // Matrix and Tensor classes. These are probably the same, but
        // anyway it shouldn't matter since g is symmetric
        Eigen::Map<const ArcMatrix<D>> g_mat(g.data());
        ArcMatrix<D> g_inv_mat = g_mat.inverse();
        return eigen_matrix_to_tensor(g_inv_mat);
    }

    /** \brief Compute the determinant of the metric
     *
     *  Computes the determinant of the matrix of metric components in \c chart
     *  at \c x.
     *
     *  \param x point where the determinant is computed.
     *  \param chart the coordinate chart in which the determinant is computed.
     *
     *  \return The determinant of the metric at \c x.
     *
     */
    double metric_determinant(const PointType &x,
                              const ChartType &chart) const {
        // Convert the Eigen tensor to a matrix to compute the inverse
        EigenRk2Tensor<D> g = metric_components(x, chart);
        Eigen::Map<const ArcMatrix<D>> g_mat(g.data());
        return g_mat.determinant();
    }

    /** \brief Get the metric tensor.
     *
     *  \param x a point.
     *
     *  \return The metric tensor at x.
     */
    Tensor<Derived, Cov, Cov> metric_tensor(const PointType &x) const {
        auto &chart = get_best_chart(x);
        return {x, chart, metric_components(x, chart)};
    }

    /** \brief Get the inverse metric tensor.
     *
     *  \param x a point.
     *
     *  \return The inverse metric tensor at x.
     */
    Tensor<Derived, Cnt, Cnt> inverse_metric_tensor(
        const ManifoldPoint<Derived> &x) const {
        auto &chart = get_best_chart(x);
        return {x, chart, inverse_metric_components(x, chart)};
    }

    /** \brief Get the Christoffel symbols of the second kind
     *
     *  Computes the Christoffel symbols
     *  \f[
     *  {\Gamma^\alpha}_{\mu\nu} =
     *  \frac{1}{2} g^{\alpha\beta}
     *  \left( g_{\beta\nu,\mu} + g_{\mu\beta,\nu} - g_{\mu\nu,\beta}\right)
     *  \f]
     *  from the metric and its derivatives.
     *
     *  \param x point where the symbols are evaluated.
     *  \param chart the coordinate chart in which the symbols are expressed.
     *
     *  \return The Christoffel symbols of the second kind.
     *
     *  \see christoffel_symbols_contracted()
     *
     */
    EigenRk3Tensor<D> christoffel_symbols(const PointType &x,
                                          const ChartType &chart) const {

        EigenRk3Tensor<D> g_d = metric_derivatives(x, chart);
        EigenRk2Tensor<D> g_inv = inverse_metric_components(x, chart);
        EigenRk3Tensor<D> ret;

        // Plain loops appear to be much faster than using Eigen shuffling and
        // contractions, likely since intermediate values are avoided.
        for (int i = 0; i < D; ++i) {
            for (int j = 0; j < D; ++j) {
                for (int k = 0; k < D; ++k) {
                    double comp = 0;
                    for (int l = 0; l < D; ++l) {
                        comp += .5 * g_inv(i, l) *
                                (g_d(j, l, k) + g_d(l, k, j) - g_d(j, k, l));
                    }
                    ret(i, j, k) = comp;
                }
            }
        }

        return ret;
    }

    /** \brief Get the Christoffel symbols contracted with a vector
     *
     *  Computes the contraction of the Christoffel symbols with a vector,
     *  usually the tangent of a curve, i.e. computes
     *  \f[
     *  {k^\nu \Gamma^\alpha}_{\mu\nu} =
     *  \frac{1}{2} k^\nu g^{\alpha\beta}
     *  \left( g_{\beta\nu,\mu} + g_{\mu\beta,\nu} - g_{\mu\nu,\beta}\right)
     *  \f]
     *  where \f$ k \f$ is a tangent vector.
     *
     *  This method is equivalent to computing the contraction using the 
     *  `Eigen` functionality:
     *  \code
     *      this->christoffel_symbols(x, chart).contract(
     *                           tangent_components,
     *                           Eigen::array<IndexPair, 1>{IndexPair(2, 0)});
     *  \endcode
     *  but is considerably faster.
     *
     *  \param x point where the symbols are evaluated.
     *  \param chart the coordinate chart in which the symbols are expressed.
     *  \param tangent_components the components of a the tangent vector to
     *         contract with in the specified chart.
     *
     *  \return The Christoffel symbols of the second kind contracted with
     *  the given vector.
     *
     */
    EigenRk2Tensor<D> christoffel_symbols_contracted(
        const PointType &x, const ChartType &chart,
        const EigenRk1Tensor<D> &tangent_components) const {

        EigenRk3Tensor<D> g_d = metric_derivatives(x, chart);
        EigenRk2Tensor<D> g_inv = inverse_metric_components(x, chart);
        EigenRk2Tensor<D> ret;

        for (int j = 0; j < D; ++j) {
            for (int i = 0; i < D; ++i) {
                
                double comp = 0;
                for (int k = 0; k < D; ++k) {
                    for (int l = 0; l < D; ++l) {

                        comp += .5 * tangent_components(k) * g_inv(i, l) *
                                (g_d(j, l, k) + g_d(l, k, j) - g_d(j, k, l));
                    }
                }
                ret(i, j) = comp;
            }
        }

        return ret;
    }

    /** \brief Get the Christoffel symbols twice contracted with vectors
     *
     *  Computes the contraction of the Christoffel symbols with two vectors,
     *  yielding
     *  \f[
     *  {\Gamma^\alpha}_{\mu\nu} x^\mu y^\nu =
     *  \frac{1}{2} g^{\alpha\beta}
     *  \left( g_{\beta\nu,\mu} + g_{\mu\beta,\nu} - g_{\mu\nu,\beta}\right)x^\mu y^\nu
     *  \f]
     *  where \f$ x \f$ and \f$ y \$ are contravariant vectors.
     *
     *  This method is equivalent to computing the contraction using the 
     *  `Eigen` functionality:
     *  \code
     *      this->christoffel_symbols(x, chart).contract(
     *                           tangent_components,
     *                           Eigen::array<IndexPair, 1>{IndexPair(2, 0)});
     *  \endcode
     *  but is considerably faster.
     *
     *  \param x point where the symbols are evaluated.
     *  \param chart the coordinate chart in which the symbols are expressed.
     *  \param x_components the components of a the vector \f$ x \f$ to
     *         contract with, in the specified chart.
     *  \param y_components the components of a the vector \f$ y \f$ to
     *         contract with, in the specified chart.
     *
     *  \return The Christoffel symbols of the second kind contracted with
     *  the given vector.
     *
     */
    EigenRk1Tensor<D> christoffel_symbols_twice_contracted(
        const PointType &x, const ChartType &chart,
        const EigenRk1Tensor<D> &x_components,
        const EigenRk1Tensor<D> &y_components) const {

        EigenRk3Tensor<D> g_d = metric_derivatives(x, chart);
        EigenRk2Tensor<D> g_inv = inverse_metric_components(x, chart);
        EigenRk1Tensor<D> ret;

        for (int a = 0; a < D; ++a) {
            double comp = 0;
            for (int b = 0; b < D; ++b) {
                for (int c = 0; c < D; ++c) {
                    for (int d = 0; d < D; ++d) {

                            comp += .5 * g_inv(a, d) * (g_d(c, d, b) + g_d(d, b, c) - g_d(b, c, d))
                                 * x_components(b) * y_components(c);
                    }
                }
            }
            ret(a) = comp;
        }

        return ret;
    }

    /** \brief Get the Levi-Civita tensor
     *
     *  Returns the Levi-Civita tensor
     *  \f$ \epsilon_{\mu\nu\rho\sigma} \f$
     *  which is completely antisymmetric and satisfies
     *  \f$ \epsilon_{0123} = \sqrt{|g|},\f$
     *  with \f$ g \f$ the determinant of the metric.
     *
     *  Only available for \c D == 4.
     *
     *  \param x a point
     *
     *  \return The Levi-Civita tensor at \c x.
     *
     */
    // TODO: generalize to other dims
    template <size_t D__ = D, typename = std::enable_if_t<D__ == 4, void>>
    Tensor<Derived, Cov, Cov, Cov, Cov> levi_civita_tensor(
        const PointType &x) const {

        auto &chart = get_best_chart(x);

        EigenRk4Tensor<4> epsilon;
        epsilon.setZero();

        const double sqrt_detg =
            std::sqrt(std::fabs(metric_determinant(x, chart)));
        
        using Index = EigenRk4Tensor<4>::Index;
        Eigen::array<Index, 4> indices{0,1,2,3};

        do {
            // Compute the sign of the permutation.
            // Using the simplest algorithm since only 4 elements.
            int sign = 1;
            for (int i = 1; i < 4; ++i) {
                for (int j = 0; j < i; ++j) {
                    if (indices[j] > indices[i]) { sign *= -1; }
                }
            }

            epsilon(indices) = sign * sqrt_detg;

        } while (std::next_permutation(indices.begin(), indices.end()));

        return {x, chart, epsilon};
    }

    /** \brief Get the chart with the best behaviour for tensor operations
     *
     *  Attempts to find the best behaved coordinate chart by looking at
     *  the condition number of the metric tensor.
     *
     *  \param point a point
     *
     *  \return The numerically best chart at \c point.
     */
    const ChartType &get_best_chart(const PointType &point) const {
        // Check if we're using a fixed chart
        if (fixed_chart_) { return *fixed_chart_; }

        // Iterate over all charts, and choose the one that gives a
        // metric whose matrix has the largest inverse condition number

        const auto ret =
            std::max_element(
                metric_component_functions_.begin(),
                metric_component_functions_.end(),
                [&point](const auto &a, const auto &b) -> bool {

                    // If the point is not on one of the charts, return the
                    // other
                    // one.
                    if (!point.on_chart(*a.first)) { return true; }
                    if (!point.on_chart(*b.first)) { return false; }

                    const auto g_a = a.second(point.coordinates(*a.first));
                    const auto g_b = b.second(point.coordinates(*b.first));

                    return inv_condition_number<D>(g_a) <
                           inv_condition_number<D>(g_b);
                })
                ->first;

        return *ret;
    }

    /** \brief Get the chart where integration of geodesics is easiest
     *
     *  In addition to looking at the condition number of the metric, this
     *  method also looks at the derivatives of the tangent vector of a 
     *  geodesic, and chooses a chart where the geodesic is the straightest.
     *
     *  \param point a point
     *
     *  \return The best chart for integration at \c point.
     */
    const ChartType &get_best_integration_chart(
        const TangentVector<Derived> &u) const {
        // Check if we're using a fixed chart
        if (fixed_chart_) { return *fixed_chart_; }

        // XXX: iterating over metric_component_functions since there is no
        // separate container just for the charts. Maybe that would be useful
        // elsewhere also

        const auto ret =
            std::max_element(
                metric_component_functions_.begin(),
                metric_component_functions_.end(),
                [this, &u](const auto &a, const auto &b) -> bool {

                    if (!u.point().on_chart(*a.first)) { return true; }
                    if (!u.point().on_chart(*b.first)) { return false; }

                    const double cond_ratio_tol =
                        this->configuration.chart_selection_cond_tol;

                    const auto g_a = a.second(u.point().coordinates(*a.first));
                    const auto g_b = b.second(u.point().coordinates(*b.first));
                    const double cond_ratio =
                        fabs(inv_condition_number<D>(g_a) /
                             inv_condition_number<D>(g_b));
                    // choose based on the condition number if there is a large
                    // enough difference
                    if (cond_ratio > cond_ratio_tol) {
                        return false; // pick a
                    }
                    if (cond_ratio < 1 / cond_ratio_tol) {
                        return true; // pick b
                    }

                    // otherwise choose based on the derivatives of the tangent
                    // vector

                    EigenRk2Tensor<D> contracted_Gamma_a =
                        this->christoffel_symbols_contracted(
                            u.point(), *a.first, u.components(*a.first));

                    Eigen::TensorFixedSize<double, Eigen::Sizes<>>
                        trans_der_a_max = u.transport_derivatives(
                                               *a.first, contracted_Gamma_a)
                                              .abs()
                                              .maximum();

                    EigenRk2Tensor<D> contracted_Gamma_b =
                        this->christoffel_symbols_contracted(
                            u.point(), *b.first, u.components(*b.first));

                    Eigen::TensorFixedSize<double, Eigen::Sizes<>>
                        trans_der_b_max = u.transport_derivatives(
                                               *b.first, contracted_Gamma_b)
                                              .abs()
                                              .maximum();
                    
                    // This comparison combined with max_element gives the
                    // chart with the smaller derivatives.
                    // Should have really just used min_element from the start,
                    // but this way the fast returns above work the same as for
                    // the condition number case.
                    const bool comparison =
                        trans_der_a_max(0) > trans_der_b_max(0);

                    // For NaN values the Eigen maximum operation results in
                    // -inf, so if either of the values is infinite we need to
                    // invert the order since -inf should be regarded as worse
                    // than any other value and otherwise the smaller value is
                    // chosen.
                    return (std::isfinite(trans_der_a_max(0)) &&
                            std::isfinite(trans_der_b_max(0)))
                               ? comparison
                               : !comparison;
                })
                ->first;

        return *ret;
    }

    /** \brief Pick the better of two charts
     *
     *  Uses the same selection logic as \c get_best_chart().
     *
     *  \param point a point.
     *  \param chart1, chart2 coordinate charts.
     *
     *  \return The better one of the charts at \c point.
     *
     *  \see get_best_chart
     *
     */
    const ChartType &choose_better_chart(const PointType &point,
                                         const ChartType &chart1,
                                         const ChartType &chart2) const {
        // If a fixed chart is used, return that regardless of the given charts
        if (fixed_chart_) { return *fixed_chart_; }

        if (&chart1 == &chart2) { return chart1; }

        const auto g_1 = metric_components(point, chart1);
        const auto g_2 = metric_components(point, chart2);
        if (inv_condition_number(g_1) > inv_condition_number(g_2)) {
            return chart1;
        }
        return chart2;
    }

    friend std::ostream &operator<<(
        std::ostream &os, const MetricSpace<Derived, D, Signature> &space) {
        os << space.name();
        return os;
    }

private:
    // Generate the missing transition functions by finding the shortest paths
    // in the graph
    void complete_transition_graph() {

        using namespace boost;
        using VertexProperty = struct {
            // XXX: the boost documentation says that this typedef is needed,
            // but the compiler says it's unused.
            // Seems to works fine without it.
            // using kind = vertex_property_tag;
            const ChartType *chart;
        };

        using Graph = adjacency_list<vecS, vecS, directedS, VertexProperty,
                                     property<edge_weight_t, int>>;
        using Vertex = typename Graph::vertex_descriptor;

        Graph g;

        std::map<const ChartType *, Vertex> chart_vertex;

        // Build the graph

        // TODO: should probably add some member to store just the charts,
        // maybe with IDs for serialization.
        // Make a list of charts here for now.
        std::vector<const ChartType *> charts;

        for (auto kv : metric_component_functions_) {
            auto chart = kv.first;
            Vertex v = add_vertex(g);
            g[v] = VertexProperty{chart};
            chart_vertex[chart] = v;

            charts.push_back(chart);
        }

        for (auto kv : transition_functions_) {
            auto k = kv.first;
            // all edges have weight 1
            add_edge(chart_vertex.at(k.first), chart_vertex.at(k.second), 1, g);
            // XXX should check that the edge is actually added from the retval
        }

        for (auto chart1 : charts) {
            for (auto chart2 : charts) {
                if (chart1 == chart2) continue;
                if (transition_functions_.find(std::make_pair(
                        chart1, chart2)) != transition_functions_.end()) {
                    continue;
                }
                // No transition map defined, generate one based on the
                // shortest path
                generate_transition(g, chart_vertex, chart1, chart2);
            }
        }
    }

    template <typename Graph, typename CVMap>
    void generate_transition(Graph &g, CVMap &chart_vertex,
                             const ChartType *chart1, const ChartType *chart2) {

        using namespace boost;
        using Vertex = typename Graph::vertex_descriptor;

        const int inf = std::numeric_limits<int>::max();

        // find the shortest path from chart1 to chart2
        std::vector<Vertex> predecessors(num_vertices(g));
        std::vector<int> dist(num_vertices(g));
        shortest_paths(g, chart_vertex.at(chart1), dist, predecessors);

        Log::log()->debug("generate_transition: Distance from {} to {}: {}",
                *chart1, *chart2, dist[get(vertex_index, g, chart_vertex.at(chart2))]);

        if (dist[get(vertex_index, g, chart_vertex.at(chart2))] == inf) {
            std::stringstream ss;
            ss << "No path between charts " << *chart1 << " and " << *chart2;

            Log::log()->error("generate_transition: {}", ss.str());
            throw std::runtime_error(ss.str());
        }

        std::vector<TransitionFunctionType> tfuncs;
        std::vector<const ChartType *> charts_on_path;
        auto cur_chart = chart2;

        while (cur_chart != chart1) {
            // Walk back along the predecessor chain
            auto pred_chart = g[predecessors[get(vertex_index, g,
                                                 chart_vertex.at(cur_chart))]]
                                  .chart;
            // store the transition functions for each transition in the chain
            tfuncs.push_back(transition_functions_.at(
                std::make_pair(pred_chart, cur_chart)));

            charts_on_path.push_back(pred_chart);
            cur_chart = pred_chart;
        }

        // The list is backwards, reverse it
        std::reverse(tfuncs.begin(), tfuncs.end());

        // create a function composing the transitions along the chain
        transition_functions_[std::make_pair(chart1, chart2)] =
            [tfuncs](const EigenRk1Tensor<D> &x) -> EigenRk1Tensor<D> {
            auto y = x;
            for (auto &f : tfuncs) {
                y = f(y);
            }
            return y;
        };

        // Log the chain
        std::reverse(charts_on_path.begin(), charts_on_path.end());

        std::stringstream ss;
        for (auto c : charts_on_path) {
            ss << *c << " -> ";
        }
        Log::log()->debug("Generated transition function\n"
                "{} -> {}\n"
                "as the chain of transitions\n"
                "{}{}", *chart1, *chart2, ss.str(), *chart2);
    }

    // Generate the missing jacobians
    void complete_jacobian_graph() {
        using namespace boost;
        using VertexProperty = struct { const ChartType *chart; };

        using Graph = adjacency_list<vecS, vecS, directedS, VertexProperty,
                                     property<edge_weight_t, int>>;
        using Vertex = typename Graph::vertex_descriptor;

        Graph g;

        std::map<const ChartType *, Vertex> chart_vertex;

        // Build the graph

        std::vector<const ChartType *> charts;

        for (auto kv : metric_component_functions_) {
            auto chart = kv.first;
            Vertex v = add_vertex(g);
            g[v] = VertexProperty{chart};
            chart_vertex[chart] = v;

            charts.push_back(chart);
        }

        for (auto kv : jacobian_functions_) {
            auto k = kv.first;
            // edges corresponding to user defined functions have weight 1
            add_edge(chart_vertex.at(k.first), chart_vertex.at(k.second), 1, g);
            // XXX should check that the edge is actually added from the retval
        }

        // Generate missing inverse jacobians
        for (auto kv : jacobian_functions_) {
            auto k = kv.first;

            if (jacobian_functions_.find(std::make_pair(k.second, k.first)) !=
                jacobian_functions_.end()) { 
                // inverse jacobian exists already
                continue;
            }
            
            auto J = kv.second;
            auto tf =
                transition_functions_.at(std::make_pair(k.second, k.first));

            jacobian_functions_[std::make_pair(k.second, k.first)] =
                [J, tf](const EigenRk1Tensor<D> &x) -> ArcMatrix<D> {
                return J(tf(x)).inverse();
            };

            // Edges corresponding to generated functions have weight 2
            // to make them less preferred in the generation of combined
            // jacobians.
            add_edge(chart_vertex.at(k.second), chart_vertex.at(k.first), 2, g);

            Log::log()->debug(
                "Generated jacobian for the transition\n{} -> {}"
                "\nas inverse of the jacobian for the transition\n{} -> {}\n",
                *k.second , *k.first,
                *k.first , *k.second);
        }

        for (auto chart1 : charts) {
            for (auto chart2 : charts) {
                if (chart1 == chart2) continue;
                if (jacobian_functions_.find(std::make_pair(chart1, chart2)) !=
                    jacobian_functions_.end()) continue;

                // No jacobian map defined, generate one based on the
                // shortest path
                generate_jacobian(g, chart_vertex, chart1, chart2);
            }
        }
    }

    template <typename Graph, typename CVMap>
    void generate_jacobian(Graph &g, CVMap &chart_vertex,
                           const ChartType *chart1, const ChartType *chart2) {

        using namespace boost;
        using Vertex = typename Graph::vertex_descriptor;

        const int inf = std::numeric_limits<int>::max();

        // compute distances in both directions
        std::vector<Vertex> predecessors1(num_vertices(g));
        std::vector<int> dist1(num_vertices(g));
        shortest_paths(g, chart_vertex.at(chart1), dist1, predecessors1);

        std::vector<Vertex> predecessors2(num_vertices(g));
        std::vector<int> dist2(num_vertices(g));
        shortest_paths(g, chart_vertex.at(chart2), dist2, predecessors2);

        int d12 = dist1[get(vertex_index, g, chart_vertex.at(chart2))];
        int d21 = dist2[get(vertex_index, g, chart_vertex.at(chart1))];

        Log::log()->debug("generate_jacobian: "
                          "Distance from {} to {}: {}\n"
                          "Distance from {} to {}: {}\n",
                          *chart1 , *chart2 , d12, 
                          *chart2 , *chart1 , d21);

        // Generate the jacobian chain in the inverse direction if the length
        // of the chain + 1 unit for the inversion is shorter than the forward
        // direction.
        // Attempts to avoid chains where J.inverse() is called repeatedly.

        int d;
        bool inverse_direction = false;
    
        // the distances can be inf == INT_MAX, so use substraction to avoid 
        // potential overflow issues
        if (d12 - 1 <= d21) {
            d = d12;
        } else {
            d = d21;
            inverse_direction = true;
        }

        if (d == inf) {
            std::stringstream ss;
            ss << "No path between charts " << *chart1 << " and " << *chart2;
            Log::log()->error("generate_jacobian: {}", ss.str());
            throw std::runtime_error(ss.str());
        }

        std::vector<TransitionFunctionType> tfuncs;
        std::vector<JacobianFunctionType> Jfuncs;
        std::vector<const ChartType *> charts_on_path;

        std::vector<Vertex> predecessors;
        const ChartType *cur_chart;
        const ChartType *final_chart;

        if (inverse_direction) {
            cur_chart = chart1;
            final_chart = chart2;
            predecessors = predecessors2;
        } else {
            cur_chart = chart2;
            final_chart = chart1;
            predecessors = predecessors1;
        }

        while (cur_chart != final_chart) {
            auto pred_chart = g[predecessors[get(vertex_index, g,
                                                 chart_vertex.at(cur_chart))]]
                                  .chart;

            // Need to store also the necessary transition functions
            // chart1 -> intermediate chart
            // that are needed for computing the jacobians
            if (pred_chart != chart1) {
                tfuncs.push_back(transition_functions_.at(
                    std::make_pair(chart1, pred_chart)));
            }

            Jfuncs.push_back(
                jacobian_functions_.at(std::make_pair(pred_chart, cur_chart)));

            charts_on_path.push_back(pred_chart);
            cur_chart = pred_chart;
        }

        std::reverse(tfuncs.begin(), tfuncs.end());
        std::reverse(Jfuncs.begin(), Jfuncs.end());

        std::reverse(charts_on_path.begin(), charts_on_path.end());

        if (inverse_direction) {

            // Compose the jacobians and invert
            jacobian_functions_[std::make_pair(chart1, chart2)] =
                [tfuncs, Jfuncs](const EigenRk1Tensor<D> &x) -> ArcMatrix<D> {
                const int size = Jfuncs.size();
                auto J = Jfuncs[0](tfuncs[0](x));
                for (int i = 1; i < size; ++i) {
                    J = Jfuncs[i](tfuncs[i](x)) * J;
                }
                return J.inverse();
            };

            std::stringstream ss;
            for (auto c : charts_on_path) {
                ss << *c << " -> ";
            }

            Log::log()->debug(
                "Generated jacobian for the transition\n{} -> {}\n"
                "\nas inverse of the jacobian for the chain of transitions\n"
                "{}{}\n",
                *chart1, *chart2, ss.str(), *chart1);

        } else {

            jacobian_functions_[std::make_pair(chart1, chart2)] =
                [tfuncs, Jfuncs](const EigenRk1Tensor<D> &x) -> ArcMatrix<D> {
                const int size = Jfuncs.size();
                auto J = Jfuncs[0](x);
                for (int i = 1; i < size; ++i) {
                    J = Jfuncs[i](tfuncs[i - 1](x)) * J;
                }
                return J;
            };

            std::stringstream ss;
            for (auto c : charts_on_path) {
                ss << *c << " -> ";
            }

            Log::log()->debug(
                "Generated jacobian for the transition\n{} -> {}\n"
                "\nas the jacobian for the chain of transitions\n"
                "{}{}\n",
                *chart1, *chart2, ss.str(), *chart2);
        }
    }

    // A simple helper to hide the noisy call syntax.
    template <typename Graph,
              typename Vertex = typename Graph::vertex_descriptor>
    static void shortest_paths(Graph &g, Vertex &v, std::vector<int> &dist,
                               std::vector<Vertex> &predecessors) {

        using namespace boost;

        dijkstra_shortest_paths(
            g, v,
            predecessor_map(make_iterator_property_map(predecessors.begin(),
                                                       get(vertex_index, g)))
                .distance_map(make_iterator_property_map(
                    dist.begin(), get(vertex_index, g))));
    }
};

} // namespace arcmancer
