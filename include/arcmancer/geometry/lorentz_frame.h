#pragma once

#include <limits>
#include <ostream>
#include <stdexcept>
#include <vector>
#include <sstream>

#include "arcmancer/geometry/types.h"
#include "arcmancer/geometry/utils.h"

namespace arcmancer {

/** \brief A local orthonormal basis.
 *
 *  \c LorentzFrame represents a set of orthonormal basis vectors at a single
 *  point in spacetime.
 *  It can be parallel transported along a ParametrizedCurve, i.e. it is a valid
 *  \c TransportType for ParametrizedPoint.
 *  It is also possible to
 *  manipulate `Tensor` components with respect to the basis of a LorentzFrame,
 *  i.e. it is valid `FrameType` for the constructors and 
 *  `Tensor::components(const FrameType&) const` method.
 *
 *  Only defined for Lorentzian spacetimes.
 *
 * \tparam MetricSpaceType the type of the underlying manifold.
 * \ingroup geometry
 * \ingroup overaligned
 *
 * \see \c LorentzFrameFW
 */
template <class MetricSpaceType>
class LorentzFrame {
    static_assert(MetricSpaceType::is_lorentzian,
                  "LorentzFrame only defined for Lorentzian spacetimes!");

public:

    EIGEN_MAKE_ALIGNED_OPERATOR_NEW

    enum class Handedness { RIGHT_HANDED, LEFT_HANDED};

    /** \brief Construct a right-handed basis.
     *
     *  Constructs a right handed basis by orthonormalizing the given vectors.
     *
     *  \param obs_u four-velocity of the observer corresponding to this frame, 
     *   becomes the timelike basis vector of the frame.
     *
     *  \param approximate_z approximate direction of the z-axis, the \c e_z 
     *   basis vector is along the part orthogonal to obs_u
     *
     *  \param approximate_x approximate direction of the x-axis.
     */
    LorentzFrame(const TangentVector<MetricSpaceType> &obs_u,
                 const TangentVector<MetricSpaceType> &approximate_z,
                 const TangentVector<MetricSpaceType> &approximate_x)
    : LorentzFrame(obs_u, approximate_z, approximate_x,
                   Handedness::RIGHT_HANDED) {}

    /** \brief Construct a basis of either handedness.
     *
     *  Constructs a basis by orthonormalizing the given vectors.
     *  The e_y basis vector is determined by the other vectors and the given 
     *  handedness.
     *
     *  \param obs_u four-velocity of the observer corresponding to this frame, 
     *   becomes the timelike basis vector of the frame.
     *
     *  \param approximate_z approximate direction of the z-axis, the \c e_z 
     *   basis vector is along the part orthogonal to obs_u
     *
     *  \param approximate_x approximate direction of the x-axis.
     *
     *  \param h handedness of the basis.
     */
    LorentzFrame(const TangentVector<MetricSpaceType> &obs_u,
                 const TangentVector<MetricSpaceType> &approximate_z,
                 const TangentVector<MetricSpaceType> &approximate_x,
                 const Handedness &h)
    : // These operations also check that everything is at the same point
      e_t_(normalized(obs_u)),

      // e_z_(normalized(project_orthogonal(obs_u, approximate_z))),
      //XXX: for consistency, should be projected orthogonal to e_t
      e_z_(normalized(project_orthogonal(e_t_, approximate_z))),

      e_x_(normalized(
          project_orthogonal(e_z_, project_orthogonal(e_t_, approximate_x)))),

      //e_y_(normalized(spatial_cross_product(obs_u, e_z_, e_x_))) {
      e_y_(normalized(
                  (h == Handedness::LEFT_HANDED ? -1.0 : 1.0) *
                  spatial_cross_product(obs_u, e_z_, e_x_))) {

          // Check that the given directions produced a sensible frame
          // The most obvious failure mode is the directions being degenerate,
          // which would produce a null basis vector
          // TODO: is_valid_frame accepts a tolerance parameter. Should
          // be user-configurable, to depend on numerical tolerances
          // used elsewhere.
          if (!is_valid_frame()) {
              std::stringstream ss;
              ss << "LorentzFrame: The given directions produced an invalid "
                    "frame."
                 << " Computed basis vectors:\n"
                 << "e_t:" << e_t_ << "\ne_x:" << e_x_ << "\ne_y:" << e_y_
                 << "\ne_z:" << e_z_;
              throw std::runtime_error(ss.str());
          }
    }

    /** \brief Construct a right-handed basis from vector components.
     *
     *  \param point the point where this frame is defined.
     *
     *  \param chart the coordinate chart where the vector components are given.
     *
     *  \param obs_u four-velocity of the observer corresponding to this frame, 
     *   becomes the timelike basis vector of the frame.
     *
     *  \param approximate_z approximate direction of the z-axis, the \c e_z 
     *   basis vector is along the part orthogonal to obs_u
     *
     *  \param approximate_x approximate direction of the x-axis.
     *
     *
     */
    LorentzFrame(const ManifoldPoint<MetricSpaceType> &point,
                 const Chart<MetricSpaceType> &chart,
                 std::initializer_list<double> obs_u,
                 std::initializer_list<double> approximate_z,
                 std::initializer_list<double> approximate_x)
    : LorentzFrame({point, chart, obs_u}, {point, chart, approximate_z},
                   {point, chart, approximate_x}) {}

    /** \brief Construct a right-handed basis from vector components.
     *
     *  \param point the point where this frame is defined.
     *
     *  \param chart the coordinate chart where the vector components are given.
     *
     *  \param obs_u four-velocity of the observer corresponding to this frame, 
     *   becomes the timelike basis vector of the frame.
     *
     *  \param approximate_z approximate direction of the z-axis, the \c e_z 
     *   basis vector is along the part orthogonal to obs_u
     *
     *  \param approximate_x approximate direction of the x-axis.
     *
     *
     */
    LorentzFrame(const ManifoldPoint<MetricSpaceType> &point,
                 const Chart<MetricSpaceType> &chart,
                 const EigenRk1Tensor<4> &obs_u,
                 const EigenRk1Tensor<4> &approximate_z,
                 const EigenRk1Tensor<4> &approximate_x)
    : LorentzFrame({point, chart, obs_u}, {point, chart, approximate_z},
                   {point, chart, approximate_x}) {}

    /** \brief Get a lightlike vector with a given spatial part.
     *
     *  Get a lightlike or null TangentVector which has the given spatial 
     *  direction as seen in this frame.
     *
     *  The vector is normalized so that its component along \c e_t is 1.
     *
     *  \param components the components of a vector defining the direction in 
     *      this frame. 
     *      The first timelike component is ignored.
     *
     *  \return A lightlike TangentVector which has the same spatial direction 
     *   as the given vector.
     *
     */
    TangentVector<MetricSpaceType> make_lightlike(
        const EigenRk1Tensor<4> &components) const {
        const double x = components(1);
        const double y = components(2);
        const double z = components(3);
        const double sq = std::sqrt(x * x + y * y + z * z);
        // normalize t component to 1
        EigenRk1Tensor<4> new_components = components / sq;
        new_components(0) = 1;
        return {*this, new_components};
    }

    /** \copybrief make_lightlike
     *
     *  \copydetails make_lightlike
     *
     *
     */
    TangentVector<MetricSpaceType> make_lightlike(
        std::initializer_list<double> components) const {
        EigenRk1Tensor<4> comp;
        comp.setValues(components);
        return make_lightlike(comp);
    }

    /** \brief Get a lightlike vector with a given spatial part.
     *
     *  Get a lightlike or null TangentVector which has the given spatial 
     *  direction as seen in this frame.
     *
     *  The vector is normalized so that its component along \c e_t is 1.
     *
     *  \param v vector defining the spatial direction of the lightlike vector.
     *      Only the part orthogonal to \c e_t is significant.
     *
     *  \return A lightlike TangentVector which has the same spatial direction 
     *   as the given vector.
     *
     */
    TangentVector<MetricSpaceType> make_lightlike(
        const TangentVector<MetricSpaceType> &v) const {
        return make_lightlike(v.components(*this));
    }

    /** \brief Get an ordered list of the basis vectors.
     */
    const std::vector<TangentVector<MetricSpaceType>,
                      aligned_allocator<TangentVector<MetricSpaceType>>>
    basis() const {
        return {e_t_, e_x_, e_y_, e_z_};
    }

    const TangentVector<MetricSpaceType> &e_t() const { return e_t_; }
    ///< \brief Get the timelike basis vector along the t axis.

    const TangentVector<MetricSpaceType> &e_x() const { return e_x_; }
    ///< \brief Get the spacelike basis vector along the x axis.

    const TangentVector<MetricSpaceType> &e_y() const { return e_y_; }
    ///< \brief Get the spacelike basis vector along the y axis.

    const TangentVector<MetricSpaceType> &e_z() const { return e_z_; }
    ///< \brief Get the spacelike basis vector along the z axis.

    /** \brief Get a transformation matrix to this frame.
     * 
     *  Get the matrix that gives the transformation of tangent vector 
     *  components from the given coordinate chart to this basis.
     *
     *  \param chart a coordinate chart.
     *
     */
    ArcMatrix<4> transformation_to_basis(
        const Chart<MetricSpaceType> &chart) const {
        return transformation_from_basis(chart).inverse();
    }

    /** \brief Get a transformation matrix from this frame.
     * 
     *  Get the matrix that gives the transformation of tangent vector 
     *  components to the given coordinate chart from this basis.
     *
     *  \param chart a coordinate chart.
     *
     */
    ArcMatrix<4> transformation_from_basis(
        const Chart<MetricSpaceType> &chart) const {
        // The matrix is J^a_(b) = (e_(b))^a, with e_(b) = e_t etc.
        ArcMatrix<4> mat;
        using CT = typename TangentVector<MetricSpaceType>::ContainerType;
        CT e_tc = e_t_.components(chart);
        CT e_xc = e_x_.components(chart);
        CT e_yc = e_y_.components(chart);
        CT e_zc = e_z_.components(chart);

        // clang-format off
        mat << e_tc(0), e_xc(0), e_yc(0), e_zc(0),
               e_tc(1), e_xc(1), e_yc(1), e_zc(1),
               e_tc(2), e_xc(2), e_yc(2), e_zc(2),
               e_tc(3), e_xc(3), e_yc(3), e_zc(3);
        // clang-format on

        return mat;
    }

    //
    // Definitions to satisfy AffinePoint ParallelTransportType requirements.
    // Of course many of these are useful otherwise also.
    static const size_t vector_size =
        TangentVector<MetricSpaceType>::component_count;
    static const size_t component_count = 4 * vector_size;
    using ContainerType =
        Eigen::TensorFixedSize<double, Eigen::Sizes<component_count>>;

    LorentzFrame(const typename MetricSpaceType::PointType &point,
                 const typename MetricSpaceType::ChartType &chart,
                 const ContainerType &components)
    : e_t_(point, chart,
           components.slice(Eigen::array<size_t, 1>{0},
                            Eigen::array<size_t, 1>{vector_size})),
      e_z_(point, chart,
           components.slice(Eigen::array<size_t, 1>{3 * vector_size},
                            Eigen::array<size_t, 1>{vector_size})),
      e_x_(point, chart,
           components.slice(Eigen::array<size_t, 1>{vector_size},
                            Eigen::array<size_t, 1>{vector_size})),
      e_y_(point, chart,
           components.slice(Eigen::array<size_t, 1>{2 * vector_size},
                            Eigen::array<size_t, 1>{vector_size})) {}

    ContainerType components(const Chart<MetricSpaceType> &chart) const {

        ContainerType ret;
        ret.slice(Eigen::array<size_t, 1>{0},
                  Eigen::array<size_t, 1>{vector_size}) =
            e_t_.components(chart);

        ret.slice(Eigen::array<size_t, 1>{vector_size},
                  Eigen::array<size_t, 1>{vector_size}) =
            e_x_.components(chart);

        ret.slice(Eigen::array<size_t, 1>{2 * vector_size},
                  Eigen::array<size_t, 1>{vector_size}) =
            e_y_.components(chart);

        ret.slice(Eigen::array<size_t, 1>{3 * vector_size},
                  Eigen::array<size_t, 1>{vector_size}) =
            e_z_.components(chart);

        return ret;
    }

    const ManifoldPoint<MetricSpaceType> &point() const { return e_t_.point();}
    ///< Get the point where this frame is defined.

    const MetricSpaceType &M() const { return e_t_.M(); }
    ///< Get the manifold where this frame is defined.

    ContainerType transport_derivatives(
        const Chart<MetricSpaceType> &chart,
        const EigenRk2Tensor<4> &contracted_Gamma) const {
        ContainerType ret;
        ret.slice(Eigen::array<size_t, 1>{0},
                  Eigen::array<size_t, 1>{vector_size}) =
            e_t_.transport_derivatives(chart, contracted_Gamma);

        ret.slice(Eigen::array<size_t, 1>{vector_size},
                  Eigen::array<size_t, 1>{vector_size}) =
            e_x_.transport_derivatives(chart, contracted_Gamma);

        ret.slice(Eigen::array<size_t, 1>{2 * vector_size},
                  Eigen::array<size_t, 1>{vector_size}) =
            e_y_.transport_derivatives(chart, contracted_Gamma);

        ret.slice(Eigen::array<size_t, 1>{3 * vector_size},
                  Eigen::array<size_t, 1>{vector_size}) =
            e_z_.transport_derivatives(chart, contracted_Gamma);

        return ret;
    }

    // The frame vectors should be orthonormal. The class needs to
    // enforce this
    bool is_valid_frame(double tolerance = 1e-10) const {

        constexpr int tl_sign = MetricSpaceType::Signature::timelike_sign;

        // if any vector is NaN, the NaN gets propagated through +- and
        // abs, and the comparison is false
        return
            fabs(dot_product(e_t_, e_t_) - tl_sign) <= tolerance &&
            fabs(dot_product(e_x_, e_x_) + tl_sign) <= tolerance &&
            fabs(dot_product(e_y_, e_y_) + tl_sign) <= tolerance &&
            fabs(dot_product(e_z_, e_z_) + tl_sign) <= tolerance;
    }

    friend std::ostream &operator<<(std::ostream &os,
                                    const LorentzFrame<MetricSpaceType> &f) {
        os << "Local Lorentz frame at " << f.point() << "\n";
        os << "Basis vectors:" << "\n";
        os << "e_t:" << "\n"
           << f.e_t_ << "\n"
           << "e_x:" << "\n"
           << f.e_x_ << "\n"
           << "e_y:" << "\n"
           << f.e_y_ << "\n"
           << "e_z:" << "\n"
           << f.e_z_ << "\n";
        return os;
    }

private:
    // XXX: Declaration order needs to match the order the members are used in
    // the constructor initialization list, since they depend on each other
    // there. Otherwise unitialized vectors are be passed to operations,
    // as according to the standard the initialization order matches declaration
    // order.
    TangentVector<MetricSpaceType> e_t_, e_z_, e_x_, e_y_;
};

/** \brief A Fermi-Walker transported \c LorentzFrame.
 *
 *  Otherwise just like a \c LorentzFrame, except that the frame is
 *  transported along curves according to Fermi-Walker transport instead of
 *  parallel transport.
 *  As a result, the components of the curve tangent stay constant with respect
 *  to the frame even for accelerated curves with a normalized timelike tangent.
 *
 *  \tparam MetricSpaceType the type of the underlying manifold.
 *  \ingroup geometry
 *  \ingroup overaligned
 */
template<typename MetricSpaceType>
class LorentzFrameFW : public LorentzFrame<MetricSpaceType> {
public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW
    using LorentzFrame<MetricSpaceType>::LorentzFrame;

    using ContainerType = typename LorentzFrame<MetricSpaceType>::ContainerType;
    
    ContainerType transport_derivatives(
        const Chart<MetricSpaceType> &chart,
        const EigenRk2Tensor<4> &contracted_Gamma,
        const TangentVector<MetricSpaceType> &tangent,
        const TangentVector<MetricSpaceType> &acceleration) const {

        ContainerType rhs;

        auto tangent_comp = tangent.components(chart);
        auto accel_comp = acceleration.components(chart);

        // The dot products are given upto a sign by the components in this
        // frame, with the spatial components getting an extra minus.
        // This also accounts for the metric signature.
        auto tangent_dots = tangent.components(*this);
        auto accel_dots = acceleration.components(*this);

        constexpr auto vector_size = LorentzFrame<MetricSpaceType>::vector_size;

        rhs.slice(Eigen::array<size_t, 1>{0},
                  Eigen::array<size_t, 1>{vector_size}) =
            accel_comp * tangent_dots(0) - tangent_comp * accel_dots(0);

        rhs.slice(Eigen::array<size_t, 1>{vector_size},
                  Eigen::array<size_t, 1>{vector_size}) =
            -accel_comp * tangent_dots(1) + tangent_comp * accel_dots(1);

        rhs.slice(Eigen::array<size_t, 1>{2 * vector_size},
                  Eigen::array<size_t, 1>{vector_size}) =
            -accel_comp * tangent_dots(2) + tangent_comp * accel_dots(2);

        rhs.slice(Eigen::array<size_t, 1>{3 * vector_size},
                  Eigen::array<size_t, 1>{vector_size}) =
            -accel_comp * tangent_dots(3) + tangent_comp * accel_dots(3);

        ContainerType ret =
            rhs + LorentzFrame<MetricSpaceType>::transport_derivatives(
                      chart, contracted_Gamma);
        return ret;
    }
    

    friend std::ostream &operator<<(std::ostream &os,
                                    const LorentzFrameFW<MetricSpaceType> &f) {
        os << "Fermi-Walker transported local Lorentz frame at " 
        << f.point() << "\n";
        os << "Basis vectors:" << "\n";
        os << "e_t:" << "\n"
           << f.e_t() << "\n"
           << "e_x:" << "\n"
           << f.e_x() << "\n"
           << "e_y:" << "\n"
           << f.e_y() << "\n"
           << "e_z:" << "\n"
           << f.e_z() << "\n";
        return os;
    }
    
};

} // namespace arcmancer
