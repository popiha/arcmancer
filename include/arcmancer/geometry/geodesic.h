#pragma once

#include "arcmancer/geometry/parametrized_curve.h"
#include "arcmancer/geometry/parametrized_point.h"
#include "arcmancer/geometry/types.h"

namespace arcmancer {

/** \brief A geodesic curve.
 *
 * A thin wrapper around \c ParametrizedCurve, with constructors accepting 
 * \c force_function hidden.
 *
 * Also implements some additional convenience methods.
 *
 * \tparam MetricSpaceType the type of the manifold this geodesic is defined on.
 *
 * \tparam TransportTypes 0 or 1 types to transport along the curve.
 * Refer to ParametrizedPoint documentation for the requirements on these types.
 *
 * \see ParametrizedPoint, ParametrizedCurve
 *
 * \ingroup geometry
 */
template <typename MetricSpaceType, typename... TransportTypes>
class Geodesic : public ParametrizedCurve<MetricSpaceType, TransportTypes...> {

public:
    using PointType = typename ParametrizedCurve<MetricSpaceType,
                                                 TransportTypes...>::PointType;
    /** \brief Construct from a point on the curve.
     *
     * \param initial_point a point on the curve.
     */
    Geodesic(const PointType &initial_point)
    : ParametrizedCurve<MetricSpaceType, TransportTypes...>(initial_point) {
        likeness_ = vector_likeness(initial_point.tangent());
    }

    /** \brief Construct from the initial tangent vector and transported object.
     *
     * The \c curve_parameter of the initial point is 0.
     *
     * \param tangent the tangent vector of the curve.
     * \param transported the object transported along the curve, if applicable.
     */
    Geodesic(const TangentVector<MetricSpaceType> &tangent,
             const TransportTypes &... transported)
    : Geodesic(PointType(0, tangent, transported...)) {}

    VectorLikeness likeness() const { return likeness_; }
    ///< The likeness of the initial tangent vector.

private:
    VectorLikeness likeness_;
};

} // namespace arcmancer
