#pragma once

// Metaprogramming tools to mainly work with templates of the form
//  template<typename... T>
//  struct TypeList {};
// The resulting types of the metafunctions are generally stored
// in a member ::type

// Note that template<template<typename> typename> syntax is a c++17 extension,
// so the c++14 compliant template<template<typename> class> is used instead.
// Altough the extension is supported by the major compilers in c++14 mode,
// it seems preferable to try to remain as standard compliant as possible.

//Also bring in library metaprogramming tools here, so only meta.h needs to be
//included
#include <type_traits>
#include <boost/mpl/vector.hpp>
#include <boost/mpl/for_each.hpp>
#include <boost/mpl/range_c.hpp>
#include <boost/mpl/transform.hpp>
#include <boost/mpl/void.hpp>
#include <boost/mpl/has_xxx.hpp>

#include "arcmancer/geometry/types.h" //since some helpers need Cov/Cnt

namespace arcmancer{

namespace mpl = boost::mpl;

namespace internal{

//A simple container for types
template<typename... T>
struct TypeList;

// Rename the container of a typelist from L<list...> to L2<list...>
template<typename L, template<typename...> class L2>
struct tl_rename;

template<template<typename...> class L, template<typename...> class L2,
         typename... list>
struct tl_rename<L<list...>, L2>{
    using type = L2<list...>;
};


//Wrap a value into a type to store it in a type list
template<typename T, T val>
struct value_container{
    using type = T;
    static const T value = val;
};

//Get element from type list, result accessed with ::type
template<size_t index, typename List>
struct tl_get;

template<size_t k, template<typename...> class L,
         typename head, typename... tail>
struct tl_get<k, L<head, tail...>> {
    static_assert(k<=sizeof...(tail), "index out of bounds");
    using type = typename tl_get<k-1, L<tail...>>::type;};

template<template<typename...> class L, typename head, typename... tail>
struct tl_get<0, L<head, tail...>>
{ using type = head;};

//Add element to the beginning of a type list
template<typename T, typename List>
struct tl_cons;

template<typename T, template<typename...> class L, typename... list>
struct tl_cons<T,L<list...>>{using type = L<T, list...>;};


//Set element of a type list, resulting list accessed with ::type
template<size_t index, typename T, typename L>
struct tl_set;

template<size_t k, typename T, template<typename...> class L,
         typename head, typename... tail>
struct tl_set<k, T, L<head,tail...>> {
    static_assert(k<=sizeof...(tail), "index out of bounds");
    using type = typename tl_cons<head, typename tl_set<k-1, T, L<tail...> >::type >::type;};

template<typename T, template<typename...> class L,
         typename head, typename... tail>
struct tl_set<0, T, L<head,tail...>>
{ using type = L<T,tail...>;};


//Remove element from type list, resulting list accessed with ::type
template<size_t index, typename L>
struct tl_remove;

template<size_t k, template<typename...> class L,
         typename head, typename... tail>
struct tl_remove<k, L<head,tail...>> {
    static_assert(k<=sizeof...(tail), "index out of bounds");
    using type = typename tl_cons<head, typename tl_remove<k-1, L<tail...> >::type >::type;
};



template<template<typename...> class L,
         typename head, typename... tail>
struct tl_remove<0, L<head,tail...>>
{ using type = L<tail...>;};



//Insert element to a type list after the element at index.
//Use tl_cons to add elements to the beginning of the list
template<size_t index, typename T, typename L>
struct tl_insert;

template<size_t k, typename T, template<typename...> class L,
         typename head, typename... tail>
struct tl_insert<k, T, L<head,tail...>> {
    static_assert(k<=sizeof...(tail), "index out of bounds");
    using type = typename tl_cons<head, typename tl_insert<k-1, T, L<tail...> >::type >::type;};

template<typename T, template<typename...> class L,
         typename head, typename... tail>
struct tl_insert<0, T, L<head,tail...>>
{ using type = L<head, T,tail...>;};




//Extract the values of the value_containers in list L.
//All the values need to be of type T
//An object of the resulting type needs to be instantiated to access the values,
//trying to use a static array caused some weird linker errors.
template<typename T, typename L>
struct tl_extract_values;

template<typename T, template<typename...> class L, typename... list>
struct tl_extract_values<T, L<list...>>{
    static const size_t size = sizeof...(list);
    T values[size] = {list::value...};
};


//Apply a metafunction F to the elements of a typelist L
//F should give the result in a member type
template<template<typename> class F, typename L>
struct tl_map;

template<template<typename> class F, template<typename...> class L,
         typename... list>
struct tl_map<F ,L<list...>> {
    using type = L<typename F<list>::type...>;
};

//Generate a list TypeList<value_container<T,val>...> containing k values
template<size_t k, typename T, T val>
struct tl_repeat_val{
    using type = typename tl_cons<value_container<T, val>, typename tl_repeat_val<k-1, T, val>::type>::type;
};

template<typename T, T val>
struct tl_repeat_val<1,T, val>{
    using type = TypeList<value_container<T,val>>;
};

//replicating std::make_index_sequence to get values wrapped in containers
template<size_t k, size_t N>
struct make_index_seq_impl{
    using type = typename tl_cons<value_container<size_t, N-k>,
                                  typename make_index_seq_impl<k-1, N>::type
                                 >::type;
};

template<size_t N>
struct make_index_seq_impl<0,N>{
    using type = TypeList<>;
};

template<size_t N>
struct make_index_seq{
    using type = typename make_index_seq_impl<N,N>::type;
};

//Helper for sizes_helper
template<typename L>
struct sizes_helper_impl;

template<typename... list>
struct sizes_helper_impl<TypeList<list...>>{
    using sizes = Eigen::Sizes<list::value...>;
};

template<size_t k, long D>
struct sizes_helper {
    using sizes = typename sizes_helper_impl<typename tl_repeat_val<k, long, D
                                            >::type>::sizes;
};

//special case
template<long D>
struct sizes_helper<0, D>{
    using sizes = Eigen::Sizes<>;
};

//Check that the indices are either Cov or Cnt
template<typename L>
struct all_Cov_or_Cnt_impl;

template<template<typename...> class L,
         typename head, typename... tail>
struct all_Cov_or_Cnt_impl<L<head,tail...>>{
    static const bool value =
        (std::is_same<Cnt,head>::value || std::is_same<Cov,head>::value)
        && all_Cov_or_Cnt_impl<L<tail...>>::value;
};

template<template<typename...> class L>
struct all_Cov_or_Cnt_impl<L<>>{
    static const bool value = true;
};

template<typename... Indices>
struct all_Cov_or_Cnt {
    static const bool value =
        all_Cov_or_Cnt_impl<TypeList<Indices...>>::value;
};

//Generate the index shuffle needed for raising and lowering indices.
//Assumes the metric is first in the contraction giving g_ab T^cdea... = T_b^cde...
//which needs to be reordered to match the conventional result T^cde_b...
template<typename L>
struct raise_lower_shuffle_impl;

template<template<typename...> class L, typename head, typename... list>
struct raise_lower_shuffle_impl<L<head,list...>>{
    //Ignoring head will drop the leading 0 from the index_seq,
    //which is needed to allow using insert for all index values
    //when generating the list in raise_lower_shuffle
    //XXX: for some reason making this static causes a linker error
    //so an actual object needs to be instantiated to access the result.
    Eigen::array<size_t, sizeof...(list)> shuffle{ {list::value...}};
};

template<size_t index, size_t total_indices>
struct raise_lower_shuffle
        : raise_lower_shuffle_impl
          <typename
            tl_insert<index, value_container<size_t, 0>,
                    typename make_index_seq<total_indices>::type
                  >::type
          >{};

//
// Functionality for transforming a sequence of Index types to a
// sequence of integers, with negative <=> covariant, positive <=>
// contravariant.

struct index_sign_transformer{
    template<typename IndexType, typename IndexPos>
    struct apply;
};

template<int i>
struct index_sign_transformer::apply<Cov, mpl::integral_c<int,i>>{
    using type = mpl::integral_c<int,-i>;
};

template<int i>
struct index_sign_transformer::apply<Cnt, mpl::integral_c<int,i>>{
    using type = mpl::integral_c<int,i>;
};

template <typename... Indices>
struct index_int_sequence {
    using type = typename mpl::transform<mpl::vector<Indices...>,
                                     mpl::range_c<int,1,sizeof...(Indices)+1>,
                                     internal::index_sign_transformer
                                     >::type;
};

//Generate the right amount of nested initializer lists to hold all the components
//of a tensor
template<size_t rank>
struct nested_double_init_list{
    using type = std::initializer_list<typename nested_double_init_list<rank-1>::type>;
};

template<>
struct nested_double_init_list<1>{
    using type = std::initializer_list<double>;
};

//rank-0 needs a special case
template<>
struct nested_double_init_list<0>{
    using type = double;
};

template<typename IndexType, typename...Indices>
struct all_indices_same_type;

template<typename IndexType, typename head, typename... tail>
struct all_indices_same_type<IndexType, head, tail...>{
    static constexpr bool value = std::is_same<IndexType, head>::value
                           && all_indices_same_type<IndexType, tail...>::value;
};

template<typename IndexType>
struct all_indices_same_type<IndexType>{
    static constexpr bool value = true;
};

BOOST_MPL_HAS_XXX_TRAIT_NAMED_DEF(has_extra_data_type , ExtraDataType, false)

//Get the type in member typedef ExtraDataType.
//Returns mpl::void_ if member typedef doesn't exist.
template<typename T, bool has_extra = has_extra_data_type<T>::value>
struct get_extra_data_type{
    using type = mpl::void_;
};

template<typename T>
struct get_extra_data_type<T, true>{
    using type = typename T::ExtraDataType;
};

// Check whether the member ::type exists
BOOST_MPL_HAS_XXX_TRAIT_NAMED_DEF(has_member_type, type, false)

}} // namespace internal namespace arcmancer
