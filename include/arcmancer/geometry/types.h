#pragma once

#include <functional>
#include <initializer_list>
#include <ostream>
#include <stdexcept>
#include <vector>

#include <unsupported/Eigen/CXX11/Tensor>
#include <Eigen/Dense>
#include <Eigen/StdVector>


// XXX: need to have a documentation string for the namespace in one place.
// This allows doxygen to extract documented namespace-scope functions.
/** \brief The main namespace of the library.
 *
 */
namespace arcmancer {

/** \brief STL compatible aligned allocator
 *
 * This is just a convenience alias for the Eigen allocator, see the
 * [Eigen documentation](https://eigen.tuxfamily.org/dox/classEigen_1_1aligned__allocator.html) 
 * for more details.
 *
 * \see aligned_vector
 *
 * \ingroup utils
 */
template<typename T>
using aligned_allocator = Eigen::aligned_allocator<T>;

/** \brief Shorthand for an aligned vector container
 *
 *\ingroup utils
 */
template <typename T>
using aligned_vector = std::vector<T, aligned_allocator<T>>;

//Most of the code works with metric spaces of arbitrary dimension
//D is used as the name of the dimension count template param


//Some commonly used Eigen::Tensor typedefs
//XXX: Eigen tensors use long internally, so we need to use it also to allow 
//template param deduction 

/** \brief Convenience definition for low rank tensor components.
 *
 *  Rank 1 tensor components in D dimensional space.
 *
 *  \tparam D dimension of the tensor.
 *  \ingroup geometry
 * \ingroup overaligned
 */
template<long D>
using EigenRk1Tensor 
    = Eigen::TensorFixedSize<double, Eigen::Sizes<D>>;

/** \copybrief EigenRk1Tensor 
 *  
 *  Rank 2 tensor components in D dimensional space.
 *
 *  \tparam D dimension of the tensor.
 *  \ingroup geometry
 * \ingroup overaligned
 */
template<long D>
using EigenRk2Tensor 
    = Eigen::TensorFixedSize<double, Eigen::Sizes<D, D>>;

/** \copybrief EigenRk1Tensor 
 * 
 *  Rank 3 tensor components in D dimensional space.
 *
 *  \tparam D dimension of the tensor.
 *  \ingroup geometry
 * \ingroup overaligned
 */
template<long D>
using EigenRk3Tensor 
    = Eigen::TensorFixedSize<double, Eigen::Sizes<D, D, D>>;

/** \copybrief EigenRk1Tensor 
 *
 *  Rank 4 tensor components in D dimensional space.
 *
 *  \tparam D dimension of the tensor.
 *  \ingroup geometry
 * \ingroup overaligned
 */
template<long D>
using EigenRk4Tensor 
    = Eigen::TensorFixedSize<double, Eigen::Sizes<D, D, D, D>>;

//For when matrix inverse etc. is needed
//XXX: and apparently matrices use int

/** \brief A D-by-D square matrix.
 *
 * \tparam D dimension of the matrix.
 *  \ingroup geometry
 * \ingroup overaligned
 */
template<int D>
using ArcMatrix    = Eigen::Matrix<double, D, D>;

/** \brief A D element column vector.
 *
 *  \tparam D number of components
 *  \ingroup geometry
 * \ingroup overaligned
 */
template<int D>
using ArcVector    = Eigen::Matrix<double, D, 1>;

/** \brief A D element row vector.
 *
 *  \tparam D number of components
 *  \ingroup geometry
 * \ingroup overaligned
 */
template<int D>
using ArcRowVector = Eigen::Matrix<double, 1, D>;

/** \brief For indexing Eigen::Tensor contractions
 *  \ingroup geometry
 * \ingroup overaligned
 */
using IndexPair    = Eigen::IndexPair<size_t>;

// Define basic types 


/** \brief A coordinate chart.
 * 
 *  A \c Chart gives meaning to the coordinates of a point and the components
 *  of tensors.
 *  These objects act as tags identifying the different coordinate systems
 *  defined on a manifold, and contain some basic information associated with
 *  the coordinate system.
 *  
 *  `Chart`s are instantiated in the `MetricSpace` objects, 
 *  and hold a reference to the object itself, as this makes explicitly
 *  passing the MetricSpace instance to tensors and other objects unnecessary.
 *
 *  Note that `Chart`s cannot be copied, as the identity of the object is
 *  significant.
 *  This means that `Chart` objects need to be passed as either references or
 *  pointers.
 *  
 *  \tparam MetricSpaceType the type of the manifold this chart is associated 
 *  with.
 *
 *  \see MetricSpace
 *  \ingroup geometry
 */
template<typename MetricSpaceType>
struct Chart {

    const MetricSpaceType &M; ///< The associated MetricSpace instance.

    const std::string info; ///< A description of the chart.

    Chart(const MetricSpaceType &M, const std::string &info)
    : M(M), info(info){}

    // prevent taking copies of a Chart, so that using Chart* as keys is safer
    Chart(Chart const&) = delete;
    Chart& operator=(Chart const&) = delete;

    friend std::ostream& operator<<(std::ostream& os,
                                    const Chart<MetricSpaceType> &chart) {
        os << chart.info;
        return os;
    }
};

//
// Tensor types

// Covariant and contravariant indices, with string representations

/** \brief Represents a covariant tensor index.
 *
 *  \see Tensor
 *  \ingroup geometry
 */
struct Cov {
    static constexpr const char* repr = "Cov";
};

/** \brief Represents a contravariant tensor index.
 *
 *  \see Tensor
 *  \ingroup geometry
 */
struct Cnt {
    static constexpr const char* repr = "Cnt";
};

// Forward declare Tensor, definition in tensor.h
template<typename MetricSpaceType, typename... Indices>
class Tensor;

// Convenience synonyms for certain tensors

/** \brief Convenience alias for tangent vectors.
 *
 *  \see Tensor
 *  \ingroup geometry
 * \ingroup overaligned
 */
template<typename MetricSpaceType>
using TangentVector = Tensor<MetricSpaceType, Cnt>;

/** \brief Convenience alias for cotangent vectors.
 *
 *  \see Tensor
 *  \ingroup geometry
 * \ingroup overaligned
 */
template<typename MetricSpaceType>
using CotangentVector = Tensor<MetricSpaceType, Cov>;


/** \brief Represents the likeness of a vector.
 *
 *  A tangent or cotangent vector can be timelike, spacelike or lightlike 
 *  (null).
 *  In the +--- signature convention a vector \f$ v^\mu \f$ is said to be
 *  * Timelike if \f$ v^\mu v_\mu > 0 \f$,
 *  * Spacelike if \f$ v^\mu v_\mu < 0 \f$,
 *  * Lightlike if \f$ v^\mu v_\mu = 0 \f$.
 *  For the -+++ convention the signs are reversed.
 *
 *  On manifolds that aren't Lorenzian, the likeness is undefined.
 *
 *  \see vector_likeness
 *  \ingroup geometry
 */
enum class VectorLikeness {Lightlike, Timelike, Spacelike, Undefined};

inline std::ostream& operator<<(std::ostream &os, VectorLikeness vl){
    if(vl == VectorLikeness::Lightlike){
        os<<"Lightlike";
    }
    if(vl == VectorLikeness::Timelike){
        os<<"Timelike";
    }
    if(vl == VectorLikeness::Spacelike){
        os<<"Spacelike";
    }
    if(vl == VectorLikeness::Undefined){
        os<<"(Undefined likeness)";
    }
    return os;
}

//Forward declaration
template<class MetricSpaceType>
class LorentzFrame;

/** \brief A point on a manifold.
 *
 *  These objects describe points on a manifold using any of the defined
 *  coordinate systems.
 *  The coordinates used to represent the point are abstracted away, and it is
 *  possible to retrieve the coordinates in any of the defined coordinate 
 *  systems, with the coordinate conversion being handled automatically.
 *
 *  Points can be compared to each other for exact equality,
 *  but due to numerical reasons two mathematically equivalent points defined
 *  using two different coordinate charts will likely compare as unequal.
 *
 *  \tparam MetricSpaceType the type of the manifold this point is located in.
 *
 *  \see MetricSpace
 *  \ingroup geometry
 * \ingroup overaligned
 */
template <typename MetricSpaceType>
class ManifoldPoint {
public:
    using ChartType = Chart<MetricSpaceType>;
    static constexpr size_t component_count = MetricSpaceType::D;

private:
    const MetricSpaceType *M_;
    const Chart<MetricSpaceType> *chart_;
    EigenRk1Tensor<MetricSpaceType::D> coordinates_;

public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW

    /** \brief Construct a point.
     * 
     *  \param chart the chart used for the coordinates.
     *  \param coordinates the coordinates of the point. 
     */
    ManifoldPoint(const ChartType &chart,
                  const EigenRk1Tensor<MetricSpaceType::D> &coordinates)
    : M_(&chart.M), chart_(&chart), coordinates_(coordinates) {}

    /** \brief Construct a point.
     * 
     *  \param chart the chart used for the coordinates.
     *  \param coordinates the coordinates of the point. 
     */
    ManifoldPoint(const ChartType &chart, 
                  std::initializer_list<double> coordinates)
    : M_(&chart.M), chart_(&chart) {
        coordinates_.setValues(coordinates);
    }

    /** \brief Get the coordinates of the point.
     *
     *  \param out_chart a coordinate chart on the same manifold as the point.
     *
     *  \return The coordinates of the point in the chart \p out_chart.
     */
    EigenRk1Tensor<MetricSpaceType::D> 
    coordinates(const ChartType &out_chart) const {
        // just return if the coordinates are stored in the requested chart 
        if (chart_ == &out_chart){
            return coordinates_;
        }
        // return the converted coordinates
        return M_->transition_function(*chart_, out_chart, coordinates_);
    }

    /** \brief Get the coordinates of the point in the chart used
     * internally.
     *
     * \warning Abstraction leak.
     *
     * \return The coordinates of the point in the chart currently used
     * internally by the point.
     */
    EigenRk1Tensor<MetricSpaceType::D>
    internal_coordinates() const {
        return coordinates_;
    }

    /** \brief Return a copy of the point using a different chart
     * internally.
     *
     * \param chart The chart to be used internally by the copy
     *
     * \return A copy of this ManifoldPoint using the given chart internally.
     */
    ManifoldPoint<MetricSpaceType> copy_with_chart(const ChartType &chart) const {
        return {chart, coordinates(chart)};
    }

    /** \brief Check if this point can be represented in the given chart.
     *
     *  \param chart a coordinate chart.
     *  \return Whether the coordinates of this point in \p chart are finite. 
     */
    bool on_chart(const Chart<MetricSpaceType>& chart) const {
        auto coords = coordinates(chart);

        //XXX: Just check that the point has finite coordinates, since we don't
        //have the valid limits defined in the charts
        for(size_t i=0; i<MetricSpaceType::D; ++i){
            if(!std::isfinite(coords(i))){
                return false;
            }
        }
        return true;
    }

    /** \brief Get the associated manifold.
     *
     *  \return The MetricSpaceType instance this point is associated with.
     */
    const MetricSpaceType& M() const {
        return *M_;
    }

    /** \brief Get the chart used internally.
     *
     *  \return the chart used internally for representing the point.
     */
    const Chart<MetricSpaceType>& chart() const {
        return *chart_;
    }

    /** \brief Construct a tangent vector "pointing" from this point to
     * another given point, using coordinate differences.
     *
     * \param Another point \a y on the manifold.
     * \return A contravariant tangent vector at this point, with
     * components derived from the coordinate differences \a y - \a point in the
     * chart internally used by this point.
     */
    const TangentVector<MetricSpaceType> difference_vector(ManifoldPoint<MetricSpaceType> other_point) const {
        EigenRk1Tensor<MetricSpaceType::D> dx = other_point.coordinates(chart()) - coordinates();
        TangentVector<MetricSpaceType> tangent(*this, chart(), dx);
        return tangent;
    }

    bool operator==(const ManifoldPoint<MetricSpaceType> &other) const{
        // throw if the two points are on different metric spaces, since
        // the user is probably doing something very wrong in this case
        if (M_ != other.M_){ 
            throw std::domain_error( 
                "Comparing ManifoldPoints on different metric spaces!");
        }
        // XXX: Two ways we can do this:
        // 1) demand bitwise exact same point
        // 2) demand same point within geodesic distance under given
        // numeric tolerance
        //
        // For now we are using no. 1.
        //XXX: Method 2 doesn't work for non-positive-definite metrics,
        //e.g. the points on the light cone are all at distance 0.

        const EigenRk1Tensor<MetricSpaceType::D> diff 
            = coordinates_ - other.coordinates(chart());
        
        bool ret = true;
        
        //Check that all the components are exactly the same
        for(size_t i = 0; ret && (i < MetricSpaceType::D); ++i) {
            ret = ret && diff(i) == 0;
        }
        return ret;
    }

    bool operator!=(const ManifoldPoint<MetricSpaceType> &other) const{
        return !(*this == other);
    }

    friend std::ostream& operator<<(std::ostream& os, 
                                    const ManifoldPoint<MetricSpaceType> &point) {
        const Eigen::Map<const ArcRowVector<MetricSpaceType::D>>
            components(point.coordinates_.data());
        
        os << "[" << point.M() << " / " << point.chart() << "]: " << components;
        return os;
    }
};

} // namespace arcmancer
