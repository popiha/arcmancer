#pragma once

#include <stdexcept>
#include <vector>

#include <Eigen/Dense>
#include <unsupported/Eigen/CXX11/Tensor>
#include <unsupported/Eigen/NonLinearOptimization>

#include "arcmancer/geometry/geodesic.h"
#include "arcmancer/geometry/utils.h"
#include "arcmancer/log.h"

namespace arcmancer {

namespace internal {

template <typename MetricSpaceType>
class RNCProblem {
public:
    using MST = MetricSpaceType;
    static constexpr long dim = MST::D;
    using Point = ManifoldPoint<MST>;

    RNCProblem(const Chart<MST> &chart, const Point &z0, const Point &z1)
    : _chart(chart), _z0(z0), _z1(z1){};

    //
    // Functions for Eigen nonlinear optimization
    int operator()(const Eigen::VectorXd &xin, Eigen::VectorXd &fvec) const {
        assert(xin.size() == dim && fvec.size() == dim);

        ArcVector<dim> x = xin;

        const TangentVector<MST> tangent{_z0, _chart, eigen_vector_to_tensor<dim>(x)};
        Geodesic<MST> gamma(tangent);

        gamma.configuration().propagation.tolerance =
            _z0.M().configuration.barycentric_interpolation.tolerance;

        auto res = gamma.compute(1);
        if (res != Geodesic<MST>::ComputationResult::success) {
            Log::log()->warn("RNCProblem: Geodesic computation unsuccessful: {}", res);
        }
        Point zhat = gamma.back().point();

        const EigenRk1Tensor<MST::D> diff =
            zhat.coordinates(_chart) - _z1.coordinates(_chart);
        fvec = eigen_tensor_to_vector(diff);

        //std::cout << "xin: " << xin.transpose() << "  => fvec: " << fvec.transpose() << "\n";

        return 0;
    }

private:
    const Chart<MST> &_chart;
    Point _z0;
    Point _z1;
};

} // namespace internal

/** \brief The method to use to compute Riemann normal coordinates.
 *
 * Riemann normal coordinates can be computed either by solving a full
 * numerical optimization, or using an explicit approximative formula.
 */
enum class NormalCoordinateMethod { 
    numerical, ///< Use numerical methods (more accurate)
    order_2    ///< Use 2nd order explicit formula (faster)
};

/** \brief Return the Riemann normal coordinates (RNC) of \c point, as
 * computed around point \c origin.
 * \tparam MetricSpaceType The type of manifold
 * \param chart Which chart are the RNC components given in
 * \param origin The origin point for the RNC
 * \param point The point for which the RNC are computed
 * \param method The \ref NormalCoordinateMethod to use for computing the
 * RNC.
 * \param rnc_guess An initial guess for the normal coordinates (in \c chart)
 * to speed up numerical convergence.
 * \return The RNC of \c point with respect to \c origin in \c chart
 * \ingroup geometry
 */
template <typename MetricSpaceType>
EigenRk1Tensor<MetricSpaceType::D> riemann_normal_coordinates(
    const Chart<MetricSpaceType> &chart,
    const ManifoldPoint<MetricSpaceType> &origin,
    const ManifoldPoint<MetricSpaceType> &point,
    const NormalCoordinateMethod &method,
    const EigenRk1Tensor<MetricSpaceType::D> &rnc_guess
    ) {
    using CoordinateType = EigenRk1Tensor<MetricSpaceType::D>;
    using Vector = ArcVector<MetricSpaceType::D>;
    using ProblemType = internal::RNCProblem<MetricSpaceType>;

    if (method == NormalCoordinateMethod::order_2) {
        // get the _coordinate_ difference vector between point and
        // origin, _in the coordinates of origin_
        const CoordinateType z0 = origin.coordinates(chart);
        const CoordinateType z1 = point.coordinates(chart);
        const CoordinateType dz = z1 - z0;

        // compute the guess using the 2nd order formula
        const CoordinateType correction =
            0.5 * origin.M().christoffel_symbols_twice_contracted(origin, chart, dz, dz);
        return dz + correction;
    }

    const CoordinateType x0 = rnc_guess;

    if (method == NormalCoordinateMethod::numerical) {
        // use x0 as initial values and try to shoot geodesics until we
        // hit the target
        Vector x = eigen_tensor_to_vector<MetricSpaceType::D>(x0);

        ProblemType problem(chart, origin, point);

        Eigen::HybridNonLinearSolver<ProblemType> solver(problem);

        // configure solver
        const auto &conf =
            point.M().configuration.normal_coordinates.optimization;

        solver.parameters.maxfev = conf.iterations;
        solver.parameters.xtol = conf.x_tol;

        Eigen::VectorXd xdyn(x.size());
        xdyn = x;
        auto status = solver.solveNumericalDiff(xdyn);
        x = xdyn;

        //std::cout << "status: " << status << "\nx:\n" << x << std::endl;
        Log::log()->debug("riemann_normal_coordinates: HybridNonLinearSolver status: {}\nfound RNC: {}",
                status, x.transpose());

        CoordinateType ret = eigen_vector_to_tensor<MetricSpaceType::D>(x);
        return ret;
    }

    // unknown method
    throw std::invalid_argument(
        "riemann_normal_coordinates: unsupported method");
}

/** \brief Return the Riemann normal coordinates (RNC) of \c point, as
 * computed around point \c origin.
 * \tparam MetricSpaceType The type of manifold
 * \param chart Which chart are the RNC components given in
 * \param origin The origin point for the RNC
 * \param point The point for which the RNC are computed
 * \param method The \ref NormalCoordinateMethod to use for computing the
 * RNC.
 * \return The RNC of \c point with respect to \c origin in \c chart
 * \ingroup geometry
 */
template <typename MetricSpaceType>
EigenRk1Tensor<MetricSpaceType::D> riemann_normal_coordinates(
    const Chart<MetricSpaceType> &chart,
    const ManifoldPoint<MetricSpaceType> &origin,
    const ManifoldPoint<MetricSpaceType> &point,
    const NormalCoordinateMethod &method) {
    using CoordinateType = EigenRk1Tensor<MetricSpaceType::D>;

    // get the _coordinate_ difference vector between point and
    // origin, _in the coordinates of origin_
    const CoordinateType z0 = origin.coordinates(chart);
    const CoordinateType z1 = point.coordinates(chart);
    const CoordinateType dz = z1 - z0;

    // compute the guess using the 2nd order formula
    const CoordinateType correction =
        0.5 * origin.M().christoffel_symbols_twice_contracted(origin, chart, dz, dz);
    const CoordinateType x0 = dz + correction;

    if (method == NormalCoordinateMethod::order_2) {
        return x0;
    }

    if (method == NormalCoordinateMethod::numerical) {
        return riemann_normal_coordinates(chart, origin, point, method, x0);
    }

    // unknown method
    throw std::invalid_argument(
        "riemann_normal_coordinates: unsupported method");
}

} // namespace arcmancer
