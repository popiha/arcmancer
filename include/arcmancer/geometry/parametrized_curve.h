#pragma once

#include <algorithm>
#include <array>
#include <deque>
#include <functional>
#include <stdexcept>
#include <string>
#include <tuple>
#include <utility>
#include <vector>

#include <boost/array.hpp>
#include <boost/numeric/odeint.hpp>
#include <boost/numeric/odeint/algebra/array_algebra.hpp>

#include <unsupported/Eigen/CXX11/Tensor>

#include "arcmancer/configuration.h"
#include "arcmancer/geometry/parametrized_point.h"
#include "arcmancer/geometry/surface.h"
#include "arcmancer/geometry/types.h"
#include "arcmancer/geometry/utils.h"
#include "arcmancer/interpolation/cubic_spline.h"
#include "arcmancer/log.h"

namespace arcmancer {

namespace odeint = boost::numeric::odeint;

/** \brief A generic parametrized curve.
 *
 * Computes and stores a sequence of ParametrizedPoints to represent a curve on
 * a manifold.
 *
 * The equation of motion of the curve is determined by an user specifiable 
 * callable \c force_function which maps the tangent vector of the curve to a 
 * vector giving the force applied to the curve.
 * The equation of motion of the curve is 
 * \f[
 *      u^\mu \nabla_\mu u^\nu = f^\nu, 
 * \f]
 * where \f$ u^\mu \f$ is the tangent vector of the curve and \f$ f^\nu \f$ is 
 * the vector returned by \c force_function.
 * \c force_function is passed through an object of type
 * `ParametrizedPoint::ForceFunction`, which is copied into the curve.
 * If no \c force_function is given, the curve is a geodesic.
 *
 * Transport of objects along the curve during curve computation is also 
 * supported, see \c ParametrizedPoint for more details.
 *
 * The points are stored in ascending order of \c curve_parameter, and points
 * between the stored ones can be approximated using interpolation.
 *
 * \tparam MetricSpaceType the type of the manifold this geodesic is defined on.
 *
 * \tparam TransportTypes 0 or 1 types to transport along the curve.
 * Refer to ParametrizedPoint documentation for the requirements on these types.
 *
 * \see ParametrizedPoint, Geodesic
 *
 * \ingroup geometry
 */
template <typename MetricSpaceType, typename... TransportTypes>
class ParametrizedCurve {
    static_assert(sizeof...(TransportTypes) < 2,
                  "Only 0 or 1 TransportTypes are supported.");
public:
    using PointType = ParametrizedPoint<MetricSpaceType, TransportTypes...>;
    using ContainerType = std::deque<PointType, aligned_allocator<PointType>>;
    using SurfaceType = Surface<MetricSpaceType>;


    /** \brief Construct a curve from a point on the curve.
     *
     *  \param initial_point a point on the curve.
     *  \param force_function a std::function giving the right hand
     *  side of the curve equation of motion.
     */
    ParametrizedCurve(PointType initial_point,
            typename PointType::ForceFunction force_function = nullptr)
        : M_(&initial_point.M()),
            force_function_(force_function),
            configuration_(initial_point.M().configuration.curve) {

        points_.push_back(initial_point);
    }

    /** \brief Construct from the initial tangent vector and transported object.
     *
     *  The \c curve_parameter of the initial point is set to 0.
     *
     *  \param tangent the tangent vector of the curve.
     *  \param transported the object transported along the curve,
     *  if applicable.
     *  \param force_function a std::function giving the right hand
     *  side of the curve equation of motion.
     */
    ParametrizedCurve(
        const TangentVector<MetricSpaceType> &tangent,
        const TransportTypes &... transported,
        typename PointType::ForceFunction force_function = nullptr)
    : ParametrizedCurve(PointType(0, tangent, transported...), force_function) {
    }

    /** \brief Information on the terminations of the curve.
     *
     * Specifies the information stored for each end of the curve.
     */
    struct Termination {

        bool hit_surface = false;
        ///< Is the geodesic terminated at this end?

        /** If terminated, at which surface?
         * The surface index is the index of the surface in the list given to
         * the \c compute method.
         */
        size_t surface_index = 0;

        /** \brief Angle between the ray and the surface normal.
         * As seen by an observer moving along the surface.
         */
        double observer_hit_angle = 0;

        double observer_dot_tangent = 0;
        ///< The dot product between the observer and ray tangent

        friend std::ostream &operator<<(std::ostream &os,
                                        const Termination &t) {
            os << "Termination:\n"
               << "hit_surface = ";
            if (t.hit_surface) {
                os << "true\n";
            } else {
                os << "false\n";
            }
            os << "surface_index = " << t.surface_index << "\n"
               << "observer_hit_angle = " << t.observer_hit_angle << "\n"
               << "observer_dot_tangent = " << t.observer_dot_tangent;

            return os;
        }
    };
    
    typename ContainerType::const_iterator begin() const {
        return points_.cbegin();
    }
    ///< Iterator to the beginning of the points on the curve.

    typename ContainerType::const_iterator end() const {
        return points_.cend();
    }
    ///< Iterator to the end of the points on the curve.
    
    typename ContainerType::const_reverse_iterator rbegin() const {
        return points_.crbegin();
    }
    ///< Iterator to the reverse beginning of the points on the curve.
    
    typename ContainerType::const_reverse_iterator rend() const {
        return points_.crend();
    }
    ///< Iterator to the reverse end of the points on the curve.

    const PointType& operator[](size_t i) const {return points_.at(i);}
    ///< Access the `i`th point on the curve with bounds checking.

    const PointType &front() const { return points_.front(); }
    ///< First point on the curve.
    const PointType &back() const { return points_.back(); }
    ///< Last point on the curve.

    // get extent of geodesic, parametrized by the affine parameter
    double extent_front() const { return front().curve_parameter(); }
    ///< Affine parameter of the first point on the curve.
    double extent_back() const { return back().curve_parameter(); }
    ///< Affine parameter of the last point on the curve.

    /// Does the curve span the given parameter value?
    bool parameter_in_extent(double parameter) const {
        return value_in_range(parameter, extent_front(), extent_back());
    }

    Termination front_termination() const { return front_termination_; }
    ///< The Termination associated with the first point on the curve.
    Termination back_termination() const { return back_termination_; }
    ///< The Termination associated with the last point on the curve.

    size_t num_points() const { return points_.size(); }
    ///< Number of points on the curve

    // Alias num_points for better match with STL containers
    size_t size() const {return points_.size();}
    ///< Number of points on the curve, alias of `num_points`

    /** \brief Describes the reason why computing an extension for the
     * curve was terminated.
     */
    enum class ComputationResult {
        success,
        overstep,
        step_limit,
        hit_surface
    };

    friend std::ostream &operator<<(std::ostream &os,
                                    const ComputationResult res) {
        switch (res) {
            case ComputationResult::success:
                os << "ComputationResult::success";
                break;
            case ComputationResult::overstep:
                os << "ComputationResult::overstep";
                break;
            case ComputationResult::step_limit:
                os << "ComputationResult::step_limit";
                break;
            case ComputationResult::hit_surface:
                os << "ComputationResult::hit_surface";
                break;
        }

        return os;
    }

    /** \brief Extend the geodesic to include the specified value of the affine
     *  parameter.
     *
     * Collisions are checked against the specified surfaces.
     *
     * The behaviour and tolerances of this function are additionally controlled
     * by the \c Configuration which is accessed through \c MetricSpace.
     *
     * \param curve_parameter the curve is extended to this value of curve
     * parameter.
     *
     * \param surfaces a list of surfaces to check for collisions.
     */
    ComputationResult compute(double curve_parameter,
                 const std::vector<SurfaceType *> &surfaces);

    /** \copybrief compute
     *
     * No collision checking is done.
     *
     * \param curve_parameter the curve is extended to this value of affine
     * parameter.
     *
     */
    ComputationResult compute(double curve_parameter) {
        return compute(curve_parameter, std::vector<SurfaceType *>{});
    }

    /** \brief Construct an interpolator over a part of the curve.
     *
     *  Constructs a function `PointType(double curve_parameter)` 
     *  that uses
     *  cubic spline interpolation to approximate points on the curve at a given
     *  value of \c curve_parameter.
     *  
     *  The spline is defined at least in the range 
     *  `curve_parameter_start < curve_parameter < curve_parameter_end`, 
     *  and is constructed by using at least \c min_window_points points, 
     *  provided that the curve contains the requested range and number of 
     *  points.
     *  Otherwise the definition range is restricted to the curve extent.
     *  If `curve_parameter_start > curve_parameter_end`, their values are 
     *  swapped.
     *
     *  The interpolation is performed in the chart used to internally represent
     *  the point at the middle of the interpolation interval.
     *  Note that the whole interpolation span needs to be representable in
     *  that chart.
     *
     *  There have to be at least 3 points on the curve to construct an 
     *  interpolator.
     *
     *  \param curve_parameter_start minimum required curve parameter of the 
     *      interpolation range.
     *  
     *  \param curve_parameter_end maximum required curve parameter of the 
     *      interpolation range.
     *
     *  \param min_window_points minimum number of curve points to use for 
     *      constructing the interpolation spline.
     *
     *  \return a function that interpolates points at specified values of the 
     *      curve parameter within the specified range.
     *
     */
    std::function<PointType(double)> interpolator(
        double curve_parameter_start, double curve_parameter_end,
        size_t min_window_points = 6) const;

    /** \brief Construct and interpolator over the whole curve
     *
     *  Equivalent to `c.interpolator(c.extent_front(), c.extent_back())`
     *  where \c c is the curve object.
     *
     *  \return a function that interpolates points at specified values of the
     *      curve parameter within the curve extent.
     */
    std::function<PointType(double)> interpolator() const {
        return interpolator(extent_front(), extent_back(), 3);
    }

    /** \brief Interpolate a point on the curve.
     *
     *  Approximates the point at the given affine parameter using
     *  cubic spline interpolation.
     *
     *  Uses a locally stored interpolator over the whole curve,
     *  for more fine-grained control and more details on the interpolation see
     *  the \c interpolator() method.
     *  The first call to this function is potentially not thread safe, but
     *  this should cause no problems in normal use.
     *
     *  \param curve_parameter the \c curve_parameter of the desired point.
     *
     */
    PointType interpolate(double curve_parameter) const {
        if(!curve_interpolator_){
            // generate an interpolator over the curve if not present
            // XXX: this can probably fail if the same curve is accessed from 
            // multiple threads, but this should not be a major use case.
            curve_interpolator_ = interpolator();    
        }
        return curve_interpolator_(curve_parameter);
    }

    /// Get a reference to the configuration of this ParametrizedCurve
    configuration::Configuration::Curve& configuration() {
        return configuration_;
    }
    
    /// Get a const reference to the configuration of this ParametrizedCurve
    const configuration::Configuration::Curve& configuration() const {
        return configuration_;
    }

    const MetricSpaceType& M() const { return *M_; }
    ///< Get a const reference to the manifold this curve is defined on.


private:
    const MetricSpaceType *M_;
    ContainerType points_;
    Termination back_termination_;
    Termination front_termination_;
    typename PointType::ForceFunction force_function_;
    mutable std::function<PointType(double)> curve_interpolator_ = nullptr;

    // Configuration parameters inherited from the underlying
    // MetricSpace
    configuration::Configuration::Curve configuration_;

    // computational details of extending the geodesic.
    // start from p at either end of the curve,
    // and propagate to given value of affine parameter storing the points.
    ComputationResult do_computation(
        const PointType &p, double target_lambda,
        const std::vector<SurfaceType *> &surfaces);

    // checks if the step size should be reduced due to being close to a
    // surface intersection. returns the absolute value of the maximum
    // suggested stepsize in the affine parameter
    double surface_intersection_stepsize_limit(
        const PointType &p_current, double stepsize,
        const std::vector<SurfaceType *> &surfaces);

    void compute_hit_angle(const SurfaceType &surface,
                           const TangentVector<MetricSpaceType> &u,
                           Termination &termination);

    // Perform Henon's trick. Templatized for convenience
    template <class StepperType, class OdeintSystemType>
    void henons_trick(OdeintSystemType &ode_system,
                      StepperType &controlled_stepper,
                      typename OdeintSystemType::StateType &state,
                      const SurfaceType &surface, PointType &p_new);
};

//
// Implementation of the computation

template <typename MST, typename... Ts>
inline
typename ParametrizedCurve<MST, Ts...>::ComputationResult 
ParametrizedCurve<MST, Ts...>::compute(
    double curve_parameter,
    const std::vector<SurfaceType *> &surfaces) {
    Log::log()->trace("compute: curve_parameter {} extent_back() {} extent_front() {}",
            curve_parameter, extent_back(), extent_front());

    // try to compute the curve to span the given extent. only extend
    // if necessary.
    ComputationResult return_code = ComputationResult::success;

    // confirm that the curve will be extended towards more negative
    // affine parameter values, and that we have not already hit a
    // surface into that direction
    if (curve_parameter < extent_front()) {
        if (front_termination_.hit_surface) {
            Log::log()->warn("compute: Curve already terminated at affine parameter {}"
                    " > target affine affine parameter {}",
                    extent_front(), curve_parameter);
            return ComputationResult::hit_surface;
        }
        // towards more negative affine parameter
        Log::log()->trace("compute: extending towards negative");
        return_code = do_computation(front(), curve_parameter, surfaces);

    } else if (curve_parameter > extent_back()) {
        // similarly for more positive affine parameter values
        if (back_termination_.hit_surface) {
            Log::log()->warn("compute: Curve already terminated at affine parameter {}"
                    " < target affine affine parameter {}",
                    extent_back(), curve_parameter);
            return ComputationResult::hit_surface;
        }
        // towards more positive affine parameter
        Log::log()->trace("compute: extending towards positive");
        return_code = do_computation(back(), curve_parameter, surfaces);
    }

    // If there was already a local interpolator, invalidate it
    if(curve_interpolator_) {curve_interpolator_ = nullptr;}

    // only one of the integrations above is run, so it's ok just to
    // return return_code
    return return_code;
}

namespace internal {
// Class handling functionality relating to integration using Boost.odeint.
// Handles computing the derivatives in the integration loop, and all
// conversions between integration state used by odeint and ParametrizedPoints.
template <typename MetricSpaceType, typename PointType>
class OdeintSystem {
public:
    static const size_t component_count = PointType::component_count;
    // TODO: We should use std::array since C+11, but this seems to be
    // incompatible with odeint::array_algebra at least in Boost 1.54
    using StateType = boost::array<double, component_count>;
    using SurfaceType = Surface<MetricSpaceType>;
    enum class SteppingMethod { NORMAL, HENON };

    OdeintSystem(
        const typename PointType::ChartType &chart,
        const typename PointType::ForceFunction &force_function,
        typename PointType::TransportExtraDataType transported_extra)
    : chart_(&chart),
      henon_surface_(nullptr),
      stepping_method_(SteppingMethod::NORMAL),
      force_function_(force_function),
      transported_extra_(transported_extra) {}

    // Changes the chart used and converts the integrator state to the new chart
    void select_better_chart(StateType &state) {
        auto p = state_to_point(state);
        chart_ = &p.M().get_best_integration_chart(p.tangent());
        point_to_state(p, state);
    }

    const typename PointType::ChartType &chart() const { return *chart_; }

    void set_henon_surface(const SurfaceType &surface) {
        henon_surface_ = &surface;
    }

    void set_stepping_method(SteppingMethod method) {
        stepping_method_ = method;
    }

    void operator()(const StateType &x, StateType &dxdt,
                    const double /* curve_parameter */) {

        PointType p = state_to_point(x);
        typename PointType::ContainerType derivs =
            p.transport_derivatives(chart(), force_function_);

        if (stepping_method_ == SteppingMethod::HENON) {

            double grad_S_dot_f =
                dot_product(p.tangent(), henon_surface_->gradient(p.point()));
            derivs = derivs / grad_S_dot_f;
        }

        components_to_state(derivs, dxdt);
    }

    // Also handle conversions between StateType and ParametrizedPoint
    // Creates a new ParametrizedPoint from components contained in state
    PointType state_to_point(const StateType &state) const {
        // Using a TensorMap instead of memcpy is somewhat cleaner, 
        // but requires casting away the constness of state, as TensorMap
        // doesn't appear to support const data.
        Eigen::TensorMap<typename PointType::ContainerType> components(
            const_cast<double*>(state.data()), 
            static_cast<size_t>(component_count) // cast needed because odr-use
            );
        return PointType(chart(), components, transported_extra_);
    }

    // Copies the components into out_state
    void components_to_state(
        const typename PointType::ContainerType &components,
        StateType &out_state) const {
        Eigen::TensorMap<typename PointType::ContainerType> out_state_tensor(
            out_state.data(), 
            static_cast<size_t>(component_count));
        out_state_tensor = components;
    }

    void point_to_state(const PointType &point,
                        StateType &out_state) const {
        components_to_state(point.components(chart()), out_state);
    }

private:
    // Store a chart pointer so that the chart used can be changed
    const typename PointType::ChartType *chart_;

    const SurfaceType *henon_surface_;

    SteppingMethod stepping_method_;

    // OdeintSystem doesn't need to be copied, so storing a reference should be 
    // ok.
    const typename PointType::ForceFunction &force_function_;

    const typename PointType::TransportExtraDataType transported_extra_;
};
} // namespace internal

template <typename MST, typename... Ts>
inline typename ParametrizedCurve<MST, Ts...>::ComputationResult
ParametrizedCurve<MST, Ts...>::do_computation(
    const PointType &p, double target_lambda,
    const std::vector<SurfaceType *> &surfaces) {

    using StateType = typename internal::OdeintSystem<MST,PointType>::StateType;

    Log::log()->trace("do_computation: from {} target_lambda {}", p, target_lambda);

    const configuration::Configuration::Curve &conf =
        configuration_;
    const configuration::Propagation &propagation_conf =
        configuration_.propagation;

    // step in negative direction if going towards more negative affine
    // parameters
    const int direction = sign(target_lambda - p.curve_parameter());
    double sampling_step_size =
        (target_lambda - p.curve_parameter()) * propagation_conf.sampling_interval;
    double h = sampling_step_size;

    const double integration_span =
        std::fabs(target_lambda - p.curve_parameter());

    // initially assume integration was successful
    ComputationResult return_code = ComputationResult::success;

    // state mutated during integration
    PointType p_current = p;

    // Initialize the system with the best chart
    // Also grab the possible extra data from ParametrizedPoint
    internal::OdeintSystem<MST, PointType> ode_system(
        M_->get_best_integration_chart(p.tangent()), force_function_,
        p.transported_extra());

    Log::log()->trace("do_computation: using chart {}", ode_system.chart());
    // controlled stepper
    // array_algebra is a special algebra for boost::arrays that should have
    // better performance than the default algebra. also seems to work for
    // std::array
    auto controlled_stepper = odeint::make_controlled(
        propagation_conf.tolerance.absolute, propagation_conf.tolerance.relative,
        odeint::runge_kutta_dopri5<StateType, double, StateType, double,
                                   odeint::array_algebra>());
    long accepted_steps = 0;

    // This is used to keep track of the integration performance:
    // for failed step attempts it is increased by configuration_.failed_step_penalty,
    // and for successful steps it is decreased by 1.
    // If the value grows too large, switching charts is attempted if
    // possible.
    size_t failed_step_score = 0;

    const bool can_attempt_chart_switching = M_->uses_multiple_charts();

    //
    // main integration loop
    while (1) {
        double curve_parameter = p_current.curve_parameter();
        double absdiff = std::fabs(target_lambda - curve_parameter);

        // check if we've hit the step limit
        if (propagation_conf.enforce_maximum_steps &&
            accepted_steps >= propagation_conf.maximum_steps) {
            Log::log()->warn("do_computation: terminated by exceeding "
                    "maximum number of steps ", propagation_conf.maximum_steps);

            // hitting step limit needs to be communicated
            return_code = ComputationResult::step_limit;
            break;
        }

        // check if we've reached end, judged by absolute and relative
        // tolerances
        // Use a similar combination of the tolerances as in odeint
        if (absdiff <=
            propagation_conf.tolerance.absolute +
                propagation_conf.tolerance.relative * integration_span) {
            // we are close enough to the termination point
            Log::log()->trace(
                "do_computation: terminated by tolerance limit. e_abs {} e_rel "
                "{}",
                absdiff, absdiff / integration_span);

            // terminated via tolerance = SUCCESS
            break;
        }

        // check if we've passed the target affine parameter value.
        // this should only happen if tolerance is set very tight and
        // numerical roundoff happens
        if ((target_lambda - curve_parameter) * direction < 0) {
            Log::log()->trace(
                "do_computation: terminated due to overstepping."
                " target {} current {} error {}",
                target_lambda, curve_parameter, absdiff);

            return_code = ComputationResult::overstep;
            break;
        }

        // check if we are close enough to desired target_lambda to reduce step
        // size
        if (absdiff < std::fabs(h)) {
            double new_h = direction * absdiff;

            // ensure at least one point between endpoints for a total of 3 
            // points
            if (accepted_steps == 0) {
                new_h *= .75;
            }

            Log::log()->trace(
                "do_computation: lambda {} near target lambda {}."
                " reducing stepsize. old h {} new h {}",
                curve_parameter, target_lambda, h, new_h);
            h = new_h;
        }

        // check that we don't step over chosen sampling interval
        if (propagation_conf.enforce_sampling_interval) {
            if (std::fabs(h) > std::fabs(sampling_step_size)) {
                double new_h = h * std::fabs(sampling_step_size / h);
                Log::log()->trace(
                    "do_computation:  step size {} over sampling "
                    "mandated stepsize {}. reducing stepsize to {}",
                    h, sampling_step_size, new_h);
                h = new_h;
            }
        }

        // check if we are close enough to a surface to reduce stepsize
        if (conf.enforce_surface_stepsize_reduction) {
            double max_surface_step =
                surface_intersection_stepsize_limit(p_current, h, surfaces);
            Log::log()->trace("do_computation: stepsize {} max_surface_step {}",
                              h, max_surface_step);

            if (max_surface_step < std::fabs(h)) {
                double new_h = direction * max_surface_step;
                Log::log()->trace(
                    "do_computation: near a surface, reduced"
                    " stepsize. old {} new {}",
                    h, new_h);
                h = new_h;
            }
        }
        // check that the stepsize is still in set bounds
        if (propagation_conf.enforce_minimum_stepsize) {
            if (std::fabs(h) < propagation_conf.minimum_stepsize) {
                Log::log()->trace(
                    "do_computation: stepsize smaller than minimum"
                    " stepsize: |step| = {} capped to {}",
                    std::fabs(h), propagation_conf.minimum_stepsize);
                h = h * std::fabs(propagation_conf.minimum_stepsize / h);
            }
        }
        if (propagation_conf.enforce_maximum_stepsize) {
            if (std::fabs(h) > propagation_conf.maximum_stepsize) {
                Log::log()->trace(
                    "do_computation: stepsize larger than maximum "
                    "stepsize: |step| = {} capped to {}",
                    std::fabs(h), propagation_conf.maximum_stepsize);
                h = h * std::fabs(propagation_conf.maximum_stepsize / h);
            }
        }

        // take one step, store in p_new
        // Can't create ParametrizedPoint without data
        PointType p_new = p_current;

        StateType state;
        ode_system.point_to_state(p_current, state);

        {
            odeint::controlled_step_result step_result;
            double lambda = curve_parameter;

            while (1) {
// XXX: having this enabled seems to improve performance due to branch
// prediction
// so leaving them enabled as a default
#ifndef ARCMANCER_NO_LOG_INNER_LOOP
                Log::log()->trace("do_computation: affine parameter {} stepping for {}", lambda , h);
#endif

                step_result =
                    controlled_stepper.try_step(ode_system, state, lambda, h);

                if (step_result == odeint::success) break;

                failed_step_score += configuration_.failed_step_penalty;

#ifndef ARCMANCER_NO_LOG_INNER_LOOP
                Log::log()->debug("do_computation: failed step. current state {} new timestep {}"
                            "\nfailed_step_score {}",
                            ode_system.state_to_point(state), h, failed_step_score);
#endif

                if (can_attempt_chart_switching &&
                    failed_step_score > configuration_.chart_switch_failed_step_score) {

                    auto &old_chart = ode_system.chart();
                    ode_system.select_better_chart(state);

                    Log::log()->debug(
                            "do_computation: failed_step_score {} over limit {}"
                            ", attempt switching to a better chart.\n"
                            "Old chart: {}\nNew chart: {}",
                            failed_step_score, 
                            configuration_.chart_switch_failed_step_score,
                            old_chart,
                            ode_system.chart());

                    // Reset the stepper since the system changed
                    controlled_stepper.reset();
                    failed_step_score = 0;
                }
            }
            p_new = ode_system.state_to_point(state);

            if (failed_step_score > 0) --failed_step_score;
        }

        // check for termination with surfaces
        auto hit_surface_it = surfaces.end();

        // it is possible to cross several surfaces during a single step, so
        // need to iterate over them all to find the surface that was hit first
        // along the curve.
        for (auto surface_it = surfaces.begin(); surface_it != surfaces.end();
             ++surface_it) {

            // need two deferences: iterator->ptr->object
            SurfaceType &surface = **surface_it;

            if (!surface.points_cross_surface(p_current.point(),
                                              p_new.point())) {
                continue;
            }

            // crossed this surface
            Log::log()->trace(
                "do_computation: crossed surface {}, between "
                "points\np_current {} p_new {}",
                surface_it - surfaces.begin(), p_current, p_new);

            // It's likely a bit safer to step from the last point before 
            // crossing.
            auto p_temp = p_current;

            // use henon's trick to step to the surface
            henons_trick(ode_system, controlled_stepper, state, surface,
                         p_temp);

            if (surface.ignore_at(p_temp.point())) {

                Log::log()->trace("do_computation: ignoring this surface");

            } else if (std::fabs(p_current.curve_parameter() -
                                 p_temp.curve_parameter()) <
                       std::fabs(p_current.curve_parameter() -
                                 p_new.curve_parameter())) {
                // This surface is closer to p_current than the (possible)
                // previous hit surface
                // If multiple surfaces were crossed, p_new moves always closer
                // to p_current,
                // so that in the end the closest surface is found.
                hit_surface_it = surface_it;
                p_new = p_temp;
            }
        }

        bool surface_was_hit = hit_surface_it != surfaces.end();

        // Make sure there are always at least 3 points on a curve to allow
        // interpolation. If not take an extra step with reduced step size.
        if (accepted_steps == 0 && surface_was_hit){
            h = .75 * (p_new.curve_parameter() - p_current.curve_parameter());
            continue;
        }

        // step complete
        accepted_steps++;
        p_current = p_new;

        // Store the new point in the correct end of the points_ deque.
        // p_new is not used after this so can use std::move, although
        // it doesn't likely have a significant impact here.
        if (direction > 0) {
            points_.push_back(std::move(p_new));
        } else {
            points_.push_front(std::move(p_new));
        }

        // if a surface was hit, fill out the termination and break
        if (surface_was_hit) {
            // get the correct end termination, mark termination status
            // as true
            Termination &termination =
                (h < 0 ? front_termination_ : back_termination_);
            termination.hit_surface = true;
            termination.surface_index = hit_surface_it - surfaces.begin();

            compute_hit_angle(**hit_surface_it, p_current.tangent(),
                              termination);

            Log::log()->trace("do_computation: terminated by surface at {} of geodesic"
                    "\nlambda {} point_hit {}",
                    (h < 0 ? "beginning" : "end"),
                    p_current.curve_parameter(),
                    p_current.point());


            return_code = ComputationResult::hit_surface;
            break;
        }
    }

    Log::log()->trace("do_computation: endpoint {}", p_current);

    return return_code;
}

template <typename MST, typename... Ts>
inline double
ParametrizedCurve<MST, Ts...>::surface_intersection_stepsize_limit(
    const PointType &p_current, double stepsize,
    const std::vector<SurfaceType *> &surfaces) {

    double max_step_abs = std::numeric_limits<double>::max();

    for_each(surfaces.begin(), surfaces.end(), [&](const auto &p_s) {
        auto n = p_s->gradient(p_current.point());
        double S = p_s->value(p_current.point());
        double covS_dot_u = dot_product(n, p_current.tangent());

        // if cov S.u * stepsize and S have opposite signs, we may
        // need to reduce stepsize
        if (covS_dot_u * stepsize * S < 0) {
            double max_stepsize =
                configuration_.surface_stepsize_reduction_tolerance *
                std::fabs(S / covS_dot_u);

            if (max_stepsize < max_step_abs) { max_step_abs = max_stepsize; }
        }
    });

    return max_step_abs;
}

template <typename MST, typename... Ts>
inline void ParametrizedCurve<MST, Ts...>::compute_hit_angle(
    const SurfaceType &surface, const TangentVector<MST> &u,
    Termination &termination) {
    // get the four-velocity of an observer moving on the surface
    auto u_surf_obs = surface.observer(u.point());

    auto grad_vector = surface.gradient(u.point()).template raise_index<0>();
    // we wish to find the minimum angle, so if the result is > pi/2,
    // convert to pi - angle
    // XXX: is this always desirable?
    termination.observer_hit_angle = vector_angle(u_surf_obs, grad_vector, u);

    if (termination.observer_hit_angle > cnst::pi / 2) {
        termination.observer_hit_angle =
            cnst::pi - termination.observer_hit_angle;
    }
    // XXX: Also compute the dot product here, maybe should reflect this in the
    // method name?
    termination.observer_dot_tangent = dot_product(u, u_surf_obs);
}

template <typename MST, typename... Ts>
template <typename StepperType, typename OdeintSystemType>
inline void ParametrizedCurve<MST, Ts...>::henons_trick(
    OdeintSystemType &ode_system, StepperType &controlled_stepper,
    typename OdeintSystemType::StateType &state, const SurfaceType &surface,
    PointType &p_new) {

    ode_system.set_henon_surface(surface);
    ode_system.set_stepping_method(OdeintSystemType::SteppingMethod::HENON);

    // after the ODE is changed to facilitate Henon's trick, the stepper
    // must be reset
    controlled_stepper.reset();

    // get a cref on the henon trick configuration struct
    const auto &conf = configuration_.henon;

    double S0 = surface.value(p_new.point());
    double S = S0;
    long steps = 0;
    while (std::fabs(S / S0) > conf.tolerance.relative &&
           std::fabs(S) > conf.tolerance.absolute &&
           steps < conf.maximum_steps) {
        double henon_stepsize = -S;
        Log::log()->trace("henons_trick: performing Henon step. "
                "S {}  |S/S0| {} stepsize {}",
                S, std::fabs(S / S0) , henon_stepsize);

        {
            odeint::controlled_step_result step_result;
            while (1) {
                step_result = controlled_stepper.try_step(ode_system, state, S,
                                                          henon_stepsize);
                if (step_result != odeint::success) {
                    Log::log()->debug("henons_trick: failed henon step. new timestep {}",
                        henon_stepsize);
                    continue;
                }
                p_new = ode_system.state_to_point(state);
                break;
            }
            Log::log()->trace("henons_trick: S {} surface.value {}", S, surface.value(p_new.point()));
            // XXX: just using S without recalculating seems more
            // numerically stable
            // TODO: this should be investigated in full
            S = surface.value(p_new.point());
            steps++;
        }
    }

    Log::log()->trace("henons_trick: residual S {} |S/S0| {}",
            surface.value(p_new.point()),
            std::fabs(surface.value(p_new.point()) / S0));

    // switch back to normal stepping, and reset stepper again
    ode_system.set_stepping_method(OdeintSystemType::SteppingMethod::NORMAL);
    controlled_stepper.reset();
}

template <typename MST, typename... Ts >
inline std::function<typename ParametrizedCurve<MST, Ts...>::PointType(double)>
ParametrizedCurve<MST, Ts...>::interpolator(double curve_param_start,
                                           double curve_param_end,
                                           size_t min_window_points) const {

    // make sure the range is in correct order
    if (curve_param_start > curve_param_end) {
        std::swap(curve_param_start, curve_param_end);
    }

    // middle point of interpolation range
    double l = (curve_param_start + curve_param_end) / 2;

    // cubic spline interpolation needs at least 3 points
    if (num_points() < 3) {
        Log::log()->error(
            "interpolator: can't construct interpolation spline, only {} available",
            num_points());

        throw std::runtime_error(
            "Need at least 3 points to construct interpolation spline, only " +
            std::to_string(num_points()) + " available.");
    }

    size_t points_left = min_window_points;
    if (points_left > num_points()) {
        Log::log()->warn("interpolator: Requested {} interpolation points, "
                "only {} available. Reducing number of points.", 
                points_left, num_points());
        points_left = num_points();
    }

    // find point closest to middle
    size_t index = 0;
    double diff = std::fabs(points_[0].curve_parameter() - l);
    for (size_t i = 0; i < num_points(); i++) {
        double new_diff;
        if ((new_diff = std::fabs(points_[i].curve_parameter() - l)) < diff) {
            diff = new_diff;
            index = i;
        }
    }

    size_t window_start = index, window_end = index;

    // ensure the minimum number of points are used
    for (size_t i = 0; i < min_window_points; i++) {
        if (window_start > 0) {
            window_start--;
            if (--points_left <= 0) { break; }
        }
        if (window_end + 1 < num_points()) {
            window_end++;
            if (--points_left <= 0) { break; }
        }
    }

    // extend to cover the whole window
    // TODO: could optimize for the typical use case where the requested range
    // contains enough points, so that it is enough to just search for the
    // points just outside the range
    while (window_start > 0 &&
           curve_param_start < points_[window_start].curve_parameter()) {
        --window_start;
    }
    while (window_end + 1 < num_points() &&
           curve_param_end > points_[window_end].curve_parameter()) {
        ++window_end;
    }

    // Warn if the requested range isn't within curve extent
    if (curve_param_start < extent_front()) {
        Log::log()->warn("interpolator: curve_param_start {} outside curve extent. Capped to {}",
                curve_param_start, extent_front());
    }
    if (curve_param_end > extent_back()) {
        Log::log()->warn("interpolator: curve_param_end {} outside curve extent. Capped to {}",
                curve_param_end, extent_back());
    }

    // how big did the window end up being?
    size_t final_window_size = window_end - window_start + 1;
    Log::log()->trace("interpolator: window start {} end {} size {}",
            window_start, window_end , final_window_size);

    // Get the ParametrizedPoint closest to the middle of the window
    const PointType closest = points_[index];
    // Use the chart that the middle point uses internally, this should be a
    // good chart and reduces the coordinate conversions done
    const typename PointType::ChartType &chart = closest.point().chart();

    // Gets around linker errors when trying to use in a dynamic context,
    // static const members have some limitations.
    constexpr size_t component_count = PointType::component_count;

    // collect the components of the points in the window into a matrix
    Eigen::MatrixXd data(final_window_size, component_count);
    for (size_t i = window_start; i <= window_end; i++) {
        typename PointType::ContainerType components =
            points_[i].components(chart);

        data.row(i - window_start) =
            Eigen::Map<Eigen::Matrix<double, component_count, 1>>(
                components.data());
    }

    // Get the boundary condition for the derivatives
    const auto left_deriv =
        points_[window_start].transport_derivatives(chart, force_function_);
    const auto right_deriv =
        points_[window_end].transport_derivatives(chart, force_function_);

    const Eigen::VectorXd left_deriv_vec =
        Eigen::Map<const Eigen::VectorXd>(left_deriv.data(), component_count);
    const Eigen::VectorXd right_deriv_vec =
        Eigen::Map<const Eigen::VectorXd>(right_deriv.data(), component_count);

    // construct an interpolator
    return [
        component_count,
        spline = interpolation::CubicSpline(data, left_deriv_vec, right_deriv_vec,
                             configuration_.propagation.tolerance.absolute),
        &chart, transported_extra = closest.transported_extra()
    ](double curve_parameter)
        ->PointType {

        Eigen::VectorXd interpolated;

        // if curve_parameter is outside the definition interval more than
        // absolute tolerance, the evaluate function will warn,
        // but will still return values.
        // TODO: would it be better to throw instead of returning possibly
        // incorrect values?
        interpolated = spline.evaluate(curve_parameter);

        Eigen::TensorMap<Eigen::Tensor<double, 1>> components(
            interpolated.data(), component_count);

        return PointType(chart, components, transported_extra);
    };
}

} // namespace arcmancer
