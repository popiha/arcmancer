#pragma once

#include <algorithm>
#include <array>
#include <cmath>
#include <functional>
#include <ostream>
#include <stdexcept>
#include <type_traits>

#include <boost/array.hpp>
#include <boost/numeric/odeint.hpp>

#include "arcmancer/geometry/parametrized_curve.h"

// Functionality for computing integrals of quantities over a given
// curve


namespace arcmancer {
namespace odeint = boost::numeric::odeint;

// We need a separate odeint wrapper for line integrals
namespace internal {
template <typename CurveType, typename IntegrandFunction, typename ValueType,
          typename StateType>
class LineIntegralSystem {
public:
    using PointType = typename CurveType::PointType;

    LineIntegralSystem(const CurveType &curve,
                       const IntegrandFunction &integrand)
    : curve_(curve), integrand_(integrand) {}

    void operator()(const StateType &x, StateType &dxdt,
                    const double parameter) {

        // Get the curve tangent at the current point.
        auto point = curve_.interpolate(parameter);

        // Need a constructor ValueType(const StateType&) and implicit 
        // conversion from return value to StateType
        dxdt = integrand_(ValueType{x}, point);
    }

private:
    const CurveType &curve_;
    const IntegrandFunction &integrand_;
};
} // namespace internal

/** \brief Computes the line integral of \c integrand over a given \c
 * ParametrizedCurve.
 *
 * Compute the line integral of \c integrand over \c curve, from \c
 * parameter_start to \c parameter_end.
 *
 * If \c parameter_start or \c parameter_end are not within the range in
 * which \c curve is defined, a warning is logged, and the integration
 * range is truncated to [parameter_start, parameter_end].
 *
 * The integration uses the `boost::odeint::runge_kutta_fehlberg78` integrator,
 * and the `StateType` and `Algebra` template parameters are passed directly to
 * it.
 * For more details on them see the odeint documentation.
 *
 * Currently only `ValueType`s that are
 * containers of `double`s or plain `double` are supported.
 * Container `ValueTypes` need to either support the `odeint::range_algebra`, or
 * `StateType` needs to be specified to be a supported type, e.g.
 * `std::vector<double>`.
 * Some checks are implemented to give simple error messages when the types used
 * are not supported, but e.g. a wrong `StateType`/`Algebra` combination will
 * likely result in a complicated error message.
 *
 * \tparam ValueType the type of the integrated value, must constructible from
 * `const StateType &`.
 * \tparam StateType the type of state used by the integrator.
 * \tparam Algebra the algebra used by the integrator.
 * \tparam CurveType the type of the curve.
 * \tparam IntegrandFunction the type of the integrated function.
 * Must be callable with arguments of type `(ValueType, CurveType::PointType)`
 * and the return value must be convertible `to StateType`.
 *
 *
 * \param curve The parametrized curve to compute the line integral over.
 * \param parameter_start Starting point of the integration.
 * \param parameter_end Ending point of the integration.
 * \param initial_value The initial value on top of which to build the integral.
 * \param integrand The quantity to integrate over the curve.
 * \param line_conf The set of numerical tolerances to use. Overrides the
 * configuration of the underlying \c MetricSpace
 * \ingroup geometry
 */
template <typename ValueType, typename StateType = ValueType,
          typename Algebra = std::conditional_t<
              std::is_floating_point<ValueType>::value,
              odeint::vector_space_algebra, odeint::range_algebra>,
          typename CurveType, typename IntegrandFunction>
inline ValueType line_integral(
    const CurveType &curve, double parameter_start, double parameter_end,
    const ValueType &initial_value, const IntegrandFunction &integrand,
    const configuration::Configuration::LineIntegral &line_conf) {

    // checks for integrand signature
    // XXX: this approach feels a bit hacky, used also in radiation_transfer.
    // It could maybe be cleaned up with some more generic tools, e.g. by
    // implementing a version of C++17 is_invocable.
    using res_of_type = std::result_of<IntegrandFunction(
        const ValueType &, const typename CurveType::PointType &)>;
    static_assert(internal::has_member_type<res_of_type>::value,
                  "integrand is not callable with the correct signature!");
    static_assert(
        std::is_convertible<typename res_of_type::type, StateType>::value,
        "Return type of integrand is not convertible to StateType!");

    // other sanity checks 
    static_assert(std::is_constructible<ValueType, const StateType &>::value,
                  "ValueType is not constructible from const StateType&");
    static_assert(!std::is_integral<ValueType>::value,
                  "ValueType is an integral type!");

    // XXX: checks that the Algebra and StateType match would be good,
    // but would likely require some complex meta trickery.
    // If it was easy, surely the odeint library would do it already?

    const auto &conf = line_conf.propagation;

    // check limits
    if (!curve.parameter_in_extent(parameter_start) ||
        !curve.parameter_in_extent(parameter_end)) {
        throw std::invalid_argument(
            fmt::format("line_integral: integration range ({}, {}) exceeds "
                        "curve span ({},{})",
                        parameter_start, parameter_end, curve.extent_front(),
                        curve.extent_back()));
    }

    internal::LineIntegralSystem<CurveType, IntegrandFunction, ValueType,
                                 StateType>
        ode_system(curve, integrand);

    // using rkf78 instead of dopri5 since that's what was used before for
    // radiation transfer, and seems to also be faster in that case.
    // TODO: can the first double tparam (Value) be made configurable?
    // This would enable using types that are not representable as an array of
    // double.
    auto controlled_stepper = odeint::make_controlled(
        conf.tolerance.absolute, conf.tolerance.relative,
        odeint::runge_kutta_fehlberg78<StateType, double, StateType, double,
                                       Algebra>());

    // XXX: Integration over single real variable function is in principle a
    // very generic operation. We should package it into a generic user
    // interface somehow. For now, this is just copypasted from
    // ParametrizedCurve

    int direction = sign(parameter_end - parameter_start);
    double sampling_step_size =
        (parameter_end - parameter_start) * conf.sampling_interval;
    double h = sampling_step_size;

    const double integration_span = std::fabs(parameter_end - parameter_start);

    double parameter = parameter_start;
    ValueType p_current = initial_value;
    long accepted_steps = 0;

    while (1) {
        double absdiff = std::fabs(parameter_end - parameter);

        // check if we've reached end, judged by absolute and relative
        // tolerances
        if (absdiff <= conf.tolerance.absolute +
                           conf.tolerance.relative * integration_span) {
            // we are close enough to the termination point
            Log::log()->trace(
                "line_inegral: terminated by tolerance limit. e_abs {} e_rel "
                "{}",
                absdiff, absdiff / integration_span);
            break;
        }

        // check if we've passed the target affine parameter value.
        // this should only happen if tolerance is set very tight and
        // numerical roundoff happens
        if ((parameter_end - parameter) * direction < 0) {
            Log::log()->trace(
                "do_computation: terminated due to overstepping. target {} "
                "current {}",
                parameter_end, parameter);
            break;
        }

        // check if we are close enough to desired parameter_end to reduce step
        // size
        if (absdiff < std::fabs(h)) {
            double new_h = direction * absdiff;
            Log::log()->trace(
                "line_integral: lambda {} near target lambda {}."
                " reducing stepsize. old h {} new h {}",
                parameter, parameter_end, h, new_h);
            h = new_h;
        }

        // check that we don't step over chosen sampling interval
        if (conf.enforce_sampling_interval) {
            if (std::fabs(h) > std::fabs(sampling_step_size)) {
                double new_h = h * std::fabs(sampling_step_size / h);
                Log::log()->trace(
                    "line_integral:  step size {} over sampling "
                    "mandated stepsize {}. reducing stepsize to {}",
                    h, sampling_step_size, new_h);
                h = new_h;
            }
        }

        // check that the stepsize is still in set bounds
        if (conf.enforce_minimum_stepsize) {
            if (std::fabs(h) < conf.minimum_stepsize) {
                Log::log()->trace(
                    "line_integral: stepsize smaller than minimum"
                    " stepsize: |step| = {} capped to {}",
                    std::fabs(h), conf.minimum_stepsize);
                h = h * std::fabs(conf.minimum_stepsize / h);
            }
        }

        if (conf.enforce_maximum_stepsize) {
            if (std::fabs(h) > conf.maximum_stepsize) {
                Log::log()->trace(
                    "line_integral: stepsize larger than maximum "
                    "stepsize: |step| = {} capped to {}",
                    std::fabs(h), conf.maximum_stepsize);
                h = h * std::fabs(conf.maximum_stepsize / h);
            }
        }

        // take one step, store in p_new
        ValueType p_new = p_current;

        StateType state = p_current;
        {
            odeint::controlled_step_result step_result;
            double lambda = parameter;
            while (1) {
                step_result =
                    controlled_stepper.try_step(ode_system, state, lambda, h);

                if (step_result == odeint::success) {
                    // successful step, update parameter
                    Log::log()->trace(
                        "line_integral: successful step from {} to {}",
                        parameter, lambda);
                    parameter = lambda;
                    break;
                }
                // not much to do here except allow stepper to try
                // again
            }

            // explicit call to the constructor for generality
            p_new = ValueType{state};
        }

        // step complete
        accepted_steps++;
        p_current = p_new;
    }

    // loop complete, return computed integral
    return p_current;
}

/** \brief \copybrief line_integral
 *
 * Compute the line integral of \c integrand over \c curve, from \c
 * parameter_start to \c parameter_end.
 * Uses the numerical configuration supplied by the underlying \c
 * MetricSpace.
 *
 * For more details see `::line_integral`.
 *
 *
 * \param curve The parametrized curve to compute the line integral over.
 * \param parameter_start Starting point of the integration.
 * \param parameter_end Ending point of the integration.
 * \param initial_value The initial value on top of which to build the integral.
 * \param integrand The quantity to integrate over the curve.
 * \ingroup geometry
 */
template <typename ValueType, typename StateType = ValueType,
          typename Algebra = std::conditional_t<
              std::is_floating_point<ValueType>::value,
              odeint::vector_space_algebra, odeint::range_algebra>,
          typename CurveType, typename IntegrandFunction>
inline ValueType line_integral(const CurveType &curve, double parameter_start,
                               double parameter_end,
                               const ValueType &initial_value,
                               const IntegrandFunction &integrand) {
    return line_integral<ValueType, StateType, Algebra>(
        curve, parameter_start, parameter_end, initial_value, integrand,
        curve.M().configuration.line_integral);
}

/** \brief \copybrief line_integral
 *
 * Compute the line integral of \c integrand over the whole span of \c curve,
 * from its `extent_front` to `extent_back`.
 * Uses the numerical configuration supplied by the underlying \c
 * MetricSpace.
 *
 * For more details see `::line_integral`.
 *
 *
 * \param curve The parametrized curve to compute the line integral over.
 * \param initial_value The initial value on top of which to build the integral.
 * \param integrand The quantity to integrate over the curve.
 * \ingroup geometry
 */
template <typename ValueType, typename StateType = ValueType,
          typename Algebra = std::conditional_t<
              std::is_floating_point<ValueType>::value,
              odeint::vector_space_algebra, odeint::range_algebra>,
          typename CurveType, typename IntegrandFunction>
inline ValueType line_integral(const CurveType &curve,
                               const ValueType &initial_value,
                               const IntegrandFunction &integrand) {
    return line_integral<ValueType, StateType, Algebra>(
        curve, curve.extent_front(), curve.extent_back(), initial_value,
        integrand, curve.M().configuration.line_integral);
}

} // namespace arcmancer
