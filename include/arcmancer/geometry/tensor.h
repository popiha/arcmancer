#pragma once

#include <sstream>
#include <stdexcept>
#include <type_traits> // std::is_same
#include <typeinfo>

#include <Eigen/Dense>
#include <boost/mpl/for_each.hpp>
#include <unsupported/Eigen/CXX11/Tensor>

#include "arcmancer/geometry/meta.h"
#include "arcmancer/geometry/types.h"
#include "arcmancer/geometry/utils.h"

namespace arcmancer {

/** \brief A general tensor.
 *
 * Implements general tensors with index types (covariant/contravarint) stored
 * at the type level.
 * This allows typechecking operations between tensors, so that only
 * coordinate invariant operations are possible.
 * The indices are numbered starting from 0.
 *
 * Tensors are associated with a fixed point on some underlying manifold, and
 * operations are only possible between tensors at the same point.
 * This is checked at runtime, and consequently tensor operations may throw
 * an exception if they are attempted between tensors at different points.
 *
 * The components of the tensor are represented internally in some fixed
 * coordinate chart, but conversions to other charts are handled automatically.
 * The only supported datatype for the components is \c double.
 *
 * Scalars can be represented as tensors with zero indices, but these tensors
 * are also implicitly convertible to \c double.
 *
 * \tparam MetricSpaceType the type of the underlying manifold.
 * \tparam Indices a list of index types (either Cnt or Cov), that specify the
 * indices of the tensor.
 *
 * \see TangentVector, CotangentVector convenience aliases for specific tensors.
 * \ingroup geometry
 * \ingroup overaligned
 *
 */
template <typename MetricSpaceType, typename... Indices>
class Tensor {

    static_assert(internal::all_Cov_or_Cnt<Indices...>::value,
                  "Indices can only be of type Cnt or Cov!");
    static constexpr size_t rank = sizeof...(Indices);
    // How many template parameters there are before the start of the index
    // types
    static constexpr size_t index_offset = 1;

    static constexpr long D = MetricSpaceType::D;

public:
    using ContainerType =
        Eigen::TensorFixedSize<double,
                               typename internal::sizes_helper<rank, D>::sizes>;
    ///< The container used to store the components of the tensor

    using PointType = ManifoldPoint<MetricSpaceType>;
    using ChartType = Chart<MetricSpaceType>;

    // some synonyms for template programming
    using CurrentType = Tensor<MetricSpaceType, Indices...>;
    using CurrentTypeList =
        typename internal::tl_rename<CurrentType, internal::TypeList>::type;

    static constexpr size_t component_count =
        ContainerType::Dimensions::total_size;

private:
    // chart and M need to be pointers so that Tensors can be assigned to
    const MetricSpaceType *M_;
    // A tensor is defined at some specific point
    ManifoldPoint<MetricSpaceType> point_;
    const ChartType *chart_;
    ContainerType components_;

public:

    EIGEN_MAKE_ALIGNED_OPERATOR_NEW

    /** \brief Construct from components given in the coordinate basis of chart
     *
     *  \param point the point on the manifold this tensor is associated with
     *
     *  \param chart the \p components are given with respect to the coordinate
     *  basis of this chart
     *
     *  \param components the components of this tensor given as the correct
     *  container.
     *
     */
    Tensor(const PointType &point, const ChartType &chart,
           const ContainerType &components)
    : M_(&chart.M), point_(point), chart_(&chart), components_(components) {
        if (&point.M() != M_) { // point is on the correct manifold
            throw std::runtime_error("point and chart have different M");
        }
    }

    /** \brief Construct from components given in the coordinate basis of chart
     *
     *  This constructor accepts the components as a nested initializer list.
     *  The initializer list works like the one used in 
     *  \c Eigen::Tensor::setValues, i.e. for a rank N tensor the list is nested
     *  N times, with the most deeply nested list corresponding to the last 
     *  index of the tensor.
     *
     *  \param point the point on the manifold this tensor is associated with
     *
     *  \param chart the \p components are given with respect to the coordinate
     *  basis of this chart
     *
     *  \param components the components of this tensor given as a nested
     *  initializer list.
     *  
     *
     */
    Tensor(const PointType &point, const ChartType &chart,
           typename internal::nested_double_init_list<rank>::type components)
    : M_(&chart.M), point_(point), chart_(&chart) {
        if (&point.M() != M_) { // point is on the correct manifold
            throw std::runtime_error("point and chart have different M");
        }
        components_.setValues(components);
    }

    /** \brief Construct from components given in a local frame.
     *
     *  The frame is given by an object of type `FrameType` and needs
     *  to define the methods
     *  \code
     *  const PointType& point() const;
     *  ArcMatrix<D> transformation_from_basis(const ChartType &chart) const;
     *  \endcode
     *  which give the point where the frame is defined and the transformation
     *  matrix for tangent vectors from the frame basis to the basis of `chart`.
     *
     *  One example of a local frame is given by `LorentzFrame`.
     *
     *  \param frame a local frame.
     *  \param components the components of the tensor with respect to the basis
     *  of the given frame, given in a container of the correct type.
     *  \tparam FrameType the type of the local frame.
     */
    template <typename FrameType>
    Tensor(const FrameType &frame, const ContainerType &components)
    : M_(&frame.point().M()),
      point_(frame.point()),
      chart_(&M_->get_best_chart(point_)) {

        ArcMatrix<D> tmat = frame.transformation_from_basis(*chart_);

        ArcMatrix<D> inv_tmat;
        // The inverse jacobian is only used if there are Cov indices
        if (!internal::all_indices_same_type<Cnt, Indices...>::value) {
            inv_tmat = tmat.inverse();
        }

        components_ = apply_transformation(components, tmat, inv_tmat);
    }

    /** \brief Construct from components given in a local frame.
     *
     *  For the requirements on `FrameType`, 
     *  see `Tensor(const FrameType&, const ContainerType&)`.
     *
     *  \param frame a local frame.
     *  \param components the components of the tensor with respect to the basis
     *  of the given frame, given as a nested initializer list.
     *  \tparam FrameType the type of the local frame.
     */
    template <typename FrameType>
    Tensor(const FrameType &frame,
           typename internal::nested_double_init_list<rank>::type components)
    : M_(&frame.point().M()),
      point_(frame.point()),
      chart_(&M_->get_best_chart(point_)) {

        ArcMatrix<D> tmat = frame.transformation_from_basis(*chart_);
        ArcMatrix<D> inv_tmat = tmat.inverse();
        // Store the given components temporarily
        components_.setValues(components);
        // Set the correct components
        components_ = apply_transformation(components_, tmat, inv_tmat);
    }

    /** \brief Get the components in the specified chart.
     *
     * \param out_chart the components are returned in the coordinate basis of
     * this chart.
     * \return The components of the tensor in \c out_chart
     */
    ContainerType components(const ChartType &out_chart) const {
        // if the components are already in the requested chart, just return
        // them
        if (chart_ == &out_chart) { return components_; }
        // Otherwise compute them in the new chart

        ArcMatrix<D> jacmat, inv_jacmat;

        // The jacobian is only used if there are Cnt indices
        if (!internal::all_indices_same_type<Cov, Indices...>::value) {
            jacmat = M_->jacobian(point_, *chart_, out_chart);
        }
        // The inverse jacobian is only used if there are Cov indices
        if (!internal::all_indices_same_type<Cnt, Indices...>::value) {
            // the inverse jacobian is the jacobian of the transition in the
            // other direction, but if we already had the jacobian it's better
            // to just invert it
            if (!internal::all_indices_same_type<Cov, Indices...>::value) {
                inv_jacmat = jacmat.inverse();
            } else {
                inv_jacmat = M_->jacobian(point_, out_chart, *chart_);
            }
        }

        return apply_transformation(components_, jacmat, inv_jacmat);
    }

    /** \brief Get the components in the given local frame.
     *
     *  For the requirements on `FrameType`, 
     *  see `Tensor(const FrameType&, const ContainerType&)`.
     *
     * \param frame the components are returned in the basis of this frame.
     * \return Components of the tensor in the given frame.
     * \tparam FrameType the type of the local frame.
     */
    template <typename FrameType>
    ContainerType components(const FrameType &frame) const {
        if (point_ != frame.point()) {
            throw std::runtime_error("Frame and Tensor at different points!");
        }
        ArcMatrix<D> inv_tmat = frame.transformation_from_basis(*chart_);
        ArcMatrix<D> tmat;
        // The jacobian is only used if there are Cnt indices
        if (!internal::all_indices_same_type<Cov, Indices...>::value) {
            tmat = inv_tmat.inverse();
        }
        return apply_transformation(components_, tmat, inv_tmat);
    }

    /** \brief Get the components in the internally used chart.
     *
     * \warning Abstraction leak.
     *
     * \return Components of the tensor in the internally used chart.
     */
    ContainerType internal_components() const {
        return components_;
    }

    /** \brief Return a copy of the tensor using a different chart
     * internally, both for base point and tensor components.
     *
     * \param chart The chart to be used internally by the copy.
     *
     * \return A copy of this Tensor using the given chart internally
     * everywhere.
     */
    CurrentType copy_with_chart(const ChartType &chart) const {
        return {point_.copy_with_chart(chart), chart, components(chart)};
    };

    /** \brief Get the point this tensor is located at.
     */
    const PointType &point() const { return point_; }

    /** \brief Get the manifold this tensor is defined on.
     */
    const MetricSpaceType &M() const { return *M_; }

    /** \brief Get the chart used internally for storing the components.
     */
    const ChartType &chart() const { return *chart_; }

    // Operations
    // indices are counted from 0

    // Raising and lowering indices

    /** \brief Raise the given index.
     *
     *  Converts a \c Cov index to \c Cnt by performing the correct contraction
     *  with the metric.
     *
     * \tparam index the position of the index to raise.
     * \return A new tensor with the given index raised.
     */
    template <size_t index>
    auto raise_index() const {
        static_assert(rank > 0, "Only available for tensors of rank >= 1.");
        // index+index_offset is used since there are extra template parameters
        // in the beginning
        static_assert(
            std::is_same<
                Cov, typename internal::tl_get<index + index_offset,
                                               CurrentTypeList>::type>::value,
            "Index is already up!");

        // Conversions between Tensor and TypeList are needed since Tensor has
        // obligatory type paremeters,
        // but the list manipulation metafunctions can need empty lists also
        using RetType = typename internal::tl_rename<
            typename internal::tl_set<index + index_offset, Cnt,
                                      CurrentTypeList>::type,
            Tensor>::type;

        EigenRk2Tensor<D> g_inv =
            M_->inverse_metric_components(point_, *chart_);

        // The ops need a temporary, so make it explicit to avoid mallocs in 
        // Eigen
        typename RetType::ContainerType temp = g_inv.contract(
            components_, Eigen::array<IndexPair, 1>{IndexPair(0, index)});

        // Need to shuffle the results indices to get the new index to the
        // correct position.
        // TODO: This could/should be eliminated for rank-1, since it's probably
        // not completely free.
        typename RetType::ContainerType ret_components =
            temp.shuffle(internal::raise_lower_shuffle<index, rank>{}.shuffle);

        return RetType{point_, *chart_, ret_components};
    }

    /** \brief Lower the given index.
     *
     *  Converts a \c Cnt index to \c Cov by performing the correct contraction
     *  with the metric.
     *
     * \tparam index the position of the index to lower.
     * \return A new tensor with the given index lowered.
     */
    template <size_t index>
    auto lower_index() const {
        static_assert(rank > 0, "Only available for tensors of rank >= 1.");
        static_assert(
            std::is_same<
                Cnt, typename internal::tl_get<index + index_offset,
                                               CurrentTypeList>::type>::value,
            "Index is already down!");

        using RetType = typename internal::tl_rename<
            typename internal::tl_set<index + index_offset, Cov,
                                      CurrentTypeList>::type,
            Tensor>::type;

        EigenRk2Tensor<D> g = M_->metric_components(point_, *chart_);

        typename RetType::ContainerType temp = g.contract(
            components_, Eigen::array<IndexPair, 1>{IndexPair(0, index)});

        typename RetType::ContainerType ret_components =
            temp.shuffle(internal::raise_lower_shuffle<index, rank>{}.shuffle);

        return RetType{point_, *chart_, ret_components};
    }

    /** \brief Contract two indices of the tensor.
     *
     *  Contract a \c Cov and a \c Cnt index.
     *
     * Example assuming a manifold of type \c MS:
     * \code
     * Tensor<MS, Cov, Cnt, Cnt> t1(...);
     *
     * Tensor<MS, Cnt> t2 = t1.contract_indices<0,1>(); // Works
     * 
     * Tensor<MS, Cov> t3 = t1.contract_indices<1,2>(); // Compile error
     * \endcode
     *
     *  \tparam index1, index2 the indices to contract.
     *  Must have \p index1 < \p index2.
     *
     *  \return A new tensor giving the result of the contraction. 
     */
    template <size_t index1, size_t index2>
    auto contract_indices() const {
        static_assert(rank > 1, "Only available for tensors of rank >= 2.");
        static_assert(index1 < index2, "Need to have index1 < index2!");
        static_assert(
            !std::is_same<
                typename internal::tl_get<index1 + index_offset,
                                          CurrentTypeList>::type,
                typename internal::tl_get<index2 + index_offset,
                                          CurrentTypeList>::type>::value,
            "The contracted indices need to be of different types!");

        using RetType = typename internal::tl_rename<
            typename internal::tl_remove<
                index1 + index_offset,
                typename internal::tl_remove<index2 + index_offset,
                                             CurrentTypeList>::type>::type,
            Tensor>::type;

        typename RetType::ContainerType ret_components;
        ret_components.setZero();
        // XXX: Future versions of Eigen will have a trace operation built-in,
        // should use that when available.
        for (int i = 0; i < D; ++i) {
            auto val = components_.chip(i, index2).chip(i, index1);
            ret_components += val;
        }
        return RetType{point_, *chart_, ret_components};
    }

    /** \brief Check that another tensor is at the same point.
     *
     *  Throws an exception if the tensors are at different points.
     *
     *  \param t2 a tensor.
     */
    template <typename... Indices2>
    void assert_at_same_point(
        const Tensor<MetricSpaceType, Indices2...> &t2) const {
        if (point_ != t2.point()) {
            std::stringstream ss;
            // XXX: Can't use the operator<< for tensor since it calls
            // dot_product, and that causes infinite recursion if point_
            // contains
            // nans.
            ss << "Operations between tensors at different base points are not"
                  " defined.\nthis->point() = "
               << point_ << "\nt2.point() = " << t2.point();
            throw std::domain_error(ss.str());
        }
    }

    /** \brief Tensor product with another tensor.
     *
     *  The resulting tensor has indices that are a concatenation of this
     *  tensors indices and the indices of \p t2.
     *
     * \param t2 a tensor.
     * \return A new tensor which is the product of \c this and t2.
     */
    template <typename... Indices2>
    auto tprod(const Tensor<MetricSpaceType, Indices2...> &t2) const {
        assert_at_same_point(t2);
        return tprod_impl(t2);
    }

    // Separate method for when we want to contract immediately, e.g. dot
    // product
    // TODO: Also for common operations where index raising lowering is needed
    // before the contraction.
    // This might also allow us to take advantage of the lazy evaluation of
    // Eigen tensor expressions.

    /** \brief Compute the tensor product and contract two indices.
     *
     * A more optimized method equivalent to
     * \code
     *  (*this).tprod(t2).contract_indices<index1, rank_of_this + index2>()
     * \endcode
     *
     * Example assuming a manifold of type \c MS:
     * \code
     * Tensor<MS, Cov, Cnt> t1(...);
     * Tensor<MS, Cov> t2(...);
     *
     * Tensor<MS, Cov> t3 = t1.contract_product<1,0>(t2);
     * \endcode
     *
     * For computing vector dot products see also `::dot_product`, which
     * allows for simpler syntax.
     *
     * \param t2 a tensor.
     * \return The contracted tensor product of \p this and \p t2.
     *
     * \tparam index1 the number of the index to contract on this tensor.
     * \tparam index2 the number of the index to contract on \p t2.
     */
    template <size_t index1, size_t index2, typename... Indices2>
    auto contract_prod(const Tensor<MetricSpaceType, Indices2...> &t2) const {
        static_assert(
            !std::is_same<
                typename internal::tl_get<index1 + index_offset,
                                          CurrentTypeList>::type,
                typename internal::tl_get<
                    index2, internal::TypeList<Indices2...>>::type>::value,
            "The contracted indices need to be of different types!");
        assert_at_same_point(t2);

        using RetType = typename internal::tl_rename<
            typename internal::tl_remove<
                index_offset + index1,
                typename internal::tl_remove<
                    index_offset + rank + index2,
                    internal::TypeList<MetricSpaceType, Indices...,
                                       Indices2...>>::type>::type,
            Tensor>::type;

        const ChartType &new_chart =
            M_->choose_better_chart(point_, chart(), t2.chart());

        typename RetType::ContainerType contracted =
            components(new_chart).contract(
                t2.components(new_chart),
                Eigen::array<IndexPair, 1>{IndexPair(index1, index2)});

        return RetType(point_, new_chart, contracted);
    }

    /**
     * \brief Generate rhs of equation of motion for parallel transport.
     *
     *  \param chart the chart in which to compute the derivative.
     *  \param contracted_Gamma is \f$ {\Gamma^\alpha}_{\mu \nu} k^\nu \f$,
     *  with \f$ k^\nu \f$ the tangent vector of the curve.
     *
     *  \return The derivative components in the given chart.
     */
    ContainerType transport_derivatives(
        const ChartType &chart,
        const EigenRk2Tensor<D> &contracted_Gamma) const {
            return transport_derivatives_impl(chart, contracted_Gamma);
    }

    // Operator overloads

    // Printing function for tensors
    // Use enable_if hacking to get different behaviour for scalars, vectors
    // and general tensors

    // Scalars
    template <typename T = CurrentType>
    friend std::enable_if_t<(T::rank==0), std::ostream &> operator<<(
        std::ostream &os, const CurrentType &t) {
        os << "Scalar" << std::endl
           << "Base point: " << t.point_ << std::endl
           << "Value: " << t.components_(0);
        return os;
    }

    // Vectors
    template <typename T = CurrentType>
    friend std::enable_if_t<
        std::is_same<T, Tensor<MetricSpaceType, Cnt>>::value, std::ostream &>
    operator<<(std::ostream &os, const CurrentType &t) {

        os << vector_likeness(t) << " tangent vector" << std::endl
           << "Base point: " << t.point_ << std::endl
           << "Components [" << *t.chart_
           << "]: " << eigen_tensor_to_vector<D>(t.components_).transpose()
           << std::endl
           << "Square norm: " << dot_product(t, t);
        return os;
    }

    template <typename T = CurrentType>
    friend std::enable_if_t<
        std::is_same<T, Tensor<MetricSpaceType, Cov>>::value, std::ostream &>
    operator<<(std::ostream &os, const CurrentType &t) {

        os << vector_likeness(t) << " cotangent vector" << std::endl
           << "Base point: " << t.point_ << std::endl
           << "Components [" << *t.chart_
           << "]: " << eigen_tensor_to_vector<D>(t.components_).transpose()
           << std::endl
           << "Square norm: " << dot_product(t, t);
        return os;
    }

    // General tensors
    template <typename T = CurrentType>
    friend std::enable_if_t<(T::rank > 1), std::ostream &> operator<<(
        std::ostream &os, const CurrentType &t) {
        // use the transformation between Indices and integers
        // to loop over Indices and print their representations.
        using index_seq =
            typename internal::index_int_sequence<Indices...>::type;

        os << "Tensor [";
        mpl::for_each<index_seq>([&os](int index_integer) {
            if (index_integer > 0) {
                os << " " << Cnt::repr;
            } else if (index_integer < 0) {
                os << " " << Cov::repr;
            }
        });
        os << " ]" << std::endl
           << "Base point: " << t.point_ << std::endl
           << "Components [" << *t.chart_ << "]: " << std::endl
           << t.components_;

        return os;
    }

    CurrentType operator+(const CurrentType &t2) const {
        assert_at_same_point(t2);
        const ChartType &new_chart =
            M_->choose_better_chart(point_, chart(), t2.chart());

        return {point_, new_chart,
                components(new_chart) + t2.components(new_chart)};
    }

    CurrentType operator-(const CurrentType &t2) const {
        assert_at_same_point(t2);
        const ChartType &new_chart =
            M_->choose_better_chart(point_, chart(), t2.chart());

        return {point_, new_chart,
                components(new_chart) - t2.components(new_chart)};
    }

    CurrentType operator-() const { return {point_, *chart_, -components_}; }

    CurrentType operator*(double a) const {
        return {point_, *chart_, a * components_};
    }

    friend CurrentType operator*(double a, const CurrentType &t) {
        return t * a;
    }

    CurrentType operator/(double a) const {
        return {point_, *chart_, components_ / a};
    }

    CurrentType &operator+=(const CurrentType &t2) {
        assert_at_same_point(t2);

        components_ += t2.components(*chart_);
        return *this;
    }

    CurrentType &operator-=(const CurrentType &t2) {
        assert_at_same_point(t2);

        components_ -= t2.components(*chart_);
        return *this;
    }

    CurrentType &operator*=(double a) {
        components_ = components_ * a;
        return *this;
    }

    CurrentType &operator/=(double a) {
        components_ = components_ / a;
        return *this;
    }

    bool operator==(const CurrentType& t2) const {
        if (point_ != t2.point()) {
            return false;    
        }
        // loop over the components directly instead of using Eigen functions
        // to allow short circuiting
        auto t2comp = t2.components(*chart_);
        for (size_t i = 0; i < component_count; ++i) {
            if (components_(i) != t2comp(i)) return false;    
        }

        return true;
    }

    // Implicit conversion to double for tensors with 0 indices
    // Needs this weird template hacking to make use of SFINAE
    template <typename T = CurrentType>
    operator std::enable_if_t<std::is_same<T, Tensor<MetricSpaceType>>::value,
                              double>() const {
        static_assert(std::is_same<T, CurrentType>::value,
                      "Somethings wrong with the templates");
        return components_(0);
    }

private:
    // TODO: for tangent and cotangent vectors using Eigen matrix operations
    // might be faster, but the difference can be small since a new
    // Eigen::Tensor
    // needs to be created at the end anyway.
    // Maybe add special case implementations of this method.
    // Same goes for some other operations involving matrix products.

    // compute the transformed components in a transformation given by a
    // Jacobian-like transformation matrix.
    // SFINAE to handle rank-0 (scalar) data separately.
    // On some compilers compilation fails otherwise, as the tensor contraction
    // calls (that end up never actually being called) result in instantiation 
    // of array types with size ULLONG_MAX due to underflow in Eigen internals.
    // At least clang 7 refuses to deal with such large arrays.
    template <typename T = CurrentType>
    static std::enable_if_t<T::rank != 0, ContainerType> apply_transformation(
        const ContainerType &components, ArcMatrix<D> &jacmat,
        ArcMatrix<D> &inv_jacmat) {
        // Use the same trick as with the parallel transport derivatives
        using index_seq =
            typename mpl::transform<mpl::vector<Indices...>,
                                    mpl::range_c<int, 1, rank + 1>,
                                    internal::index_sign_transformer>::type;

        // compute the components  in the requested chart
        ContainerType new_components = components;

        const EigenRk2Tensor<D> jac =
            Eigen::TensorMap<Eigen::Tensor<double, 2>>(jacmat.data(), D, D);
        const EigenRk2Tensor<D> inv_jac =
            Eigen::TensorMap<Eigen::Tensor<double, 2>>(inv_jacmat.data(), D, D);

        // for each index, compute new coefficients
        auto f = [&new_components, &jac, &inv_jac](int index_pos) -> void {
            // for shuffling the indices back to the correct positions
            // after contraction
            Eigen::array<size_t, rank> shuffle;

            if (index_pos < 0) {
                // negative => index is Cov, use J^-1, contract with
                // first index
                size_t index = -index_pos - 1;

                // The contraction (J^-1)^a_b T^i_ja^k gives a tensor
                // with indices T'_b^i_j^k, so the contracted index must
                // be shuffled back to place to give the correct
                // expression T'^i_jb^k. Indices that came before the
                // contracted index need to be moved back a step, and the
                // new 0th index goes to the position of the contracted
                // index. The i-th dimension of output corresponds to the
                // shuffle[i]-th dimension of input in the shuffle
                // method.
                for (size_t i = 0; i < index; ++i) {
                    shuffle[i] = i + 1;
                }
                for (size_t i = index + 1; i < rank; ++i) {
                    shuffle[i] = i;
                }
                shuffle[index] = 0;
                new_components = inv_jac
                                     .contract(new_components,
                                               Eigen::array<IndexPair, 1>{
                                                   IndexPair(0, index)})
                                     .shuffle(shuffle);
            } else {
                // positive => index is Cnt, use J, contract
                // with second index
                size_t index = index_pos - 1;
                for (size_t i = 0; i < index; ++i) {
                    shuffle[i] = i + 1;
                }
                for (size_t i = index + 1; i < rank; ++i) {
                    shuffle[i] = i;
                }
                shuffle[index] = 0;

                new_components = jac.contract(new_components,
                                              Eigen::array<IndexPair, 1>{
                                                  IndexPair(1, index)})
                                     .shuffle(shuffle);
            }
        };

        boost::mpl::for_each<index_seq>(f);
        return new_components;
    }

    template <typename T = CurrentType>
    static std::enable_if_t<T::rank == 0, ContainerType> apply_transformation(
        const ContainerType &components, ArcMatrix<D> &, ArcMatrix<D> &) {

        return components;
    }

    // Same SFINAE considerations as above for apply_transformation.
    template <typename T = CurrentType>
    std::enable_if_t<T::rank != 0, ContainerType> transport_derivatives_impl(
        const ChartType &chart,
        const EigenRk2Tensor<D> &contracted_Gamma) const {

        ContainerType comp = components(chart);
        ContainerType ret;
        ret.setZero();

        // Container for temporary values on the stack to avoid unnecessary
        // mallocs in Eigen internals.
        ContainerType temp;

        // Generate a sequence of integer index positions, with negative values
        // signifying Cov and positive Cnt. Need to offset the list so that it
        // also
        // works with the 0th index.
        using index_seq =
            typename mpl::transform<mpl::vector<Indices...>,
                                    mpl::range_c<int, 1, rank + 1>,
                                    internal::index_sign_transformer>::type;
        // function that is called on each index, adds the correct term to the
        // return value
        auto f = [&comp, &ret, &contracted_Gamma, &temp](int index_pos) {
            auto index_shuffle = [](size_t index) {
                Eigen::array<size_t, rank> shuffle;
                // The contraction with Gamma brings the index at `index` to
                // position 0, and needs to be shuffled back.
                // The indices between 0 and `index` need also to be shifted
                // downwards.
                for (size_t i = 0; i < index; ++i) {
                    shuffle[i] = i + 1;
                }

                shuffle[index] = 0;

                for (size_t i = index + 1; i < rank; ++i) {
                    shuffle[i] = i;
                }

                return shuffle;
            };

            if (index_pos < 0) {
                // index is Cov, contract with first index of Gamma
                size_t index = -index_pos - 1;

                // Complicated Eigen tensor expressions can lead to temporary
                // heap allocations, so store intermediate results in the
                // temporary value on the stack instead.
                temp = contracted_Gamma.contract(
                    comp, Eigen::array<IndexPair, 1>{IndexPair(0, index)});
                ret += temp.shuffle(index_shuffle(index));
            }

            else { // index is Cnt, contract with second index of Gamma
                size_t index = index_pos - 1;

                temp = contracted_Gamma.contract(
                    comp, Eigen::array<IndexPair, 1>{IndexPair(1, index)});
                ret -= temp.shuffle(index_shuffle(index));
            }
        };

        // call the function on each index
        boost::mpl::for_each<index_seq>(f);
        return ret;
    }

    template <typename T = CurrentType>
    std::enable_if_t<T::rank == 0, ContainerType> transport_derivatives_impl(
        const ChartType &,
        const EigenRk2Tensor<D> &) const {
        ContainerType ret;
        ret.setZero();
        return ret;
    }

    // Similar problems with contraction when one of the types is a a scalar,
    // so SFINAE the generic one and add special cases for scalars
    template <typename T = CurrentType, typename... Indices2,
              typename = std::enable_if_t<(T::rank > 0), void>>
    auto tprod_impl(const Tensor<MetricSpaceType, Indices2...> &t2) const {
        const ChartType &new_chart =
            M_->choose_better_chart(point_, chart(), t2.chart());

        using RetType = Tensor<MetricSpaceType, Indices..., Indices2...>;
        // The tensor product is a contraction of no indices
        typename RetType::ContainerType ret_comp =
            components(new_chart).contract(t2.components(new_chart),
                                           Eigen::array<IndexPair, 0>{});
        return RetType{point_, new_chart, ret_comp};
    }
    // need to have template parameters in different order to overload
    template<typename T = CurrentType,
             typename = std::enable_if_t<T::rank == 0, void>,
             typename... Indices2>
    auto tprod_impl(const Tensor<MetricSpaceType, Indices2...> &t2) const {

        return t2 * double(*this);
    }

    auto tprod_impl(const Tensor<MetricSpaceType> &t2) const {

        return *this * double(t2);
    }

};


} // namespace arcmancer

