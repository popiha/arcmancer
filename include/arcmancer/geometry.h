#pragma once
/** \file
 * \brief Convenience header for the general geometry headers
 *
 * This header brings in all the geometry headers except for `metric_space.h`,
 * which is only needed when defining spacetimes and manifolds.
 *
 * \ingroup geometry
 */
#include "arcmancer/geometry/geodesic.h"
#include "arcmancer/geometry/line_integral.h"
#include "arcmancer/geometry/lorentz_frame.h"
#include "arcmancer/geometry/normal_coordinates.h"
#include "arcmancer/geometry/parametrized_curve.h"
#include "arcmancer/geometry/parametrized_point.h"
#include "arcmancer/geometry/surface.h"
#include "arcmancer/geometry/tensor.h"
#include "arcmancer/geometry/types.h"
#include "arcmancer/geometry/utils.h"
