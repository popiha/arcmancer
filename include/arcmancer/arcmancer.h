#pragma once
/** \file
 * \brief Convenience header for the library
 *
 * Brings in the commonly used parts of the library.
 * Does not include specific implementations of spacetimes/manifolds and 
 * radiation models, or some interpolation utilities.
 *
 */
#include "arcmancer/appinfo.h"
#include "arcmancer/configuration.h"
#include "arcmancer/general_utils.h"
#include "arcmancer/geometry.h"
#include "arcmancer/hdf5_utils.h"
#include "arcmancer/image_plane.h"
#include "arcmancer/log.h"
#include "arcmancer/radiation/radiation_transfer.h"
#include "arcmancer/units.h"
