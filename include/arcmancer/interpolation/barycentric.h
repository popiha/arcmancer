#pragma once

#include <algorithm>
#include <array>
#include <functional>
#include <iterator>
#include <stdexcept>
#include <type_traits>
#include <utility>
#include <vector>

#include <Eigen/Dense>
#include <unsupported/Eigen/CXX11/Tensor>

#include "arcmancer/general_utils.h"
#include "arcmancer/geometry/normal_coordinates.h"
#include "arcmancer/geometry/types.h"
#include "arcmancer/log.h"

#include "arcmancer/other_spaces/tensor_space.h"
#include "arcmancer/other_spaces/velocity_space.h"

namespace arcmancer {
/** \brief Tools for interpolating both gridded and ungridded
 * multidimensional data.
 *
 * \ingroup utils
 */
namespace interpolation {

/** \brief Used to set the type of barycentric interpolation method to
 * use.
 */
enum class BarycentricInterpolationMethod {
    numerical, ///< full accuracy: normal coordinates and parallel propagation
               /// done numerically
    order_2,   ///< 2nd order accuracy: normal coordinates and parallel
               ///< propagation
               /// analytically to 2nd order
    coordinate_only, /**< least accuracy: using coordinate differences in the
                       chart of interpolation point, instead of normal
                       coordinates, no parallel propagation */
};

inline std::ostream &operator<<(std::ostream &os,
                                BarycentricInterpolationMethod method) {
    if (method == BarycentricInterpolationMethod::numerical)
        os << "numerical";
    else if (method == BarycentricInterpolationMethod::order_2)
        os << "order_2";
    else if (method == BarycentricInterpolationMethod::coordinate_only)
        os << "coordinate_only";
    return os;
}

namespace internal {

template <int dim>
class RNCBarycenterProblem {
public:
    RNCBarycenterProblem(const aligned_vector<ArcVector<dim>> &rncs) :
        _rncs(rncs){};

    //
    // Compatibility with Eigen unsupported nonlinear optimization
    int operator()(const Eigen::VectorXd &xin, Eigen::VectorXd &fvec) const {
        assert(xin.size() == dim && fvec.size() == 1);

        fvec(0) = 0;
        for (const auto &dx : _rncs) {
            fvec(0) += std::exp(xin.dot(dx));
        }

        //std::cout << "xin: " << xin.transpose() << " => fvec: " << fvec.transpose() << "\n";

        return 0;
    }

    int df(const Eigen::VectorXd &xin, Eigen::MatrixXd &jac) const {
        assert(jac.rows() == 1 && jac.cols() == dim);

        jac.setZero(1, dim);
        for (const auto &dx : _rncs) {
            jac += std::exp(xin.dot(dx)) * dx.transpose();
        }
        return 0;
    }

    int inputs() const { return dim; }
    int values() const { return 1; }

private:
    const aligned_vector<ArcVector<dim>> _rncs;
};

template <typename MetricSpaceType>
class VelocityRNCProblem {
public:
    static constexpr long dim = MetricSpaceType::D;

    VelocityRNCProblem(
        const Chart<MetricSpaceType> &chart, const std::vector<double> bcoords,
        const aligned_vector<ManifoldPoint<MetricSpaceType>> &points,
        const BarycentricInterpolationMethod &method)
    : chart_(chart), bcoords_(bcoords), points_(points), method_(method), 
      rnc_method_(method_ == BarycentricInterpolationMethod::numerical
                         ? NormalCoordinateMethod::numerical
                         : NormalCoordinateMethod::order_2){
          vertex_rncs_.resize(bcoords.size());
    };


    //
    // Compatibility with Eigen unsupported nonlinear optimization
    int operator()(const Eigen::VectorXd &xin, Eigen::VectorXd &fvec) const {
        assert(xin.size() == dim && fvec.size() == dim);

        ArcVector<dim> coords = xin;
        auto point = make_point(chart_, eigen_vector_to_tensor<dim>(coords));

        aligned_vector<EigenRk1Tensor<dim>> vertex_rncs_local(bcoords_.size());

// TODO: Does it pay off to have OpenMP parallelization over such a
// potentially deeply buried inner loop?
//#pragma omp parallel for
        for (size_t i = 0; i < vertex_rncs_local.size(); i++) {
            vertex_rncs_local[i] = riemann_normal_coordinates(
                    chart_, point, points_[i], rnc_method_);
        }

        fvec.setZero(dim);

        for (size_t i = 0; i < vertex_rncs_local.size(); i++) {
            fvec += bcoords_[i] * eigen_tensor_to_vector<dim>(vertex_rncs_local[i]);
        }

        //std::cout << "VelRNCProb: xin: " << xin.transpose() << " => fvec: " << fvec.transpose() << "\n";

        return 0;
    }

private:
    const Chart<MetricSpaceType> &chart_;
    const std::vector<double> bcoords_;
    const aligned_vector<ManifoldPoint<MetricSpaceType>> points_;
    const BarycentricInterpolationMethod method_;
    const NormalCoordinateMethod rnc_method_;
    aligned_vector<EigenRk1Tensor<dim>> vertex_rncs_;
};

template <typename MetricSpaceType, typename DataType>
DataType backprop_numerical(const Chart<MetricSpaceType> &chart,
                            const ManifoldPoint<MetricSpaceType> &point,
                            const DataType &vertex_data,
                            const ArcVector<MetricSpaceType::D> &rnc) {

    using MST = MetricSpaceType;

    // propagate a geodesic from z0 to z1, then propagate the data
    // back
    Geodesic<MST> gamma1({point, chart, eigen_vector_to_tensor<MST::D>(rnc)});

    // need a strictening of tolerance here for the
    // propagation

    auto &conf1 = gamma1.configuration().propagation;
    conf1.tolerance =
        point.M().configuration.barycentric_interpolation.tolerance;

    auto res1 = gamma1.compute(1);
    if (res1 != Geodesic<MST>::ComputationResult::success) {
        Log::log()->warn(
            "backprop_numerical: Geodesic computation unsuccessful: {}", res1);
    }

    // Check that gamma1.back() is now actually
    // somewhere near the corresponding vertex. This has to
    // be done in coordinates, since in semi-Riemannian
    // spaces the norm could be zero for two very different
    // points on the manifold
    {
        const EigenRk1Tensor<MST::D> dx =
            gamma1.back().point().coordinates(chart) -
            vertex_data.point().coordinates(chart);
        Eigen::Tensor<double, 0> maxerr = dx.abs().maximum();
        if (maxerr() > gamma1.num_points() * conf1.tolerance.absolute) {
            Log::log()->warn(
                "backprop_numerical: forward computed point\n{}\n"
                "not within tolerance.absolute {} of target "
                "point\n{}\ndiff {}\n",
                gamma1.back().point(), conf1.tolerance.absolute,
                vertex_data.point(), eigen_tensor_to_vector(dx).transpose());
        }
    }

    // Then go back, this time parallel propagating the
    // data.
    // XXX: The point x1 = gamma1.back() will not be at the exact
    // same position as x2 = vertex_data[i].point(). So either we
    // move the data from x2 to x1, or we start from x2 with
    // the curve tangent vector defined at x1.
    // The latter seems to work numerically better.
    // TODO: This should be investigated more thoroughly.
#if 1
    Geodesic<MST, DataType> gamma2(
        gamma1.back().tangent(),
        {gamma1.back().point(), chart, vertex_data.components(chart)});
#else
    auto z1_tangent = gamma1.back().tangent();
    Geodesic<MST, DataType> gamma2(
        {vertex_data.point(), chart, z1_tangent.components()}, vertex_data);
#endif

    // similarly, stricter tolerances on the way back
    //

    auto &conf2 = gamma2.configuration().propagation;
    conf2.tolerance =
        point.M().configuration.barycentric_interpolation.tolerance;

    auto res2 = gamma2.compute(-1);
    if (res2 != Geodesic<MST, DataType>::ComputationResult::success) {
        Log::log()->warn(
            "backprop_numerical: Geodesic computation unsuccessful: {}", res2);
    }

    // Similarly, compare front of gamma2 vs the original point
    {
        const EigenRk1Tensor<MST::D> dx =
            gamma2.front().point().coordinates(chart) -
            point.coordinates(chart);
        Eigen::Tensor<double, 0> maxerr = dx.abs().maximum();
        if (maxerr() > gamma2.num_points() * conf2.tolerance.absolute) {
            Log::log()->warn(
                "backprop_numerical: backward computed point\n{}\n"
                "not within tolerance.absolute {} of target "
                "point\n{}\ndiff {}\n",
                gamma2.front().point(), conf2.tolerance.absolute, point,
                eigen_tensor_to_vector(dx).transpose());
        }
    }

    // XXX: similarly, we need to force the parallel transported
    // data to exist precisely at the interpolation point, since the
    // geodesics probably do not exactly hit it
    DataType parallel_propagated = {
        point, chart, gamma2.front().transported().components(chart)};

    Log::log()->debug("backprop_numerical: Parallel propagated data:\n{}",
                      parallel_propagated);

    return parallel_propagated;
}

template <typename MetricSpaceType, typename DataType>
DataType backprop_order2(const Chart<MetricSpaceType> &chart,
                         const ManifoldPoint<MetricSpaceType> &point,
                         const DataType &vertex_data) {

    using MST = MetricSpaceType;
    using CoordinateType = EigenRk1Tensor<MST::D>;

    // To second order, we can just use the usual derivatives,
    // evaluated at point and using coordinate differences to
    // substitute for tangent vectors
    const CoordinateType dz =
        vertex_data.point().coordinates(chart) - point.coordinates(chart);
    const EigenRk2Tensor<MST::D> contracted_Gamma =
        point.M().christoffel_symbols_contracted(point, chart, dz);
    auto derivatives =
        vertex_data.transport_derivatives(chart, contracted_Gamma);

    // the resulting components at point are then just the negative of
    // derivatives added to components at vertex
    DataType parallel_propagated = {
        point, chart, vertex_data.components(chart) - derivatives};

    // Log::log()->debug("backprop_order2: Parallel propagated data {}:\n{}",
    //                  it - data_begin, parallel_propagated);

    return parallel_propagated;
}

template <typename MetricSpaceType, typename InputIterator>
inline aligned_vector<typename std::iterator_traits<InputIterator>::value_type>
backpropagate_data(
    const Chart<MetricSpaceType> &chart,
    const ManifoldPoint<MetricSpaceType> &point, InputIterator data_begin,
    InputIterator data_end,
    const aligned_vector<ArcVector<MetricSpaceType::D>> &vertex_rncs,
    const BarycentricInterpolationMethod &method) {

    using MST = MetricSpaceType;
    using DataType = typename std::iterator_traits<InputIterator>::value_type;

    aligned_vector<DataType> parprop_data;
    parprop_data.reserve(vertex_rncs.size());

    auto back_inserter_it = std::back_inserter(parprop_data);

    if (std::is_same<DataType, Tensor<MST>>::value) {
        // Scalar data, just shift the base point
        std::transform(data_begin, data_end, back_inserter_it,
                       [&](const auto &vertex_data) -> DataType {
                           return {point, chart, vertex_data.components(chart)};
                       });
        return parprop_data;
    }

    // Data is tensorial

    if (method == BarycentricInterpolationMethod::numerical) {
        std::transform(
            data_begin, data_end, vertex_rncs.begin(), back_inserter_it,
            [&](const auto &vertex_data, const auto &rnc) {
                return backprop_numerical(chart, point, vertex_data, rnc);
            });
    } else if (method == BarycentricInterpolationMethod::order_2) {
        std::transform(data_begin, data_end, back_inserter_it,
                       [&](const auto &vertex_data) {
                           return backprop_order2(chart, point, vertex_data);
                       });
    } else if (method == BarycentricInterpolationMethod::coordinate_only) {
        // no parallel propagation -- we have to  manually move each
        // vertex data structure to the interpolation position
        std::transform(data_begin, data_end, back_inserter_it,
                       [&](const auto &vertex_data) -> DataType {
                           return {point, chart, vertex_data.components(chart)};
                       });
    } else {
        // unknown method
        throw std::invalid_argument("backpropagate_data: unsupported method");
    }

    if (Log::log()->level() <= Log::level::debug) {
        Log::log()->debug("backpropagate_data: method = {}", method);
        for (size_t i = 0; i < parprop_data.size(); ++i) {
            Log::log()->debug(
                "backpropagate_data: Parallel propagated data {}:\n{}", i,
                parprop_data[i]);
        }
    }

    return parprop_data;
}

template <typename MetricSpaceType, typename InputIterator>
inline std::pair<
    std::vector<double>,
    aligned_vector<typename std::iterator_traits<InputIterator>::value_type>>
get_bcoords_and_data(const Chart<MetricSpaceType> &chart,
                     const ManifoldPoint<MetricSpaceType> &point,
                     InputIterator data_begin, InputIterator data_end,
                     const BarycentricInterpolationMethod &method) {

    using DataType = typename std::iterator_traits<InputIterator>::value_type;

    auto bcoords_and_rncs = barycentric_coordinates(
            chart, point, data_begin, data_end, method);

    auto &bcoords = bcoords_and_rncs.first;
    auto &vertex_rncs = bcoords_and_rncs.second;

    aligned_vector<DataType> parprop_data = internal::backpropagate_data(
        chart, point, data_begin, data_end, vertex_rncs, method);

    return {bcoords, parprop_data};
}


} // namespace internal


/** Find the barycentric coordinates of \c point with respect to
 * vertices given via iterators \c data_begin \c data_end.
 *
 * \tparam MetricSpaceType Type of the manifold
 * \tparam InputIterator Type of the vertex data iterator
 *
 * \param chart The coordinate chart to use for all numerical work. The
 * result should be mostly independent of this choice.
 * \param point Where to compute the interpolant at.
 * \param data_begin Iterator to start of vertex data
 * \param data_end Iterator to end of vertex data
 * \param method Which \ref BarycentricInterpolationMethod to use.
 *
 * \return A pair of the barycentric coordinates of \c point as a
 * std::vector, and the Riemann normal coordinates of the vertices with
 * respect to the \c point.
 *
 * \ingroup interpolation
 */
template <typename MetricSpaceType, typename InputIterator>
inline std::pair<std::vector<double>, aligned_vector<ArcVector<MetricSpaceType::D>>>
barycentric_coordinates(const Chart<MetricSpaceType> &chart,
                     const ManifoldPoint<MetricSpaceType> &point,
                     InputIterator data_begin, InputIterator data_end,
                     const BarycentricInterpolationMethod &method) {
    using MST = MetricSpaceType;
    using Vector = ArcVector<MST::D>;
    using CoordinateType = EigenRk1Tensor<MST::D>;
    using DataType = typename std::iterator_traits<InputIterator>::value_type;
    using ProblemType = internal::RNCBarycenterProblem<MST::D>;

    // 1) find the riemann normal coordinates of vertices, with
    // point as origin
    // 2) use optimization to find the barycentric coordinates of point

    // 1)
    aligned_vector<Vector> vertex_rncs;

    std::transform(
        data_begin, data_end, std::back_inserter(vertex_rncs),
        [&chart, &point, &method](const DataType &vx) {
            CoordinateType rnc;

            // if using coordinate only method, then instead of
            // computing RNCs, compute coordinate differences
            if (method == BarycentricInterpolationMethod::coordinate_only) {
                rnc = vx.point().coordinates(chart) - point.coordinates(chart);
            } else {
                NormalCoordinateMethod rnc_method =
                    (method == BarycentricInterpolationMethod::numerical
                         ? NormalCoordinateMethod::numerical
                         : NormalCoordinateMethod::order_2);
                rnc = riemann_normal_coordinates(chart, point, vx.point(),
                                                 rnc_method);
            }
            return eigen_tensor_to_vector<MST::D>(rnc);
        });

    if (Log::log()->level() <= Log::level::debug) {
        Log::log()->debug("barycentric_coordinates: Found following RNCs:");

        auto it = data_begin;
        for (size_t i = 0; i < vertex_rncs.size() && it != data_end;
             i++, ++it) {

            Log::log()->debug(
                "dz {} rnc {}",
                eigen_tensor_to_vector<MST::D>(it->point().coordinates(chart) -
                                               point.coordinates(chart))
                    .transpose(),
                vertex_rncs[i].transpose());
        }
    }

    // 2)
    // first, find the betas through entropy maximization
    ProblemType problem(vertex_rncs);

    Vector beta = Vector::Zero(); // TODO: better initial guess

    Eigen::LevenbergMarquardt<ProblemType> solver(problem);

    // configure solver
    const auto &conf =
        point.M().configuration.barycentric_interpolation.optimization;

    solver.parameters.maxfev = conf.iterations;
    solver.parameters.ftol = conf.f_tol;
    solver.parameters.xtol = conf.x_tol;
    solver.parameters.gtol = conf.min_grad_norm;

    Eigen::VectorXd beta_dyn(beta.size());
    beta_dyn = beta;
    auto status = solver.minimize(beta_dyn);
    beta = beta_dyn;

    Log::log()->debug("barycentric_coordinates: LevenbergMarquardt status: {}\nfound beta: {}",
            status, beta.transpose());

    // then compute the actual barycentric coordinates
    std::vector<double> bcoords(vertex_rncs.size());
    std::transform(vertex_rncs.begin(), vertex_rncs.end(), bcoords.begin(),
                   [&beta](const Vector &rnc) -> double {
                       return std::exp(beta.dot(rnc));
                   });

    Log::log()->debug(
        "barycentric_coordinates: Found following raw barycentric "
        "coordinates:");
    for (auto &b : bcoords) {
        Log::log()->debug("{}", b);
    }

    // normalize
    const double bcoord_sum =
        std::accumulate(bcoords.begin(), bcoords.end(), 0.0);
    for (double &b : bcoords) {
        b /= bcoord_sum;
    }

    Log::log()->debug(
        "barycentric_coordinates: Sum of barycentric coordinates: {}",
        bcoord_sum);
    Log::log()->debug(
        "barycentric_coordinates: Normalized barycentric coordinates:");
    for (auto &b : bcoords) {
        Log::log()->debug("{}", b);
    }

    return {bcoords, vertex_rncs};
}

/** \brief Interpolate velocity data defined on a 4-dimensional
 * Lorentzian manifold using barycentric interpolation
 *
 * \tparam MetricSpaceType Type of the manifold
 * \tparam InputIterator Type of the vertex data iterator
 *
 * \param chart The coordinate chart to use for all numerical work. The
 * result should be mostly independent of this choice.
 * \param point Where to compute the interpolant at.
 * \param data_begin Iterator to start of vertex data
 * \param data_end Iterator to end of vertex data
 * \param method Which \ref BarycentricInterpolationMethod to use.
 *
 * \return The interpolated data.
 *
 * \ingroup interpolation
 */
template <typename MetricSpaceType, typename InputIterator>
inline std::enable_if_t<
    MetricSpaceType::is_lorentzian,
    typename std::iterator_traits<InputIterator>::value_type>
barycentric_interpolation_velocity(
    const Chart<MetricSpaceType> &chart,
    const ManifoldPoint<MetricSpaceType> &point,
    InputIterator data_begin,
    InputIterator data_end,
    const BarycentricInterpolationMethod &method) {

    using DataType = typename std::iterator_traits<InputIterator>::value_type;
    using VSpaceType = velocity_space::VelocitySpaceType<MetricSpaceType>;
    using VSpaceImmersionType =
        velocity_space::VelocitySpaceImmersion<MetricSpaceType>;
    using ProblemType = internal::VelocityRNCProblem<VSpaceType>;

    auto bcoords_and_data = internal::get_bcoords_and_data(
        chart, point, data_begin, data_end, method);

    std::vector<double> &bcoords = bcoords_and_data.first;
    auto &parprop_data = bcoords_and_data.second;

    // Need to project all the data on a velocity manifold
    tensor_space::TensorSpace<MetricSpaceType, Cnt> TM(chart, point);

    velocity_space::VelocitySpaceImmersion<MetricSpaceType> vimmersion(
        TM.canonical);
    VSpaceType VSpace(TM.canonical,
                      std::bind(&VSpaceImmersionType::immersion, &vimmersion,
                                std::placeholders::_1),
                      std::bind(&VSpaceImmersionType::jacobian, &vimmersion,
                                std::placeholders::_1),
                      std::bind(&VSpaceImmersionType::jacobian_derivative,
                                &vimmersion, std::placeholders::_1));

    // copy the configuration over
    TM.configuration = point.M().configuration;
    VSpace.configuration = point.M().configuration;

    // move the velocities to the constraint manifold as points
    aligned_vector<ManifoldPoint<VSpaceType>> vel_points;

    std::transform(parprop_data.begin(), parprop_data.end(),
                   std::back_inserter(vel_points),
                   [&VSpace](const DataType &x) {
                       return velocity_space::send_to_manifold(VSpace, x);
                       // XXX: Normalize to kill propagation induced
                       // errors?
                       //return velocity_space::send_to_manifold(VSpace, normalized(x));
                   });

    // Now need to find a point on the VelocitySpace manifold which
    // yields a set of RNCs zi for which sum bcoords[i] * zi = 0;
    ProblemType problem(VSpace.canonical, bcoords, vel_points, method);

    // initial guess for the minimzing point is phi_i * x_i
    ArcVector<VSpaceType::D> minimizer = ArcVector<VSpaceType::D>::Zero();
    for (size_t i = 0; i < vel_points.size(); i++) {
        minimizer += bcoords[i] * eigen_tensor_to_vector(vel_points[i].coordinates(VSpace.canonical));
    }
    Log::log()->debug("barycentric_interpolation_velocity: using initial minimizer value {}", minimizer.transpose());

    Eigen::HybridNonLinearSolver<ProblemType> solver(problem);

    // configure solver
    const auto &conf =
        point.M().configuration.barycentric_interpolation.optimization;
    solver.parameters.maxfev = conf.iterations;
    solver.parameters.xtol = conf.x_tol;

    Eigen::VectorXd xdyn(minimizer.size());
    xdyn = minimizer;
    auto status = solver.solveNumericalDiff(xdyn);
    minimizer = xdyn;

    //std::cout << "status: " << status << "\nminimizer:\n" << minimizer << std::endl;
    Log::log()->debug("barycentric_interpolation_velocity: "
            "HybridNonLinearsolver status: {}\nfound velocity {}",
            status, minimizer.transpose());

    auto min_point = make_point(
        VSpace.canonical, eigen_vector_to_tensor<VSpaceType::D>(minimizer));

    const auto min_point_immersed = VSpace.immerse(min_point);
    DataType interpolant = TM.point_to_tensor(min_point_immersed);

    return interpolant;
}

/** \brief Interpolate tensorial data using barycentric interpolation
 *
 * \tparam MetricSpaceType Type of the manifold
 * \tparam InputIterator Type of the vertex data iterator
 *
 * \param chart The coordinate chart to use for all numerical work. The
 * result should be mostly independent of this choice.
 * \param point Where to compute the interpolant at.
 * \param data_begin Iterator to start of vertex data
 * \param data_end Iterator to end of vertex data
 * \param method Which \ref BarycentricInterpolationMethod to use.
 *
 * \return The interpolated data.
 *
 * \ingroup interpolation
 */
template <typename MetricSpaceType, typename InputIterator>
inline typename std::iterator_traits<InputIterator>::value_type
barycentric_interpolation(const Chart<MetricSpaceType> &chart,
                          const ManifoldPoint<MetricSpaceType> &point,
                          InputIterator data_begin, InputIterator data_end,
                          const BarycentricInterpolationMethod &method) {

    using DataType = typename std::iterator_traits<InputIterator>::value_type;

    auto bcoords_and_data = internal::get_bcoords_and_data(
        chart, point, data_begin, data_end, method);

    auto &bcoords = bcoords_and_data.first;
    auto &parprop_data = bcoords_and_data.second;

    DataType interpolant = std::inner_product(
        bcoords.begin(), bcoords.end(), parprop_data.begin(),
        DataType{point, chart, typename DataType::ContainerType().setZero()});

    return interpolant;
}

/** \copybrief barycentric_interpolation()
 *
 * \tparam MetricSpaceType Type of the manifold
 * \tparam DataType Type of data to be interpolated. Currently must be a Tensor.
 *
 * \param chart The coordinate chart to use for all numerical work. The
 * result should be mostly independent of this choice.
 * \param point Where to compute the interpolant at.
 * \param vertex_data Vector of the tensor data to be interpolated.
 * Implicitly contains the points where the data is located.
 * \param method Which \ref BarycentricInterpolationMethod to use.
 *
 * \return The interpolated data.
 *
 * \ingroup interpolation
 */
template <typename MetricSpaceType, typename DataType>
inline DataType barycentric_interpolation(
    const Chart<MetricSpaceType> &chart,
    const ManifoldPoint<MetricSpaceType> &point,
    const aligned_vector<DataType> &vertex_data,
    const BarycentricInterpolationMethod &method) {

    return barycentric_interpolation(chart, point, vertex_data.begin(),
                                     vertex_data.end(), method);
}

/** \brief \copybrief barycentric_interpolation()
 * This overload implicitly uses the chart of \c point
 *
 * \tparam MetricSpaceType Type of the manifold
 * \tparam DataType Type of data to be interpolated. Must be an Eigen Tensor.
 *
 * \param point Where to compute the interpolant at.
 * \param vertex_data Vector of the tensor data to be interpolated.
 * Implicitly contains the points where the data is located.
 * \param method Which \ref BarycentricInterpolationMethod to use.
 *
 * \return The interpolated data.
 *
 * \ingroup interpolation
 */
template <typename MetricSpaceType, typename DataType>
inline DataType barycentric_interpolation(
    const ManifoldPoint<MetricSpaceType> &point,
    const aligned_vector<DataType> &vertex_data,
    const BarycentricInterpolationMethod &method) {
    return barycentric_interpolation(point.chart(), point, vertex_data, method);
}

} // namespace interpolation
} // namespace arcmancer
