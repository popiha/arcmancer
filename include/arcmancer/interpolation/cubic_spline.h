#pragma once

#include <Eigen/Core>
#include <Eigen/Dense>
#include <unsupported/Eigen/CXX11/Tensor>

#include "arcmancer/log.h"
#include "arcmancer/general_utils.h"

namespace arcmancer {
namespace interpolation {

/** \brief Cubic spline interpolation.
 *
 * Interpolates vector-valued data that depends on a single parameter.
 *
 * \ingroup utils
 * \see `cubic_spline_interpolation()`
 */
class CubicSpline {
public:

    /** \brief Construct with natural boundary conditions
     *  
     *  The data is given in a matrix where each row corresponds to a single
     *  point.
     *  The first column contains the values of the independent parameter.
     *  
     *  The second derivative of the spline is set to 0 at the endpoints.
     *
     *  \param data the data to be interpolated
     *  \param limit_tol tolerance for warning of extrapolation.
     */
    CubicSpline(const Eigen::MatrixXd &data, double limit_tol = 0)
    : num_points(data.rows()),
      num_vars(data.cols()),
      original_data(data),
      xs(data.col(0))
       {
        construct_spline(data, limit_tol, true,
                         Eigen::VectorXd::Zero(data.cols()),
                         Eigen::VectorXd::Zero(data.cols()));
    }

    /** \brief Construct with given first derivative
     *
     *  As above, but the first derivative of the spline is set to the specified
     *  values.
     *
     *  \param data the data to be interpolated
     *  \param left_boundary_deriv the derivative at the left boundary,
     *  first value is the derivative of the independent parameter and must be
     * 1.
     *  \param right_boundary_deriv the derivative at the right boundary.
     *  \param limit_tol tolerance for warning of extrapolation.
     *
     */
    CubicSpline(const Eigen::MatrixXd &data,
                const Eigen::VectorXd &left_boundary_deriv,
                const Eigen::VectorXd &right_boundary_deriv,
                double limit_tol = 0)
    : num_points(data.rows()),
      num_vars(data.cols()),
      original_data(data),
      xs(data.col(0))
       {
        construct_spline(data, limit_tol, false, left_boundary_deriv,
                         right_boundary_deriv);
    }

    /** \brief Evaluate the value of the interpolant
     *
     *  Returns the interpolated data as a vector, in the same format as
     *  the rows of \c data.
     *
     * \param x value of the independent parameter
     * \return Vector of interpolated data.
     *
     */
    Eigen::VectorXd evaluate(double x) const {
        if (!value_in_range(x, limits)) {
            Log::log()->warn("CubicSpline::evaluate: value {}"
                             " outside spline definition interval ({}, {})",
                    x, limits.first, limits.second);
        }

        // find the indexes which bracket x
        size_t start = 0, end = num_points-1;
        size_t index = 0;
        while ( end > start + 1) {

#ifdef ARCMANCER_DEBUG_CUBIC_SPLINE
            Log::log()->debug("CubicSpline::evaluate: start {} end {}"
                              " x_start {} x_end {} index {} x_index {}",
                    start, end, xs(start), xs(end), index, xs(index));
#endif
            index = (end + start) >> 1;
            if (xs(index) > x) {
                end = index;
            }
            else {
                start = index;
            }
        }
#ifdef ARCMANCER_DEBUG_CUBIC_SPLINE
        Log::log()->debug("CubicSpline::evaluate: start {} end {}"
                          " x_start {} x_end {} index {} x_index {}",
                start, end, xs(start), xs(end), index, xs(index));
#endif
        double dx = xs(end) - xs(start);
        double A = (xs(end) - x) / dx;
        double B = (x - xs(start)) / dx;

#ifdef ARCMANCER_DEBUG_CUBIC_SPLINE
        Log::log()->debug("CubicSpline::evaluate: dx {} A {} B {}", dx, A, B);
#endif
        Eigen::VectorXd ret = A * original_data.row(start) 
                            + B * original_data.row(end) 
                            + dx*dx/6 * (
                                         A*(A*A - 1) * d.row(start)
                                         + B*(B*B - 1) * d.row(end) 
                                        );
#ifdef ARCMANCER_DEBUG_CUBIC_SPLINE
        Log::log()->debug("CubicSpline::evaluate: return value {}",
                          ret.transpose());
#endif
        return ret;
    }

private:
    std::pair<double,double> limits;
    size_t num_points, num_vars;
    Eigen::MatrixXd original_data;
    Eigen::MatrixXd d;
    Eigen::VectorXd xs;
    
    // Construct  the system with either natural boundary conditions or
    // given derivatives.
    void construct_spline(const Eigen::MatrixXd &data, double limit_tol,
                          bool natural_boundary,
                          const Eigen::VectorXd &left_boundary_deriv,
                          const Eigen::VectorXd &right_boundary_deriv) {

        // add tolerance to limits, warning if the interpolation point is
        // beyond
        // the end points by e.g. 1e-15 isn't very useful
        limits = {xs(0) - limit_tol, xs(num_points - 1) + limit_tol};

        Eigen::VectorXd a = Eigen::VectorXd::Zero(num_points);
        Eigen::VectorXd b = Eigen::VectorXd::Zero(num_points);
        Eigen::VectorXd c = Eigen::VectorXd::Zero(num_points);
        d = Eigen::MatrixXd::Zero(num_points, num_vars);

        
        // setup the tridiagonal system
        for (size_t i = 1; i <= num_points - 2; i++) {
            a(i) = (xs(i) - xs(i - 1)) / 6;
            b(i) = (xs(i + 1) - xs(i - 1)) / 3;
            c(i) = (xs(i + 1) - xs(i)) / 6;
            d.row(i) = (data.row(i + 1) - data.row(i)) / (xs(i + 1) - xs(i)) -
                       (data.row(i) - data.row(i - 1)) / (xs(i) - xs(i - 1));
        }
        b(0) = b(num_points - 1) = 1;

#ifdef ARCMANCER_DEBUG_CUBIC_SPLINE
        Log::log()->debug("CubicSpline(): a {}", a.transpose());
        Log::log()->debug("CubicSpline(): b {}", b.transpose());
        Log::log()->debug("CubicSpline(): c {}", c.transpose());
        Log::log()->debug("CubicSpline(): d-matrix\n{}", d);
#endif
        // boundary conditions
        // for natural boundary conditions the current state is already correct
        if(!natural_boundary){
            d.row(0) = 3 * ((data.row(1) - data.row(0)) / (xs(1) - xs(0)) -
                            left_boundary_deriv.transpose()) /
                       (xs(1) - xs(0));
            c(0) = .5;
        }

        // solve the tridiagonal system
        for (size_t i = 1; i <= num_points-2; i++) {
            c(i) = c(i)/(b(i) - a(i) * c(i-1));
            d.row(i) = (d.row(i) - a(i) * d.row(i-1)) / (b(i) - a(i) * c(i-1));
        }

        if(!natural_boundary) {
            d.row(num_points - 1) =
                (3 * (right_boundary_deriv.transpose() -
                      (data.row(num_points - 1) - data.row(num_points - 2)) /
                          (xs(num_points - 1) - xs(num_points - 2))) /
                     (xs(num_points - 1) - xs(num_points - 2)) -
                 .5 * d.row(num_points - 2)) /
                (1 - .5 * c(num_points - 2));
        }

        // solution is stored in matrix d
        for (size_t j = num_points-1; j > 0; j--) {
            const size_t i = j-1;
            d.row(i) = d.row(i) - c(i) * d.row(i+1);
        }

#ifdef ARCMANCER_DEBUG_CUBIC_SPLINE
        Log::log()->debug("CubicSpline(): Original data\n{}", data);
        Log::log()->debug("CubicSpline(): d-matrix\n{}", d);
#endif
    }
};

/** \brief Interpolate a single value from data
 *
 *  A simple wrapper around \c CubicSpline, see its documentation for data 
 *  format etc.
 *
 *  \param data the data to interpolate.
 *  \param x the value of the independent parameter.
 *  \param limit_tol tolerance for warning of extrapolation.
 *  \return Vector of interpolated data at x.
 *
 *  \ingroup utils
 */
inline
Eigen::VectorXd cubic_spline_interpolation(const Eigen::MatrixXd &data,
                                           double x, double limit_tol = 0) {
    if(data.rows() < 3){
        throw std::runtime_error(
            "Need at least 3 datapoints for cubic interpolation");
    }
    CubicSpline spline(data, limit_tol);
    return spline.evaluate(x);
}

} // namespace interpolation
}
