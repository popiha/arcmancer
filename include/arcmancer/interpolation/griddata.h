#pragma once

#include <algorithm>
#include <array>
#include <functional>
#include <utility>

#include <Eigen/Dense>
#include <unsupported/Eigen/CXX11/Tensor>

#include "arcmancer/interpolation/nlinear.h"

namespace arcmancer {
namespace interpolation {
/** \brief A wrapper class for multilinear interpolation on a grid.
 *
 *  A simple wrapper for using the `::interpolate` function with a specific
 *  set of gridded data.
 *
 * \ingroup utils
 */
template <int dim, typename DataType>
class GridData {
public:
    /** \brief Constructor.
     *
     * \param data_values the data to interpolate.
     * \param grid_points the coordinates of grid points.
     */
    GridData(Eigen::Tensor<DataType, dim> data_values,
             GridSpec<dim> grid_points)
    : data_(data_values), grid_(grid_points) {}

    /** \brief Interpolate a value.
     *
     *  \param pt the coordinates of the point where the value is interpolated.
     */
    std::pair<DataType, bool> interpolate(const PointType<dim> &pt) const {
        return interpolation::interpolate<dim, DataType>(grid_, data_, pt);
    }

    const Eigen::Tensor<DataType, dim>& data() const { return data_; }
    ///< \brief Get the data.
private:
    Eigen::Tensor<DataType, dim> data_;
    GridSpec<dim> grid_;
};
}} // namespace arcmancer::interpolation
