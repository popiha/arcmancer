#pragma once

#include <algorithm>
#include <array>
#include <functional>
#include <iterator>
#include <stdexcept>
#include <type_traits>
#include <utility>
#include <vector>

#include <Eigen/Dense>
#include <unsupported/Eigen/CXX11/Tensor>

#include "arcmancer/general_utils.h"
#include "arcmancer/geometry/types.h"
#include "arcmancer/log.h"

namespace arcmancer {
/** \brief Tools for interpolating both gridded and ungridded
 * multidimensional data.
 *
 * \ingroup utils
 */
namespace interpolation {

// The coordinate grid is defined by a number of vectors equal to the dimension.
// This forces the grid to be rectangular, unlike if a
// Tensor<double, dim> was used instead.
// TODO: Add support for unequal grids.
/** \brief Type for defining interpolation grids
 *
 * The coordinate grid is defined by a number of vectors equal to the dimension,
 * with each vector containing the values of the grid coordinate at the grid
 * points along that dimension.
 * For example `GridSpec<2> grid` defines a two-dimensional grid, and
 * the coordinates of the point at indices `(i,j)` are
 * `(grid[0][i], grid[1][j])`.
 *
 * Grids where some dimensions only span a single cell are also supported.
 *
 * \tparam dim the dimension of the grid
 */
template <int dim>
using GridSpec = std::array<std::vector<double>, dim>;

/** \brief Type for coordinate points on the interpolation grid
 *
 * \tparam dim the dimension of the grid
 */
template <int dim>
using PointType = Eigen::Matrix<double, dim, 1>;

/** \brief Find the grid cell containing the point
 *
 * \param grid the definition of the interpolation grid.
 * \param pt the coordinates of the point on the grid.
 *
 * \return A pair `{indices, extrapolating}` where \c indices is an array
 * of indices to the interpolation grid and \c extrapolating is true if
 * \c pt is outside the grid.
 *
 * \tparam dim the dimension of the grid
 */
template <int dim>
inline std::pair<Eigen::array<Eigen::Index, dim>, bool> find_nearest_gridcell(
    const GridSpec<dim> &grid, const PointType<dim> &pt) {

    // iterate through dimensions, and find the "bottom-left" corner
    // of the gridcell containing, or nearest to the given point.
    bool extrapolating = false;
    Eigen::array<Eigen::Index, dim> ret{};

    for (int i = 0; i < dim; i++) {
        const std::vector<double> &griddef = grid[i];
        double coord = pt[i];
        auto min = griddef.begin();

        // find minimum distance between coord and gridpoints along
        // this dimension, with the constraint that the distance be
        // positive (i.e. find bottom-left corner of containing
        // cell). in addition, check if we're extrapolating (required
        // coordinate not within grid bounds)
        if (coord < griddef.front()) {
            extrapolating = true;
            min = griddef.begin();
        } else if (coord > griddef.back()) {
            extrapolating = true;
            min = griddef.end();
        } else {
            min = std::lower_bound(griddef.begin(), griddef.end(), coord);
        }

        // if extrapolating over the right edge need to bring the index further
        // inside
        if (min == griddef.end()) { min--; }

        if (min != griddef.begin()) { min--; }

        ret[i] = min - griddef.begin();
    }

    return {ret, extrapolating};
}

/** \brief Linearly interpolate data on a multidimensional grid
 *
 * \param grid the definition of the interpolation grid.
 * \param data the data to interpolate.
 * \param pt the coordinate point where to evaluate the interpolant.
 *
 * \return A pair `{interpolant, extrapolating}` where `interpolant` is
 * the interpolated value at \c pt and \c extrapolating is true if
 * \c pt is outside the grid.
 *
 * \tparam dim the dimension of the grid
 * \tparam DataType the type of data to interpolate.
 *  Needs to  support addition assignment (`operator+=`) and multiplication by 
 *  a `double`.
 */
template <int dim, typename DataType>
inline std::pair<DataType, bool> interpolate(
    const GridSpec<dim> &grid, const Eigen::Tensor<DataType, dim> &data,
    const PointType<dim> &pt) {

    Eigen::array<Eigen::Index, dim> corner_index;
    bool extrapolating;
    std::tie(corner_index, extrapolating) =
        find_nearest_gridcell<dim>(grid, pt);

    // array of normalized distances
    std::array<double, dim> k_array;
    for (int i = 0; i < dim; i++) {
        if (grid[i].size() > 1) {
            double cell_size =
                grid[i][corner_index[i] + 1] - grid[i][corner_index[i]];
            double k_i = (pt(i) - grid[i][corner_index[i]]) / cell_size;
            k_array[i] = k_i;
        } else {
            k_array[i] = 1;
        }
    }

    // the n == 0 case of the following loop separately to get an initial 
    // value for the sum without zero initialization
    double k_product = 1;
    for (int i = 0; i < dim; i++) {
        k_product *= 1 - k_array[i];
    }
    DataType interpolant = data(corner_index) * k_product;

    // loop through a bitfield of dim bits to compute all the terms in
    // the multiple tensor contraction L_a1...ad D^a1...D^ad
    for (size_t n = 1; n < (1 << dim); n++) {
        k_product = 1;

        Eigen::array<Eigen::Index, dim> index = corner_index;
        for (int i = 0; i < dim; i++) {
            index[i] += grid[i].size() > 1 ? (n >> i) & 1 : 0;
            k_product *= ((n >> i) & 1) == 1 ? k_array[i] : 1 - k_array[i];
        }
        interpolant += data(index) * k_product;
    }

    return {interpolant, extrapolating};
}

} // namespace interpolation
} // namespace arcmancer
