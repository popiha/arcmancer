#pragma once

#include <cstddef>
#include <cmath>
#include <limits>

namespace arcmancer {

// Define several configuration structures for different functionality.
// All of these are contained within one MetricSpace configuration,
// which is semi-global (for that MetricSpace).

namespace configuration {

/** \brief General purpose set of numerical tolerance parameters.
 *
 *  In most use cases the error in a quantity after e.g. an integration step is
 *  constrained to be under `absolute + relative * (abs(val) + abs(dval))`,
 *  where `val` is the previous value or other characteristic magnitude
 *  of the quantity and `dval` its derivative if applicable.
 *
 *  The default values ensure that only the relative tolerance is significicant
 *  regardless of the magnitude of the quantity,
 *  which is generally the desired behaviour.
 *  This may fail e.g. if the both the integrated quantity and its derivatives
 *  vanish at the considered point but are otherwise of order unity.
 *  In this case the absolute tolerance should be set to a larger value,
 *  e.g. `std::numeric_limits<double>::epsilon()`.
 *
 */
struct Tolerance {
    /// Relative numerical tolerance (when appropriate)
    double relative = 1e-10;
    /// Absolute numerical tolerance
    double absolute = std::numeric_limits<double>::min();
};


/** \brief Control parameters for Henon's trick integration
 */
struct HenonStepping {
    Tolerance tolerance;
    long maximum_steps = 10;
};

/** \brief Collects parameters controlling numerical 
 * integration.
 */
struct Propagation {
    /// Tolerances used in numerical integration.
    Tolerance tolerance;

    bool enforce_sampling_interval = false;
    ///< Whether to use the set \c sampling_interval.

    double sampling_interval = 1e-3;
    ///< \brief Maximum relative stepsize.
    ///
    /// If used, the maximum stepsize is `sampling_interval*dlambda`, where
    /// \c dlambda is the parameter length of the computed curve portion.


    // maximum and minimum stepsizes
    bool enforce_maximum_stepsize = false;
    ///< Whether to use the set \c maximum_stepsize.
    bool enforce_minimum_stepsize = false;
    ///< Whether to use the set \c minimum_stepsize.
    double maximum_stepsize = 1;
    ///< Maximum parameter length of a step if enabled.
    double minimum_stepsize = 0.1;
    ///< Minimum parameter length of a step if enabled.

    bool enforce_maximum_steps = true;
    ///< Whether to use the set \c maximum_steps.
    long maximum_steps = 100000;
    ///< Maximum number of succesfull steps to perform when extending a curve.
};


/** \brief Collects parameters controlling numerical optimization
 * routines.
 */
struct NumericalOptimization {
    /// Maximum number of iterations
    size_t iterations = 100;
    /// Minimum change in independent variable
    double x_tol = std::sqrt(std::numeric_limits<double>::epsilon());
    /// Minimum change in target function
    double f_tol = std::sqrt(std::numeric_limits<double>::epsilon());
    /// Minimum norm of the gradient
    double min_grad_norm = 0;
};

/** \brief Collection of all Arcmancer configuration parameters.
 *
 *  A semi-global configuration associated with a \c MetricSpace.
 *  Access to the instance is provided through `MetricSpace::configuration`.
 *  These options control mainly the integration of curves and computation of
 *  line integrals.
 *
 * \ingroup geometry
 */
struct Configuration {
    double chart_selection_cond_tol = 5;
    ///< Tolerance used in get_best_integration_chart, should be >1.

    /// Configuration for single ManifoldPoints, Tensors and the like
    struct Pointwise {
        Tolerance tolerance;
    } pointwise;

    /// Configuration for curve propagation
    struct Curve {
        Propagation propagation;
        HenonStepping henon;

        bool enforce_surface_stepsize_reduction = true;
        ///< Whether to reduce stepsize near surfaces.
        double surface_stepsize_reduction_tolerance = 1.1;
        ///< Controls stepsize reduction near surfaces, should be slightly over 1.


        size_t failed_step_penalty = 5;
        ///< \brief Controls automatic chart switching in curve integration.
        ///
        /// Each failed step increases the \c failed_step_score by this amount in
        /// curve integration. The score decreases by 1 for each successfull step,
        /// and is used as a heuristic for switching charts.

        size_t chart_switch_failed_step_score = 8;
        ///< \brief Controls automatic chart switching in curve integration.
        ///
        ///  Attempt switching charts if the \c failed_step_score is above this 
        ///  limit.
    } curve;

    /// Configuration for line integrals
    struct LineIntegral {
        Propagation propagation;
    } line_integral;

    /// Configuration for barycentric interpolation
    struct BarycentricInterpolation {
        NumericalOptimization optimization;
        Tolerance tolerance;
    } barycentric_interpolation;

    /// Configuration for riemann normal coordinates
    struct NormalCoordinates {
        NumericalOptimization optimization;
        Tolerance tolerance;
    } normal_coordinates;
};


} // namespace configuration
} // namespace arcmancer
