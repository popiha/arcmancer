#pragma once

#include <iostream>
#include <vector>
#include <stdexcept>


// Helpers to deal with std::vector and other containers
template <class T>
inline std::ostream& operator<<(std::ostream& os, const std::vector<T>& v) {
    os << "[";
    for (const auto &x : v) {
        os << " " << x;
    }
    os << "]";
    return os;
}


#include <spdlog/spdlog.h>
#include <spdlog/sinks/stdout_sinks.h>
#include <spdlog/sinks/file_sinks.h>



namespace arcmancer {

namespace spd = spdlog;

namespace internal {

template <class T>
struct static_resource_holder {
    static T static_resource_;
};

template <class T>
T static_resource_holder<T>::static_resource_;

} // namespace internal

using logger_function = std::shared_ptr<spd::logger>(*)();


/**
 * \brief Logging facilities
 *
 * Allows logging to the console and/or rotating log files based on specified
 * log level.
 *
 * Used internally by Arcmancer, but should provide enough capabilities also for
 * use by applications.
 * Wraps functionality provided by 
 * <a href=https://github.com/gabime/spdlog>spdlog</a>, 
 * see the 
 * <a href=https://github.com/gabime/spdlog/wiki>spdlog documentation</a> 
 * for additional details.
 *
 * \ingroup utils
 *
 */
struct Log : private internal::static_resource_holder<logger_function> {
    /** \brief Convenience alias for log levels
     *
     * Alias for the spdlog log levels.
     * Possible values are:
     *  - `trace`
     *  - `debug`
     *  - `info`
     *  - `warn`
     *  - `err`
     *  - `critical`
     *  - `off`
     */
    using level = spd::level::level_enum;

    static constexpr size_t max_log_size() { return 1024 * 1024 * 10; }
    static constexpr size_t rotation_size() { return 5; }
    //XXX: seems that format_str isn't actually used?
    static constexpr const char*  format_str() {
        return "[%Y-%m-%d %T.%e] [thread %t] [level %l] %v";
    };

    static spd::sink_ptr console_sink() {
        static auto _l = std::make_shared<spd::sinks::stdout_sink_mt>();
        return _l;
    }

    static spd::sink_ptr file_sink() {
        static auto _l = std::make_shared<spd::sinks::rotating_file_sink_mt>(
                        "arcmancer.log",
                        max_log_size(),
                        rotation_size());
        return _l;
    }

    /** \brief Get a reference to a spdlog console logger.
     */
    static std::shared_ptr<spd::logger> console() {
        static auto _l = std::make_shared<spd::logger>("console", console_sink());
        return _l;
    }

    /** \brief Get a reference to a spdlog file logger.
     */
    static std::shared_ptr<spd::logger> file() {
        static auto _l = std::make_shared<spd::logger>("file", file_sink());
        return _l;
    }

    /** \brief Get a reference to a combination logger
     *
     * Prints to stdout and to a log file.
     */
    static std::shared_ptr<spd::logger> combo() {
        static std::vector<spd::sink_ptr> _sinks = {
            console_sink(),
            file_sink()
        };
        static auto _l = std::make_shared<spd::logger>(
                "combined", _sinks.begin(), _sinks.end());
        return _l;
    }

    /** \brief Set the default logger
     */
    static void set_default_logger(logger_function func) {
        static_resource_ = func;
    }

    /** \brief Get a reference to the default logger
     *
     *  Initially the default logger is `console`.
     */
    static std::shared_ptr<spd::logger> log() {
        // C++ guarantees that the default initialization value of
        // static pointers is null
        if (static_resource_ == nullptr) {
            static_resource_ = &console;
        }
        return (*static_resource_)();
    }

    /** \brief Set global logging level.
     *
     *  Sets the log level for all sinks.
     *
     * \param level The new global logging \ref level.
     */
    static void set_level(level log_level) {
        spd::set_level(log_level);

        // also explicitly set level for all sinks
        console()->set_level(log_level);
        file()->set_level(log_level);
        combo()->set_level(log_level);
    }

    /** \brief Set global logging level.
     *
     *  Sets the log level for all sinks using given integer, mapping
     *  0 -> trace
     *  ...
     *  6 -> off.
     *
     *  Throws an 
     *
     * \param level The new global logging \ref level.
     */
    static void set_level(int level_number) {
        switch (level_number) {
            case static_cast<int>(level::trace):
            case static_cast<int>(level::debug):
            case static_cast<int>(level::info):
            case static_cast<int>(level::warn):
            case static_cast<int>(level::err):
            case static_cast<int>(level::critical):
            case static_cast<int>(level::off):
                set_level(static_cast<level>(level_number));
                break;
            default:
                throw std::invalid_argument(
                        std::string("set_level: unknown log level number ")
                        + std::to_string(level_number));
        }
    }
};


} // namespace arcmancer
