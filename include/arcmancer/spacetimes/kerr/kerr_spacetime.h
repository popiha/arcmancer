#pragma once

#include <cmath>
#include <string>

#include "arcmancer/geometry.h"
#include "arcmancer/geometry/metric_space.h"

namespace arcmancer {
/** \brief Classes related to the Kerr spacetime
 *  \ingroup manifolds
 */
namespace kerr {

using namespace std::literals::string_literals; // for ""s suffix operator

/** \brief The Kerr spacetime
 *
 *  Implements the Kerr spacetime describing a spinning, uncharged black hole.
 *
 *  Currently provides three coordinate charts: the Boyer-Lindquist coordinates 
 *  and ingoing and outgoing cartesian Kerr-Schild coordinates.
 *  The ingoing Kerr-Schild coordinates are regular over the event horizon for 
 *  trajectories entering the black hole in the future, while the outgoing 
 *  coordinates are regular for trajectories exiting from the hole in the past.
 *  See e.g. Boyer, Lindquist (1967) [https://doi.org/10.1063/1.1705193] and
 *  Carter (1968) [https://doi.org/10.1103/PhysRev.174.1559] for more details.
 *
 *  \ingroup manifolds
 */
class KerrSpacetime : public MetricSpace<KerrSpacetime, 4> {
public:
    std::string name() const { return name_; };

    const ChartType boyer_lindquist = {*this, "Boyer-Lindquist"s};
    ///< \brief The Boyer-Lindquist chart
    const ChartType kerr_schild_in = {*this, "Ingoing Kerr-Schild"s};
    ///< \brief The ingoing Kerr-Schild chart
    const ChartType kerr_schild_out = {*this, "Outgoing Kerr-Schild"s};
    ///< \brief The outgoing Kerr-Schild chart

    /** \brief Constructor
     *  
     *  \param mass the mass of the black hole.
     *  \param chi the dimensionless spin parameter, \f$ \chi = a/M = J/M^2 \f$.
     */
    KerrSpacetime(double mass, double chi)
    : mass_(mass),
      chi_(chi),
      a_(mass * chi),
      name_("Kerr Spacetime (mass = " + std::to_string(mass) + ", chi = " +
            std::to_string(chi) + ")") {
        // check that spin is physical
        if (std::abs(chi) > 1) {
            throw std::runtime_error(
                "Non-dimensional spin chi = " + std::to_string(chi) +
                " is outside physical limits -1 =< chi =< 1");
        }

        add_chart(boyer_lindquist, &KerrSpacetime::boyer_lindquist_metric,
                  &KerrSpacetime::boyer_lindquist_metric_derivatives);

        add_chart(kerr_schild_in, &KerrSpacetime::kerr_schild_in_metric,
                  &KerrSpacetime::kerr_schild_in_metric_derivatives);

        add_chart(kerr_schild_out, &KerrSpacetime::kerr_schild_out_metric,
                  &KerrSpacetime::kerr_schild_out_metric_derivatives);

        add_transition(boyer_lindquist, kerr_schild_in,
                       &KerrSpacetime::bl_to_ksi_function,
                       &KerrSpacetime::bl_to_ksi_jacobian);

        add_transition(boyer_lindquist, kerr_schild_out,
                       &KerrSpacetime::bl_to_kso_function,
                       &KerrSpacetime::bl_to_kso_jacobian);

        add_transition(kerr_schild_in, boyer_lindquist,
                       &KerrSpacetime::ksi_to_bl_function);

        add_transition(kerr_schild_out, boyer_lindquist,
                       &KerrSpacetime::kso_to_bl_function);

        add_transition(kerr_schild_in, kerr_schild_out,
                       &KerrSpacetime::ksi_to_kso_function,
                       &KerrSpacetime::ksi_to_kso_jacobian);

        add_transition(kerr_schild_out, kerr_schild_in,
                       &KerrSpacetime::kso_to_ksi_function);

        complete_chart_graph();
    }

    /** \brief Compute the value of Carter's constant for given four-momentum
     *  \param p four-momentum
     *  \return the value of Carter's constant.
     */
    double carters_constant(const CotangentVector<KerrSpacetime> &p) const {

        using namespace std;

        auto p_c = p.components(boyer_lindquist);
        const double pt = p_c(0);
        const double pth = p_c(2);
        const double pphi = p_c(3);

        const double th = p.point().coordinates(boyer_lindquist)(2);

        const double a = chi_ * mass_;

        const double m2 = dot_product(p, p);

        return pow(pth, 2) + pow(pphi * cos(th) / sin(th), 2) -
               pow(a * cos(th), 2) * (pt * pt - m2);
    }

    /** \brief The Boyer-Lindquist r coordinate of the horizon
     */
    double horizon_r() const {
        return mass_ * (1 + std::sqrt(1 - chi_ * chi_));
    }

    double mass() const { return mass_; } ///< \brief Mass of the black hole.
    double chi() const { return chi_; } ///< \brief The spin parameter chi.

private:
    double mass_, chi_, a_;
    std::string name_;

    EigenRk2Tensor<4> boyer_lindquist_metric(const EigenRk1Tensor<4> &x) const {
        using namespace std;
        const double r = x(1), th = x(2);
        const double r2 = r * r;
        const double a = a_;
        const double a2 = a * a;
        const double rs = 2 * mass_;
        const double R2 = r2 + pow(a * cos(th), 2);
        const double D = r2 + a2 - rs * r;
        const double sinth2 = pow(sin(th), 2);

        const double dtdphi = rs * r * a * sinth2 / R2;
        const double dphidphi = -(r2 + a2 + rs * r * a2 * sinth2 / R2) * sinth2;

        EigenRk2Tensor<4> ret;
        // clang-format off
        ret.setValues({
            {1 - rs*r/R2, 0    , 0  , dtdphi  },
            {0          , -R2/D, 0  , 0       },
            {0          , 0    , -R2, 0       },
            {dtdphi     , 0    , 0  , dphidphi}
            });
        // clang-format on
        return ret;
    }

    EigenRk3Tensor<4> boyer_lindquist_metric_derivatives(
        const EigenRk1Tensor<4> &x) const {

        const double r = x(1), th = x(2);
        const double r2 = r * r;
        const double a = a_;
        const double a2 = a * a;
        const double rs = 2 * mass_;
        const double st = sin(th), ct = cos(th);
        const double st2 = st * st, ct2 = ct * ct;

        EigenRk3Tensor<4> ret;
        ret.setZero();
        // helper container
        EigenRk2Tensor<4> der;
        // g_ab,r
        {

            const double g11 = ((-a2 * ct2 + r2) * rs) / pow(a2 * ct2 + r2, 2);

            const double g14 =
                0.5 * (2 * a * (a * ct - r) * (a * ct + r) * rs * st2) /
                pow(a2 * ct2 + r2, 2);

            const double g22 =
                (r2 * rs + a2 * (2 * (-1 + ct2) * r - ct2 * rs)) /
                pow(a2 + r * (r - rs), 2);

            const double g33 = -2 * r;

            const double g44 = -2 * r * st2 +
                               (a2 * (-(a2 * ct2) + r2) * rs * pow(st, 4)) /
                                   pow(a2 * ct2 + r2, 2);
            // clang-format off
            der.setValues({{g11, 0, 0, g14},
                           {0, g22, 0, 0},
                           {0, 0, g33, 0},
                           {g14, 0, 0, g44}});
            // clang-format on

            ret.chip(1, 2) = der;
        }
        // g_ab,theta
        {

            const double g11 =
                (-2 * a2 * ct * r * rs * st) / pow(a2 * ct2 + r2, 2);

            const double g14 =
                0.5 * (4 * a * ct * r * rs * st * (r2 + a2 * (ct2 + st2))) /
                pow(a2 * ct2 + r2, 2);

            const double g22 = (2 * a2 * ct * st) / (a2 + r2 - r * rs);

            const double g33 = 2 * a2 * ct * st;

            const double g44 =
                -2 * ct * (a2 + r2) * st -
                (4 * a2 * ct * r * rs * pow(st, 3)) / (a2 * ct2 + r2) -
                (2 * pow(a, 4) * ct * r * rs * pow(st, 5)) /
                    pow(a2 * ct2 + r2, 2);

            // clang-format off
            der.setValues({{g11, 0, 0, g14},
                           {0, g22, 0, 0},
                           {0, 0, g33, 0},
                           {g14, 0, 0, g44}});
            // clang-format on
            ret.chip(2, 2) = der;
        }

        return ret;
    }

    // Term coming from the integral of dr/(r^2 -2mr +a^2) in coordinate
    // transformations between Kerr-Schild and Boyer-Lindquist.
    double bl_ks_log_term(double r) const {
        using namespace std;

        const double rtm2a2 = sqrt(mass_ * mass_ - a_ * a_);

        if (rtm2a2 > 1e-10) {
            return log((r - mass_ - rtm2a2) / (r - mass_ + rtm2a2)) /
                   (2 * rtm2a2);
        } else {
            // Use series expansion when rtm2a2 is small to get the correct
            // limit
            const double x = rtm2a2 / (r - mass_);

            // XXX: probably doesn't need the second term to remain accurate
            return -(1 + pow(x, 2) / 3) / (r - mass_);
        }
    }

    // Boyer-Lindquist to ingoing Kerr-Schild
    // Using the transformation of Carter 1968 (doi: 10.1103/PhysRev.174.1559)
    EigenRk1Tensor<4> bl_to_ksi_function(const EigenRk1Tensor<4> &x_bl) const {
        using namespace std;

        const double r = x_bl(1);
        const double th = x_bl(2);
        const double phi_bl = x_bl(3);

        const double log_term = bl_ks_log_term(r);
        const double phi = phi_bl + a_ * log_term;
        const double t =
            x_bl(0) +
            2 * mass_ *
                (mass_ * log_term + .5 * log(a_ * a_ - 2 * mass_ * r + r * r));

        const double x = (r * cos(phi) - a_ * sin(phi)) * sin(th);
        const double y = (r * sin(phi) + a_ * cos(phi)) * sin(th);
        const double z = r * cos(th);
        EigenRk1Tensor<4> ret;
        ret.setValues({t, x, y, z});
        return ret;
    }

    ArcMatrix<4> bl_to_ksi_jacobian(const EigenRk1Tensor<4> &x_bl) const {
        using namespace std;

        const double r = x_bl(1);
        const double th = x_bl(2);
        const double phi_bl = x_bl(3);
        const double phi = phi_bl + a_ * bl_ks_log_term(r);

        const double ct = cos(th), st = sin(th);
        const double cp = cos(phi), sp = sin(phi);

        const double D = r * r + a_ * a_ - 2 * mass_ * r;
        const double dxdr = (cp + a_ / D * (-r * sp - a_ * cp)) * st;
        const double dydr = (sp + a_ / D * (r * cp - a_ * sp)) * st;

        ArcMatrix<4> ret;
        // clang-format off
        ret << 1, 2*mass_*r/D, 0                , 0                 ,
               0, dxdr       , (r*cp - a_*sp)*ct, (-r*sp - a_*cp)*st,
               0, dydr       , (r*sp + a_*cp)*ct, ( r*cp - a_*sp)*st,
               0, ct         , -r*st            , 0                 ;
        // clang-format on
        return ret;
    }

    EigenRk1Tensor<4> ksi_to_bl_function(const EigenRk1Tensor<4> &x_ks) const {
        using namespace std;

        const double x = x_ks(1);
        const double y = x_ks(2);
        const double z = x_ks(3);

        const double sqsum = x * x + y * y + z * z - a_ * a_;
        const double r =
            sqrt(.5 * (sqsum + sqrt(sqsum * sqsum + 4 * z * z * a_ * a_)));
        const double th = acos(z / r);

        const double phi = atan2(r * y - a_ * x, r * x + a_ * y);

        const double log_term = bl_ks_log_term(r);
        const double phi_bl = phi - a_ * log_term;

        const double t_bl =
            x_ks(0) -
            2 * mass_ *
                (mass_ * log_term + .5 * log(a_ * a_ - 2 * mass_ * r + r * r));

        EigenRk1Tensor<4> ret;
        ret.setValues({t_bl, r, th, phi_bl});
        return ret;
    }

    EigenRk2Tensor<4> kerr_schild_in_metric(
        const EigenRk1Tensor<4> &x_ks) const {
        using namespace std;

        const double x = x_ks(1);
        const double y = x_ks(2);
        const double z = x_ks(3);

        const double a2 = a_ * a_;
        const double sqsum = x * x + y * y + z * z - a2;
        const double r2 = .5 * (sqsum + sqrt(sqsum * sqsum + 4 * z * z * a2));
        const double r = sqrt(r2);
        const double F = -2 * mass_ * r2 * r / (r2 * r2 + a2 * z * z);

        // Just writing everything out explicitly is significantly faster than
        // computing l_a l_b using a tensor product
        EigenRk2Tensor<4> ret;
        const double /*l_t = 1,*/ l_x = (r * x + a_ * y) / (a2 + r2),
                                  l_y = (r * y - a_ * x) / (a2 + r2),
                                  l_z = z / r;

        // clang-format off
        ret.setValues({{1+F   , F*l_x      , F*l_y      , F*l_z     },
                       {F*l_x , F*l_x*l_x-1, F*l_x*l_y  , F*l_x*l_z  },
                       {F*l_y , F*l_x*l_y  , F*l_y*l_y-1, F*l_y*l_z  },
                       {F*l_z , F*l_x*l_z  , F*l_y*l_z  , F*l_z*l_z-1}});
        // clang-format on
        return ret;
    }

    EigenRk3Tensor<4> kerr_schild_in_metric_derivatives(
        const EigenRk1Tensor<4> &x_ks) const {

        using namespace std;

        const double x = x_ks(1);
        const double y = x_ks(2);
        const double z = x_ks(3);

        const double a2 = a_ * a_;
        const double R2 = x * x + y * y + z * z;
        const double sqsum = R2 - a2;
        const double r2 = .5 * (sqsum + sqrt(sqsum * sqsum + 4 * z * z * a2));
        const double r = sqrt(r2);
        const double r2a2_2 = pow(r2 + a2, 2);

        const double F = -2 * mass_ * r2 * r / (r2 * r2 + a2 * z * z);
        const double F_r = 2 * mass_ * r2 * (r2 * r2 - 3 * a2 * z * z) /
                           pow((a2 * z * z + r2 * r2), 2);

        const double /*l_t = 1,*/ l_x = (r * x + a_ * y) / (a2 + r2),
                                  l_y = (r * y - a_ * x) / (a2 + r2),
                                  l_z = z / r;

        EigenRk2Tensor<4> l2; // l_a l_b
        // clang-format off
        l2.setValues({{1   , l_x   , l_y    , l_z    },
                      {l_x, l_x*l_x, l_x*l_y, l_x*l_z},
                      {l_y, l_x*l_y, l_y*l_y, l_y*l_z},
                      {l_z, l_x*l_z, l_y*l_z, l_z*l_z}});
        // clang-format on

        EigenRk3Tensor<4> ret;
        ret.setZero();

        // x
        {
            // derivatives of the functions
            const double r_x =
                (x + sqsum * x / sqrt(sqsum * sqsum + 4 * z * z * a2)) /
                (2 * r);
            const double F_x = F_r * r_x;

            const double /*l_tx = 0,*/
                l_xx = ((r + x * r_x) * (r2 + a2) -
                        2 * r * (r * x + a_ * y) * r_x) /
                       r2a2_2,
                l_yx = ((-a_ + y * r_x) * (r2 + a2) -
                        2 * r * (r * y - a_ * x) * r_x) /
                       r2a2_2,
                l_zx = -z / r2 * r_x;

            EigenRk2Tensor<4> l2x; // l_a l_b,x
            // clang-format off
        l2x.setValues({{0, l_xx    , l_yx    , l_zx    },
                       {0, l_x*l_xx, l_x*l_yx, l_x*l_zx},
                       {0, l_y*l_xx, l_y*l_yx, l_y*l_zx},
                       {0, l_z*l_xx, l_z*l_yx, l_z*l_zx}});
            // clang-format on

            ret.chip(1, 2) =
                F_x * l2 + F * (l2x + l2x.shuffle(Eigen::array<int, 2>{1, 0}));
        }
        // y
        {
            const double r_y =
                (y + sqsum * y / sqrt(sqsum * sqsum + 4 * z * z * a2)) /
                (2 * r);
            const double F_y = F_r * r_y;

            const double /*l_ty = 0,*/
                l_xy = ((a_ + x * r_y) * (r2 + a2) -
                        2 * r * (r * x + a_ * y) * r_y) /
                       r2a2_2,
                l_yy = ((r + y * r_y) * (r2 + a2) -
                        2 * r * (r * y - a_ * x) * r_y) /
                       r2a2_2,
                l_zy = -z / r2 * r_y;

            EigenRk2Tensor<4> l2y;
            // clang-format off
        l2y.setValues({{0, l_xy    , l_yy    , l_zy    },
                       {0, l_x*l_xy, l_x*l_yy, l_x*l_zy},
                       {0, l_y*l_xy, l_y*l_yy, l_y*l_zy},
                       {0, l_z*l_xy, l_z*l_yy, l_z*l_zy}});
            // clang-format on

            ret.chip(2, 2) =
                F_y * l2 + F * (l2y + l2y.shuffle(Eigen::array<int, 2>{1, 0}));
        }
        // z
        {
            const double r_z = (z +
                                (sqsum * z + 2 * a2 * z) /
                                    sqrt(sqsum * sqsum + 4 * z * z * a2)) /
                               (2 * r);
            const double F_z =
                -6 * mass_ * r2 * r_z / (r2 * r2 + a2 * z * z) -
                F / (r2 * r2 + a2 * z * z) * (4 * r * r2 * r_z + 2 * a2 * z);

            const double /*l_tz = 0,*/
                l_xz = (x * r_z * (r2 + a2) - 2 * r * (r * x + a_ * y) * r_z) /
                       r2a2_2,
                l_yz = (y * r_z * (r2 + a2) - 2 * r * (r * y - a_ * x) * r_z) /
                       r2a2_2,
                l_zz = (r - z * r_z) / r2;

            EigenRk2Tensor<4> l2z;
            // clang-format off
        l2z.setValues({{0, l_xz    , l_yz    , l_zz    },
                       {0, l_x*l_xz, l_x*l_yz, l_x*l_zz},
                       {0, l_y*l_xz, l_y*l_yz, l_y*l_zz},
                       {0, l_z*l_xz, l_z*l_yz, l_z*l_zz}});
            // clang-format on

            ret.chip(3, 2) =
                F_z * l2 + F * (l2z + l2z.shuffle(Eigen::array<int, 2>{1, 0}));
        }

        return ret;
    }

    // Outgoing Kerr-Schild
    /// See e.g. Boyer & Lindquist 1967 (doi: 10.1063/1.1705193)
    // Note that B&L use a different sign convention for the a terms than
    // Carter.
    // Here Carter's convention is used.
    EigenRk1Tensor<4> bl_to_kso_function(const EigenRk1Tensor<4> &x_bl) const {
        using namespace std;

        const double r = x_bl(1);
        const double th = x_bl(2);
        const double phi_bl = x_bl(3);

        const double log_term = bl_ks_log_term(r);

        const double phi = phi_bl - a_ * log_term;
        const double t =
            x_bl(0) -
            2 * mass_ *
                (mass_ * log_term + .5 * log(a_ * a_ - 2 * mass_ * r + r * r));

        const double x = (r * cos(phi) + a_ * sin(phi)) * sin(th);
        const double y = (r * sin(phi) - a_ * cos(phi)) * sin(th);
        const double z = r * cos(th);
        EigenRk1Tensor<4> ret;
        ret.setValues({t, x, y, z});
        return ret;
    }

    ArcMatrix<4> bl_to_kso_jacobian(const EigenRk1Tensor<4> &x_bl) const {
        using namespace std;

        const double r = x_bl(1);
        const double th = x_bl(2);
        const double phi_bl = x_bl(3);

        const double phi = phi_bl - a_ * bl_ks_log_term(r);

        const double ct = cos(th), st = sin(th);
        const double cp = cos(phi), sp = sin(phi);

        const double D = r * r + a_ * a_ - 2 * mass_ * r;
        const double dxdr = (cp - a_ / D * (-r * sp + a_ * cp)) * st;
        const double dydr = (sp - a_ / D * (r * cp + a_ * sp)) * st;

        ArcMatrix<4> ret;
        // clang-format off
        ret << 1,-2*mass_*r/D, 0                , 0                 ,
               0, dxdr       , (r*cp + a_*sp)*ct, (-r*sp + a_*cp)*st,
               0, dydr       , (r*sp - a_*cp)*ct, ( r*cp + a_*sp)*st,
               0, ct         , -r*st            , 0                 ;
        // clang-format on
        return ret;
    }

    EigenRk1Tensor<4> kso_to_bl_function(const EigenRk1Tensor<4> &x_ks) const {
        using namespace std;

        const double x = x_ks(1);
        const double y = x_ks(2);
        const double z = x_ks(3);

        const double sqsum = x * x + y * y + z * z - a_ * a_;
        const double r =
            sqrt(.5 * (sqsum + sqrt(sqsum * sqsum + 4 * z * z * a_ * a_)));
        const double th = acos(z / r);

        const double phi = atan2(r * y + a_ * x, r * x - a_ * y);

        const double log_term = bl_ks_log_term(r);

        const double phi_bl = phi + a_ * log_term;

        const double t_bl =
            x_ks(0) +
            2 * mass_ *
                (mass_ * log_term + .5 * log(a_ * a_ - 2 * mass_ * r + r * r));

        EigenRk1Tensor<4> ret;
        ret.setValues({t_bl, r, th, phi_bl});
        return ret;
    }

    EigenRk2Tensor<4> kerr_schild_out_metric(
        const EigenRk1Tensor<4> &x_ks) const {
        using namespace std;

        const double x = x_ks(1);
        const double y = x_ks(2);
        const double z = x_ks(3);

        const double a2 = a_ * a_;
        const double sqsum = x * x + y * y + z * z - a2;
        const double r2 = .5 * (sqsum + sqrt(sqsum * sqsum + 4 * z * z * a2));
        const double r = sqrt(r2);

        const double F = -2 * mass_ * r2 * r / (r2 * r2 + a2 * z * z);

        EigenRk2Tensor<4> ret;
        const double /*l_t = -1,*/ l_x = (r * x - a_ * y) / (a2 + r2),
                                   l_y = (r * y + a_ * x) / (a2 + r2),
                                   l_z = z / r;
        // clang-format off
        ret.setValues({{1+F   , -F*l_x     , -F*l_y     , -F*l_z     },
                       {-F*l_x, F*l_x*l_x-1, F*l_x*l_y  , F*l_x*l_z  },
                       {-F*l_y, F*l_x*l_y  , F*l_y*l_y-1, F*l_y*l_z  },
                       {-F*l_z, F*l_x*l_z  , F*l_y*l_z  , F*l_z*l_z-1}});
        // clang-format on
        return ret;
    }

    EigenRk3Tensor<4> kerr_schild_out_metric_derivatives(
        const EigenRk1Tensor<4> &x_ks) const {

        using namespace std;

        const double x = x_ks(1);
        const double y = x_ks(2);
        const double z = x_ks(3);

        const double a2 = a_ * a_;
        const double R2 = x * x + y * y + z * z;
        const double sqsum = R2 - a2;
        const double r2 = .5 * (sqsum + sqrt(sqsum * sqsum + 4 * z * z * a2));
        const double r = sqrt(r2);
        const double r2a2_2 = pow(r2 + a2, 2);

        const double F = -2 * mass_ * r2 * r / (r2 * r2 + a2 * z * z);
        const double F_r = 2 * mass_ * r2 * (r2 * r2 - 3 * a2 * z * z) /
                           pow((a2 * z * z + r2 * r2), 2);

        const double /*l_t = -1,*/ l_x = (r * x - a_ * y) / (a2 + r2),
                                   l_y = (r * y + a_ * x) / (a2 + r2),
                                   l_z = z / r;

        EigenRk2Tensor<4> l2; // l_a l_b
        // clang-format off
        l2.setValues({{1   , -l_x   , -l_y   , -l_z   },
                      {-l_x, l_x*l_x, l_x*l_y, l_x*l_z},
                      {-l_y, l_x*l_y, l_y*l_y, l_y*l_z},
                      {-l_z, l_x*l_z, l_y*l_z, l_z*l_z}});
        // clang-format on

        EigenRk3Tensor<4> ret;
        ret.setZero();

        // x
        {
            // derivatives of the functions
            const double r_x =
                (x + sqsum * x / sqrt(sqsum * sqsum + 4 * z * z * a2)) /
                (2 * r);
            const double F_x = F_r * r_x;

            const double /*l_tx = 0,*/
                l_xx = ((r + x * r_x) * (r2 + a2) -
                        2 * r * (r * x - a_ * y) * r_x) /
                       r2a2_2,
                l_yx = ((a_ + y * r_x) * (r2 + a2) -
                        2 * r * (r * y + a_ * x) * r_x) /
                       r2a2_2,
                l_zx = -z / r2 * r_x;

            EigenRk2Tensor<4> l2x; // l_a l_b,x
            // clang-format off
        l2x.setValues({{0, -l_xx   , -l_yx   , -l_zx   },
                       {0, l_x*l_xx, l_x*l_yx, l_x*l_zx},
                       {0, l_y*l_xx, l_y*l_yx, l_y*l_zx},
                       {0, l_z*l_xx, l_z*l_yx, l_z*l_zx}});
            // clang-format on

            ret.chip(1, 2) =
                F_x * l2 + F * (l2x + l2x.shuffle(Eigen::array<int, 2>{1, 0}));
        }
        // y
        {
            const double r_y =
                (y + sqsum * y / sqrt(sqsum * sqsum + 4 * z * z * a2)) /
                (2 * r);
            const double F_y = F_r * r_y;
            
            const double /*l_ty = 0,*/
                l_xy = ((-a_ + x * r_y) * (r2 + a2) -
                        2 * r * (r * x - a_ * y) * r_y) /
                       r2a2_2,
                l_yy = ((r + y * r_y) * (r2 + a2) -
                        2 * r * (r * y + a_ * x) * r_y) /
                       r2a2_2,
                l_zy = -z / r2 * r_y;

            EigenRk2Tensor<4> l2y;
            // clang-format off
        l2y.setValues({{0, -l_xy   , -l_yy   , -l_zy   },
                       {0, l_x*l_xy, l_x*l_yy, l_x*l_zy},
                       {0, l_y*l_xy, l_y*l_yy, l_y*l_zy},
                       {0, l_z*l_xy, l_z*l_yy, l_z*l_zy}});
            // clang-format on

            ret.chip(2, 2) =
                F_y * l2 + F * (l2y + l2y.shuffle(Eigen::array<int, 2>{1, 0}));
        }
        // z
        {
            const double r_z = (z +
                                (sqsum * z + 2 * a2 * z) /
                                    sqrt(sqsum * sqsum + 4 * z * z * a2)) /
                               (2 * r);
            const double F_z =
                -6 * mass_ * r2 * r_z / (r2 * r2 + a2 * z * z) -
                F / (r2 * r2 + a2 * z * z) * (4 * r * r2 * r_z + 2 * a2 * z);

            const double /*l_tz = 0,*/
                l_xz = (x * r_z * (r2 + a2) - 2 * r * (r * x - a_ * y) * r_z) /
                       r2a2_2,
                l_yz = (y * r_z * (r2 + a2) - 2 * r * (r * y + a_ * x) * r_z) /
                       r2a2_2,
                l_zz = (r - z * r_z) / r2;

            EigenRk2Tensor<4> l2z;
            // clang-format off
        l2z.setValues({{0, -l_xz   , -l_yz   , -l_zz   },
                       {0, l_x*l_xz, l_x*l_yz, l_x*l_zz},
                       {0, l_y*l_xz, l_y*l_yz, l_y*l_zz},
                       {0, l_z*l_xz, l_z*l_yz, l_z*l_zz}});
            // clang-format on

            ret.chip(3, 2) =
                F_z * l2 + F * (l2z + l2z.shuffle(Eigen::array<int, 2>{1, 0}));
        }

        return ret;
    }

    EigenRk1Tensor<4> ksi_to_kso_function(const EigenRk1Tensor<4> &X_in) const {
        using namespace std;

        const double x_in = X_in(1), y_in = X_in(2), z_in = X_in(3);

        const double sqsum = x_in * x_in + y_in * y_in + z_in * z_in - a_ * a_;
        const double r = sqrt(
            .5 * (sqsum + sqrt(sqsum * sqsum + 4 * z_in * z_in * a_ * a_)));

        const double log_term = bl_ks_log_term(r);

        const double t_out = X_in(0) - 
            4 * mass_ *
                (mass_ * log_term + .5 * log(a_ * a_ - 2 * mass_ * r + r * r));

        // xy rotation coeffs
        const double a2pr2 = a_ * a_ + r * r;
        const double a2mr2 = a_ * a_ - r * r;
        const double c = cos(a_ * log_term);
        const double s = sin(a_ * log_term);

        const double R1 =
            (-2 * a2mr2 * c * c - 4 * s * a_ * r * c + a2mr2) / a2pr2;
        const double R2 =
            (4 * r * a_ * c * c - 2 * s * a2mr2 * c - 2 * a_ * r) / a2pr2;

        const double x_out = R1 * x_in + R2 * y_in;
        const double y_out = -R2 * x_in + R1 * y_in;

        EigenRk1Tensor<4> ret;
        // the z coordinates are the same
        ret.setValues({t_out, x_out, y_out, z_in});
        
        return ret;
    }

    ArcMatrix<4> ksi_to_kso_jacobian(const EigenRk1Tensor<4> &X_in) const {
        
        const double x_in = X_in(1), y_in = X_in(2), z_in = X_in(3);

        const double sqsum = x_in * x_in + y_in * y_in + z_in * z_in - a_ * a_;
        const double r = sqrt(
            .5 * (sqsum + sqrt(sqsum * sqsum + 4 * z_in * z_in * a_ * a_)));

        const double log_term = bl_ks_log_term(r);

        // r depends on (x,y,z), so easiest to compute the derivatives using the
        // chain rule.
        // partial derivatives of r:
        const double r_x =
            (x_in +
             sqsum * x_in / sqrt(sqsum * sqsum + 4 * z_in * z_in * a_ * a_)) /
            (2 * r);
        const double r_y =
            (y_in +
             sqsum * y_in / sqrt(sqsum * sqsum + 4 * z_in * z_in * a_ * a_)) /
            (2 * r);
        const double r_z =
            (z_in + (sqsum * z_in + 2 * a_ * a_ * z_in) /
                        sqrt(sqsum * sqsum + 4 * z_in * z_in * a_ * a_)) /
            (2 * r);

        // r derivative of t_out 

        const double dlog_termdr =
            1 / ( (r - mass_) * (r - mass_) - mass_ * mass_ + a_ * a_);

        const double dt_outdr =
            -4 * mass_ *
            (mass_ * dlog_termdr +
             (r - mass_) / (a_ * a_ - 2 * mass_ * r + r * r));

        // xy terms
        const double a2pr2 = a_ * a_ + r * r;
        const double a2mr2 = a_ * a_ - r * r;
        const double c = cos(a_ * log_term);
        const double s = sin(a_ * log_term);

        const double R1 =
            (-2 * a2mr2 * c * c - 4 * s * a_ * r * c + a2mr2) / a2pr2;
        const double R2 =
            (4 * r * a_ * c * c - 2 * s * a2mr2 * c - 2 * a_ * r) / a2pr2;

        const double dR1dr = 4 * a_ * (a2pr2 * dlog_termdr - 1) *
                             (-2 * r * a_ * c * c + s * a2mr2 * c + a_ * r) /
                             (a2pr2 * a2pr2);

        const double dR2dr = -4 * a_ * (a2pr2 * dlog_termdr - 1) *
                             (a2mr2 * c * c + 2 * s * a_ * r * c - .5 * a2mr2) /
                             (a2pr2 * a2pr2);

        const double dx_outdx_in = R1 + (x_in * dR1dr + y_in * dR2dr) * r_x;
        const double dx_outdy_in = R2 + (x_in * dR1dr + y_in * dR2dr) * r_y;
        const double dx_outdz_in = (x_in * dR1dr + y_in * dR2dr) * r_z;

        const double dy_outdx_in = -R2 + (-x_in * dR2dr + y_in * dR1dr) * r_x;
        const double dy_outdy_in = R1 + (-x_in * dR2dr + y_in * dR1dr) * r_y;
        const double dy_outdz_in = (-x_in * dR2dr + y_in * dR1dr) * r_z;
        ArcMatrix<4> ret;
        // clang-format off
        ret << 1, dt_outdr*r_x, dt_outdr*r_y, dt_outdr*r_z,
               0, dx_outdx_in , dx_outdy_in , dx_outdz_in ,
               0, dy_outdx_in , dy_outdy_in , dy_outdz_in ,
               0, 0           , 0           , 1           ;
        // clang-format on
        return ret;
    }

    EigenRk1Tensor<4> kso_to_ksi_function(
        const EigenRk1Tensor<4> &X_out) const {
            
        const double x_out = X_out(1), y_out = X_out(2), z_out = X_out(3);

        const double sqsum =
            x_out * x_out + y_out * y_out + z_out * z_out - a_ * a_;
        const double r = sqrt(
            .5 * (sqsum + sqrt(sqsum * sqsum + 4 * z_out * z_out * a_ * a_)));

        const double log_term = bl_ks_log_term(r);

        const double t_in = X_out(0) + 
            4 * mass_ *
                (mass_ * log_term + .5 * log(a_ * a_ - 2 * mass_ * r + r * r));
        
        // xy rotation coeffs
        const double a2pr2 = a_ * a_ + r * r;
        const double a2mr2 = a_ * a_ - r * r;
        const double c = cos(a_ * log_term);
        const double s = sin(a_ * log_term);

        const double R1 =
            (-2 * a2mr2 * c * c - 4 * s * a_ * r * c + a2mr2) / a2pr2;
        const double R2 =
            (4 * r * a_ * c * c - 2 * s * a2mr2 * c - 2 * a_ * r) / a2pr2;

        const double x_in = R1 * x_out - R2 * y_out;
        const double y_in = R2 * x_out + R1 * y_out;
        
        EigenRk1Tensor<4> ret;
        // the z coordinates are the same
        ret.setValues({t_in, x_in, y_in, z_out});
        
        return ret;
    
    }

};
}
} // namespace arcmancer::kerr
