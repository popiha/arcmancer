#pragma once

#include <vector>
#include <iostream>

#include "arcmancer/geometry/surface.h"
#include "arcmancer/spacetimes/agm/agm_functions.h"

#include "kerr_spacetime.h"

namespace arcmancer {
namespace kerr {

// Examples of Surface implementations for the Kerr spacetime



/** \brief  Surface of a rotating neutron star using the relations in 
 * AlGendy & Morsink (2014).
 *
 * Models the oblate surface of a rotating neutron star in a Kerr background
 * spacetime.
 * Similar to `AGMNeutronStarSurface`.
 *
 * \ingroup manifolds
 */
class KerrNeutronStarSurface : public Surface<KerrSpacetime> {

public:

    /**\brief Constructor
     *
     * \param M the background spacetime.
     * \param equatorial_radius the equatorial radius in coordinate units.
     * \param angular_velocity the angular velocity of the surface 
     * in coordinate units.
     */
    KerrNeutronStarSurface(const KerrSpacetime &M, double equatorial_radius, double angular_velocity) 
        : Surface<KerrSpacetime>(M) {
        // TODO: refactor this so that we don't store our own copies of
        // all the parameters

        mass_             = M.mass();
        angular_velocity_ = angular_velocity;
        R_eq_             = equatorial_radius;

        compactness_      = mass_ / R_eq_;
        Omega_bar_        = agm::Omega_bar(angular_velocity_, mass_, R_eq_);
        q_                = agm::q(Omega_bar_, compactness_);
        beta_             = agm::beta(Omega_bar_, compactness_);

        // o2-term = R_polar / R_equatorial - 1
        o2term_ = agm::o2(Omega_bar_, compactness_);

        // ellipsoidal flattening parameter
        double flattening = 1 - radius(0) / R_eq_;

        Log::log()->info(
                "KerrNeutronStarSurface:\n"
                "    eq. radius  {}\n"
                "    mass        {}\n"
                "    ang. vel.   {}\n"
                "    compactness {}\n"
                "    flattening  {}\n"
                "    Omega_bar   {}\n"
                "    o2term      {}\n"
                "    q           {}\n"
                "    beta        {}",
                R_eq_,
                mass_,
                angular_velocity_,
                compactness_,
                flattening,
                Omega_bar_,
                o2term_,
                q_,
                beta_);

    }

    /** \brief Radius as a function of colatitude, in schwarzschild coordinates
     *
     *  \param colat the colatitude.
     *  \return Radius of the surface at the given colatitude.
     */
    double radius(double colat) const {
        return R_eq_ * (1 + o2term_ * std::pow(std::cos(colat), 2));
    }

    double value(const PointType &position) const override {
        // Get radius in the boyer-lindquist chart
        const EigenRk1Tensor<4> bl = position.coordinates(M().boyer_lindquist);
        const double r_sch = bl(1);

        // radius depends on colatitude
        double star_radius = radius(bl(2));

        double retval = star_radius - r_sch;

        Log::log()->debug(
                 "KerrNeutronStarSurface: pos {}\n"
                 "  r_sch {}\n"
                 "  theta {}\n"
                 "  r(th) {}\n"
                 "  value {}\n",
                 position,
                 r_sch,
                 bl(2),
                 star_radius,
                 retval
                 );

        return retval;
    }

    CotangentVector<KerrSpacetime> gradient(const PointType &position) const override {
        const EigenRk1Tensor<4> bl_x = position.coordinates(M().boyer_lindquist);

        // derivative of the radius function w.r.t theta
        double radius_th = -R_eq_ * o2term_ * std::sin(2 * bl_x(2));

        // grad = (0, -1, radius_th, 0)

        return CotangentVector<KerrSpacetime>(
                position, position.M().boyer_lindquist,
                {0, -1, radius_th, 0});
    }

    TangentVector<KerrSpacetime> observer(const PointType &position) const override {
        // compute the four-velocity at the surface of a rotating
        // neutron star

        // need the metric components in the boyer-lindquist chart
        const KerrSpacetime &M = position.M();
        EigenRk2Tensor<4> g = M.metric_components(position, M.boyer_lindquist);

        // dphi / dt
        // = angular velocity as measured by observer at infinity
        // = (u^phi / u^t) evaluated at any radial distance
        double dpdt = angular_velocity_;

        // time component
        double ut =
            1 / std::sqrt(g(0, 0) + 2 * dpdt * g(0, 3) + dpdt * dpdt * g(3, 3));
        // phi component
        double uphi = dpdt * ut;

        TangentVector<KerrSpacetime> ret { position, M.boyer_lindquist, {ut, 0, 0, uphi} };

        // if trying to get an observer at a point that is rotating
        // above the speed of light, the observer will be spacelike.
        // notify the user, as any results will most likely be bogus

        // TODO: Add check for timelikedness

        return ret;
    }

private:

    double mass_;
    double angular_velocity_;

    double R_eq_;
    double compactness_;
    double Omega_bar_;
    double o2term_;
    double q_;
    double beta_;
};

} // namespace kerr
} // namespace arcmancer
