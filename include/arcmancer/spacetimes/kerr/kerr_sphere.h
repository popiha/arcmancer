#pragma once
#include "kerr_spacetime.h"
#include "arcmancer/geometry.h"

namespace arcmancer{ namespace kerr{

/** \brief A sphere of constant Boyer-Lindquist r
 *  
 *  This is the simplest possible implementation using the Boyer-Lindquist 
 *  coordinates, and as a result behaves poorly near the event horizon.
 *  In particular, if the curve uses coordinates that are regular across the
 *  horizon, it can cross the horizon during the same step as it crosses this 
 *  sphere, which results in potential failure of the implementation.
 *
 *  For surfaces that behave well near the horizon, see \c OutgoingKerrSphere
 *  and \c IngoingKerrSphere.
 *
 *  The observer is a zero-angular momentum observer (ZAMO)
 *  \ingroup manifolds
 */
class KerrSphere : public Surface<KerrSpacetime> {
public:
    /** \brief Constructor
     *  \param M the underlying spacetime
     *  \param r the radius of the sphere
     */
    KerrSphere(const KerrSpacetime &M, double r)
    :Surface<KerrSpacetime>(M), r_(r){}

    double value(const PointType &position) const override {
        return position.coordinates(M().boyer_lindquist)(1)-r_;    
    }

    CotangentVector<KerrSpacetime> 
    gradient(const PointType& position) const override{
         return {position, M().boyer_lindquist, {0,1,0,0}};
    }

    TangentVector<KerrSpacetime> observer(const PointType &position)
    const override {
        return normalized(CotangentVector<KerrSpacetime>
                            {position, M().boyer_lindquist, {1,0,0,0}}
                         ).raise_index<0>();    
    }

private:
    double r_;
        
};


/** \brief A sphere of constant r defined in the outgoing K-S coordinates
 *  
 *  Behaves well for the purpose of a surface marking the past event horizon 
 *  as well as other general purpose spheres when dealing with mainly outgoing
 *  curves.
 *
 *  The observer has zero momentum along the spatial coordinates.
 *  \ingroup manifolds
 */
class OutgoingKerrSphere : public Surface<KerrSpacetime> {
public:
    /** \brief Constructor
     *  \param M the underlying spacetime
     *  \param r the radius of the sphere
     */
    OutgoingKerrSphere(const KerrSpacetime &M, double r)
    :Surface<KerrSpacetime>(M), 
     r2_(r*r), a2_(std::pow(M.mass()*M.chi(),2)){}

    double value(const PointType &position) const override {
        // Cannot use BL r directly, since coordinate conversions would
        // fail inside the event horizon.
        auto coords = position.coordinates(M().kerr_schild_out);
        
        const double x = coords(1);
        const double y = coords(2);
        const double z = coords(3);
        
        const double R2 = x*x + y*y + z*z;
        const double sqsum = R2 - a2_;
        const double r2 = .5*(sqsum + sqrt(sqsum*sqsum + 4*z*z*a2_));

        return r2-r2_;    
    }

    CotangentVector<KerrSpacetime> 
    gradient(const PointType& position) const override{
        auto coords = position.coordinates(M().kerr_schild_out);
        
        const double x = coords(1);
        const double y = coords(2);
        const double z = coords(3);
        
        const double R2 = x*x + y*y + z*z;
        const double sqsum = R2 - a2_;
        const double r = sqrt(.5*(sqsum + sqrt(sqsum*sqsum + 4*z*z*a2_)));
        const double r_x = (x + sqsum*x/sqrt(sqsum*sqsum + 4*z*z*a2_))/(2*r);
        const double r_y = (y + sqsum*y/sqrt(sqsum*sqsum + 4*z*z*a2_))/(2*r);
        const double r_z 
            = (z + (sqsum*z+2*a2_*z)/sqrt(sqsum*sqsum + 4*z*z*a2_))/(2*r);

        return {position,
                M().kerr_schild_out,
                {0, 2 * r * r_x, 2 * r * r_y, 2 * r * r_z}};
    }

    TangentVector<KerrSpacetime> observer(const PointType &position)
    const override {
        return normalized( CotangentVector<KerrSpacetime> 
                          {position, M().kerr_schild_out, {1,0,0,0}}
                         ).raise_index<0>();    
    }

private:
    double r2_;
    double a2_;
        
};

// XXX: Since the only necessary change compared to the outgoing version is
// the different chart tag, it would be simple to merge the two implementations.
// However, this may not be worth the trouble, as having the definition chart
// visible at the type level is also somewhat nice.

/** \brief A sphere of constant r defined in the ingoing K-S coordinates
 *  
 *  Behaves well for the purpose of a surface marking the future event horizon 
 *  as well as other general purpose spheres when dealing with mainly ingoing 
 *  curves.
 *
 *  The observer has zero momentum along the spatial coordinates.
 *  \ingroup manifolds
 */
class IngoingKerrSphere : public Surface<KerrSpacetime> {
public:
    /** \brief Constructor
     *  \param M the underlying spacetime
     *  \param r the radius of the sphere
     */
    IngoingKerrSphere(const KerrSpacetime &M, double r)
    :Surface<KerrSpacetime>(M), 
     r2_(r*r), a2_(std::pow(M.mass()*M.chi(),2)){}

    double value(const PointType &position) const override {
        auto coords = position.coordinates(M().kerr_schild_in);
        
        const double x = coords(1);
        const double y = coords(2);
        const double z = coords(3);
        
        const double R2 = x*x + y*y + z*z;
        const double sqsum = R2 - a2_;
        const double r2 = .5*(sqsum + sqrt(sqsum*sqsum + 4*z*z*a2_));

        return r2-r2_;    
    }

    CotangentVector<KerrSpacetime> 
    gradient(const PointType& position) const override{
        auto coords = position.coordinates(M().kerr_schild_in);
        
        const double x = coords(1);
        const double y = coords(2);
        const double z = coords(3);
        
        const double R2 = x*x + y*y + z*z;
        const double sqsum = R2 - a2_;
        const double r = sqrt(.5*(sqsum + sqrt(sqsum*sqsum + 4*z*z*a2_)));
        const double r_x = (x + sqsum*x/sqrt(sqsum*sqsum + 4*z*z*a2_))/(2*r);
        const double r_y = (y + sqsum*y/sqrt(sqsum*sqsum + 4*z*z*a2_))/(2*r);
        const double r_z 
            = (z + (sqsum*z+2*a2_*z)/sqrt(sqsum*sqsum + 4*z*z*a2_))/(2*r);

        return {position,
                M().kerr_schild_in,
                {0, 2 * r * r_x, 2 * r * r_y, 2 * r * r_z}};
    }

    TangentVector<KerrSpacetime> observer(const PointType &position)
    const override {
        return normalized( CotangentVector<KerrSpacetime> 
                          {position, M().kerr_schild_in, {1,0,0,0}}
                         ).raise_index<0>();    
    }

private:
    double r2_;
    double a2_;
        
};

}} //namespace arcmancer::kerr
