#pragma once

#include <vector>
#include <iostream>

#include "arcmancer/geometry/surface.h"

#include "agm.h"

namespace arcmancer {
namespace agm {

// Examples of Surface implementations for the AGM spacetime


/** \brief Surface of a rotating neutron star.
 *
 * Uses the relations in AlGendy and Morsink 2014 
 * [doi: https://doi.org/10.1088/0004-637X/791/2/78].
 */
class AGMNeutronStarSurface : public Surface<AGMSpacetime> {

public:

    /** \brief Constructor.
     *
     * \param M the underlying spacetime.
     *
     */
    AGMNeutronStarSurface(const AGMSpacetime &M) : Surface<AGMSpacetime>(M) {
        // TODO: refactor this so that we don't store our own copies of
        // all the parameters

        mass_             = M.mass();
        angular_velocity_ = M.angular_velocity();
        R_eq_             = M.R_eq();
        compactness_      = M.compactness();
        Omega_bar_        = M.Omega_bar();
        q_                = M.q();
        beta_             = M.beta();

        // o2-term = R_polar / R_equatorial - 1
        o2term_ = agm::o2(Omega_bar_, compactness_);

        // ellipsoidal flattening parameter
        double flattening = 1 - radius(0) / R_eq_;

        Log::log()->info(
                "AGMNeutronStarSurface:\n"
                "    eq. radius  {}\n"
                "    mass        {}\n"
                "    ang. vel.   {}\n"
                "    compactness {}\n"
                "    flattening  {}\n"
                "    Omega_bar   {}\n"
                "    o2term      {}\n"
                "    q           {}\n"
                "    beta        {}",
                R_eq_,
                mass_,
                angular_velocity_,
                compactness_,
                flattening,
                Omega_bar_,
                o2term_,
                q_,
                beta_);

    }

    /** \brief Radius as a function of colatitude, in schwarzschild coordinates.
     * \param colat the colatitude.
     */
    double radius(double colat) const {
        return R_eq_ * (1 + o2term_ * std::pow(std::cos(colat), 2));
    }

    double value(const PointType &position) const override {
        // Get coordinates in the isotropic chart
        const EigenRk1Tensor<4> iso_x = position.coordinates(M().isotropic);

        // radius depends on colatitude. the AGM formula gives it in
        // Schwarzschild coordinates
        double star_radius = radius(iso_x(2));

        // convert the r coordinate to schwarzschild
        // TODO: in the future, we can also just ask for the coordinates
        // in the Schwarzschild-like chart as well, but this is ok for
        // now, and actually has less computation

        double r_sch = agm::isotropic_to_schwarzschild(mass_, q_, beta_, iso_x);

        double retval = star_radius - r_sch;

        Log::log()->debug(
                 "AGMNeutronStarSurface: pos {}\n"
                 "  r_iso {}\n"
                 "  r_sch {}\n"
                 "  theta {}\n"
                 "  r(th) {}\n"
                 "  value {}\n",
                 position,
                 iso_x(1),
                 r_sch,
                 iso_x(2),
                 star_radius,
                 retval
                 );

        return retval;
    }

    CotangentVector<AGMSpacetime> gradient(const PointType &position) const override {
        // Get coordinates in the isotropic chart
        const EigenRk1Tensor<4> iso_x = position.coordinates(position.M().isotropic);

        // derivative of the radius function w.r.t theta
        double radius_th = R_eq_ * o2term_ * std::sin(2 * iso_x(2));

        // gradient of function above
        const double r = iso_x(1);

        // values of the metric functions
        const double nu =
            agm::metric_nu(mass_, q_, beta_, iso_x);
        const double B = agm::metric_B(mass_, beta_, iso_x);
        const double emnu = std::exp(-nu);

        // values of the derivatives of the metric functions w.r.t.
        // r and theta
        const double nu_dr =
            agm::metric_nu_dr(mass_, q_, beta_, iso_x);
        const double nu_th =
            agm::metric_nu_th(mass_, q_, iso_x);
        const double B_dr = agm::metric_B_dr(mass_, beta_, iso_x);
        const double B_th = 0;

        const double S_dr =
            -emnu * B - emnu * r * B_dr + emnu * r * B * nu_dr;
        const double S_th =
            -radius_th + emnu * r * B * nu_th;

        // We can now compute the gradient in the isotropic chart
        CotangentVector<AGMSpacetime> ret(position, position.M().isotropic, 
                {0, S_dr, S_th, 0});

        Log::log()->debug("AGMNeutronStarSurface: gradient:\n"
            "nu     {}\n"
            "B      {}\n"
            "emnu   {}\n"
            "rad_th {}\n"
            "nu_dr  {}\n"
            "nu_th  {}\n"
            "B_dr   {}\n"
            "B_th   {}\n"
            "S_dr   {}\n"
            "S_th   {}\n"
            "grad   {}\n",
            nu   ,
            B    ,
            emnu ,
            radius_th,
            nu_dr,
            nu_th,
            B_dr ,
            B_th ,
            S_dr,
            S_th,
            ret);

        return ret;
    }

    TangentVector<AGMSpacetime> observer(const PointType &position) const override {
        // compute the four-velocity at the surface of a rotating
        // neutron star

        // need the metric components in the isotropic chart
        const AGMSpacetime &M = position.M();
        EigenRk2Tensor<4> g = M.metric_components(position, M.isotropic);

        // dphi / dt
        // = angular velocity as measured by observer at infinity
        // = (u^phi / u^t) evaluated at any radial distance
        double dpdt = angular_velocity_;

        // time component
        double ut =
            1 / std::sqrt(g(0, 0) + 2 * dpdt * g(0, 3) + dpdt * dpdt * g(3, 3));
        // phi component
        double uphi = dpdt * ut;

        TangentVector<AGMSpacetime> ret { position, M.isotropic, {ut, 0, 0, uphi} };

        // if trying to get an observer at a point that is rotating
        // above the speed of light, the observer will be spacelike.
        // notify the user, as any results will most likely be bogus

        // TODO: Add check for timelikedness
#if 0
        if (vector_type(metric, ret) != VectorType::timelike) {
            Log::log->err( 
                    "observer_fourvel: observer not timelike! squared magnitude {}\n"
                       "    pos: {}\n"
                       " metric: {}\n"
                       "     ut: {}\n"
                       "   uphi: {}\n"
                       "    ret: {}\n",
                       metric.sqnorm(ret),
                       position,
                       g,
                       ut,
                       uphi,
                       ret);
        }
#endif

        return ret;
    }

private:

    double mass_;
    double angular_velocity_;

    double R_eq_;
    double compactness_;
    double Omega_bar_;
    double o2term_;
    double q_;
    double beta_;
};

} // namespace agm
} // namespace arcmancer
