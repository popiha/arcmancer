#pragma once

#include <string>

#include "arcmancer/geometry/metric_space.h"

#include "agm_functions.h"

namespace arcmancer {
/**
 * \brief Classes and functions related to the AlGendy-Morsink (AGM) spacetime
 * \ingroup manifolds
 */
namespace agm {

using namespace std::literals::string_literals; // for ""s suffix operator

/** \brief The AlGendy-Morsink (AGM) spacetime.
 *
 * The AGM spacetime 
 * (AlGendy and Morsink 2014 [doi: https://doi.org/10.1088/0004-637X/791/2/78])
 * is a specific choice of the Butterworth-Ipser spacetime
 * (Butterworth and Ipser 1976 [doi: https://doi.org/10.1086/154163])
 * which describes the exterior of an axisymmetric, rotating fluid body.
 * The AGM spacetime is a special case representing the spacetime around a 
 * physically realistic oblate rotating neutron star.
 *
 * The AGM representation is accurate up to second order in the dimensionless 
 * rotation parameter \f$ \bar{\Omega} = \Omega R_e^{3/2} M^{−1/2} \f$ ,
 * where \f$ \Omega \f$ is the rotational angular velocity of the star as seen 
 * by a distant observer, \f$ R_e \f$ is the equatorial radius of the
 * star and \f$ M \f$ is the mass of the star.
 *
 */
class AGMSpacetime : public MetricSpace<AGMSpacetime, 4> {
public:

    std::string name() const { return "AlGendy-Morsink Spacetime"s; }
    ///< \brief Get a description of this spacetime.

    /** \brief Isotropic spherical coordinates.
     *
     * The \f$ (t, \bar{r}, \theta, \phi) \f$ spherical coordinate system
     * which reduces to the isotropic coordinates at the limit
     * of zero spin.
     */
    const ChartType isotropic = {*this, "Isotropic coordinates"s};
    

    // TODO: Implement schwarzschild-like chart
    //const ChartType schwarzshild = {*this, "Schwarzschild-like coordinates"s};

    /** \brief Constructor.
     *
     * The unit system assumes G = c = 1.
     *
     * \param R_eq the equatorial radius of the neutron star.
     * \param mass the mass of the star.
     * \param angvel the angular velocity of the star.
     */
    AGMSpacetime(double R_eq, double mass, double angvel)
    : mass_(mass), angular_velocity_(angvel), R_eq_(R_eq) {

        compactness_ = mass_ / R_eq_;
        Omega_bar_   = agm::Omega_bar(angular_velocity_, mass_, R_eq_);
        j_           = agm::j(Omega_bar_, compactness_, R_eq_, mass_);

        q_           = agm::q(Omega_bar_, compactness_);
        beta_        = agm::beta(Omega_bar_, compactness_);

        Log::log()->info(
                "AGMSpacetime():\n"
                "    eq. radius  {}\n"
                "    mass        {}\n"
                "    ang. vel.   {}\n"
                "    compactness {}\n"
                "    Omega_bar   {}\n"
                "    q           {}\n"
                "    beta        {}\n"
                "    j           {}",
                R_eq_            ,
                mass_            ,
                angular_velocity_,
                compactness_     ,
                Omega_bar_       ,
                q_               ,
                beta_            ,
                j_               );


        add_chart(isotropic, &AGMSpacetime::isotropic_metric,
                             &AGMSpacetime::isotropic_metric_derivatives);
    }

    // bunch of getters. slightly inconvenient to need to have these

    double mass()               const { return mass_;             } 
    ///< \brief Get the mass of the star.
    double angular_velocity()   const { return angular_velocity_; } 
    ///< \brief Get the angular velovity of the star.
    double R_eq()               const { return R_eq_;             } 
    ///< \brief Get the equatorial radius of the star.
    double compactness()        const { return compactness_;      } 
    ///< \brief Get the equatorial radius of the star.
    double Omega_bar()          const { return Omega_bar_;        } 
    ///< \brief Get the dimensionless rotation parameter.
    double q()                  const { return q_;                } 
    ///< \brief Get the dimensionless  moment of energy density.
    double beta()               const { return beta_;             } 
    ///< \brief Get the dimensionless  moment of pressure.
    double j()                  const { return j_;                } 
    ///< \brief Get the dimensionless angular momentum.

protected:

    // metric functions
    //
    // NOTE:
    // these are computed using coordinates in which the r coordinate
    // reduces to the _isotropic_ r coordinate (not the Schwarzschild
    // coordinate) in the limit of vanishing spin

    EigenRk2Tensor<4> isotropic_metric(const EigenRk1Tensor<4>& x) const {
        using namespace std;
        const double r = x(1);
        const double th = x(2);
        const double nu    = agm::metric_nu(mass_, q_, beta_, x);
        const double B     = agm::metric_B(mass_, beta_, x);
        const double omega = agm::metric_omega(mass_, j_, x);
        const double zeta  = agm::metric_zeta(mass_, beta_, x);

        // straight outta mathematica
        // NOTE: The diagonal terms have to be POSITIVE in order to
        // conform to the sign convention (+---)
        EigenRk2Tensor<4> ret;
        ret.setValues(
                {
                {exp(2*nu) - (pow(B,2)*pow(r,2)*pow(omega,2)*pow(sin(th),2))/exp(2*nu),0,0,((pow(B,2)*pow(r,2)*omega*pow(sin(th),2))/exp(2*nu))},
                {0,-exp(2*zeta - 2*nu),0,0},
                {0,0,-(exp(2*zeta - 2*nu)*pow(r,2)),0},
                {((pow(B,2)*pow(r,2)*omega*pow(sin(th),2))/exp(2*nu)),0,0,-((pow(B,2)*pow(r,2)*pow(sin(th),2))/exp(2*nu))}
            });

        return ret;
    }

    EigenRk3Tensor<4> 
    isotropic_metric_derivatives(const EigenRk1Tensor<4> &x) const {
        using namespace std;

        const double r = x(1);
        const double th = x(2);
        const double nu       = agm::metric_nu(mass_, q_, beta_, x);
        const double B        = agm::metric_B(mass_, beta_, x);
        const double omega    = agm::metric_omega(mass_, j_, x);
        const double zeta     = agm::metric_zeta(mass_, beta_, x);

        const double nu_dr    = agm::metric_nu_dr(mass_, q_, beta_, x);
        const double nu_th    = agm::metric_nu_th(mass_, q_, x);

        const double B_dr     = agm::metric_B_dr(mass_, beta_, x);
        const double omega_dr = agm::metric_omega_dr(mass_, j_, x);

        const double zeta_dr  = agm::metric_zeta_dr(mass_, beta_, x);
        const double zeta_th  = agm::metric_zeta_th(mass_, beta_, x);

        EigenRk3Tensor<4> ret;
        ret.setZero();

        // straight outta mathematica
        //

        // r derivatives

        // TODO: look into the Eigen::Tensor::chip interface. It should
        // yield a nice lvalue, but it apparently doesn't support
        // setValues _or_ assignment of initialization lists, which is
        // slightly inconvenient.
        EigenRk2Tensor<4> der;
        der.setValues(
                {
                    {(2*exp(4*nu)*nu_dr + 2*B*r*omega*pow(sin(th),2)*(-(r*omega*B_dr) - B*(omega + r*omega_dr) + B*r*omega*nu_dr))/exp(2*nu),
                        0,0, -(B*r*pow(sin(th),2)*(-2*r*omega*B_dr + B*(-(r*omega_dr) + 2*omega*(-1 + r*nu_dr))))/exp(2*nu)},
                    {0,-2*exp(2*zeta - 2*nu)*(zeta_dr - nu_dr),0,0},
                    {0,0,-2*exp(2*zeta - 2*nu)*r*(1 + r*zeta_dr - r*nu_dr),0},
                    {-(B*r*pow(sin(th),2)*(-2*r*omega*B_dr + B*(-(r*omega_dr) + 2*omega*(-1 + r*nu_dr))))/exp(2*nu),
                        0,0, (2*B*r*pow(sin(th),2)*(-(r*B_dr) + B*(-1 + r*nu_dr)))/exp(2*nu)}
                });

        ret.chip(1,2) = der;

        // theta derivatives
        der.setValues(
                {
                    {2*exp(2*nu)*nu_th + (2*pow(B,2)*pow(r,2)*pow(omega,2)*sin(th)*(-cos(th) + sin(th)*nu_th))/exp(2*nu),
                        0,0, -(2*pow(B,2)*pow(r,2)*omega*sin(th)*(-cos(th) + sin(th)*nu_th))/exp(2*nu)},
                    {0,-2*exp(2*zeta - 2*nu)*(zeta_th - nu_th),0,0},
                    {0,0,-2*exp(2*zeta - 2*nu)*pow(r,2)*(zeta_th - nu_th),0},
                    {-(2*pow(B,2)*pow(r,2)*omega*sin(th)*(-cos(th) + sin(th)*nu_th))/exp(2*nu),
                        0,0, (2*pow(B,2)*pow(r,2)*sin(th)*(-cos(th) + sin(th)*nu_th))/exp(2*nu)}
                });
        ret.chip(2,2) = der;

        return ret;
    }

private:

    // mass of the central object
    double mass_;
    // angular velocity measured by distant observer
    double angular_velocity_;

    // quantities defined in Algendy and Morsink
    double R_eq_;
    double compactness_;
    double Omega_bar_;
    double q_;
    double beta_;
    double j_;

};

} // namespace agm
} // namespace arcmancer
