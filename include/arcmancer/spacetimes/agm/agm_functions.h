#pragma once

#include <cmath>

#include <boost/math/special_functions/legendre.hpp>

#include "arcmancer/geometry/types.h"

// Formulae from AlGendy & Morsink (2014)

namespace arcmancer {
namespace agm {
using boost::math::legendre_p;

inline
double Omega_bar(double angular_velocity, double mass, double equatorial_radius) {
    return angular_velocity * std::sqrt(std::pow(equatorial_radius, 3) / mass);
}

inline constexpr
double o2(double Omega_bar, double compactness) {
    const double a0 = -0.788, a1 = 1.030;
    return Omega_bar * Omega_bar * (a0 + a1 * compactness);
}

inline
double i(double compactness) {
    const double x = compactness;
    const double a0 = 1.146, a1 = -2.53, a2 = 5.6;
    return std::sqrt(x) * (a0 + a1 * x + a2 * x*x);
}

inline
double j(double Omega_bar, double compactness, double equatorial_radius, double mass) {
    return i(compactness) * Omega_bar * std::sqrt(equatorial_radius / mass);
}

inline constexpr
double q(double Omega_bar, double compactness) {
    const double a2 = -0.11;
    return Omega_bar*Omega_bar * (a2/(compactness * compactness));
}

inline constexpr
double beta(double Omega_bar, double compactness) {
    const double a1 = 0.4454;
    return a1 * Omega_bar*Omega_bar * compactness;
}

//
// metric functions
// NOTE: all functions expect the position to be given 
// *in the isotropic chart*

inline
double metric_nu(double mass, double q, double beta, const EigenRk1Tensor<4> &x) {
    const double r = x(1);
    const double th = x(2);
    const double nu0 = std::log((1-mass/(2*r)) / (1 + mass/(2*r)));

    return nu0 + (beta/3 - q * legendre_p(2, std::cos(th)))*std::pow(mass/r, 3);
}

inline
double metric_B(double mass, double beta, const EigenRk1Tensor<4> &x) {
    const double r = x(1);
    const double b0 = 1 - std::pow(mass/(2*r), 2); // (1 - mass/(2*r))*(1 + mass/(2*r));

    return b0 + beta * std::pow(mass/r, 2);
}

inline
double metric_omega(double mass, double j, const EigenRk1Tensor<4> &x) {
    const double r = x(1);

    return 2*j/r * std::pow(mass/r, 2) * (1 - 3 * (mass/r));
}

inline
double metric_zeta(double mass, double beta, const EigenRk1Tensor<4> &x) {
    const double r = x(1);
    const double th = x(2);
    const double b0 = 1 - std::pow(mass/(2*r), 2); // (1 - mass/(2*r))*(1 + mass/(2*r));
    return std::log(b0) + beta * (4./3 * legendre_p(2, std::cos(th)) - 1./3) * std::pow(mass/r, 2);
}

//
// derivatives of the metric functions w.r.t. r and theta

inline
double metric_nu_dr(double mass, double q, double beta, const EigenRk1Tensor<4> &x) {
    const double r = x(1);
    const double th = x(2);
    const double nu0_dr = -4 * mass / (mass*mass - 4 * r *r);

    return nu0_dr + ( beta/3 - q * legendre_p(2, std::cos(th)) ) * (-3) * std::pow(mass, 3)/std::pow(r, 4);
}

inline
double metric_nu_th(double mass, double q, const EigenRk1Tensor<4> &x) {
    const double r = x(1);
    const double th = x(2);

    return 3 * q * 0.5 * std::sin(2*th) * std::pow(mass/r, 3);
}

inline
double metric_B_dr(double mass, double beta, const EigenRk1Tensor<4> &x) {
    const double r = x(1);
    return 0.5 * (1 - 4*beta) * mass*mass / std::pow(r, 3);
}

inline
double metric_omega_dr(double mass, double j, const EigenRk1Tensor<4> &x) {
    const double r = x(1);
    return 6 * j * mass*mass * (4*mass - r)/pow(r,5);
}

inline
double metric_zeta_dr(double mass, double beta, const EigenRk1Tensor<4> &x) {
    const double r = x(1);
    const double th = x(2);
    const double zeta0_dr = -2*mass*mass/(mass*mass*r - 4*std::pow(r,3));
    return zeta0_dr + beta * (4./3 * legendre_p(2, std::cos(th)) - 1./3) * (-2) * (mass*mass/std::pow(r,3));
}

inline
double metric_zeta_th(double mass, double beta, const EigenRk1Tensor<4> &x) {
    const double r = x(1);
    const double th = x(2);
    return - 2 * beta * mass*mass * std::sin(2*th) / (r*r);
}

// conversion from isotropic radius to schwarzschild radius at given
// position x
inline
double isotropic_to_schwarzschild(double mass, double q, double beta, const EigenRk1Tensor<4> &x) {
    // only r coordinate changes
    const double B = metric_B(mass, beta, x);
    const double nu = metric_nu(mass, q, beta, x);
    double r_sch = B * std::exp(-nu) * x(1);
    return r_sch;
}

} // namespace agm
} // namespace arcmancer
