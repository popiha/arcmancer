#pragma once
/** \file
 * \brief Convenience header for the AGM spacetime
 *
 * \ingroup manifolds
 */

#include "agm/agm.h"
#include "agm/agm_surface.h"
