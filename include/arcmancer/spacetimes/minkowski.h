#pragma once

#include <cmath>
#include <string>

#include "arcmancer/geometry/metric_space.h"

namespace arcmancer {
/** \brief Classes related to the Minkowski spacetime
 * \ingroup manifolds
 */
namespace minkowski {

using namespace std::literals::string_literals; // for ""s suffix operator

/** \brief The Minkowski spacetime
 *
 *  Has two charts: cartesian and spherical coordinates.
 *  \ingroup manifolds
 */
class MinkowskiSpacetime : public MetricSpace<MinkowskiSpacetime, 4> {

public:
    std::string name() const { return "Minkowski Spacetime"s; }

    // Charts are held as members that hold a reference to *this
    // initializing them could be done in the constructor initializer list also.
    const ChartType cartesian = {*this, "Cartesian coordinates"s};
    ///< \brief Cartesian coordinates

    const ChartType spherical = {*this, "Spherical coordinates"s};
    ///< \brief Spherical coordinates

    /** \brief Constructor
     */
    MinkowskiSpacetime() {
        // Add the chart definitions to the maps that are used internally
        // XXX:The syntax is a bit cumbersome since the standard doesn't allow
        // taking member function pointers as &function.

        add_chart(cartesian, &MinkowskiSpacetime::cartesian_metric,
                  &MinkowskiSpacetime::cartesian_metric_derivatives);
        add_chart(spherical, &MinkowskiSpacetime::spherical_metric,
                  &MinkowskiSpacetime::spherical_metric_derivatives);

        add_transition(spherical, cartesian,
                       &MinkowskiSpacetime::spherical_to_cartesian_function,
                       &MinkowskiSpacetime::spherical_to_cartesian_jacobian);
        add_transition(cartesian, spherical,
                       &MinkowskiSpacetime::cartesian_to_spherical_function,
                       &MinkowskiSpacetime::cartesian_to_spherical_jacobian);

        // Asserts that all added charts have transitions between each other
        validate_transitions();
    }

private:
    // The metrics and transitions between charts are currently implemented as
    // member functions that must be explicitly paired with the charts using
    // add_chart
    // and add_transition in the constructor

    EigenRk2Tensor<4> cartesian_metric(const EigenRk1Tensor<4> &) const {

        EigenRk2Tensor<4> g;
        // clang-format off
        g.setValues({{1,0,0,0},
                     {0,-1,0,0},
                     {0,0,-1,0},
                     {0,0,0,-1}});
        // clang-format on
        return g;
    }

    EigenRk3Tensor<4> cartesian_metric_derivatives(
        const EigenRk1Tensor<4> &) const {
        EigenRk3Tensor<4> g_derivs;
        g_derivs.setZero();

        return g_derivs;
    }

    EigenRk2Tensor<4> spherical_metric(const EigenRk1Tensor<4> &x) const {

        EigenRk2Tensor<4> g;
        double r = x(1);
        double th = x(2);
        double r2 = r * r;
        double sinth = std::sin(th);
        // clang-format off
        g.setValues({{1,0,0,0},
                     {0,-1,0,0},
                     {0,0,-r2,0},
                     {0,0,0,-r2*sinth*sinth}});
        // clang-format on
        return g;
    }

    EigenRk3Tensor<4> spherical_metric_derivatives(
        const EigenRk1Tensor<4> &x) const {
        EigenRk3Tensor<4> g_derivs;
        g_derivs.setZero();

        double r = x(1);
        double th = x(2);

        g_derivs(2, 2, 1) = -2 * r;
        g_derivs(3, 3, 1) = -2 * r * std::sin(th) * std::sin(th);
        g_derivs(3, 3, 2) = -2 * r * r * std::cos(th) * std::sin(th);

        return g_derivs;
    }

    EigenRk1Tensor<4> spherical_to_cartesian_function(
        const EigenRk1Tensor<4> &x) const {

        using std::cos;
        using std::sin;

        const double r = x(1);
        const double th = x(2);
        const double phi = x(3);

        EigenRk1Tensor<4> ret;
        ret.setValues({x(0), r * sin(th) * cos(phi), r * sin(th) * sin(phi),
                       r * cos(th)});
        return ret;
    }

    ArcMatrix<4> spherical_to_cartesian_jacobian(
        const EigenRk1Tensor<4> &x) const {

        using std::cos;
        using std::sin;

        const double r = x(1);
        const double th = x(2);
        const double phi = x(3);

        ArcMatrix<4> ret = ArcMatrix<4>::Zero();
        // clang-format off
        ret << 
            1, 0, 0, 0,
            0, sin(th)*cos(phi), r*cos(th)*cos(phi), -r*sin(th)*sin(phi),
            0, sin(th)*sin(phi), r*cos(th)*sin(phi), r*sin(th)*cos(phi),
            0, cos(th), -r*sin(th), 0;
        // clang-format on
        return ret;
    }

    EigenRk1Tensor<4> cartesian_to_spherical_function(
        const EigenRk1Tensor<4> &X) const {

        const double x = X(1);
        const double y = X(2);
        const double z = X(3);

        const double r = std::sqrt(x * x + y * y + z * z);
        const double th = std::acos(z / r);
        const double phi = std::atan2(y, x);

        EigenRk1Tensor<4> ret;
        ret.setValues({X(0), r, th, phi});
        return ret;
    }

    ArcMatrix<4> cartesian_to_spherical_jacobian(
        const EigenRk1Tensor<4> &x) const {
        return spherical_to_cartesian_jacobian(
                   cartesian_to_spherical_function(x))
            .inverse();
    }
};

/** \brief A version of the Minkowski spacetime with -+++ signature
 *
 *  Otherwise identical to \c MinkowskiSpacetime, only the metric signature is
 *  different.
 *  \ingroup manifolds
 */
// Just a copy, as e.g. templatizing MinkowskiSpacetime with the signature would
// require changing all the places where it is used.
// Since this is pretty much just for testing that the signature handling works,
// this seems good enough for now.
class MinkowskiSpacetimeSpacelike
    : public MetricSpace<MinkowskiSpacetimeSpacelike, 4,
                         LorentzianSpacelikeSignature> {

public:
    std::string name() const {
        return "Minkowski Spacetime with -+++ signature"s;
    }

    const ChartType cartesian = {*this, "Cartesian coordinates"s};

    const ChartType spherical = {*this, "Spherical coordinates"s};

    MinkowskiSpacetimeSpacelike() {

        add_chart(cartesian, &MinkowskiSpacetimeSpacelike::cartesian_metric,
                  &MinkowskiSpacetimeSpacelike::cartesian_metric_derivatives);
        add_chart(spherical, &MinkowskiSpacetimeSpacelike::spherical_metric,
                  &MinkowskiSpacetimeSpacelike::spherical_metric_derivatives);

        add_transition(
            spherical, cartesian,
            &MinkowskiSpacetimeSpacelike::spherical_to_cartesian_function,
            &MinkowskiSpacetimeSpacelike::spherical_to_cartesian_jacobian);
        add_transition(
            cartesian, spherical,
            &MinkowskiSpacetimeSpacelike::cartesian_to_spherical_function,
            &MinkowskiSpacetimeSpacelike::cartesian_to_spherical_jacobian);

        validate_transitions();
    }

private:
    EigenRk2Tensor<4> cartesian_metric(const EigenRk1Tensor<4> &) const {

        EigenRk2Tensor<4> g;
        // clang-format off
        g.setValues({{-1,0,0,0},
                     {0,1,0,0},
                     {0,0,1,0},
                     {0,0,0,1}});
        // clang-format on
        return g;
    }

    EigenRk3Tensor<4> cartesian_metric_derivatives(
        const EigenRk1Tensor<4> &) const {
        EigenRk3Tensor<4> g_derivs;
        g_derivs.setZero();

        return g_derivs;
    }

    EigenRk2Tensor<4> spherical_metric(const EigenRk1Tensor<4> &x) const {

        EigenRk2Tensor<4> g;
        double r = x(1);
        double th = x(2);
        double r2 = r * r;
        double sinth = std::sin(th);
        // clang-format off
        g.setValues({{-1,0,0,0},
                     {0,1,0,0},
                     {0,0,r2,0},
                     {0,0,0,r2*sinth*sinth}});
        // clang-format on
        return g;
    }

    EigenRk3Tensor<4> spherical_metric_derivatives(
        const EigenRk1Tensor<4> &x) const {
        EigenRk3Tensor<4> g_derivs;
        g_derivs.setZero();

        double r = x(1);
        double th = x(2);

        g_derivs(2, 2, 1) = 2 * r;
        g_derivs(3, 3, 1) = 2 * r * std::sin(th) * std::sin(th);
        g_derivs(3, 3, 2) = 2 * r * r * std::cos(th) * std::sin(th);

        return g_derivs;
    }

    EigenRk1Tensor<4> spherical_to_cartesian_function(
        const EigenRk1Tensor<4> &x) const {

        using std::cos;
        using std::sin;

        const double r = x(1);
        const double th = x(2);
        const double phi = x(3);

        EigenRk1Tensor<4> ret;
        ret.setValues({x(0), r * sin(th) * cos(phi), r * sin(th) * sin(phi),
                       r * cos(th)});
        return ret;
    }

    ArcMatrix<4> spherical_to_cartesian_jacobian(
        const EigenRk1Tensor<4> &x) const {

        using std::cos;
        using std::sin;

        const double r = x(1);
        const double th = x(2);
        const double phi = x(3);

        ArcMatrix<4> ret = ArcMatrix<4>::Zero();
        // clang-format off
        ret << 
            1, 0, 0, 0,
            0, sin(th)*cos(phi), r*cos(th)*cos(phi), -r*sin(th)*sin(phi),
            0, sin(th)*sin(phi), r*cos(th)*sin(phi), r*sin(th)*cos(phi),
            0, cos(th), -r*sin(th), 0;
        // clang-format on
        return ret;
    }

    EigenRk1Tensor<4> cartesian_to_spherical_function(
        const EigenRk1Tensor<4> &X) const {

        const double x = X(1);
        const double y = X(2);
        const double z = X(3);

        const double r = std::sqrt(x * x + y * y + z * z);
        const double th = std::acos(z / r);
        const double phi = std::atan2(y, x);

        EigenRk1Tensor<4> ret;
        ret.setValues({X(0), r, th, phi});
        return ret;
    }

    ArcMatrix<4> cartesian_to_spherical_jacobian(
        const EigenRk1Tensor<4> &x) const {
        return spherical_to_cartesian_jacobian(
                   cartesian_to_spherical_function(x))
            .inverse();
    }
};

} // namespace minkowski
} // namespace arcmancer
