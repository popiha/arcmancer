#pragma once

#include <cmath>
#include <tuple>
#include <utility>

#include "arcmancer/geometry.h"
#include "arcmancer/geometry/metric_space.h"

#include "arcmancer/spacetimes/agm/agm_functions.h"

#include "ht_functions.h"

namespace arcmancer {
/**
 * \brief Classes and functions related to the Hartle-Thorne spacetime
 * \ingroup manifolds
 */
namespace ht {

using namespace std::literals::string_literals; // for ""s suffix operator

/** \brief The Hartle-Thorne spacetime
 *
 * Implements the Hartle-Thorne spacetime, describing a rigidly
 * rotating, slightly oblate body, such as a neutron star or a white
 * dwarf.
 *
 * The Hartle-Thorne spacetime is originally described in Hartle & Thorne (1996)
 * [https://doi.org/10.1086/149707]. The implementation used in the code
 * is based on a version derived as a perturbation of the Kerr
 * spacetime, see Glampedakis & Babak (2006)
 * [https://doi.org/10.1088/0264-9381/23/12/013]. The mass
 * quadrupole moment parametrization through the eta-parameter is from
 * Bauböck et al. (2012) [https://doi.org/10.1088/0004-637X/753/2/175],
 * as are the relations linking mass, radius and angular velocity of the
 * rotating body to the mass quadrupole parameter and the angular
 * momentum.
 *
 * Currently, the only coordinate system offered is the
 * Boyer-Lindquist-like coordinates used in Glampedakis & Babak (2006).
 * In addition, constructing the spacetime from mass, radius and angular
 * velocity of the rotating body only works for neutron stars.
 * 
 * \ingroup manifolds
 */
class HartleThorneSpacetime : public MetricSpace<HartleThorneSpacetime, 4> {
public:
    std::string name() const { return name_; };

    /// \brief The Boyer-Lindquist-like spherical coordinate chart
    const ChartType boyer_lindquist = {*this, "Boyer-Lindquist"s};

    /** \brief Compute non-dimensional angular momentum and the eta
     * parameter from given mass, radius and angular velocity.
     *
     * Note: Currently this function only works for neutron stars, using
     * the Bauböck et al. (2012) relations.
     *
     * \param mass Mass of the rotating body
     * \param equatorial radius Equatorial radius of the rotating body
     * \param angular_velocity The angular velocity of the rotating body,
     * as seen by an observer at infinity
     * \return Values of the non-dimensional angular momentum and the
     * mass quadrupole parameter.
     */
    static std::pair<double, double> chi_eta_from_neutron_star_parameters(
            double mass, double equatorial_radius, double angular_velocity) {
        using std::pow;
        using std::log;

        // compactness
        double x = mass / equatorial_radius; 
        // normalized dimensionless frequency (= Omega_bar/(2*pi))
        double eps0 = agm::Omega_bar(angular_velocity, mass, equatorial_radius) / (2*cnst::pi); 

        // The following formulae are the relations found by Bauböck et al (2013) 
        // <http://dx.doi.org/10.1088/0004-637X/777/1/68>. They use 'a'
        // to mean chi.
        double chi = eps0 * (1.1035 - 2.146*x + 4.5756*x*x);

        double l = log(chi/eps0 * pow(x, -3./2));
        double q = chi*chi * (-2.014 + 0.601 * l + 1.10 * l*l - 0.412 * pow(l,3)
                + 0.0459 * pow(l,4));

        // find out eta from this
        double eta = q/(-chi*chi) - 1;

        return {chi, eta};
    }

    /** \brief Defines which parametrization to use for the
     * Hartle-Thorne spacetime.
     */
    enum class Parametrization {
        chi_eta, //< Uses p1 = normalized spin (chi = J/M^2) and p2 = eta parameter, for which q = -chi^2(1+eta)
        r_omega, //< Uses p1 = equatorial radius and p2 = angular velocity
    };

    /** \brief Create a Hartle-Thorne spacetime, which is a Kerr spacetime with
     * a perturbed mass quadrupole moment.
     *
     */
    HartleThorneSpacetime(double mass, double p1, double p2, Parametrization par)
    : mass_(mass) {
        if (par == Parametrization::chi_eta) {
            chi_ = p1;
            eta_ = p2;
        }
        else {
            std::tie(chi_, eta_) = chi_eta_from_neutron_star_parameters(mass, p1, p2);
        }


        // check that spin is physical
        // XXX: HT metric works for slow rotation only, but slow
        // rotation is defined by the physical size of the system.
        // Without this extra knowledge, the upper limit is set by the
        // Kerr limit chi_max = +- 1.
        if (std::abs(chi_) > 1) {
            throw std::runtime_error(
                "Non-dimensional spin chi = " + std::to_string(chi_) +
                " is outside GR limits -1 =< chi =< 1");
        }

        add_chart(boyer_lindquist, &HartleThorneSpacetime::boyer_lindquist_metric,
                  &HartleThorneSpacetime::boyer_lindquist_metric_derivatives);

        complete_chart_graph();

        const double q = -chi_*chi_*(1+eta_);
        name_ = "HartleThorne Spacetime (mass = " + std::to_string(mass) +
            ", chi = " + std::to_string(chi_) + 
            ", eta = " + std::to_string(eta_) + 
            ", q = " + std::to_string(q) + ")";
    }


    // The Boyer-Lindquist r of the horizon
    // TODO: Is the horizon really here for the Hartle-Thorne metric?
    // Probably not.
#if 0
    double horizon_r() const {
        const double chi = chi_ / mass_;
        return mass_ * (1 + std::sqrt(1 - chi * chi));
    }
#endif

    /// \brief Mass of the rotating body
    double mass() const { return mass_; }
    /// \brief Dimensionless angular momentum of the rotating body
    double chi() const { return chi_; }
    /// \brief The mass quadrupole deviation parameter
    double eta() const { return eta_; }

private:
    double mass_, chi_, eta_;
    std::string name_;

    EigenRk2Tensor<4> boyer_lindquist_metric(const EigenRk1Tensor<4> &x) const {
        using namespace std;
        const double r = x(1), th = x(2);
        // There is potential for massive confusion here. Bauböck (2012)
        // seems to use 'a' in two different meanings: 
        // normalized spin parameter J/M in e.g. eqs. (3)-(5)
        // dimensionless spin parameter J/M^2 in eq. (7)
        const double a = chi_ * mass_;
        const double rs = 2 * mass_;
        const double Sigma = r*r + pow(a*cos(th),2);
        const double Delta = r*r + a*a - rs*r;
        const double dtdphi = rs*r*a*pow(sin(th),2)/Sigma;

        // quadrupole correction. given in contravariant form, in
        // Glampedakis (2006) and in Bauböck (2012). 
        Eigen::Matrix4f h;

        double f1 = (1 - 3*pow(cos(th), 2)) * F1(r, mass_);
        double f2 = (1 - 3*pow(cos(th), 2)) * F2(r, mass_);
        double mult = eta_ * chi_ * chi_;

        h.setZero();
        h.diagonal() << -1/(1-rs/r) * f1, -(1-rs/r) * f1, 1/(r*r) * f2, 1/pow(r*sin(th), 2) * f2;
        h *= mult;

        // lower the indices of the h-perturbation with the kerr part of
        // the metric
        Eigen::Matrix4f g, gk;

        // clang-format off
        gk<<
            1 - rs*r/Sigma , 0            , 0      , dtdphi                                                        ,
            0              , -Sigma/Delta , 0      , 0                                                             ,
            0              , 0            , -Sigma , 0                                                             ,
            dtdphi         , 0            , 0      , -(r*r + a*a + rs*r*a*a*sin(th)*sin(th)/Sigma)*sin(th)*sin(th);
        // clang-format on

        g = gk + gk.transpose()*h*gk;

        // TensorMap doesn't seem to work with fixed size tensors, so just brute force it
        EigenRk2Tensor<4> ret;
        ret.setValues( {
                { g(0,0), g(0,1), g(0,2), g(0,3) },
                { g(1,0), g(1,1), g(1,2), g(1,3) },
                { g(2,0), g(2,1), g(2,2), g(2,3) },
                { g(3,0), g(3,1), g(3,2), g(3,3) },
                });

        return ret;
    }

    EigenRk3Tensor<4> boyer_lindquist_metric_derivatives(const EigenRk1Tensor<4> &x) const {
        using namespace std;

        EigenRk3Tensor<4> ret;
        ret.setZero();

        const double r = x(1), th = x(2);
        const double a = chi_ * mass_;
        const double rs = 2 * mass_;
        const double st = sin(th), ct = cos(th);
        const double Sigma = r*r + pow(a*cos(th),2);
        const double Delta = r*r + a*a - rs*r;
        const double dtdphi = rs*r*a*pow(sin(th),2)/Sigma;

        double f1 = (1 - 3*pow(cos(th), 2)) * F1(r, mass_);
        double f2 = (1 - 3*pow(cos(th), 2)) * F2(r, mass_);
        double mult = eta_ * chi_ * chi_;

        // we also need the undifferentiated metric components
        Eigen::Matrix4f gk, h;

        h.setZero();
        h.diagonal() << -1/(1-rs/r) * f1, -(1-rs/r) * f1, 1/(r*r) * f2, 1/pow(r*sin(th), 2) * f2;
        h *= mult;

        gk <<
            1 - rs*r/Sigma , 0            , 0      , dtdphi                                                        ,
            0              , -Sigma/Delta , 0      , 0                                                             ,
            0              , 0            , -Sigma , 0                                                             ,
            dtdphi         , 0            , 0      , -(r*r + a*a + rs*r*a*a*sin(th)*sin(th)/Sigma)*sin(th)*sin(th);


        // helper, contains derivative wrt one dimension at a time
        EigenRk2Tensor<4> der;

        // d/dr
        {
            // the kerr contribution
            const double g00 = ((-(pow(a,2)*pow(ct,2)) + pow(r,2))*rs)/pow(pow(a,2)*pow(ct,2) + pow(r,2),2);
            const double g03 = 0.5*(2*a*(a*ct - r)*(a*ct + r)*rs*pow(st,2))/pow(pow(a,2)*pow(ct,2) + pow(r,2),2);
            const double g11 = (pow(r,2)*rs + pow(a,2)*(2*(-1 + pow(ct,2))*r - pow(ct,2)*rs))/pow(pow(a,2) + r*(r - rs),2);
            const double g22 = -2*r;
            const double g33 = -2*r*pow(st,2) + (pow(a,2)*(-(pow(a,2)*pow(ct,2)) 
                        + pow(r,2))*rs*pow(st,4))/pow(pow(a,2)*pow(ct,2) + pow(r,2),2);

            // derivatives of the contravariant h-perturbation
            const double h00 = mult * -(((-1 + 3*pow(cos(th),2))*(rs*F1(r, mass_) + r*(-r + rs)*F1_r(r, mass_)))/pow(r - rs,2));
            const double h11 = mult * ((-1 + 3*pow(cos(th),2))*(rs*F1(r, mass_) + r*(r - rs)*F1_r(r, mass_)))/ pow(r,2);
            const double h22 = mult * ((-1 + 3*pow(cos(th),2))*(2*F2(r, mass_) - r*F2_r(r, mass_)))/ pow(r,3);
            const double h33 = mult * ((-1 + 3*pow(cos(th),2))*pow(sin(th),-2)*(2*F2(r, mass_) - r*F2_r(r, mass_)))/pow(r,3);

            Eigen::Matrix4f h_r = Eigen::Matrix4f::Zero(); 
            Eigen::Matrix4f gk_r, g_r;
            // clang-format off
            gk_r << g00 , 0   , 0   , g03  ,
                   0   , g11 , 0   , 0    ,
                   0   , 0   , g22 , 0    ,
                   g03 , 0   , 0   , g33;
            h_r.diagonal() << h00, h11, h22, h33;
            // clang-format on

            // total derivative
            g_r = gk_r + gk_r.transpose()*h*gk + gk.transpose() * h * gk_r + gk.transpose() * h_r * gk;

            // clang-format off
            der.setValues( {
                    { g_r(0,0), g_r(0,1), g_r(0,2), g_r(0,3) },
                    { g_r(1,0), g_r(1,1), g_r(1,2), g_r(1,3) },
                    { g_r(2,0), g_r(2,1), g_r(2,2), g_r(2,3) },
                    { g_r(3,0), g_r(3,1), g_r(3,2), g_r(3,3) },
                    });
            // clang-format on

            // store g_ab,1 (offset 1 of dimension 2)
            ret.chip(1, 2) = der;
        }

        // d/th
        {

            const double g00 = (-2*pow(a,2)*ct*r*rs*st)/pow(pow(a,2)*pow(ct,2) + pow(r,2),2);
            const double g03 = 0.5*(4*a*ct*r*rs*st*(pow(r,2) + pow(a,2)*(pow(ct,2) + pow(st,2))))
                /pow(pow(a,2)*pow(ct,2) + pow(r,2),2);
            const double g11 = (2*pow(a,2)*ct*st)/(pow(a,2) + pow(r,2) - r*rs);
            const double g22 = 2*pow(a,2)*ct*st;
            const double g33 = -2*ct*(pow(a,2) + pow(r,2))*st - (4*pow(a,2)*ct*r*rs*pow(st,3))
                /(pow(a,2)*pow(ct,2) + pow(r,2)) - 
                (2*pow(a,4)*ct*r*rs*pow(st,5))/pow(pow(a,2)*pow(ct,2) + pow(r,2),2);

            const double h00 = mult * (-3 * r * F1(r, mass_) * sin(2 * th)) / (r - rs);
            const double h11 = mult * 6 * (-1 + rs / r) * cos(th) * F1(r, mass_) * sin(th);
            const double h22 = mult * (6 * cos(th) * F2(r, mass_) * sin(th)) / pow(r, 2);
            const double h33 = mult * (4 * 1/tan(th) * pow(sin(th), -2) * F2(r, mass_)) / pow(r, 2);

            Eigen::Matrix4f h_th = Eigen::Matrix4f::Zero(); 
            Eigen::Matrix4f gk_th, g_th;
            // clang-format off
            gk_th << g00 , 0   , 0   , g03  ,
                   0   , g11 , 0   , 0    ,
                   0   , 0   , g22 , 0    ,
                   g03 , 0   , 0   , g33;
            h_th.diagonal() << h00, h11, h22, h33;
            // clang-format on

            // total derivative
            g_th = gk_th + gk_th.transpose()*h*gk + gk.transpose() * h * gk_th + gk.transpose() * h_th * gk;

            // clang-format off
            der.setValues( {
                    { g_th(0,0), g_th(0,1), g_th(0,2), g_th(0,3) },
                    { g_th(1,0), g_th(1,1), g_th(1,2), g_th(1,3) },
                    { g_th(2,0), g_th(2,1), g_th(2,2), g_th(2,3) },
                    { g_th(3,0), g_th(3,1), g_th(3,2), g_th(3,3) },
                    });
            // clang-format on

            // store g_ab,2 (offset 2 of dimension 2)
            ret.chip(2,2) = der;
        }

        return ret;
    }

};


}
}
