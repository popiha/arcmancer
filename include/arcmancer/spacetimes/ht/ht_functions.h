#pragma once

#include <cmath>

namespace arcmancer {
namespace ht {

// The Glampedakis-Papas (2006) \mathcal{F}-functions

inline double F1(double r, double M) {
    return -5*(r-M)/(8*M*r*(r-2*M)) * (2*M*M + 6*M*r - 3*r*r)
        - 15*r*(r-2*M)/(16*M*M) * std::log(r/(r-2*M));
}

inline double F2(double r, double M) {
    return 5/(8*M*r) * (2*M*M - 3*M*r - 3*r*r)
        + 15/(16*M*M) * (r*r - 2*M*M) * std::log(r/(r-2*M));
}

// Derivatives of these w.r.t r
inline double F1_r(double r, double M) {
    using std::pow;
    using std::log;
    const double retval = (5*(6*M + pow(M,3)*(pow(r,-2) + pow(-2*M + r,-2)) + 
        3*(M - r)*log(r/(-2*M + r))))/(8.*pow(M,2));

    return retval;
}

inline double F2_r(double r, double M) {
    using std::pow;
    using std::log;
    const double retval = 
        (5*(2*M*(-4*pow(M,2) - 3*M*r - 3*pow(r,2) - (6*pow(M,3))/(-2*M + r)) + 
            3*pow(r,3)*log(r/(-2*M + r))))/(8.*pow(M,2)*pow(r,2));
    return retval;
}

} // namespace ht 
} // namespace arcmancer
