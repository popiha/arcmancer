#pragma once

#include <vector>
#include <iostream>
#include <tuple>
#include <utility>
#include <stdexcept>

#include "arcmancer/geometry/surface.h"
#include "arcmancer/spacetimes/agm.h"
#include "arcmancer/log.h"

#include "ht.h"

namespace arcmancer {
namespace ht {



/** \brief An oblate ellipsoid representing the surface of a neutron star.
 * 
 * Surface of a rotating neutron star using the relations in
 * AlGendy & Morsink (2014), represented in the Hartle-Thorne metric.
 *
 * \ingroup manifolds
 */
class HTNeutronStarSurface : public Surface<HartleThorneSpacetime> {

public:

    HTNeutronStarSurface(const HartleThorneSpacetime &M,
            double equatorial_radius, double angular_velocity) :
        Surface<HartleThorneSpacetime>(M), R_eq_(equatorial_radius), angular_velocity_(angular_velocity) {

        // check that given radius and velocity correspond to what's
        // used by the metric
        double chi, eta;
        std::tie(chi, eta) = HartleThorneSpacetime::chi_eta_from_neutron_star_parameters(
                M.mass(), equatorial_radius, angular_velocity);

        if (chi != M.chi() || eta != M.eta()) {
            throw std::runtime_error(fmt::format(
                        "HTNeutronStarSurface: radius {} and angular_velocity {} yield chi {} eta {}, "
                        "but spacetime has a {} eta {}",
                        equatorial_radius, angular_velocity, chi, eta, M.chi(), M.eta()));
        }

        double compactness = M.mass() / R_eq_;

        Omega_bar_   = agm::Omega_bar(angular_velocity, M.mass(), equatorial_radius);

        // o2-term = R_polar / R_equatorial - 1
        o2term_ = agm::o2(Omega_bar_, compactness);
    }

    // radius as a function of colatitude, in schwarzschild coordinates
    double radius(double colat) const {
        return R_eq_ * (1 + o2term_ * std::pow(std::cos(colat), 2));
    }

    double value(const PointType &position) const override {
        // Get coordinates in the BL chart
        const EigenRk1Tensor<4> bl_x = position.coordinates(M().boyer_lindquist);

        // radius depends on colatitude. the AGM formula gives it in
        // Schwarzschild coordinates
        double star_radius = radius(bl_x(2));

        // just return the difference between the radial coordinates of
        // the point and the surface

        double retval = star_radius - bl_x(1);

        return retval;
    }

    CotangentVector<HartleThorneSpacetime> gradient(const PointType &position) const override {
        const EigenRk1Tensor<4> bl_x = position.coordinates(M().boyer_lindquist);

        // derivative of the radius function w.r.t theta
        double radius_th = -R_eq_ * o2term_ * std::sin(2 * bl_x(2));

        // grad = (0, -1, radius_th, 0)

        return CotangentVector<HartleThorneSpacetime>(
                position, position.M().boyer_lindquist,
                {0, -1, radius_th, 0});
    }

    TangentVector<HartleThorneSpacetime> observer(const PointType &position) const override {
        // compute the four-velocity at the surface of a rotating
        // neutron star

        // need the metric components
        const HartleThorneSpacetime &M = position.M();
        EigenRk2Tensor<4> g = M.metric_components(position, M.boyer_lindquist);

        // dphi / dt
        // = angular velocity as measured by observer at infinity
        // = (u^phi / u^t) evaluated at any radial distance
        double dpdt = angular_velocity_;

        // time component
        double ut =
            1 / std::sqrt(g(0, 0) + 2 * dpdt * g(0, 3) + dpdt * dpdt * g(3, 3));
        // phi component
        double uphi = dpdt * ut;

        TangentVector<HartleThorneSpacetime> ret { position, M.boyer_lindquist, {ut, 0, 0, uphi} };

        // NOTE: if trying to get an observer at a point that is rotating
        // above the speed of light, the observer will be spacelike.

        return ret;
    }

private:
    double R_eq_;
    double angular_velocity_;
    double Omega_bar_;
    double o2term_;
};

} // namespace ht
} // namespace arcmancer
