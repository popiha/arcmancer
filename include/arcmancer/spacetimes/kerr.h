#pragma once
/** \file
 * \brief Convenience header for the Kerr spacetime
 *
 * \ingroup manifolds
 */
#include "kerr/kerr_spacetime.h"
#include "kerr/kerr_sphere.h"
#include "kerr/kerr_surface.h"
