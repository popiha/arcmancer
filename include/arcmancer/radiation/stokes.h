#pragma once

#include <boost/numeric/odeint.hpp>
#include <boost/foreach.hpp>
#include <boost/iterator/iterator_facade.hpp>
#include <boost/range.hpp>

namespace arcmancer {


/** \brief Stokes intensities at some frequency
 *
 * Contains the invariant Stokes intensities corresponding to a single observed
 * frequency
 *
 * \ingroup radiation
 */
class StokesVector {
public:
    static const size_t component_count = 5;

    /** \brief Construct from components in an Eigen vector
     *
     * \param frequency the observed frequency.
     * \param invariant_IQUV the invariant Stokes intensities
     *
     */
    StokesVector(double frequency, const Eigen::Vector4d& invariant_IQUV)
    : nu_IQUV_({frequency, invariant_IQUV(0), invariant_IQUV(1),
                invariant_IQUV(2), invariant_IQUV(3)}) {}

    /** \brief Construct from an array of components
     *  
     *  Convenient as allows using an initializer list as the argument.
     *
     * \param frequency_IQUV an array containing the frequency followed by the 
     *        invariant Stokes intensities.
     */
    StokesVector(std::array<double, 5> frequency_IQUV)
    : nu_IQUV_(frequency_IQUV) {}

    /** \brief Get the observed frequency
     */
    double frequency() const {return nu_IQUV_[0];}

    /** \brief Get the vector of invariant intensities
     */
    Eigen::Vector4d invariant_IQUV_vector() const {
        Eigen::Vector4d ret {
            nu_IQUV_[1],
            nu_IQUV_[2],
            nu_IQUV_[3],
            nu_IQUV_[4]
        };
        return ret;
    }

    /** \brief Get the vector of observed intensities
     */
    Eigen::Vector4d rest_frame_IQUV_vector() const {
        return std::pow(frequency(), 3) * invariant_IQUV_vector();
    }
    
    /** \brief Get the observed Stokes I
     */
    double rest_frame_I() const {
        return std::pow(frequency(), 3) * nu_IQUV_[1];
    }
    
    /** \brief Get the observed Stokes Q
     */
    double rest_frame_Q() const {
        return std::pow(frequency(), 3) * nu_IQUV_[2];
    }
    
    /** \brief Get the observed Stokes U
     */
    double rest_frame_U() const {
        return std::pow(frequency(), 3) * nu_IQUV_[3];
    }

    /** \brief Get the observed Stokes V
     */
    double rest_frame_V() const {
        return std::pow(frequency(), 3) * nu_IQUV_[4];
    }

    std::array<double, 5> data() const { return nu_IQUV_; }

private:
    // using an array for internal storage to avoid having to deal with
    // Eigen type alignment.
    // TODO: Since alignment issues need to be dealt with anyway, this could
    // be changed to use an Eigen vector directly, which should be more optimal.
    std::array<double, 5> nu_IQUV_;
};


/** \brief A spectrum of Stokes intensities
 *  
 *  Allows conveniently handling multiple frequencies at the same time,
 *  and provides some functionality needed by \c ::radiation_transfer internals.
 * 
 *  \ingroup radiation
 */
class PolarizationSpectrum {
public:

    /** \brief Constructor
     */
    PolarizationSpectrum(const std::vector<StokesVector>& stokes_vectors)
    : stokes_vectors_(stokes_vectors) {}

    /** \brief Get the contained `StokesVector`s
     */
    const std::vector<StokesVector> &stokes_vectors() const {
        return stokes_vectors_;
    }

    /** \brief Access a contained \c StokesVector
     */
    const StokesVector& operator[](size_t idx) const {
        return stokes_vectors_[idx];
    }

    /** \brief Get the number of contained elements
     */
    size_t size() const {return stokes_vectors_.size();}

    // implicit conversion to/from std::vector<double> for line_integral.
    // this shuffling of data could be maybe avoided if somehow this class was
    // made compatible with range_algebra.

    PolarizationSpectrum(const std::vector<double>& values) {

        if (values.size() % StokesVector::component_count) {
            Log::log()->warn(
                "PolarizationSpectrum(): values.size() {} not divisible "
                "by StokesVector::component_count {}",
                values.size(),
                // Need a static cast to work around linking errors in debug 
                // build due to odr-use of static const integer member
                static_cast<size_t>(StokesVector::component_count));
        }

        const size_t imax = (values.size() / StokesVector::component_count) *
                            StokesVector::component_count;

        for(size_t i=0; i<imax; i+=StokesVector::component_count){
            stokes_vectors_.emplace_back(
                std::array<double, 5>{values[i], values[i + 1], values[i + 2],
                                      values[i + 3], values[i + 4]});
        }
    }

    operator std::vector<double>() const {
        std::vector<double> ret;
        for (const auto& s: stokes_vectors_){
            auto data = s.data();
            ret.insert(ret.end(), data.begin(), data.end());
        }
        return ret;
    }

private:
    std::vector<StokesVector> stokes_vectors_;
};

// TODO: Need to implemented Boost.Range compatible iterator that
// flattens a PolarizationSpectrum to a vector of doubles. Check
// StackOverflow, this is definitely a solved problem.
//
// In addition, we need a bunch of specializations to make
// PolarizationSpectrum work with OdeInts range_algebra.

} // namespace arcmancer
