#pragma once

#include "arcmancer/geometry/types.h"

// Some basic types used for defining radiation models

namespace arcmancer {

/** \brief Stokes parameter emission/absorbtion coefficients
 *
 *  Contains the coefficients on the right hand side of the non-relativistic
 *  polarized transfer equation
 *  \f[
 *      \frac{\mathrm{d}}{\mathrm{d}s} \begin{pmatrix} I\\Q\\U\\V \end{pmatrix}
 *      = \begin{pmatrix} j_I\\j_Q\\j_U\\j_V \end{pmatrix}
 *        - 
 *        \begin{pmatrix} 
 *        a_I & a_Q & a_U & a_V \\
 *        a_Q & a_I & r_V & -r_U \\
 *        a_U & -r_V & a_I & r_Q \\
 *        a_V & r_U & -r_Q & a_I
 *        \end{pmatrix}
 *      \begin{pmatrix} I\\Q\\U\\V \end{pmatrix}.
 *  \f]
 *
 *  The unit system used for radiative transfer is determined by the units used
 *  for the values of these coefficients, and is independent from the unit 
 *  system of the spacetime, apart from a single conversion factor, given as the
 *  \c length_unit member.
 *
 *
 *  \ingroup radiation
 *  \see radiation_transfer
 */
struct RadiationData {
    double j_I, j_Q, j_U, j_V;
    double a_I, a_Q, a_U, a_V;
    double r_Q, r_U, r_V;
    double length_unit; 
    ///< Unit of length used by the spacetime in the units used by the 
    /// radiation quantities, typically \f$ G (\mathrm{unit mass})/c^2 \f$
    /// for spacetimes using natural G=c=1 units.
};

/** \brief Basic fluid data.
 *
 *  Basic fluid data for use in radiation transfer, should cover most common 
 *  usecases.
 *
 *  \ingroup radiation
 *  \ingroup overaligned
 */
template<typename MetricSpaceType>
struct FluidData {
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW

    double number_density;
    double temperature;
    double pressure;
    double magnetic_field_strength;
//XXX: should the magnetic_field member be named reference_direction instead?
    TangentVector<MetricSpaceType> magnetic_field;
    TangentVector<MetricSpaceType> fluid_velocity;
};


/** \brief Type of functions describing the radiating fluid
 *  
 *  The properties of \c FluidDataType are unspecified apart from the 
 *  requirement that it have the public members
 *  \code
 *      TangentVector<MetricSpaceType> magnetic_field;
 *      TangentVector<MetricSpaceType> fluid_velocity;
 *  \endcode
 *  which give the vector along the magnetic field in the fluid rest frame,
 *  and the normalized four-velocity of the fluid.
 *  
 *  The magnetic field vector is used to define the standard frame for
 *  \c ::RadiationFunction. 
 *  For this purpose only its direction orthogonal to \c fluid_velocity is 
 *  significant.
 *
 *  \ingroup radiation
 *  \see radiation_transfer
 */
template <typename MetricSpaceType,  typename FluidDataType>
using FluidFunction = std::function<FluidDataType(
    const ManifoldPoint<MetricSpaceType> &)>;

/** \brief Type of functions defining the radiation model
 *
 *  The function's arguments are an arbitrary type returned by a 
 *  \c ::FluidFunction,
 *  the frequency in the fluid restframe and the inclination of the ray, i.e.
 *  the angle between the ray and the reference direction given by the magnetic
 *  field.
 *
 *  The return value is the set of emissivities and absorbtivities described by
 *  \c RadiationData given in a standard frame defined by the direction of the
 *  ray and the magnetic field.
 *  This frame is taken to follow the conventions of Shcherbakov (2011) 
 *  [https://doi.org/10.1111/j.1365-2966.2010.17502.x]
 *
 *  \ingroup radiation
 *  \see radiation_transfer
 */
template <typename FluidDataType>
using RadiationFunction = std::function<RadiationData(
    const FluidDataType &, double /*frequency*/, double /*ray inclination */)>;
    
} // namespace arcmancer
