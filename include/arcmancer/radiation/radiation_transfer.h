#pragma once

#include <stdexcept>
#include <utility>
#include <type_traits>

#include <boost/numeric/odeint.hpp>

#include "arcmancer/geometry/line_integral.h"
#include "arcmancer/geometry/meta.h"
#include "arcmancer/geometry/types.h"
#include "arcmancer/radiation/radiation_types.h"
#include "arcmancer/radiation/stokes.h"

namespace arcmancer {

/**
 * \brief Wraps two TangentVectors for parallel transport along a
 *  ParametrizedCurve to describe the polarization frame along the ray.
 *
 *  \ingroup radiation
 *  \ingroup overaligned
 */
template<typename MetricSpaceType>
class PolarizationFrame {
    static_assert(MetricSpaceType::D == 4, "Only defined for D == 4");
public:

    EIGEN_MAKE_ALIGNED_OPERATOR_NEW

    static const size_t vector_size
        =TangentVector<MetricSpaceType>::component_count;
    //These two are needed by AffinePoint
    static const size_t component_count = 2*vector_size;
    using ContainerType
        = Eigen::TensorFixedSize<double, Eigen::Sizes<component_count>>;

    /**
     * \brief Construct from the given vertical and horizontal vectors.
     *
     * The vectors need to be located at the same base point.
    */
    PolarizationFrame(const TangentVector<MetricSpaceType> &vertical,
                      const TangentVector<MetricSpaceType> &horizontal)
    : vertical_(vertical), horizontal_(horizontal) {

        if (vertical.point() != horizontal_.point()) {
            throw std::runtime_error(
                "vertical and horizontal at different points");
        }
    }

    //Getters specific to PolarizationFrame
    const TangentVector<MetricSpaceType>& vertical() const {
        return vertical_;
    }

    const TangentVector<MetricSpaceType>& horizontal() const {
        return horizontal_;
    }

    /** \brief Compute the rotation angle of the frame
     *
     * Computes the angle of rotation of the frame compared to the standard 
     * frame.
     *
     *
     * \param obs_u the four-velocity of the observer/fluid.
     *
     * \param B the magnetic field or some other direction defining the 
     *  preferred local vertical axis (l-axis in Chandrasekhar (1960)).
     *
     * \param ray_tangent the tangent vector of the ray
     *
     * \return the polarization rotation angle in radians.
     *
     */
    double polarization_angle(
        const TangentVector<MetricSpaceType> &obs_u,
        const TangentVector<MetricSpaceType> &B,
        const TangentVector<MetricSpaceType> &ray_tangent) const {

        const int sign = MetricSpaceType::Signature::spacelike_sign;

        const double s =
            -sign * screen_inner_product(obs_u, ray_tangent, horizontal_, B);

        const double c =
            sign * screen_inner_product(obs_u, ray_tangent, vertical_, B);

        return std::atan2(s, c);
    }

    //
    //Methods needed for use in AffinePoint
    PolarizationFrame(const typename MetricSpaceType::PointType &point,
                      const typename MetricSpaceType::ChartType &chart,
                      const ContainerType &components)
    : vertical_(point, chart,
                components.slice(Eigen::array<size_t, 1>{0},
                                 Eigen::array<size_t, 1>{vector_size})),
      horizontal_(point, chart,
                  components.slice(Eigen::array<size_t, 1>{vector_size},
                                   Eigen::array<size_t, 1>{vector_size})) {}

    ContainerType components(const typename MetricSpaceType::ChartType &chart)
    const {
        ContainerType ret;
        ret.slice(Eigen::array<size_t, 1> {0},
                  Eigen::array<size_t, 1> {vector_size})
            = vertical_.components(chart);

        ret.slice(Eigen::array<size_t, 1> {vector_size},
                  Eigen::array<size_t, 1> {vector_size})
            = horizontal_.components(chart);

        return ret;
    }

    const typename MetricSpaceType::PointType& point() const{
        return vertical_.point();
    }

    const MetricSpaceType& M() const {return vertical_.M();}

    friend std::ostream &operator<<(
        std::ostream &os, const PolarizationFrame<MetricSpaceType> &rf) {
        os << "PolarizationFrame at point: " << rf.point() << std::endl;
        os << "Vertical:" << std::endl << rf.vertical_ << std::endl;
        os << "Horizontal:" << std::endl << rf.horizontal_ << std::endl;
        return os;
    }

    ContainerType transport_derivatives(
        const typename MetricSpaceType::ChartType &chart,
        const EigenRk2Tensor<4> &contracted_Gamma) const {
        ContainerType ret;
        ret.slice(Eigen::array<size_t, 1>{0},
                  Eigen::array<size_t, 1>{vector_size}) =
            vertical_.transport_derivatives(chart, contracted_Gamma);

        ret.slice(Eigen::array<size_t, 1>{vector_size},
                  Eigen::array<size_t, 1>{vector_size}) =
            horizontal_.transport_derivatives(chart, contracted_Gamma);
        return ret;
    }

private:
    TangentVector<MetricSpaceType> vertical_, horizontal_;
};



/** \brief Perform polarized radiative transfer along a section of a ray
 *
 * The ray tangent does not need to be normalized in any particular way.
 * The normalization is defined by the \p observer_dot_tangent parameter, which
 * gives the value of \f$ k^\mu u_\mu \f$, where \f$ k^\mu, u^\mu \f$ are the
 * ray tangent vector and the observer 4-velocity, respectively.
 *
 * The \c fluid and \c radiation parameters must be functions or objects from
 * which it is possible to construct objects of type 
 * `::FluidFunction<MetricSpaceType, FluidDataType>` and 
 * `::RadiationFunction<FluidDataType>`, respectively,
 * where `FluidDataType` is some suitable type.
 * However, this is only used to give better error messages and the \c fluid
 * and \c radiation objects are not copied.
 *
 * The solution of the radiative transfer equation is computed using the
 * `line_integral()` function,
 * and the tolerances and other integration parameters
 * are controlled by the `line_integral_` parameters in `Configuration`.
 *
 * \param ray a geodesic with a transported \c PolarizationFrame along which to
 * perform the transfer.
 * \param parameter_start starting curve parameter for the computation.
 * \param parameter_end final curve parameter.
 * \param observer_dot_tangent normalization of the curve tangent.
 * \param initial_values spectrum of the initial invariant Stokes intensities.
 * \param fluid a function giving the fluid model.
 * \param radiation a function giving the radiation model.
 *
 * \tparam MetricSpaceType the type of the underlying spacetime.
 * \tparam Fluid a type convertible to a `::FluidFunction`.
 * \tparam Radiation a type convertible to `::RadiationFunction`.
 *
 * \return The spectrum of invariant Stokes intensities at the point at
 * \p parameter_end.
 *
 *  \ingroup radiation
 */
template <typename MetricSpaceType, typename Fluid, typename Radiation>
inline PolarizationSpectrum radiation_transfer(
    const Geodesic<MetricSpaceType, PolarizationFrame<MetricSpaceType>> &ray,
    double parameter_start, double parameter_end, double observer_dot_tangent,
    const PolarizationSpectrum &initial_values,
    const Fluid &fluid, const Radiation &radiation) {

    // check that Fluid and Radiation are suitable types
    static_assert(internal::has_member_type<std::result_of<Fluid(
                      const ManifoldPoint<MetricSpaceType> &)>>::value,
                  "radiation_transfer: argument fluid is not convertible to "
                  "FluidFunction (incorrect argument types)");
    using FluidDataType =
        std::result_of_t<Fluid(const ManifoldPoint<MetricSpaceType> &)>;
    // This should not really fail if the above check passes, but does no harm
    // to have it anyway.
    static_assert(
        std::is_constructible<FluidFunction<MetricSpaceType, FluidDataType>,
                              Fluid>::value,
        "radiation_transfer: argument fluid is not convertible to a "
        "FluidFunction");

    static_assert(
        std::is_constructible<RadiationFunction<FluidDataType>,
                              Radiation>::value,
        "radiation_transfer: argument radiation is not convertible to a "
        "RadiationFunction");

    using PointType =
        ParametrizedPoint<MetricSpaceType, PolarizationFrame<MetricSpaceType>>;

    const auto radiation_vector_field =
        [&radiation, &fluid,
         observer_dot_tangent](const PolarizationSpectrum &spectrum,
                               const PointType &point) -> PolarizationSpectrum {
        const auto fluid_data = fluid(point.point());
        const auto &u = fluid_data.fluid_velocity;
        const auto &B_vec = fluid_data.magnetic_field;
        const auto &k = point.tangent();

        // redshift
        const double G = observer_dot_tangent / dot_product(u, k);

        const double ray_inclination = vector_angle(u, k, B_vec);

        const double pol_angle =
            point.transported().polarization_angle(u, B_vec, k);

        const double s2chi = std::sin(2 * pol_angle);
        const double c2chi = std::cos(2 * pol_angle);

        Eigen::Matrix4d Rmat;
        // clang-format off
            Rmat <<
                1 , 0     , 0      , 0  ,
                0 , c2chi , -s2chi , 0  ,
                0 , s2chi , c2chi  , 0  ,
                0 , 0     , 0      , 1;
        // clang-format on

        std::vector<StokesVector> der;
        der.reserve(spectrum.stokes_vectors().size());

        for (const auto &stokes : spectrum.stokes_vectors()) {
            const double nu0 = stokes.frequency();
            const double nu = nu0 / G;

            const auto rad = radiation(fluid_data, nu, ray_inclination);

            Eigen::Vector4d J;
            J << rad.j_I, rad.j_Q, rad.j_U, rad.j_V;

            // clang-format off
                Eigen::Matrix4d Mul;
                Mul << rad.a_I,  rad.a_Q,  rad.a_U,  rad.a_V,
                       rad.a_Q,  rad.a_I,  rad.r_V, -rad.r_U,
                       rad.a_U, -rad.r_V,  rad.a_I,  rad.r_Q,
                       rad.a_V,  rad.r_U, -rad.r_Q,  rad.a_I;
            // clang-format on

            // the multiplicative factor comes from the fact that
            // ds = |u_em^a k_a| dlambda = (nu/nu_0) * u_obs^a k_a dlambda
            // and additionally accounting for the internal units of the
            // radiation quantities
            Eigen::Vector4d I_der = MetricSpaceType::Signature::timelike_sign *
                                    rad.length_unit * observer_dot_tangent /
                                    nu0 *
                                    (std::pow(nu, -2) * Rmat * J -
                                     nu * Rmat * Mul * Rmat.transpose() *
                                         stokes.invariant_IQUV_vector());

            der.emplace_back(0, I_der);
        }

        return PolarizationSpectrum{der};
    };

    return line_integral<PolarizationSpectrum, std::vector<double>>(
        ray, parameter_start, parameter_end, initial_values,
        radiation_vector_field);
}

/** \brief Perform polarized radiative transfer over the whole ray
 *  
 *  A convenience overload.
 *  \ingroup radiation
 */
template <typename MetricSpaceType, typename Fluid, typename Radiation>
inline PolarizationSpectrum radiation_transfer(
    const Geodesic<MetricSpaceType, PolarizationFrame<MetricSpaceType>> &ray,
    double observer_dot_tangent, const PolarizationSpectrum &initial_values,
    const Fluid &fluid, const Radiation &radiation) {

    return radiation_transfer(ray, ray.extent_front(), ray.extent_back(),
                              observer_dot_tangent, initial_values,
                              fluid,
                              radiation);
}


}//namespace arcmancer
