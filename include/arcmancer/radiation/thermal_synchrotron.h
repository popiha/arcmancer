#pragma once

#include <boost/math/special_functions/gamma.hpp>
#include <boost/math/special_functions/bessel.hpp>

#include "arcmancer/radiation/radiation_types.h"
#include "arcmancer/units.h"

namespace arcmancer {
/** \brief Ultrarelativistic thermal synchrotron emission from Dexter 2016
 *  
 *  Implements the thermal synchrotron radiation model from Dexter 2016 
 *  [https://doi.org/10.1093/mnras/stw1526].
 *  This object can be used as the radiation model in `::radiation_transfer` 
 *  with a fluid model returning data as `FluidData<MetricSpaceType>`.
 *
 *  This model assumes cgs units for all the quantities.
 *
 *  Note that the model is only valid when the dimensionless electron 
 *  temperature \f$ \theta_e > 1\f$.
 *  Small regions of colder fluid can be worked around by clipping the 
 *  temperature at a configurable lower limit.
 *
 *  \ingroup radiation
 */
template<typename MetricSpaceType>
class ThermalSynchrotron {

    double length_unit_; // conversion factor between CGS and the internal 
                         // geometric units to pass to RadiationData
    double theta_e_min_; // lower cutoff for electron temperature
    bool scalar_transport_; // disregard polarization effects
public:

    /** \brief Constructor
     * 
     *  \param length_unit the length unit used by the spacetime in cgs units. 
     *  \param theta_e_min minimum cut off for the dimensionless electron 
     *  temperature.
     *  \param scalar_transport if true all polarization related components are
     *  set to zero.
     *
     */
    ThermalSynchrotron(double length_unit, double theta_e_min = 0.05,
                       bool scalar_transport = false)
    : length_unit_(length_unit),
      theta_e_min_(theta_e_min),
      scalar_transport_(scalar_transport) {}

    double planck_flux(double nu, double T) const {
        using namespace std;

        const double x = units::h_cgs * nu / (units::k_cgs * T);

        // for numerical reasons, case of small x needs to handled with
        // the Rayleigh-Jeans approximation
        double expm1 = 1;
        if (fabs(x)<= std::sqrt(numeric_limits<double>::epsilon())) {
            expm1 = x + x*x/2;
        }
        else {
            expm1 = exp(x) - 1;
        }

        return 2 * units::h_cgs * pow(nu, 3) / pow(units::c_cgs, 2) 
            * 1 / expm1;
    }

    // Shorthand alias
    double bessel_k(int order, double x)  const {
        return boost::math::cyl_bessel_k(order, x);
    }

    // fitting functions f(X) and g(X) from Shcherbakov (2008)
    double f_function(double X)  const {
        using namespace std;

        return 2.011 * exp(-pow(X, 1.035)/4.7)
            - cos(X/2) * exp(-pow(X, 1.2)/2.73) 
            - 0.011 * exp(-X/47.2);
    }

    double g_function(double X)  const {
        using namespace std;

        // The formula is g = 1 - 0.11 * log(1 + 0.035 * X),
        // so use log1p for stability, as X is often tiny
        return 1 - 0.11 * log1p(0.035 * X);
    }

    // modified fitting functions in Dexter (2016)
    double f_function_D16(double X)  const { using namespace std;

        return f_function(X) + (0.011 * exp(-X/47.2) 
            - pow(2,-1./3) / pow(3,23./6) * 1e4 * cnst::pi * pow(X, -8./3) )
            * 0.5 * (1 + tanh(10 * log(X/120)) );
    }

    double g_function_D16(double X)  const {
        using namespace std;

        return 0.43793091 * log(1+0.00185777 * pow(X, 1.50316886));
    }

    // emissivity functions from Dexter (2016), originally from Huang et
    // al (2009 and Madhevan (1996)
    double I_Q(double x)  const {
        using namespace std;

        return 2.5651 * (
                1 + 0.93193 * pow(x, -1./3) + 0.499873 * pow(x, -2./3) 
                ) * exp(-1.8899 * pow(x, 1./3));
    }

    double I_V(double x)  const {
        using namespace std;

        return (1.81384/x + 3.42319 * pow(x, -2./3) + 0.0292545 / sqrt(x)
                + 2.03773 * pow(x, -1./3) ) * exp(-1.8899 * pow(x, 1./3));
    }

    double I_I(double x)  const {
        using namespace std;

        return 2.5651 * (1 + 1.92 * pow(x, -1./3) + 0.9977 * pow(x, -2./3))
            * exp(-1.8899 * pow(x, 1./3));
    }

    /** \brief Compute the radiation data
     *
     * \param fluid data describing the fluid properties.
     * \param nu frequency of the radiation in the fluid rest frame.
     * \param th_B angle of the ray with the magnetic field.
     *
     */
    RadiationData operator()(const FluidData<MetricSpaceType> &fluid, 
        double nu, double th_B) const {

        using namespace std;

        double n = fluid.number_density;
        double B = fluid.magnetic_field_strength;
        double T = fluid.temperature;

        // Enforce minimum values for theta and nu_c.
        const double nu_c_min = 1;

        // shorthand for e^2, which comes up all the time
        constexpr double e2 = units::e_cgs * units::e_cgs;

        // dimensionless electron temperature
        const double theta_e =
            max(units::k_cgs * T / (units::m_e_cgs * pow(units::c_cgs, 2)),
                theta_e_min_);

        const double nu_B = units::e_cgs * B /
            (2 * cnst::pi * units::m_e_cgs * units::c_cgs);

        const double nu_c =
            max(3. / 2 * nu_B * sin(th_B) * pow(theta_e, 2), nu_c_min);

        const double x = nu / nu_c;

        // This is given in Dexter (2016)
        double j_I = n * e2 * nu /
                     (2 * sqrt(3) * units::c_cgs * pow(theta_e, 2)) * I_I(x);
        double j_Q = n * e2 * nu /
                     (2 * sqrt(3) * units::c_cgs * pow(theta_e, 2)) * I_Q(x);
        double j_U = 0;
        double j_V = 2 * n * e2 * nu * (1 / tan(th_B)) /
                     (3 * sqrt(3) * units::c_cgs * pow(theta_e, 3)) * I_V(x);

        const double B_nu = planck_flux(nu, T);

        double a_I = j_I / B_nu;
        double a_Q = j_Q / B_nu;
        double a_U = j_U / B_nu;
        double a_V = j_V / B_nu;

        // Faraday cofficients come originally from Shcherbakov (2008)
        // and Huang & Shcherbakov (2011), but use formulae modified by
        // Dexter. There is also apparently sign confusion issues, as
        // documented by Dexter.
        // TODO: This should be checked out, but Dexter seems confident
        // enough in having figured out the sign right.

        // Due to Stokes Q being aligned with magnetic field, U
        // components should be zero throughout
        double r_U = 0.0;

        // the angular frequency we are considering
        const double w = 2 * cnst::pi * nu;

        // square of the plasma angular frequency
        const double w_p2 = 4 * cnst::pi * n * e2 / units::m_e_cgs;

        // cyclotron angular frequency
        const double Omega_0 = 2 * cnst::pi * nu_B;
            //units::e_cgs * B / (units::m_e_cgs * units::c_cgs);

        // Shcherbakov (2008) X-parameter, eq. (27)
        const double xpar = theta_e * sqrt(
                sqrt(2) * sin(th_B) * 1e3 * Omega_0/w );

        // difference of diagonal elements of dielectric tensor:
        // eps^1_1 - eps^2_2
        const double diagonal_D16 = 
            f_function_D16(xpar) * w_p2 * pow(Omega_0, 2)/pow(w,4) * (
                bessel_k(1, 1/theta_e) / bessel_k(2, 1/theta_e) + 6 * theta_e
            ) * pow(sin(th_B), 2);

        // the non-diagonal component
        const double non_diagonal_D16 =
            w_p2 * Omega_0 / pow(w,3) * (
                    bessel_k(0, 1/theta_e) - g_function_D16(xpar)
                )/ bessel_k(2, 1/theta_e) * cos(th_B);

        // The Shcherbakov (2008) versions
        // XXX: according to Dexter, the sign in r_Q here is likely
        // erroneous, so it's been changed
        const double r_Q_D16 = w / (2*units::c_cgs) * diagonal_D16;
        const double r_V_D16 = w / units::c_cgs * non_diagonal_D16;

        // use D16 versions
        double r_Q = r_Q_D16;
        double r_V = r_V_D16;

        if (scalar_transport_) {
            // leave out all polarization effects
            j_Q = j_U = j_V = 0;
            a_Q = a_U = a_V = 0;
            r_Q = r_U = r_V = 0;
        }

        // filter possible nans from 0/0 limits
        // if anything is nan, then everything should vanish
        if(std::isnan(j_I) || std::isnan(j_Q) || std::isnan(j_U) || std::isnan(j_V)
           || std::isnan(a_I) || std::isnan(a_Q) || std::isnan(a_U) || std::isnan(a_V)
           || std::isnan(r_Q) || std::isnan(r_U) || std::isnan(r_V)) {

            return {0,0,0,0,
                    0,0,0,0,
                    0,0,0,
                    length_unit_};
        }

        return {j_I, j_Q, j_U, j_V, 
                a_I, a_Q, a_U, a_V, 
                r_Q, r_U, r_V, 
                length_unit_};
    }
};

} // namespace arcmancer
