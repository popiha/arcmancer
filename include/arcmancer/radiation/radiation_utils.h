#pragma once

#include <Eigen/Dense>

#include "arcmancer/interpolation/cubic_spline.h"

//
// radiation models and helper functions for computing radiative
// transfer

namespace arcmancer {

/** \brief Pure electron scattering athmosphere properties
 *
 *  Based on tabulated results from classic Chandrasekhar's 1960 book,
 *  Radiative Transfer.
 *
 *  \param theta Angle with the atmosphere normal
 *  \return A vector of cos(theta), I/F, degree of polarization
 *
 *  \ingroup radiation
 */
inline
Eigen::Vector3d electron_scattering_atmosphere(double theta) {
    static const int table_rows = 21, table_cols = 3;
    static constexpr double chandra_table[21][3] = {
        // cos(theta), I/F,  degree of polarization
        {0.00, 0.414410, 0.117130},
        {0.05, 0.474900, 0.0897900},
        {0.10, 0.523970, 0.0744800},
        {0.15, 0.570010, 0.0631100},
        {0.20, 0.614390, 0.0541000},
        {0.25, 0.657700, 0.0466700},
        {0.30, 0.700290, 0.0404100},
        {0.35, 0.742340, 0.0350200},
        {0.40, 0.783980, 0.0303300},
        {0.45, 0.825300, 0.0261900},
        {0.50, 0.866370, 0.0225200},
        {0.55, 0.907220, 0.0192300},
        {0.60, 0.947890, 0.0162700},
        {0.65, 0.988420, 0.0135800},
        {0.70, 1.02882, 0.0111230},
        {0.75, 1.06911, 0.00888000},
        {0.80, 1.10931, 0.00681800},
        {0.85, 1.14943, 0.00491900},
        {0.90, 1.18947, 0.00315500},
        {0.95, 1.22945, 0.00152200},
        {1.00, 1.26938, 0.00000},
    };
    static Eigen::MatrixXd *table_matrix = nullptr;
    static interpolation::CubicSpline *interpolator = nullptr;

    if (table_matrix == nullptr) {
        table_matrix = new Eigen::MatrixXd(table_rows, table_cols);
        for (int i = 0; i < table_rows; i++) {
            (*table_matrix)(i,0) = chandra_table[i][0];
            (*table_matrix)(i,1) = chandra_table[i][1];
            (*table_matrix)(i,2) = chandra_table[i][2];
        }
    }

    if (interpolator == nullptr) {
        interpolator = new interpolation::CubicSpline(*table_matrix);
    }

    return interpolator->evaluate(std::cos(theta));
}

}

