#include "test.h" // Brings in the UnitTest++ framework

#include <arcmancer/arcmancer.h>
#include <arcmancer/spacetimes/ht.h>

using namespace arcmancer;
using HT = ht::HartleThorneSpacetime;

SUITE(hartle_thorne) {

const double PI = cnst::pi;

const double tolerance = 1e-6;

static void test_rk2(const EigenRk2Tensor<4> &mref, const EigenRk2Tensor<4> &mcomp, double tol) {
    for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 4; j++) {
            const double ref      = mref(i,j);
            const double computed = mcomp(i,j);

            const double rel = (computed-ref)/(ref + 1e-30);

            //std::cout << "ref: " << ref << " computed: " << computed << " rel: " << rel << std::endl;
            CHECK_CLOSE(0.0, rel, tol);
        }
    }
}

TEST(hartle_thorne_test)
{
    // hartle thorne metric with mass 2, a 0.9, chi 0.9/mass, eta parameter 1.33
    double mass = 2.0, chi = 0.9/mass, eta = 1.33;
    ht::HartleThorneSpacetime M(mass, chi, eta, HT::Parametrization::chi_eta);

    // compute stuff at
    // test point: t, r, theta, phi
    ManifoldPoint<HT> p0 {M.boyer_lindquist, { 1.0, 10.0, 0.11, 0.22 }};

    //
    // F-functions and their derivatives
    double r0 = p0.coordinates(M.boyer_lindquist)(1);
    double F1   = ht::F1(r0, mass);
    double F2   = ht::F2(r0, mass);
    double F1_r = ht::F1_r(r0, mass);
    double F2_r = ht::F2_r(r0, mass);

    double F1_ref   = -0.01681866754257732;
    double F2_ref   = 0.01467751245417411;
    double F1_r_ref = 0.00662613309975716;
    double F2_r_ref = -0.005504888596918672;

    //
    // metric and derivatives
    EigenRk2Tensor<4> g     = M.metric_components(p0, M.boyer_lindquist);
    EigenRk3Tensor<4> g_der = M.metric_derivatives(p0, M.boyer_lindquist);

    // references from mathematica
    EigenRk2Tensor<4> g_ref, g_r_ref, g_th_ref;

    // clang-format off
    g_ref.setValues({
        {0.5977814105187471   , 0                  , 0                  , 0.004299211791545664}  ,
        {0                    , -1.672291631526272 , 0                  , 0}                     ,
        {0                    , 0                  , -101.5890254083513 , 0}                     ,
        {0.004299211791545664 , 0                  , 0                  , -1.224444483626812}
        });

    g_r_ref.setValues({
        {0.04083871114419594    , 0                  , 0                 , -0.0004205452531360242} ,
        {0                      , 0.1139503403113395 , 0                 , 0}                      ,
        {0                      , 0                  , -19.8594133147896 , 0}                      ,
        {-0.0004205452531360242 , 0                  , 0                 , -0.2393254425665526}
        });

    g_th_ref.setValues({
            {0.001112648459724103 , 0                    , 0                  , 0.07786116585315225} ,
            {0                    , 0.007847378191842624 , 0                  , 0}                   ,
            {0                    , 0                    , 0.4424909636224758 , 0}                   ,
            {0.07786116585315225  , 0                    , 0                  , -22.17045555595077}
            });
    // clang-format on


    Log::log()->info(">>> Testing Glampedakis & Papas F-functions");
    //std::cout << ">>> F-functions" << std::endl;
    CHECK_CLOSE(F1_ref, F1, tolerance);
    CHECK_CLOSE(F2_ref, F2, tolerance);
    CHECK_CLOSE(F1_r_ref, F1_r, tolerance);
    CHECK_CLOSE(F2_r_ref, F2_r, tolerance);

    Log::log()->info(">>> Testing Hartle-Thorne metric");
    //std::cout << ">>> Metric " << std::endl;
    test_rk2(g_ref, g, tolerance);

    Log::log()->info(">>> Testing Hartle-Thorne metric r-derivative");
    //std::cout << ">>> R-derivative" << std::endl;
    test_rk2(g_r_ref, g_der.chip(1,2), tolerance);

    Log::log()->info(">>> Testing Hartle-Thorne metric theta-derivative");
    //std::cout << ">>> Theta-derivative" << std::endl;
    test_rk2(g_th_ref, g_der.chip(2,2), tolerance);

    //
    // Also test the surface
    //

    // use angular velocity 600 Hz (in proper units)
    const double solar_mass_per_s  = 2.03e5;
    const double solar_mass_per_km = 0.6772;
    const double angvel = 600 * 2*PI/solar_mass_per_s;
    // equatorial radius 12 km
    const double radius = 12 * solar_mass_per_km;

    ht::HartleThorneSpacetime M2(mass, radius, angvel, HT::Parametrization::r_omega);
    ht::HTNeutronStarSurface surface(M2, radius, angvel);

    // value and derivative
    ManifoldPoint<HT> p1 {M2.boyer_lindquist, { 1.0, 10.0, 0.11, 0.22 }};
    double val = surface.value(p1);
    auto grad = surface.gradient(p1).components(M2.boyer_lindquist);

    // references from Mathematica
    double val_ref = -2.2707177523814126019;
    ArcVector<4> grad_ref(0, -1.0000000000000000000, 0.087719995235863351679, 0);

    Log::log()->info(">>> Testing Hartle-Thorne surface value");
    //std::cout << ">>> Surface value" << std::endl;
    CHECK_CLOSE(val_ref, val, tolerance);

    Log::log()->info(">>> Testing Hartle-Thorne surface gradient");
    //std::cout << ">>> Surface value" << std::endl;
    for (int i = 0; i < 4; i++) {
        CHECK_CLOSE(grad_ref(i), grad(i), tolerance);
    }
}

} // SUITE
