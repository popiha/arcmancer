
#include "test.h" // Brings in the UnitTest++ framework

#include <arcmancer/geometry.h>
#include <arcmancer/spacetimes/minkowski.h>
#include <arcmancer/spacetimes/agm.h>
#include <arcmancer/spacetimes/kerr.h>

using namespace arcmancer;
using namespace arcmancer::minkowski;
using namespace arcmancer::agm;
using namespace arcmancer::kerr;

// Geometry tests
// Mostly Tensor related, but also some curve tests
SUITE(geometry) {

TEST(spatial_cross_product) {
    double tol = 1e-15;
    {
        MinkowskiSpacetime M;
        using Tangent = TangentVector<MinkowskiSpacetime>;
        auto p = M.create_point(M.cartesian, {0, 1, 0, 0});
        EigenRk1Tensor<4> zero;
        zero.setZero();

        Tangent e_x = {p, M.cartesian, {0, 1, 0, 0}};
        Tangent e_y = {p, M.cartesian, {0, 0, 1, 0}};
        Tangent e_z = {p, M.cartesian, {0, 0, 0, 1}};
        Tangent u_obs = {p, M.cartesian, {1, 0, 0, 0}};

        M.use_fixed_chart(M.spherical);
        // check basis vector products
        CHECK_ARRAY_CLOSE(
            spatial_cross_product(u_obs, e_x, e_y).components(M.cartesian),
            e_z.components(M.cartesian), 4, tol);
        CHECK_ARRAY_CLOSE(
            spatial_cross_product(u_obs, e_y, e_x).components(M.cartesian),
            (-e_z).components(M.cartesian), 4, tol);
        CHECK_ARRAY_CLOSE(
            spatial_cross_product(u_obs, e_x, e_x).components(M.cartesian),
            zero, 4, tol);
        CHECK_ARRAY_CLOSE(
            spatial_cross_product(u_obs, e_z, e_x).components(M.cartesian),
            e_y.components(M.cartesian), 4, tol);
        CHECK_ARRAY_CLOSE(
            spatial_cross_product(u_obs, e_y, e_z).components(M.cartesian),
            e_x.components(M.cartesian), 4, tol);
    }

    {
        // check orthogonality
        AGMSpacetime M2(1, 1, 0);
        using Tangent2 = TangentVector<AGMSpacetime>;
        auto p2 = M2.create_point(M2.isotropic, {0, 5, cnst::pi / 4, 0});
        Tangent2 u_obs2(p2, M2.isotropic, {1, .1, -.2, .05});
        Tangent2 v1(p2, M2.isotropic, {.4, .9, .2, -.1});
        Tangent2 v2(p2, M2.isotropic, {-.2, .1, .3, .5});
        Tangent2 v3 = spatial_cross_product(u_obs2, v1, v2);
        CHECK_CLOSE(dot_product(v1, v3), 0, tol);
        CHECK_CLOSE(dot_product(v2, v3), 0, tol);
        CHECK_CLOSE(dot_product(u_obs2, v3), 0, tol);
    }

    {
        // check that the metric signature is handled correctly
        MinkowskiSpacetimeSpacelike M3;
        using Tangent = TangentVector<decltype(M3)>;
        auto p = M3.create_point(M3.cartesian, {0, 1, 0, 0});
        EigenRk1Tensor<4> zero;
        zero.setZero();

        Tangent e_x = {p, M3.cartesian, {0, 1, 0, 0}};
        Tangent e_y = {p, M3.cartesian, {0, 0, 1, 0}};
        Tangent e_z = {p, M3.cartesian, {0, 0, 0, 1}};
        Tangent u_obs = {p, M3.cartesian, {1, 0, 0, 0}};

        M3.use_fixed_chart(M3.spherical);
        double tol = 1e-15;
        // check basis vector products
        CHECK_ARRAY_CLOSE(
            spatial_cross_product(u_obs, e_x, e_y).components(M3.cartesian),
            e_z.components(M3.cartesian), 4, tol);
        CHECK_ARRAY_CLOSE(
            spatial_cross_product(u_obs, e_y, e_x).components(M3.cartesian),
            (-e_z).components(M3.cartesian), 4, tol);
        CHECK_ARRAY_CLOSE(
            spatial_cross_product(u_obs, e_x, e_x).components(M3.cartesian),
            zero, 4, tol);
        CHECK_ARRAY_CLOSE(
            spatial_cross_product(u_obs, e_z, e_x).components(M3.cartesian),
            e_y.components(M3.cartesian), 4, tol);
        CHECK_ARRAY_CLOSE(
            spatial_cross_product(u_obs, e_y, e_z).components(M3.cartesian),
            e_x.components(M3.cartesian), 4, tol);
    }
}

TEST(jacobian){
    {
    // basic correctness checks
    MinkowskiSpacetime M;
    auto x = M.create_point(M.cartesian, {0,0,2,0});


    double tol=1e-15;
    //Check that the components are organized correctly
    //TODO: should check for higher ranks also
    Tensor<MinkowskiSpacetime, Cov, Cov> t(x, M.spherical, {{0,1,0,0},
                                                            {0,0,0,0},
                                                            {0,0,0,0},
                                                            {0,0,0,0}});
    EigenRk2Tensor<4> expected;
    expected.setZero();
    expected(0,2) = 1;
    auto res = t.components(M.cartesian);
    CHECK_ARRAY_CLOSE(expected.data(),res.data(),16,tol);    

    EigenRk4Tensor<4> comp;
    comp.setZero();
    comp(0,1,2,3) = 1;
    comp(0,0,0,1) = 2;
    comp(0,0,0,2) = 3;
    comp(0,0,0,3) = 5;
    Tensor<MinkowskiSpacetime,Cov,Cov,Cov,Cov> t2(x,M.spherical,comp);
    //For x = (0,0,2,0) in cartesian, dr = dy, dth = -dz/2, dphi = -dx/2
    //so the non-zero components in cartesian coordinates are
    EigenRk4Tensor<4> expected2;
    expected2.setZero();
    expected2(0,2,3,1) = 0.25;
    expected2(0,0,0,2) = 2;
    expected2(0,0,0,3) = -1.5;
    expected2(0,0,0,1) = -5./2.;
    auto res2 = t2.components(M.cartesian);
    CHECK_ARRAY_CLOSE(expected2.data(),res2.data(),4*4*4*4,tol);
    }

    {
    double tol = 1e-10;
    // More complicated round-trip in Kerr
    KerrSpacetime M(1,.99);
    auto x = M.create_point(M.boyer_lindquist, {0,5,1,1});
    LorentzFrame<KerrSpacetime> frame(x, M.kerr_schild_in, {1, 0, 0, 0},
                                      {0, 0, 0, 1}, {0, 1, 0, 0});
    
    {
    // vector
    EigenRk1Tensor<4> vec_comp;
    vec_comp.setRandom();
    TangentVector<KerrSpacetime> vec{x, M.kerr_schild_in, vec_comp};
    CotangentVector<KerrSpacetime> vec2{
        x, M.kerr_schild_out, 
        vec.lower_index<0>().components(M.kerr_schild_out)};
    
    auto vec_comp2 = vec2.raise_index<0>().components(M.kerr_schild_in);
    CHECK_ARRAY_CLOSE(vec_comp.data(), vec_comp2.data(), 4, tol);
    }
    {
    // vector with frame (every stage goes through some internal chart)
    EigenRk1Tensor<4> vec_comp;
    vec_comp.setRandom();
    TangentVector<KerrSpacetime> vec{frame, vec_comp};
    CotangentVector<KerrSpacetime> vec2{
        frame, 
        vec.lower_index<0>().components(frame)};
    
    auto vec_comp2 = vec2.raise_index<0>().components(frame);
    CHECK_ARRAY_CLOSE(vec_comp.data(), vec_comp2.data(), 4, tol);
    }

    {
    // rank 2 tensor
    using T = Tensor<KerrSpacetime, Cov, Cnt>;
    T::ContainerType t_comp;
    t_comp.setRandom();
    T t{x, M.kerr_schild_out, t_comp};
    
    auto t_ = t.raise_index<0>().lower_index<1>();
    decltype(t_) t2{x, M.kerr_schild_in, t_.components(M.kerr_schild_in)};

    auto t_comp2 = t2.lower_index<0>()
                     .raise_index<1>().components(M.kerr_schild_out);
    CHECK_ARRAY_CLOSE(t_comp.data(), t_comp2.data(), T::component_count, tol);

    }

    {
    // high rank case
    using T = Tensor<KerrSpacetime, Cov, Cnt, Cnt, Cov, Cov>;
    T::ContainerType t_comp;
    t_comp.setRandom();
    T t{x, M.kerr_schild_out, t_comp};
    
    auto t_ = t.raise_index<0>().lower_index<1>().raise_index<4>();
    decltype(t_) t2{x, M.kerr_schild_in, t_.components(M.kerr_schild_in)};

    auto t_comp2 = t2.lower_index<0>()
                     .raise_index<1>()
                     .lower_index<4>().components(M.kerr_schild_out);
    CHECK_ARRAY_CLOSE(t_comp.data(), t_comp2.data(), T::component_count, tol);
        
    }
    {
    // high rank case with frame
    using T = Tensor<KerrSpacetime, Cov, Cnt, Cnt, Cov, Cov>;
    T::ContainerType t_comp;
    t_comp.setRandom();
    T t{frame, t_comp};
    
    auto t_ = t.raise_index<0>().lower_index<1>().raise_index<4>();
    decltype(t_) t2{frame, t_.components(frame)};

    auto t_comp2 = t2.lower_index<0>()
                     .raise_index<1>()
                     .lower_index<4>().components(frame);
    CHECK_ARRAY_CLOSE(t_comp.data(), t_comp2.data(), T::component_count, tol);
        
    }
    
    }
}

TEST(vector_angle){
    {
        MinkowskiSpacetime M;
        using Tangent = TangentVector<MinkowskiSpacetime>;
        auto p = M.create_point(M.cartesian, {0, 1, 0, 0});

        Tangent u_obs{p, M.cartesian, {1, 0, 0, 0}};
        Tangent a{p, M.cartesian, {1, 1, 0, 0}};
        Tangent b{p, M.cartesian, {0, 1, 1, 0}};

        const double tol = 1e-15;

        CHECK_CLOSE(vector_angle(u_obs, a, a), 0, tol);
        CHECK_CLOSE(vector_angle(u_obs, a, b), cnst::pi / 4, tol);
        CHECK_CLOSE(vector_angle(u_obs, a, -a), cnst::pi, tol);
        CHECK_CLOSE(vector_angle(u_obs, b, -a), cnst::pi * .75, tol);
    }
    {
        // Handling of signature
        MinkowskiSpacetimeSpacelike M2;
        using Tangent = TangentVector<decltype(M2)>;
        auto p = M2.create_point(M2.cartesian, {0, 1, 0, 0});

        Tangent u_obs{p, M2.cartesian, {1, 0, 0, 0}};
        Tangent a{p, M2.cartesian, {1, 1, 0, 0}};
        Tangent b{p, M2.cartesian, {0, 1, 1, 0}};

        const double tol = 1e-15;

        CHECK_CLOSE(vector_angle(u_obs, a, a), 0, tol);
        CHECK_CLOSE(vector_angle(u_obs, a, b), cnst::pi / 4, tol);
        CHECK_CLOSE(vector_angle(u_obs, a, -a), cnst::pi, tol);
        CHECK_CLOSE(vector_angle(u_obs, b, -a), cnst::pi * .75, tol);
    }
}

TEST(dot_product) {
    {
        MinkowskiSpacetime M;
        using Tangent = TangentVector<MinkowskiSpacetime>;
        using Cotangent = CotangentVector<MinkowskiSpacetime>;
        auto p = M.create_point(M.cartesian, {0, 1, 0, 0});

        Tangent a{p, M.cartesian, {1, 1, 0, 0}};
        Tangent b{p, M.cartesian, {0, 1, 1, 0}};
        Cotangent c{p, M.cartesian, {0, 1, 1, 0}};

        const double tol = 1e-15;

        CHECK_CLOSE(0, dot_product(a, a), tol);
        CHECK_CLOSE(-1, dot_product(a, b), tol);
        CHECK_CLOSE(1, dot_product(a, c), tol);
        CHECK_CLOSE(2, dot_product(c, b), tol);
        CHECK_CLOSE(-2, dot_product(c, c), tol);
    }

    {
        // With the other signature, mostly just for a sanity check
        MinkowskiSpacetimeSpacelike M;
        using Tangent = TangentVector<decltype(M)>;
        using Cotangent = CotangentVector<decltype(M)>;

        auto p = M.create_point(M.cartesian, {0, 1, 0, 0});

        Tangent a{p, M.cartesian, {1, 1, 0, 0}};
        Tangent b{p, M.cartesian, {0, 1, 1, 0}};
        Cotangent c{p, M.cartesian, {0, 1, 1, 0}};

        const double tol = 1e-15;

        CHECK_CLOSE(0, dot_product(a, a), tol);
        CHECK_CLOSE(1, dot_product(a, b), tol);
        CHECK_CLOSE(1, dot_product(a, c), tol);
        CHECK_CLOSE(2, dot_product(c, b), tol);
        CHECK_CLOSE(2, dot_product(c, c), tol);
    }
}

TEST(screen_project) {
    const double tol = 1e-15;
    {
        MinkowskiSpacetime M;
        using Tangent = TangentVector<MinkowskiSpacetime>;
        
        auto p = M.create_point(M.cartesian, {0, 1, 0, 0});

        EigenRk1Tensor<4> expected;
        expected.setValues({0,0,1,.5});

        Tangent k{p, M.cartesian, {.1, .1, 0, 0}};
        Tangent u{p, M.cartesian, {1, 0, 0, 0}};

        Tangent c{p, M.cartesian, {.5, 1, 1, .5}};
        
        CHECK_CLOSE(0, dot_product(u, screen_project(u,k,c)), tol);
        CHECK_CLOSE(0, dot_product(k, screen_project(u,k,c)), tol);

        CHECK_ARRAY_CLOSE(
            expected, screen_project(u, k, c).components(M.cartesian), 4, tol);
    }

    {
        MinkowskiSpacetimeSpacelike M;
        using Tangent = TangentVector<decltype(M)>;

        auto p = M.create_point(M.cartesian, {0, 1, 0, 0});

        EigenRk1Tensor<4> expected;
        expected.setValues({0,0,1,.5});

        Tangent k{p, M.cartesian, {.1, .1, 0, 0}};
        Tangent u{p, M.cartesian, {1, 0, 0, 0}};

        Tangent c{p, M.cartesian, {.5, 1, 1, .5}};

        CHECK_CLOSE(0, dot_product(u, screen_project(u,k,c)), tol);
        CHECK_CLOSE(0, dot_product(k, screen_project(u,k,c)), tol);

        CHECK_ARRAY_CLOSE(
            expected, screen_project(u, k, c).components(M.cartesian), 4, tol);
    }
}

TEST(screen_inner_product) {
    const double tol = 1e-15;
    {
        MinkowskiSpacetime M;
        using Tangent = TangentVector<MinkowskiSpacetime>;

        auto p = M.create_point(M.cartesian, {0, 1, 0, 0});

        Tangent k{p, M.cartesian, {1, -1, 0, 0}};
        Tangent u{p, M.cartesian, {1, 0, 0, 0}};

        Tangent a{p, M.cartesian, {.5, -3, 1, .5}};
        Tangent b{p, M.cartesian, {4, 2, 10, -13}};

        CHECK_CLOSE(
            dot_product(screen_project(u, k, a), screen_project(u, k, b)),
            screen_inner_product(u, k, a, b), tol);
    }

    {
        MinkowskiSpacetimeSpacelike M;
        using Tangent = TangentVector<decltype(M)>;

        auto p = M.create_point(M.cartesian, {0, 1, 0, 0});

        Tangent k{p, M.cartesian, {1, -1, 0, 0}};
        Tangent u{p, M.cartesian, {1, 0, 0, 0}};

        Tangent a{p, M.cartesian, {.5, -3, 1, .5}};
        Tangent b{p, M.cartesian, {4, 2, 10, -13}};

        CHECK_CLOSE(
            dot_product(screen_project(u, k, a), screen_project(u, k, b)),
            screen_inner_product(u, k, a, b), tol);
    }
}

TEST(scalar_tensor_ops) {
    
    using MS = MinkowskiSpacetime;
    MS M;

    auto x = M.create_point(M.cartesian, {0,1,1,1});
    const double val = 123;
    Tensor<MS> s{x, M.spherical, val};
    TangentVector<MS> v{x, M.cartesian, {1,2,3}};

    auto ss = s.tprod(s);
    bool res = std::is_same<Tensor<MS>, decltype(ss)>::value;
    CHECK(res);
    CHECK(double(ss) == val*val);

    auto sv = s.tprod(v);
    auto vs = v.tprod(s);

    res = std::is_same<TangentVector<MS>, decltype(sv)>::value;
    CHECK(res);
    res = std::is_same<TangentVector<MS>, decltype(vs)>::value;
    CHECK(res);
    CHECK(v*val == sv);
    CHECK(v*val == vs);

    CHECK(s.components(M.cartesian)(0) == val);
    CHECK(s.transport_derivatives(
        M.spherical, M.christoffel_symbols_contracted(
                         x, M.spherical, v.components(M.spherical)))(0) == 0);
    }

// curve related things

// check that the different ways of computing the christoffel symbol contraction
// are equivalent
TEST(christoffel_contraction) {
    KerrSpacetime M(1,.99);
    auto x = M.create_point(M.boyer_lindquist, {0,5,1,1});
    
    TangentVector<KerrSpacetime> k{x, M.boyer_lindquist, {1,.1,.3,.2}};
    const auto &chart = M.kerr_schild_out;
    auto k_comp = k.components(chart);

    EigenRk2Tensor<4> Gamma1 = M.christoffel_symbols(x, chart).contract(
        k_comp, Eigen::array<IndexPair, 1>{IndexPair(2, 0)});

    EigenRk2Tensor<4> Gamma2 =
        M.christoffel_symbols_contracted(x, chart, k_comp);

    CHECK_ARRAY_CLOSE(Gamma1.data(), Gamma2.data(), 16, 1e-15);
}

// Trivial forward propagation
TEST(curve_forward_propagation) {
         
    using MS = MinkowskiSpacetime;
    MS M;
    M.use_fixed_chart(M.cartesian);
    
    auto x = make_point(M.cartesian, {0,1,0,0});

    Geodesic<MS> geo({x, M.cartesian, {1,1,0,0}});

    auto result = geo.compute(1000);

    CHECK_EQUAL(result, Geodesic<MS>::ComputationResult::success);
    CHECK_CLOSE(geo.extent_back(), 1000,
                geo.configuration().propagation.tolerance.relative * 1000);
    CHECK_EQUAL(geo.extent_front(), 0);

    CHECK(std::is_sorted(geo.begin(), geo.end(), [](auto& p1, auto& p2) {
        return p1.curve_parameter() < p2.curve_parameter();
    }));

    CHECK_ARRAY_CLOSE(geo.front().tangent().components(M.cartesian).data(),
                      geo.back().tangent().components(M.cartesian).data(), 
                      4, 3e-16);

    double expected_coord[] = {1000, 1001, 0, 0};
    CHECK_ARRAY_CLOSE(
        expected_coord, geo.back().point().coordinates(M.cartesian).data(), 4,
        geo.configuration().propagation.tolerance.relative * 1000);
}

// Trivial backward propagation
TEST(curve_backward_propagation) {
         
    using MS = MinkowskiSpacetime;
    MS M;
    M.use_fixed_chart(M.cartesian);
    
    auto x = make_point(M.cartesian, {0,1,0,0});

    Geodesic<MS> geo({x, M.cartesian, {1,1,0,0}});

    auto result = geo.compute(-1000);

    CHECK_EQUAL(result, Geodesic<MS>::ComputationResult::success);
    CHECK_CLOSE(geo.extent_front(), -1000,
                geo.configuration().propagation.tolerance.relative * 1000);
    CHECK_EQUAL(geo.extent_back(), 0);

    CHECK(std::is_sorted(geo.begin(), geo.end(), [](auto& p1, auto& p2) {
        return p1.curve_parameter() < p2.curve_parameter();
    }));
    
    CHECK_ARRAY_CLOSE(geo.back().tangent().components(M.cartesian).data(),
                      geo.front().tangent().components(M.cartesian).data(), 
                      4, 3e-16);
    
    double expected_coord[] = {-1000, -999, 0, 0};
    CHECK_ARRAY_CLOSE(
        expected_coord, geo.front().point().coordinates(M.cartesian).data(), 4,
        geo.configuration().propagation.tolerance.relative * 1000);
}

TEST(curve_multiple_propagation) {
         
    using MS = MinkowskiSpacetime;
    MS M;
    M.use_fixed_chart(M.cartesian);
    
    auto x = make_point(M.cartesian, {0,1,0,0});

    Geodesic<MS> geo({x, M.cartesian, {1,1,0,0}});

    auto result = geo.compute(500);
    CHECK_EQUAL(result, Geodesic<MS>::ComputationResult::success);
    
    result = geo.compute(-200);
    CHECK_EQUAL(result, Geodesic<MS>::ComputationResult::success);
    
    result = geo.compute(1000);
    CHECK_EQUAL(result, Geodesic<MS>::ComputationResult::success);
    
    result = geo.compute(-1000);
    CHECK_EQUAL(result, Geodesic<MS>::ComputationResult::success);
    
    CHECK_CLOSE(geo.extent_front(), -1000,
                geo.configuration().propagation.tolerance.relative * 1000);
    CHECK_CLOSE(geo.extent_back(), 1000,
                geo.configuration().propagation.tolerance.relative * 1000);

    CHECK(std::is_sorted(geo.begin(), geo.end(), [](auto& p1, auto& p2) {
        return p1.curve_parameter() < p2.curve_parameter();
    }));
    
    CHECK_ARRAY_CLOSE(geo.back().tangent().components(M.cartesian).data(),
                      geo.front().tangent().components(M.cartesian).data(), 
                      4, 3e-16);
    
    double expected_coord[] = {-1000, -999, 0, 0};
    CHECK_ARRAY_CLOSE(
        expected_coord, geo.front().point().coordinates(M.cartesian).data(), 4,
        geo.configuration().propagation.tolerance.relative * 1000);
    
    double expected_coord2[] = {1000, 1001, 0, 0};
    CHECK_ARRAY_CLOSE(
        expected_coord2, geo.back().point().coordinates(M.cartesian).data(), 4,
        geo.configuration().propagation.tolerance.relative * 1000);
}

// Check that tensor parallel transport works correctly
TEST(tensor_parallel_transport) {
    using MS = MinkowskiSpacetime;
    MS M;
    // transport is non-trivial in spherical coords
    M.use_fixed_chart(M.spherical);
    
    M.configuration.curve.propagation.tolerance = {1e-14, 1e-14};
    double err_tol = 1e-8;

    auto x = make_point(M.cartesian, {0,1,0,0});
    
    EigenRk1Tensor<4> u_comp;
    u_comp.setRandom();

    // tangent+cotangent vector
    {
    EigenRk1Tensor<4> trans_comp;
    trans_comp.setRandom();

    ParametrizedPoint<MS, CotangentVector<MS>> p{
        0, {x, M.cartesian, u_comp}, {x, M.cartesian, trans_comp}};
    
    Geodesic<MS, CotangentVector<MS>> geo{p};
    geo.compute(10);

    CHECK_ARRAY_CLOSE(u_comp.data(),
                      geo.back().tangent().components(M.cartesian).data(), 4,
                      err_tol);
    
    CHECK_ARRAY_CLOSE(trans_comp.data(),
                      geo.back().transported().components(M.cartesian).data(),
                      4, err_tol);
    }

    // two index case
    {
    using T = Tensor<MS, Cov, Cnt>;
    T::ContainerType trans_comp;
    trans_comp.setRandom();

    ParametrizedPoint<MS, T> p{
        0, {x, M.cartesian, u_comp}, {x, M.cartesian, trans_comp}};
    
    Geodesic<MS, T> geo{p};
    geo.compute(10);
    
    CHECK_ARRAY_CLOSE(trans_comp.data(),
                      geo.back().transported().components(M.cartesian).data(),
                      T::component_count, err_tol);
    }

    // High-rank generic case, this is pretty slow
    {
    using T = Tensor<MS, Cov, Cnt, Cov, Cov, Cnt>;
    T::ContainerType trans_comp;
    trans_comp.setRandom();

    ParametrizedPoint<MS, T> p{
        0, {x, M.cartesian, u_comp}, {x, M.cartesian, trans_comp}};
    
    Geodesic<MS, T> geo{p};
    geo.compute(10);
    
    CHECK_ARRAY_CLOSE(trans_comp.data(),
                      geo.back().transported().components(M.cartesian).data(),
                      T::component_count, err_tol);
    }
}

} // SUITE(geometry)
