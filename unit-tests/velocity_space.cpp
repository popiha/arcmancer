#include "test.h" // Brings in the UnitTest++ framework

#include <array>
#include <iostream>

#include <arcmancer/general_utils.h>
#include <arcmancer/geometry.h>
#include <arcmancer/other_spaces/velocity_space.h>
#include <arcmancer/spacetimes/kerr.h>
#include <arcmancer/spacetimes/minkowski.h>

using namespace arcmancer;
using namespace arcmancer::minkowski;
using namespace arcmancer::kerr;
using namespace arcmancer::tensor_space;
using namespace arcmancer::velocity_space;

SUITE(velocity_space) {

    TEST(immersion_functions) {
        // The ambient space
        KerrSpacetime Mamb(1.0, 0.9375);
        auto base_point = make_point(Mamb.kerr_schild_out, {0, 10.5, 10.5, 5});

        // Space of contravariant vectors at base_point
        TensorSpace<KerrSpacetime, Cnt> TM(Mamb.kerr_schild_out, base_point);

        // The velocity space as an immersion onto TM
        using VSpaceType = velocity_space::VelocitySpaceType<KerrSpacetime>;
        using VSpaceImmersionType =
            velocity_space::VelocitySpaceImmersion<KerrSpacetime>;

        VSpaceImmersionType vimmersion(TM.canonical);

        // clang-format off
        aligned_vector<EigenRk1Tensor<3>> test_pts = {
            make_EigenRk1Tensor<3>({0, 0, 0}),
            make_EigenRk1Tensor<3>({1, 0, 0}),
            make_EigenRk1Tensor<3>({1.29907, 0.0267525, 0.0127511}),
        };

        aligned_vector<EigenRk1Tensor<4>> imms = {
            make_EigenRk1Tensor<4>({1.0707619355825088725, 0, 0, 0}),
            make_EigenRk1Tensor<4>(
                {1.4439292033784796310, 1.0000000000000000000, 0, 0}),
            make_EigenRk1Tensor<4>(
                {1.6660459103091262989, 1.2990699999999999470, 0.026752499999999998503, 0.012751099999999999587})};

        aligned_vector<VSpaceType::JacobianType> jacs(test_pts.size());
        jacs[0].setValues(
            {{-0.09212885670137941, -0.1038754015184086, -0.04683529267317012},
             {1.000000000000000, 0, 0},
             {0, 1.000000000000000, 0},
             {0, 0, 1.000000000000000}});
        jacs[1].setValues(
            {{0.6975178843735239, -0.05512740889009624, -0.02485582045354231},
             {1.000000000000000, 0, 0},
             {0, 1.000000000000000, 0},
             {0, 0, 1.000000000000000}});
        jacs[2].setValues(
            {{0.7899506695933058, -0.03082730690803483, -0.01345789741606669},
             {1.000000000000000, 0, 0},
             {0, 1.000000000000000, 0},
             {0, 0, 1.000000000000000}});

        aligned_vector<VSpaceType::JacobianDerivativeType> jacders(
            test_pts.size());

        jacders[0].setValues(
            {{{1.1327851700145106228, 0.069931274638276252897, 0.031530580549535336399},
              {0.069931274638276252897, 1.1496095354750219819, 0.035550769128805087549},
              {0.031530580549535336399, 0.035550769128805087549, 1.0867910498151871490}},
             {{0, 0, 0}, {0, 0, 0}, {0, 0, 0}},
             {{0, 0, 0}, {0, 0, 0}, {0, 0, 0}},
             {{0, 0, 0}, {0, 0, 0}, {0, 0, 0}}});
        jacders[1].setValues(
            {{{0.38371027819289615923, 0.023687941505707636042, 0.010680408037201567090},
              {0.023687941505707636042, 0.79982768653951807902, 0.024084346923804608731},
              {0.010680408037201567090, 0.024084346923804608731, 0.75727045813250593032}},
             {{0, 0, 0}, {0, 0, 0}, {0, 0, 0}},
             {{0, 0, 0}, {0, 0, 0}, {0, 0, 0}},
             {{0, 0, 0}, {0, 0, 0}, {0, 0, 0}}});
        jacders[2].setValues(
            {{{0.24307082291487769332, 0.0058384079650917689724, 0.0024147443469655022544},
              {0.0058384079650917689724, 0.68504804342292324737, 0.019914022761655787025},
              {0.0024147443469655022544, 0.019914022761655787025, 0.64981159312105174486}},
             {{0, 0, 0}, {0, 0, 0}, {0, 0, 0}},
             {{0, 0, 0}, {0, 0, 0}, {0, 0, 0}},
             {{0, 0, 0}, {0, 0, 0}, {0, 0, 0}}});
        // clang-format on

        for (size_t i = 0; i < test_pts.size(); i++) {
            auto immersed = vimmersion.immersion(test_pts[i]);
            auto jac = vimmersion.jacobian(test_pts[i]);
            auto jacder = vimmersion.jacobian_derivative(test_pts[i]);

            ArcVector<4> immvec = eigen_tensor_to_vector(immersed);
            ArcMatrix<4> g = eigen_tensor_to_matrix(Mamb.metric_components(base_point, Mamb.kerr_schild_out));
            double norm = immvec.transpose() * g *  immvec;

            CHECK_CLOSE(1, norm, 1e-15);
            CHECK_ARRAY_CLOSE(imms[i].data(), immersed.data(), 4, 1e-15);
            CHECK_ARRAY_CLOSE(jacs[i].data(), jac.data(), 3 * 3, 1e-15);
            CHECK_ARRAY_CLOSE(jacders[i].data(), jacder.data(), 3 * 3 * 3, 1e-15);

#if 0
            std::cout << "Test point:\n"
                      << test_pts[i] << "\n"
                      << "Immersed point:\n"
                      << immersed << "\n"
                      << "vs. expected:\n"
                      << imms[i] << "\n"
                      << "norm: " << norm << " expected 1\n"
                      << "Jacobian:\n"
                      << jac << "\n"
                      << "vs. expected:\n"
                      << jacs[i] << "\n"
                      << "DJacobian:\n"
                      << jacder << "\n"
                      << "vs. expected:\n"
                      << jacders[i] << "\n";
#endif
        }
    }

    TEST(metric_and_derivatives_kerr) {
        // The ambient space
        KerrSpacetime Mamb(1.0, 0.9375);
        auto base_point = make_point(Mamb.kerr_schild_out, {3, 10, 5, 1});

        // Space of contravariant vectors at base_point
        TensorSpace<KerrSpacetime, Cnt> TM(Mamb.kerr_schild_out, base_point);

        // The velocity space as an immersion onto TM
        // VelocitySpace<KerrSpacetime> VSpace(TM.canonical);
        using VSpaceType =
            velocity_space::VelocitySpaceType<KerrSpacetime>;
        using VSpaceImmersionType =
            velocity_space::VelocitySpaceImmersion<KerrSpacetime>;

        VSpaceImmersionType vimmersion(TM.canonical);
        VSpaceType VSpace(TM.canonical,
                          std::bind(&VSpaceImmersionType::immersion,
                                    &vimmersion, std::placeholders::_1),
                          std::bind(&VSpaceImmersionType::jacobian, &vimmersion,
                                    std::placeholders::_1),
                          std::bind(&VSpaceImmersionType::jacobian_derivative,
                                    &vimmersion, std::placeholders::_1));

        // clang-format off
        aligned_vector<EigenRk1Tensor<3>> test_pts = {
            make_EigenRk1Tensor<3>({0, 0, 0}),
            make_EigenRk1Tensor<3>({1.29907, 0.0267525, 0.0127511}),
            make_EigenRk1Tensor<3>({1, 0, 0}),
            make_EigenRk1Tensor<3>({0, 1, 0}),
            make_EigenRk1Tensor<3>({0, 0, 1}),
            make_EigenRk1Tensor<3>({2, 0, 0}),
            make_EigenRk1Tensor<3>({0, 2, 0}),
            make_EigenRk1Tensor<3>({0, 0, 2}),
            make_EigenRk1Tensor<3>({2, 2, 0}),
            make_EigenRk1Tensor<3>({2, 2, 2}),
            make_EigenRk1Tensor<3>({10, 0, 0}),
        };


        // Test values from Mathematica
        aligned_vector<EigenRk2Tensor<3>> gs(test_pts.size());
        int index=0;
        gs[index++].setValues({{-1.157490143669324, -0.09596568260010938, -0.01655329901152607}, 
                {-0.09596568260010938, -1.058476118075310, -0.01008665432587470}, 
                {-0.01655329901152607, -0.01008665432587470, -1.001739865757824}}); 
        gs[index++].setValues({{-0.3912048716182658, -0.01808188919527503, 
                0.001019847034115661}, {-0.01808188919527504, -1.050560155839874, 
                -0.008300552822933967}, {0.001019847034115661, -0.008300552822933965, 
                -1.001336862498848}}); 
        gs[index++].setValues({{-0.5364984619122000, -0.04448024148879628, 
                -0.007672479552269586}, {-0.04448024148879630, -1.054207541338620, 
                -0.009350359588405764}, {-0.007672479552269588, 
                -0.009350359588405764, -1.001612860909635}}); 
        gs[index++].setValues({{-1.153016245645423, 
                -0.04661976972064071, -0.01608306150960209}, {-0.04661976972064072, 
                -0.5142037397378179, -0.004900058950067291}, {-0.01608306150960209, 
                -0.004900058950067291, -1.001690440557018}}); 
        gs[index++].setValues({{-1.157353256897545, 
                -0.09588227145966187, -0.008269455634415973}, {-0.09588227145966187, 
                -1.058425291992845, -0.005038943620207156}, {-0.008269455634415987, 
                -0.005038943620207154, -0.5004345883767384}}); 
        gs[index++].setValues({{-0.2055947156851397, 
                -0.01704553368130998, -0.002940215795822844}, {-0.01704553368130998, 
                -1.051932972975270, -0.008958015062523641}, {-0.002940215795822845, 
                -0.008958015062523641, -1.001545184672917}}); 
        gs[index++].setValues({{-1.150451870589910, 
                -0.01833539054982318, -0.01581352802590216}, {-0.01833539054982318, 
                -0.2022345122416649, -0.001927175854900499}, {-0.01581352802590216, 
                -0.001927175854900500, -1.001662110731129}}); 
        gs[index++].setValues({{-1.157271238994590, 
                -0.09583229433817923, -0.003306058124445968}, {-0.09583229433817923, 
                -1.058394838731036, -0.002014526860133224}, {-0.003306058124445982, 
                -0.002014526860133231, -0.2000694978967041}}); 
        gs[index++].setValues({{-0.5663644763445204, 
                0.4484652941523313, -0.003989984163055547}, {0.4484652941523313, 
                -0.5570512860442014, 0.001484248702504503}, {-0.003989984163055547, 
                0.001484248702504503, -1.001472855054844}}); 
        gs[index++].setValues({{-0.7230815628139198, 
                0.3023630722353601, 
                0.3352056048425417}, {0.3023630722353601, -0.6932305775375447, 
                0.3124568964911358}, {0.3352056048425417, 
                0.3124568964911358, -0.7169058825133696}}); 
        gs[index++].setValues({{-0.009914346171963517, 
                -0.0008219828074821686, -0.0001417853426967691}, 
                {-0.0008219828074821772, -1.050587903954752, -0.008726001606416064}, 
                {-0.0001417853426967722, -0.008726001606416064, -1.001505164240513}});

        aligned_vector<EigenRk3Tensor<3>> gders(test_pts.size());
        index=0;
        gders[index++].setValues({{{-6.320741603756936e-17, -6.408703138392130e-18, 
                -9.406786332714404e-19}, {-1.681704399425297e-17, 
                -3.415394245047648e-17, -5.257765075236916e-19}, 
                {6.431896139629990e-18, -9.209563852948977e-20, 
                -3.062727954325484e-17}}, {{-2.119536716292384e-17, 
                -2.922520370884817e-17, -4.703393166357202e-19}, 
                {-3.204351569196065e-18, -3.008739729506429e-17, 
                -3.713084710103962e-19}, {-3.665844764151841e-20, 
                -1.544680365132953e-19, -1.531363977162742e-17}}, 
                {{-4.336808689942018e-19, -2.168404344971009e-19, 
                -5.551115123125783e-17}, {-2.168404344971009e-19, 
                -2.168404344971009e-19, -2.775557561562891e-17}, 
                {5.421010862427522e-20, 2.710505431213761e-20, 
                3.469446951953614e-18}}}); 
        gders[index++].setValues({{{0.3979909023406865, 
                0.01839554647436508, -0.001037537798745069}, {0.02942329651517676, 
                0.5353266870053792, 0.004169542437667206}, {0.004044774539841318, 
                0.004433200776857922, 0.5093413734495764}}, {{0.02942329651517676, 
                0.5353266870053792, 0.004169542437667206}, {0.001869688735297807, 
                0.1086291630212946, 0.0008582869821942825}, {0.0001582049492920990,
                0.01268429994364958, 
                0.05186653701685068}}, {{0.004044774539841317, 
                0.004433200776857922, 0.5093413734495764}, {0.0001582049492920989, 
                0.01268429994364958, 
                0.05186653701685068}, {-0.00002379375401072026, 
                0.0001936577794660554, 
                0.02336189858984486}}}); 
        gders[index++].setValues({{{0.5756611992683129, 0.04772716228844492,
                0.008232546957690882}, {0.04772716228844491, 0.5675592163473134, 
                0.005357727280808489}, {0.008232546957690882, 0.005357727280808489,
                0.5374226262519779}}, {{0.04772716228844491, 0.5675592163473134, 
                0.005357727280808489}, {0.003956983765803274, 0.09378281203610409, 
                0.0008318125049987411}, {0.0006825474866056063, 
                0.008504292057268331, 
                0.04462372237429315}}, {{0.008232546957690882, 
                0.005357727280808489, 0.5374226262519779}, {0.0006825474866056064, 
                0.008504292057268331, 0.04462372237429314}, {0.0001177338849599899,
                0.0001434808854968223, 
                0.01536970838923885}}}); 
        gders[index++].setValues({{{0.1075067037122946, 
                0.004346805857611141, 0.001499577247961099}, {0.5950586684181401, 
                0.04794411987213867, 0.008498409994540445}, {0.006399636198028393, 
                0.0004568792397394036, 0.04677738561962995}}, {{0.5950586684181401,
                0.04794411987213868, 0.008498409994540447}, {0.04794411987213869, 
                0.5288109719207148, 0.005039257274120731}, {0.008498409994540447, 
                0.005039257274120733, 0.5150969811717551}}, {{0.006399636198028391,
                0.0004568792397394034, 
                0.04677738561962994}, {0.008498409994540445, 0.005039257274120731, 
                0.5150969811717551}, {0.0001576158989892169, 
                0.00004802115542826914, 
                0.009816684416896534}}}); 
        gders[index++].setValues({{{0.01914136282252209, 
                0.001585788379925398, 
                0.0001367677929791472}, {0.006624732000132525, 
                0.009235746354541198, 0.00008333864142325245}, {0.5792479846184897,
                0.04802447437125419, 
                0.008276643253017283}}, {{0.006624732000132534, 
                0.009235746354541186, 
                0.00008333864142325246}, {0.0009662907201252646, 
                0.01066669074510644, 0.00005078190561525253}, {0.04802447437125419,
                0.5296980162787760, 0.005043323352863904}}, {{0.5792479846184897, 
                0.04802447437125419, 0.008276643253017283}, {0.04802447437125419, 
                0.5296980162787760, 0.005043323352863908}, {0.008276643253017291, 
                0.005043323352863909, 0.5008695544875907}}}); 
        gders[index++].setValues({{{0.1690767484706136,
                0.01401788660364164, 0.002417971322380621}, {0.01401788660364163, 
                0.4331248214343099, 0.003783676214521530}, {0.002417971322380621, 
                0.003783676214521530, 0.4118420847151503}}, {{0.01401788660364163, 
                0.4331248214343100, 0.003783676214521530}, {0.001162200873922699, 
                0.07172303568532222, 
                0.0006107765898637173}, {0.0002004701895120727, 
                0.006491208181509417, 
                0.03419642135216427}}, {{0.002417971322380621, 
                0.003783676214521530, 0.4118420847151503}, {0.0002004701895120727, 
                0.006491208181509417, 
                0.03419642135216427}, {0.00003457947570402478, 
                0.0001053539895442041, 
                0.01177903588882250}}}); 
        gders[index++].setValues({{{0.08437593742416255, 
                0.001344746186458179, 0.001159788849301962}, {0.4659945189057523, 
                0.01483219505841569, 0.006466753298192026}, {0.005014140559102966, 
                0.0001413420878311602, 0.03679268289721559}}, {{0.4659945189057523,
                0.01483219505841569, 0.006466753298192026}, {0.01483219505841569, 
                0.1635951917664966, 0.001558965876078866}, {0.006466753298192026, 
                0.001558965876078866, 0.4051487248028846}}, {{0.005014140559102964,
                0.0001413420878311601, 
                0.03679268289721558}, {0.006466753298192026, 0.001558965876078866, 
                0.4051487248028846}, {0.0001219017975692442, 
                0.00001485602710284591, 
                0.007721516138278821}}}); 
        gders[index++].setValues({{{0.01530402392746268, 
                0.001267308541124124, 
                0.00004372008128885975}, {0.005296362261390545, 
                0.007384343172941667, 0.00002664057157143176}, {0.4630912114725338,
                0.03835955830684315, 
                0.002645765555900870}}, {{0.005296362261390536, 
                0.007384343172941671, 
                0.00002664057157143177}, {0.0007722269240498145, 
                0.008528659325000111, 
                0.00001623327388079263}, {0.03835955830684315, 0.4235131645597043, 
                0.001612181509625101}}, {{0.4630912114725338, 0.03835955830684315, 
                0.002645765555900869}, {0.03835955830684315, 0.4235131645597042, 
                0.001612181509625101}, {0.002645765555900874, 0.001612181509625102,
                0.1601112159545576}}}); 
        gders[index++].setValues({{{0.2670956343349056, -0.2114947656885576,
                0.001881663479136140}, {0.01725111402809995, 0.03395768453821670, 
                0.0005165293595662819}, {0.003779150843513846, 
                -0.002597454197158102, 0.2361656568868946}}, {{0.01725111402809995, 
                0.03395768453821668, 0.0005165293595662819}, {-0.1947881951784409, 
                0.2419518657190061, -0.0006446744703026918}, 
                {-0.001380958004811424, 0.002469309086421693, 
                0.2174844083686145}}, {{0.003779150843513846, 
                -0.002597454197158101, 0.2361656568868946}, {-0.001380958004811424, 
                0.002469309086421692, 
                0.2174844083686145}, {0.00003999137921762145, 
                -0.00001487653842456966, 
                0.01003770418276088}}}); 
        gders[index++].setValues({{{0.2473311642349125, 
                -0.1034237553874150, -0.1146575943398924}, {0.06168265342686039, 
                0.07114334917945649, -0.1060055328793137}, {0.04280842738591734, 
                -0.09531146470506638, 0.07618784269032722}}, {{0.06168265342686041, 
                0.07114334917945649, -0.1060055328793137}, {-0.09483389030381464, 
                0.2174265265246149, -0.09799974192432306}, {-0.09444063403607067, 
                0.04700338728953920, 
                0.06915490944427211}}, {{0.04280842738591735, -0.09531146470506638,
                0.07618784269032719}, {-0.09444063403607067, 0.04700338728953921, 
                0.06915490944427211}, {-0.09284307787872556, -0.08654228794383739, 
                0.1985639491713475}}}); 
        gders[index++].setValues({{{0.001965885200350569, 
                0.0001629884420156227, 
                0.00002811417939212628}, {0.0001629884420156196, 
                0.1041656781962089, 
                0.0008662914573716939}, {0.00002811417939212602, 
                0.0008662914573716939, 
                0.09929288994373145}}, {{0.0001629884420156196, 0.1041656781962089,
                0.0008662914573716938}, {0.00001351311471592629, 
                0.01727130389599108, 
                0.0001434524659707192}, {2.330902280994392e-6, 
                0.001561305892938456, 
                0.008244572457384646}}, {{0.00002811417939212602, 
                0.0008662914573716938, 
                0.09929288994373145}, {2.330902280994392e-6, 
                0.001561305892938456, 
                0.008244572457384648}, {4.020616680728321e-7, 
                0.00002474438256276600, 0.002839975058488595}}});

        // clang-format on

        for (size_t i = 0; i < test_pts.size(); i++) {
            ManifoldPoint<VSpaceType> pt = make_point(VSpace.canonical, test_pts[i]);
            auto metric = VSpace.metric_components(pt, VSpace.canonical);
            auto metricder = VSpace.metric_derivatives(pt, VSpace.canonical);

            CHECK_ARRAY_CLOSE(gs[i].data(), metric.data(), 3*3, 1e-15);
            CHECK_ARRAY_CLOSE(gders[i].data(), metricder.data(), 3*3*3, 1e-15);

#if 0
            std::cout << "Test point:\n"
                      << test_pts[i] << "\n"
                      << "Metric:\n"
                      << metric << "\n"
                      << "vs. expected:\n"
                      << gs[i] << "\n"
                      << "DMetric:\n"
                      << metricder << "\n"
                      << "vs. expected:\n"
                      << gders[i] << "\n";
#endif
        }

    }

    TEST(metric_and_derivatives_mink) {
        // The ambient space
        MinkowskiSpacetime Mamb;
        auto base_point = make_point(Mamb.cartesian, {1, 1, 1, 1});

        // Space of contravariant vectors at base_point
        TensorSpace<MinkowskiSpacetime, Cnt> TM(Mamb.cartesian, base_point);

        // The velocity space as an immersion onto TM
        // VelocitySpace<MinkowskiSpacetime> VSpace(TM.canonical);
        using VSpaceType =
            velocity_space::VelocitySpaceType<MinkowskiSpacetime>;
        using VSpaceImmersionType =
            velocity_space::VelocitySpaceImmersion<MinkowskiSpacetime>;

        VSpaceImmersionType vimmersion(TM.canonical);
        VSpaceType VSpace(TM.canonical,
                          std::bind(&VSpaceImmersionType::immersion,
                                    &vimmersion, std::placeholders::_1),
                          std::bind(&VSpaceImmersionType::jacobian, &vimmersion,
                                    std::placeholders::_1),
                          std::bind(&VSpaceImmersionType::jacobian_derivative,
                                    &vimmersion, std::placeholders::_1));

        auto test_point = make_point(VSpace.canonical, {1, 2, 3});
        auto g = VSpace.metric_components(test_point, VSpace.canonical);
        auto gder = VSpace.metric_derivatives(test_point, VSpace.canonical);

        // Test values from Mathematica
        // clang-format off
        EigenRk2Tensor<3> g_expected;
        g_expected.setValues(
                {{-0.9333333333333333 , 0.13333333333333333 , 0.2}   ,
                {0.13333333333333333  , -0.7333333333333333 , 0.4}   ,
                {0.2                  , 0.4                 , -0.4}}
                );

        EigenRk3Tensor<3> gder_expected;
        gder_expected.setValues(
                { { {0.12444444444444444   , -0.017777777777777778 , -0.02666666666666667}    ,
                    {0.11555555555555555   , 0.03111111111111111   , -0.05333333333333334}    ,
                    {0.17333333333333334   , -0.05333333333333334  , -0.013333333333333334} } ,
                  { {0.11555555555555555   , 0.03111111111111111   , -0.05333333333333334}    ,
                    {-0.035555555555555556 , 0.19555555555555557   , -0.10666666666666667}    ,
                    {-0.05333333333333334  , 0.09333333333333334   , -0.02666666666666667} }  ,
                  { {0.17333333333333334   , -0.05333333333333334  , -0.013333333333333334}   ,
                    {-0.05333333333333334  , 0.09333333333333334   , -0.02666666666666667}    ,
                    {-0.08                 , -0.16                 , 0.16} } }
                );
        // clang-format on

        CHECK_ARRAY_CLOSE(g_expected.data(), g.data(), 3 * 3, 1e-15);
        CHECK_ARRAY_CLOSE(gder_expected.data(), gder.data(), 3 * 3 * 3, 1e-15);
    }

    TEST(normal_coordinates_mink) {
        // The ambient space
        MinkowskiSpacetime Mamb;
        auto base_point = make_point(Mamb.cartesian, {0, 0, 0, 0});

        // Space of contravariant vectors at base_point
        TensorSpace<MinkowskiSpacetime, Cnt> TM(Mamb.cartesian, base_point);

        // The velocity space as an immersion onto TM
        // VelocitySpace<MinkowskiSpacetime> VSpace(TM.canonical);
        using VSpaceType =
            velocity_space::VelocitySpaceType<MinkowskiSpacetime>;
        using VSpaceImmersionType =
            velocity_space::VelocitySpaceImmersion<MinkowskiSpacetime>;

        VSpaceImmersionType vimmersion(TM.canonical);
        VSpaceType VSpace(TM.canonical,
                          std::bind(&VSpaceImmersionType::immersion,
                                    &vimmersion, std::placeholders::_1),
                          std::bind(&VSpaceImmersionType::jacobian, &vimmersion,
                                    std::placeholders::_1),
                          std::bind(&VSpaceImmersionType::jacobian_derivative,
                                    &vimmersion, std::placeholders::_1));

        auto pt_x = make_point(VSpace.canonical, {1.44262, 0, 0});
        auto pt_y1 = make_point(VSpace.canonical, {1, 0, 0});
        auto pt_y2 = make_point(VSpace.canonical, {2, 0, 0});

        auto rnc_y1 = riemann_normal_coordinates(
            VSpace.canonical, pt_x, pt_y1, NormalCoordinateMethod::numerical);
        auto rnc_y2 = riemann_normal_coordinates(
            VSpace.canonical, pt_x, pt_y2, NormalCoordinateMethod::numerical);

        // XXX: Reference values computed with the implementation
        // itself! Should be confirmed somehow.
        EigenRk1Tensor<3> rnc_y1_ref, rnc_y2_ref;
        rnc_y1_ref.setValues({-0.4935, 0, 0});
        rnc_y2_ref.setValues({0.4935, 0, 0});

        CHECK_ARRAY_CLOSE(rnc_y1_ref.data(), rnc_y1.data(), 3, 1e-3);
        CHECK_ARRAY_CLOSE(rnc_y2_ref.data(), rnc_y2.data(), 3, 1e-3);
    }
}
