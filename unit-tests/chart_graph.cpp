
#include <random>

#include "test.h" // Brings in the UnitTest++ framework

#include "arcmancer/geometry/metric_space.h"
using namespace arcmancer;

// For testing the automatic generation of transitions
class TestSpace : public MetricSpace<TestSpace, 5, OtherSignature> {
public:
    std::string name() const { return "TestSpace"; }

    const ChartType chart0{*this, "chart0"};
    const ChartType chart1{*this, "chart1"};
    const ChartType chart2{*this, "chart2"};
    const ChartType chart3{*this, "chart3"};
    const ChartType chart4{*this, "chart4"};
    const ChartType chart5{*this, "chart5"};

    TestSpace() {
        // For now the metric doesn't matter
        auto g = [](const EigenRk1Tensor<D> &) -> EigenRk2Tensor<D> {
            EigenRk2Tensor<D> ret;
            return ret;
        };
        auto g_der = [](const EigenRk1Tensor<D> &) -> EigenRk3Tensor<D> {
            EigenRk3Tensor<D> ret;
            return ret;
        };

        add_chart(chart0, g, g_der);
        add_chart(chart1, g, g_der);
        add_chart(chart2, g, g_der);
        add_chart(chart3, g, g_der);
        add_chart(chart4, g, g_der);
        add_chart(chart5, g, g_der);

        // Each of the charts charti is shifted in the (i-1)th dimension by 1
        // unit with respect to chart0.
        //
        // The initial transition function graph will be like this:
        //
        //       chart4<-->chart3<-->chart5
        //                   ^
        //                   |
        //                   v
        // chart1<--------+chart0
        //    +              ^
        //    |              |
        //    |              |
        //    v              |
        // chart2+-----------+
        //
        // The jacobians are defined in a similar way, with the (i-i)th
        // dimension scaled by 2.
        // The transition functions and jacobian don't need to be consistent for
        // this test.
        // The jacobian graph is like the transition function graph, but with
        // all the connections going in one direction only.

        add_transition(chart0, chart1,
                       [](const EigenRk1Tensor<D> &x) -> EigenRk1Tensor<D> {
                           auto y = x;
                           y(0) += 1;
                           return y;
                       },
                       [](const EigenRk1Tensor<D> &) -> ArcMatrix<D> {
                           ArcMatrix<D> J = ArcMatrix<D>::Identity();
                           J(0, 0) = 2;
                           return J;
                       });

        add_transition(chart1, chart2,
                       [](const EigenRk1Tensor<D> &x) -> EigenRk1Tensor<D> {
                           auto y = x;
                           y(0) -= 1;
                           y(1) += 1;
                           return y;
                       },
                       [](const EigenRk1Tensor<D> &) -> ArcMatrix<D> {
                           ArcMatrix<D> J = ArcMatrix<D>::Identity();
                           J(0, 0) = .5;
                           J(1, 1) = 2;
                           return J;
                       });

        add_transition(chart2, chart0,
                       [](const EigenRk1Tensor<D> &x) -> EigenRk1Tensor<D> {
                           auto y = x;
                           y(1) -= 1;
                           return y;
                       },
                       [](const EigenRk1Tensor<D> &) -> ArcMatrix<D> {
                           ArcMatrix<D> J = ArcMatrix<D>::Identity();
                           J(1, 1) = .5;
                           return J;
                       });

        add_transition(chart0, chart3,
                       [](const EigenRk1Tensor<D> &x) -> EigenRk1Tensor<D> {
                           auto y = x;
                           y(2) += 1;
                           return y;
                       },
                       [](const EigenRk1Tensor<D> &) -> ArcMatrix<D> {
                           ArcMatrix<D> J = ArcMatrix<D>::Identity();
                           J(2, 2) = 2;
                           return J;
                       });

        add_transition(chart3, chart0,
                       [](const EigenRk1Tensor<D> &x) -> EigenRk1Tensor<D> {
                           auto y = x;
                           y(2) -= 1;
                           return y;
                       });

        add_transition(chart3, chart4,
                       [](const EigenRk1Tensor<D> &x) -> EigenRk1Tensor<D> {
                           auto y = x;
                           y(2) -= 1;
                           y(3) += 1;
                           return y;
                       },
                       [](const EigenRk1Tensor<D> &) -> ArcMatrix<D> {
                           ArcMatrix<D> J = ArcMatrix<D>::Identity();
                           J(2, 2) = .5;
                           J(3, 3) = 2;
                           return J;
                       });

        add_transition(chart4, chart3,
                       [](const EigenRk1Tensor<D> &x) -> EigenRk1Tensor<D> {
                           auto y = x;
                           y(3) -= 1;
                           y(2) += 1;
                           return y;
                       });

        add_transition(chart3, chart5,
                       [](const EigenRk1Tensor<D> &x) -> EigenRk1Tensor<D> {
                           auto y = x;
                           y(2) -= 1;
                           y(4) += 1;
                           return y;
                       },
                       [](const EigenRk1Tensor<D> &) -> ArcMatrix<D> {
                           ArcMatrix<D> J = ArcMatrix<D>::Identity();
                           J(2, 2) = .5;
                           J(4, 4) = 2;
                           return J;
                       });

        add_transition(chart5, chart3,
                       [](const EigenRk1Tensor<D> &x) -> EigenRk1Tensor<D> {
                           auto y = x;
                           y(4) -= 1;
                           y(2) += 1;
                           return y;
                       });

        complete_chart_graph();
    }
};

class TestSpace2 : public MetricSpace<TestSpace2, 2, OtherSignature> {
public:
    std::string name() const { return "TestSpace2"; }

    const ChartType chart0{*this, "chart0"};
    const ChartType chart1{*this, "chart1"};
    const ChartType chart2{*this, "chart2"};
    const ChartType chart3{*this, "chart3"};

    TestSpace2() {
        // For now the metric doesn't matter
        auto g = [](const EigenRk1Tensor<D> &) -> EigenRk2Tensor<D> {
            EigenRk2Tensor<D> ret;
            return ret;
        };
        auto g_der = [](const EigenRk1Tensor<D> &) -> EigenRk3Tensor<D> {
            EigenRk3Tensor<D> ret;
            return ret;
        };

        add_chart(chart0, g, g_der);
        add_chart(chart1, g, g_der);
        add_chart(chart2, g, g_der);
        add_chart(chart3, g, g_der);

        // simple case where the jacobians depend on coordinates
        // only works for x_i>0
        add_transition(chart0, chart1,
                       [](const EigenRk1Tensor<D> &x) -> EigenRk1Tensor<D> {
                           return x.pow(2);
                       },
                       [](const EigenRk1Tensor<D> &x) -> ArcMatrix<D> {
                           ArcMatrix<D> J;
                           J << 2*x(0), 0, 0, 2*x(1);
                           return J;
                       });
        add_transition(chart1, chart0,
                       [](const EigenRk1Tensor<D> &x) -> EigenRk1Tensor<D> {
                           EigenRk1Tensor<D> y;
                           y(0) = std::sqrt(x(0));
                           y(1) = std::sqrt(x(1));
                           return y;
                       });

        add_transition(chart1, chart2,
                       [](const EigenRk1Tensor<D> &x) -> EigenRk1Tensor<D> {
                           return x.pow(3);
                       },
                       [](const EigenRk1Tensor<D> &x) -> ArcMatrix<D> {
                           ArcMatrix<D> J;
                           J << 3*x(0)*x(0), 0, 0, 3*x(1)*x(1);
                           return J;
                       });
        
        add_transition(chart2, chart1,
                       [](const EigenRk1Tensor<D> &x) -> EigenRk1Tensor<D> {
                           EigenRk1Tensor<D> y;
                           y(0) = std::cbrt(x(0));
                           y(1) = std::cbrt(x(1));
                           return y;
                       });

        add_transition(chart2, chart3,
                       [](const EigenRk1Tensor<D> &x) -> EigenRk1Tensor<D> {
                           return x.pow(4);
                       },
                       [](const EigenRk1Tensor<D> &x) -> ArcMatrix<D> {
                           ArcMatrix<D> J;
                           J << 4*std::pow(x(0), 3), 0, 0, 4*std::pow(x(1), 3);
                           return J;
                       });
        
        add_transition(chart3, chart2,
                       [](const EigenRk1Tensor<D> &x) -> EigenRk1Tensor<D> {
                           return x.pow(0.25);
                       });

        complete_chart_graph();
    }
};

SUITE(chart_graph) {

TEST(generated_transitions) {
    TestSpace M;
    constexpr size_t D = TestSpace::D;

    std::vector<const Chart<TestSpace> *> charts{
        &M.chart0, &M.chart1, &M.chart2, &M.chart3, &M.chart4, &M.chart5};
    const double tol = 1e-15;

    for (int i = 0; i < 6; ++i) {
        for (int j = 0; j < 6; ++j) {
            if (j == i) continue;

            ManifoldPoint<TestSpace> x0(*charts[i], {0, 0, 0, 0, 0});
            EigenRk1Tensor<D> expected;
            expected.setZero();

            if (i > 0) expected(i - 1) = -1;
            if (j > 0) expected(j - 1) = 1;

            CHECK_ARRAY_CLOSE(expected, x0.coordinates(*charts[j]), D, tol);
        }
    }
}

TEST(generated_jacobians) {
    TestSpace M;
    constexpr size_t D = TestSpace::D;

    ManifoldPoint<TestSpace> x0(M.chart0, {0, 0, 0, 0, 0});

    std::vector<const Chart<TestSpace> *> charts{
        &M.chart0, &M.chart1, &M.chart2, &M.chart3, &M.chart4, &M.chart5};
    const double tol = 1e-15;

    for (int i = 0; i < 6; ++i) {
        for (int j = 0; j < 6; ++j) {
            if (j == i) continue;
            ArcMatrix<D> expected_J = ArcMatrix<D>::Identity();

            if (i > 0) expected_J(i - 1, i - 1) = .5;
            if (j > 0) expected_J(j - 1, j - 1) = 2;

            CHECK_ARRAY_CLOSE(expected_J.data(),
                              M.jacobian(x0, *charts[i], *charts[j]).data(),
                              D * D, tol);
        }
    }
}


TEST(generated_jacobians_with_transitions) {
    using namespace std;
    TestSpace2 M;
    constexpr size_t D = TestSpace2::D;
    
    // can't use too tight tol since the values can become quite large
    const double tol = 1e-6; 

    random_device rd;
    mt19937 gen(rd());
    // use an area that doesn't give too large values
    uniform_real_distribution<> dis(1, 1.5);
    
    for(int i=0; i<1000; ++i){
        double x1 = dis(gen), x2 = dis(gen);

        ManifoldPoint<TestSpace2> x0(M.chart0, {x1,x2});
        
        // chart0 -> chart2

        ArcMatrix<D> expected_J02;
        expected_J02 << 2*x1*3*pow(pow(x1,2),2), 0,
                        0, 2*x2*3*pow(pow(x2,2),2); 

        CHECK_ARRAY_CLOSE(expected_J02.data(),
                          M.jacobian(x0, M.chart0, M.chart2).data(),
                          D * D, tol);

        // chart2 -> chart0
        
        ArcMatrix<D> expected_J20 = expected_J02.inverse();

        CHECK_ARRAY_CLOSE(expected_J20.data(),
                          M.jacobian(x0, M.chart2, M.chart0).data(),
                          D * D, tol);

        // chart1 -> chart0
        ArcMatrix<D> expected_J10;
        expected_J10 << 1./(2*x1), 0,
                        0, 1./(2*x2); 
        
        CHECK_ARRAY_CLOSE(expected_J10.data(),
                          M.jacobian(x0, M.chart1, M.chart0).data(),
                          D * D, tol);

        // chart2 -> chart1
        ArcMatrix<D> expected_J21;
        expected_J21 << 1./(3*pow(pow(x1,2),2)), 0,
                        0, 1./(3*pow(pow(x2,2),2)); 
        
        CHECK_ARRAY_CLOSE(expected_J21.data(),
                          M.jacobian(x0, M.chart2, M.chart1).data(),
                          D * D, tol);

        // chart0-> chart3
        ArcMatrix<D> expected_J03;
        expected_J03 << 2*x1*3*pow(pow(x1,2),2)*4*pow(pow(pow(x1,2),3),3), 0,
                        0, 2*x2*3*pow(pow(x2,2),2)*4*pow(pow(pow(x2,2),3),3); 

        CHECK_ARRAY_CLOSE(expected_J03.data(),
                          M.jacobian(x0, M.chart0, M.chart3).data(),
                          D * D, tol);
        
        // chart3 -> chart0
        
        ArcMatrix<D> expected_J30 = expected_J03.inverse();

        CHECK_ARRAY_CLOSE(expected_J30.data(),
                          M.jacobian(x0, M.chart3, M.chart0).data(),
                          D * D, tol);

        // Not covering all the cases, but this should be convincing enough
    }
    
}

} // SUITE(chart_graph)

