#include "test.h" // Brings in the UnitTest++ framework

#include <arcmancer/geometry/types.h>
#include <arcmancer/geometry/utils.h>
#include <arcmancer/other_spaces/euclidean_space.h>

using namespace arcmancer;
using namespace arcmancer::euclidean;

template <long D>
using ES = EuclideanSpace<D>;


SUITE(euclidean_space) {

    TEST(dim2) {
        ES<2> M;
        auto x = M.create_point(M.cartesian, {0, 0});
        EigenRk2Tensor<2> g = M.metric_components(x, M.cartesian);
        EigenRk3Tensor<2> gd = M.metric_derivatives(x, M.cartesian);

        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < 2; j++) {
                CHECK_EQUAL((i==j ? 1.0 : 0.0), g(i,j));
            }
        }

        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < 2; j++) {
                for (int k = 0; k < 2; k++) {
                    CHECK_EQUAL(0.0, gd(i,j,k));
                }
            }
        }
    }

}
