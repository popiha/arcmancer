#include "test.h" // Brings in the UnitTest++ framework

#include <iostream>
#include <array>

#include <arcmancer/geometry.h>
#include <arcmancer/general_utils.h>
#include <arcmancer/spacetimes/minkowski.h>
#include <arcmancer/other_spaces/tensor_space.h>

using namespace arcmancer;
using namespace arcmancer::minkowski;
using namespace arcmancer::tensor_space;

SUITE(tensor_space) {

    TEST(index_array) {
        MinkowskiSpacetime Mbase;

        auto base = make_point(Mbase.cartesian, {0,0,0,0});

        TensorSpace<MinkowskiSpacetime, Cnt, Cov> M2(Mbase.cartesian, base);

        std::array<int, 16> touch1{}, touch2{}, expected{};
        expected.fill(1);

        for (size_t i = 0; i < 16; i++) {
            for (size_t j = 0; j < 16; j++) {
                auto aix = M2.index_array(i);
                auto bix = M2.index_array(j);
                touch1[aix[0]*4 + bix[0]] = 1;
                touch2[aix[1]*4 + bix[1]] = 1;
            }
        }

        CHECK_ARRAY_EQUAL(expected, touch1, 16);
        CHECK_ARRAY_EQUAL(expected, touch2, 16);
    }

    TEST(cartesian_rank2) {

        MinkowskiSpacetime Mbase;
        auto base = make_point(Mbase.cartesian, {0,0,0,0});

        TensorSpace<MinkowskiSpacetime, Cnt> M(Mbase.cartesian, base);
        auto test_point = make_point(M.canonical, { 0,0,0,0 });

        TensorSpace<MinkowskiSpacetime, Cnt, Cov> M2(Mbase.cartesian, base);
        auto test_point2 = make_point(M2.canonical,{
                0,0,0,0,
                0,0,0,0,
                0,0,0,0,
                0,0,0,0
                });

        {
            EigenRk2Tensor<4> ref_gcomps;
            ref_gcomps.setZero();
            ref_gcomps(0,0) = 1;
            ref_gcomps(1,1) = -1;
            ref_gcomps(2,2) = -1;
            ref_gcomps(3,3) = -1;

            EigenRk2Tensor<16> ref_gcomps2;
            ref_gcomps2.setZero();
            for (size_t i = 0; i < 16; i++) {
                for (size_t j = 0; j < 16; j++) {
                    auto aix = M2.index_array(i);
                    auto bix = M2.index_array(j);
                    ref_gcomps2(i, j) = ref_gcomps(aix[0], bix[0]) * ref_gcomps(aix[1], bix[1]);
                }
            }

            EigenRk2Tensor<4> g = M.metric_components(test_point, M.canonical);
            EigenRk2Tensor<16> g2 = M2.metric_components(test_point2, M2.canonical);

            CHECK_ARRAY_EQUAL(ref_gcomps.data(), g.data(), 4*4);
            CHECK_ARRAY_EQUAL(ref_gcomps2.data(), g2.data(), 16*16);
        }

        {
            EigenRk2Tensor<4> pcomps;
            pcomps.setValues({
                    {11, 12, 13, 14},
                    {21, 22, 23, 24},
                    {31, 32, 33, 34},
                    {41, 42, 43, 44},
                    });

            EigenRk1Tensor<16> cat_pcomps = pcomps.reshape(std::array<double,1> {16});

            Tensor<MinkowskiSpacetime, Cnt, Cov> t2(base, Mbase.cartesian, pcomps);
            EigenRk1Tensor<16> cat_pcomps2 = M2.concatenate(t2.components(Mbase.cartesian));

            CHECK_ARRAY_EQUAL(cat_pcomps.data(), cat_pcomps2.data(), 16);
        }
    }

}
