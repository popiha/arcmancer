#include "test.h" // Brings in the UnitTest++ framework

#include <cstdio>
#include <exception>
#include <iostream>
#include <vector>
#include <algorithm>

#include <H5Cpp.h>


#include "arcmancer/arcmancer.h"
#include "arcmancer/spacetimes/minkowski.h"
#include "arcmancer/radiation/thermal_synchrotron.h"

using namespace arcmancer;
using namespace arcmancer::minkowski;


SUITE(radiation_transfer) {

// test radiation transfer with constant absorption coefficient
TEST(constant_absorption) {
    using Point = ManifoldPoint<MinkowskiSpacetime>; 
    using Tangent = TangentVector<MinkowskiSpacetime>; 

    MinkowskiSpacetime M;
    M.use_fixed_chart(M.spherical);
    auto& curve_conf = M.configuration.curve.propagation;
    auto& line_conf = M.configuration.line_integral.propagation;
    
    // set looser absolute tolerances since they're used for checking the 
    // accuracy also (old default).
    curve_conf.tolerance.absolute = 1e-10;
    line_conf.tolerance.absolute = 1e-10;


    auto x0 = M.create_point(M.cartesian, {0,1,0,0});

    Geodesic<MinkowskiSpacetime, PolarizationFrame<MinkowskiSpacetime>> geo(
        {x0, M.cartesian, {1, 0, 0, 1}},
        {{x0, M.cartesian, {0, 1, 0, 0}}, {x0, M.cartesian, {0, 0, 1, 0}}});

    {
        geo.compute(10);

        auto p_end = geo.back();
        CHECK_CLOSE(p_end.curve_parameter(), 10, curve_conf.tolerance.absolute);
        //XXX: doesn't reach absolute tolerance, as the integrator takes
        //multiple steps
        CHECK_CLOSE(p_end.point().coordinates(M.cartesian)(3), 10,
                    curve_conf.tolerance.absolute*3);
    }

    {
        RadiationFunction<FluidData<MinkowskiSpacetime>> radiation =
            [](const FluidData<MinkowskiSpacetime>&, double, double)
            -> RadiationData{
                return {0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1};
            };

        FluidFunction<MinkowskiSpacetime, FluidData<MinkowskiSpacetime>> fluid =
            [&M](const Point& x)
            -> FluidData<MinkowskiSpacetime> {
                return {0,
                        0,
                        0,
                        0,
                        Tangent{x, M.cartesian, {0, 1, 0, 0}},
                        Tangent{x, M.cartesian, {1, 0, 0, 0}}};
            };

        // generate initial conditions
        // using 1 as the frequency can hide errors, so using something else
        // The solution should be independent of the frequency
        PolarizationSpectrum initial_intensities(
            {{5.0, {100.0, 0, 0, 0}},
             {10.0, {100.0, 0, 0, 0}}});

        PolarizationSpectrum result 
            = radiation_transfer(geo, 1, initial_intensities, fluid, radiation);


        //doesn't attain absolute tolerance because takes multiple steps
        CHECK_CLOSE(result.stokes_vectors()[0].invariant_IQUV_vector()(0),
                    100 * std::exp(-10), line_conf.tolerance.absolute * 10);
        CHECK_CLOSE(result.stokes_vectors()[1].invariant_IQUV_vector()(0),
                    100 * std::exp(-10), line_conf.tolerance.absolute * 10);
    }
}

// same but with the spacelike signature
TEST(constant_absorption_spacelike) {
    using Point = ManifoldPoint<MinkowskiSpacetimeSpacelike>; 
    using Tangent = TangentVector<MinkowskiSpacetimeSpacelike>; 

    MinkowskiSpacetimeSpacelike M;
    M.use_fixed_chart(M.spherical);
    auto& curve_conf = M.configuration.curve.propagation;
    auto& line_conf = M.configuration.line_integral.propagation;
    
    curve_conf.tolerance.absolute = 1e-10;
    line_conf.tolerance.absolute = 1e-10;
    
    auto x0 = M.create_point(M.cartesian, {0,1,0,0});

    Geodesic<MinkowskiSpacetimeSpacelike,
             PolarizationFrame<MinkowskiSpacetimeSpacelike>>
        geo({x0, M.cartesian, {1, 0, 0, 1}},
            {{x0, M.cartesian, {0, 1, 0, 0}}, {x0, M.cartesian, {0, 0, 1, 0}}});

    {
        geo.compute(10);

        auto p_end = geo.back();
        CHECK_CLOSE(10, p_end.curve_parameter(), curve_conf.tolerance.absolute);
        //XXX: doesn't reach absolute tolerance, as the integrator takes
        //multiple steps
        CHECK_CLOSE(10, p_end.point().coordinates(M.cartesian)(3),
                    curve_conf.tolerance.absolute*3);
    }

    {
        auto radiation =
            [](const FluidData<MinkowskiSpacetimeSpacelike>&, double, double)
            -> RadiationData{
                return {0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1};
            };

        auto fluid =
            [&M](const Point& x)
            -> FluidData<MinkowskiSpacetimeSpacelike> {
                return {0,
                        0,
                        0,
                        0,
                        Tangent{x, M.cartesian, {0, 1, 0, 0}},
                        Tangent{x, M.cartesian, {1, 0, 0, 0}}};
            };

        // generate initial conditions
        // using 1 as the frequency can hide errors, so using something else
        // The solution should be independent of the frequency
        PolarizationSpectrum initial_intensities(
            {{5.0, {100.0, 0, 0, 0}},
             {10.0, {100.0, 0, 0, 0}}});

        PolarizationSpectrum result = radiation_transfer(
            geo, 1, initial_intensities, fluid, radiation);

        //doesn't attain absolute tolerance because takes multiple steps
        CHECK_CLOSE(result.stokes_vectors()[0].invariant_IQUV_vector()(0),
                    100 * std::exp(-10), line_conf.tolerance.absolute * 10);
        CHECK_CLOSE(result.stokes_vectors()[1].invariant_IQUV_vector()(0),
                    100 * std::exp(-10), line_conf.tolerance.absolute * 10);
    }
}

// Should get the same results with both signatures from ThermalSynchrotron
TEST(consistent_results_with_both_signatures) {
    using Point = ManifoldPoint<MinkowskiSpacetime>; 
    using Tangent = TangentVector<MinkowskiSpacetime>; 
    using PointS = ManifoldPoint<MinkowskiSpacetimeSpacelike>; 
    using TangentS = TangentVector<MinkowskiSpacetimeSpacelike>; 

    MinkowskiSpacetime M;
    MinkowskiSpacetimeSpacelike MS;

    M.use_fixed_chart(M.spherical);
    MS.use_fixed_chart(MS.spherical);
    M.configuration.line_integral.propagation.tolerance.absolute =
        MS.configuration.line_integral.propagation.tolerance.absolute = 1e-80;

    auto x0 = M.create_point(M.cartesian, {0,1,0,0});
    auto x0S = MS.create_point(MS.cartesian, {0,1,0,0});

    Geodesic<MinkowskiSpacetime,
             PolarizationFrame<MinkowskiSpacetime>>
        geo({x0, M.cartesian, {1, 0, 0, 1}},
            {{x0, M.cartesian, {0, 1, 0, 0}}, {x0, M.cartesian, {0, 0, 1, 0}}});

    Geodesic<MinkowskiSpacetimeSpacelike,
             PolarizationFrame<MinkowskiSpacetimeSpacelike>>
        geoS({x0S, MS.cartesian, {1, 0, 0, 1}},
             {{x0S, MS.cartesian, {0, 1, 0, 0}},
              {x0S, MS.cartesian, {0, 0, 1, 0}}});

    geo.compute(50);
    geoS.compute(50);


    ThermalSynchrotron<MinkowskiSpacetime> radiation(1);
    ThermalSynchrotron<MinkowskiSpacetimeSpacelike> radiationS(1);

    auto fluid =
        [&M](const Point& x)
        -> FluidData<MinkowskiSpacetime> {
            return {1e7,
                    1e10,
                    0,
                    1,
                    Tangent{x, M.spherical, {0, 1, 0, 1e-2}},
                    normalized(Tangent{x, M.cartesian, {1, .1, 0, .1}})};
        };

    auto fluidS =
        [&MS](const PointS& x)
        -> FluidData<MinkowskiSpacetimeSpacelike> {
            return {1e7,
                    1e10,
                    0,
                    1,
                    TangentS{x, MS.spherical, {0, 1, 0, 1e-2}},
                    normalized(TangentS{x, MS.cartesian, {1, .1, 0, .1}})};
        };
        
        PolarizationSpectrum initial_intensities(
            {{150e9, {0.0, 0, 0, 0}},
             {250e9, {0.0, 0, 0, 0}}});

        PolarizationSpectrum result = radiation_transfer(
            geo, 1, initial_intensities, fluid, radiation);
        
        PolarizationSpectrum resultS = radiation_transfer(
            geoS, -1, initial_intensities, fluidS, radiationS);

        // all Stokes params should be non-zero with this configuration
        CHECK((result.stokes_vectors()[0].invariant_IQUV_vector().array() != 0)
                  .all());
        CHECK((result.stokes_vectors()[1].invariant_IQUV_vector().array() != 0)
                  .all());

        // gives exactly the same result for both if all goes well
        CHECK_ARRAY_EQUAL(resultS.stokes_vectors()[0].invariant_IQUV_vector(),
                          result.stokes_vectors()[0].invariant_IQUV_vector(),
                          4);
        CHECK_ARRAY_EQUAL(resultS.stokes_vectors()[1].invariant_IQUV_vector(),
                          result.stokes_vectors()[1].invariant_IQUV_vector(),
                          4);
}
} // SUITE(radiation_transfer)
