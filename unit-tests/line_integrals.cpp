#include "test.h" // Brings in the UnitTest++ framework

#include <cstdio>
#include <exception>
#include <iostream>
#include <vector>
#include <algorithm>
#include <array>



#include "arcmancer/arcmancer.h"
#include "arcmancer/spacetimes/minkowski.h"
#include "arcmancer/geometry/line_integral.h"

using namespace arcmancer;
using namespace arcmancer::minkowski;

SUITE(line_integrals) {

TEST(minkowski_array) {
    using Point = ManifoldPoint<MinkowskiSpacetime>; 
    using ParPoint = ParametrizedPoint<MinkowskiSpacetime>; 
    using ParCurve = ParametrizedCurve<MinkowskiSpacetime>;
    using Tangent = TangentVector<MinkowskiSpacetime>; 

    MinkowskiSpacetime M;
    M.use_fixed_chart(M.cartesian);

    // Adjust configuration
    auto& conf = M.configuration;
    conf.curve.propagation.enforce_maximum_stepsize = true;
    conf.curve.propagation.maximum_stepsize         = 0.1;

    auto &propconf = conf.line_integral.propagation;
    // set looser absolute tolerance since it's used for checking the 
    // accuracy also (old default).
    propconf.tolerance.absolute = 1e-10;

    // construct initial point and direction
    Point x0 = M.create_point(M.cartesian, {0, 1, 2, 3});
    Tangent v0 = Tangent(x0, M.cartesian, {1, 0.1, 0.1, 0.1});
    ParPoint initial_point(0.0, v0);

    ParCurve curve(initial_point);
    //ParametrizedCurve<ParPoint> curve({x0, M.cartesian, {1, 0.1, 0.1, 0.1}});
    //Geodesic<MinkowskiSpacetime> curve(v0);

    // propagate for a parameter interval of 10 units, then compute a
    // line integral
    curve.compute(10);
    size_t N_pts = curve.num_points();
    auto p_end = curve.back();

    // check curve itself
    CHECK_CLOSE(p_end.curve_parameter(), 10, propconf.tolerance.absolute);

    CHECK_CLOSE(p_end.point().coordinates(M.cartesian)(0), 10,
                propconf.tolerance.absolute*std::sqrt(N_pts));
    CHECK_CLOSE(p_end.point().coordinates(M.cartesian)(1), 2,
                propconf.tolerance.absolute*std::sqrt(N_pts));
    CHECK_CLOSE(p_end.point().coordinates(M.cartesian)(2), 3,
                propconf.tolerance.absolute*std::sqrt(N_pts));
    CHECK_CLOSE(p_end.point().coordinates(M.cartesian)(3), 4,
                propconf.tolerance.absolute*std::sqrt(N_pts));

    // compute a line integral over the curve
    using Type = std::array<double, 4>;
    Type initial_value = {0,0,0,0};
    // uses the array as StateType
    Type integral = line_integral(curve,
            0, 10, // range
            initial_value,
            [](const Type &, const ParPoint &pt) -> Type {
                auto comps = pt.point().coordinates(pt.M().cartesian);
                return {0, -comps(2), comps(1), 0};
            }
            );

    // check the result
    CHECK_CLOSE(integral[0] , 0.0   , propconf.tolerance.absolute*std::sqrt(N_pts));
    CHECK_CLOSE(integral[1] , -25.0 , propconf.tolerance.absolute*std::sqrt(N_pts));
    CHECK_CLOSE(integral[2] , 15.0  , propconf.tolerance.absolute*std::sqrt(N_pts));
    CHECK_CLOSE(integral[3] , 0     , propconf.tolerance.absolute*std::sqrt(N_pts));
}

TEST(minkowski_scalar) {
    
    MinkowskiSpacetime M;
    double r0=1, th0=1, phi0=0;
    Geodesic<MinkowskiSpacetime> geo(
        {{M.spherical, {0, r0, th0, phi0}}, M.spherical, {1, .5, .1, 1}});
    geo.compute(1);
    // integrating the components with line_integral should give the same result
    // angles won't wrap over this curve length
    double r_int = line_integral(geo, r0, 
        [&](double, auto &p){return p.tangent().components(M.spherical)(1);});
    double th_int = line_integral(geo, th0, 
        [&](double, auto &p){return p.tangent().components(M.spherical)(2);});
    double phi_int = line_integral(geo, phi0, 
        [&](double, auto &p){return p.tangent().components(M.spherical)(3);});

    // assume that under 100 steps are taken to give this tolerance bound
    double tol =
        100 * M.configuration.line_integral.propagation.tolerance.relative;
    auto expected = geo.back().point().coordinates(M.spherical);
    CHECK_CLOSE(expected(1), r_int, tol);
    CHECK_CLOSE(expected(2), th_int, tol);
    CHECK_CLOSE(expected(3), phi_int, tol);
}

} // SUITE(line_integrals)
