
#include "test.h" // Brings in the UnitTest++ framework

#include <iostream>

#include <Eigen/Dense>

#include <arcmancer/geometry/types.h>
#include <arcmancer/general_utils.h>
#include <arcmancer/other_spaces/two_sphere.h>
#include <arcmancer/spacetimes/minkowski.h>
#include <arcmancer/spacetimes/kerr.h>
#include <arcmancer/interpolation/barycentric.h>

using namespace arcmancer::interpolation;


SUITE(barycentric_interpolation) {

    using namespace arcmancer;

    TEST(vector_interpolation) {
        using boost::math::double_constants::pi;
        using SpaceType = two_sphere::TwoSphere;

        SpaceType M;
        using PointType = ManifoldPoint<SpaceType>;

        // use a specific polygon from mathematica to compare to
        auto center = M.create_point(M.spherical, { 0.1821642599672888,0.2635299645635458 });

        aligned_vector<PointType> vertices;
        vertices.push_back(M.create_point(M.spherical, {0.15, 0.15}));
        vertices.push_back(M.create_point(M.spherical, {0.25, 0.175}));
        vertices.push_back(M.create_point(M.spherical, {0.3, 0.225}));
        vertices.push_back(M.create_point(M.spherical, {0.225, 0.45}));
        vertices.push_back(M.create_point(M.spherical, {0.1, 0.4}));
        vertices.push_back(M.create_point(M.spherical, {0.075, 0.2}));

        aligned_vector<TangentVector<SpaceType>> vertex_data  = {
            {vertices[0], center.chart(), {0.9916789082443916,0.9347800758315326}},
            {vertices[1], center.chart(), {0.9706287342800042,0.9871953364232141}},
            {vertices[2], center.chart(), {0.9596627996278301,0.9782577704269217}},
            {vertices[3], center.chart(), {1.0154412119205163,-0.4609727243736283}},
            {vertices[4], center.chart(), {1.0049864434765312,-0.05895977609086722}},
            {vertices[5], center.chart(), {0.9971935916354756,0.9995783879806918}}
        };

        // before interpolating, set a reference tolerance
        M.configuration.curve.propagation.tolerance.absolute = 1e-10;
        M.configuration.curve.propagation.tolerance.relative = 1e-15;

        auto numerical_interpolant = barycentric_interpolation(center, vertex_data,
                BarycentricInterpolationMethod::numerical);

        auto approx_interpolant = barycentric_interpolation(center, vertex_data,
                BarycentricInterpolationMethod::order_2);

        // The numerical interpolant should be quite close to the value obtained from
        // Mathematica. Both in the base manifold position and the
        // value. However, the tolerance also depends on what used on
        // Mathematica side, and the results might agree only around
        // 1e-3.
        // XXX: Investigate this

        const double tol = 1e-3;
        TangentVector<SpaceType> reference {center, center.chart(),
            {0.99546310457339,0.6119938075034511}};

        for (size_t i = 0; i < 2; i++) {
            CHECK_CLOSE(reference.point().internal_coordinates()(i),
                    numerical_interpolant.point().coordinates(reference.point().chart())(i),
                    tol);

            CHECK_CLOSE(reference.internal_components()(i),
                    numerical_interpolant.components(reference.chart())(i), 
                    tol);
        }

        // The 2nd order approximation should be somewhat close,
        // depending on the size of the interpolation region.
        // From mathematica we have the error in parallel propagation is
        // ~ 0.143007 * size^2. RNC should scale similarly, but don't know the effect
        // Would like within 0.1 here
        const double approx_tol = 0.1;

        for (size_t i = 0; i < 2; i++) {
            CHECK_CLOSE(reference.point().internal_coordinates()(i),
                    approx_interpolant.point().coordinates(reference.point().chart())(i),
                    approx_tol);

            CHECK_CLOSE(reference.internal_components()(i),
                    approx_interpolant.components(reference.chart())(i),
                    approx_tol);
        }
    }

    // test that constant scalar data is handled correctly
    TEST(constant_scalar_interpolation) {
        using boost::math::double_constants::pi;
        using SpaceType = two_sphere::TwoSphere;

        SpaceType M;
        using PointType = ManifoldPoint<SpaceType>;

        // use a specific polygon from mathematica to compare to
        auto center = M.create_point(M.spherical, { 0.1821642599672888,0.2635299645635458 });

        aligned_vector<PointType> vertices;
        vertices.push_back(M.create_point(M.spherical, {0.15, 0.15}));
        vertices.push_back(M.create_point(M.spherical, {0.25, 0.175}));
        vertices.push_back(M.create_point(M.spherical, {0.3, 0.225}));
        vertices.push_back(M.create_point(M.spherical, {0.225, 0.45}));
        vertices.push_back(M.create_point(M.spherical, {0.1, 0.4}));
        vertices.push_back(M.create_point(M.spherical, {0.075, 0.2}));

        aligned_vector<Tensor<SpaceType>> vertex_data  = {
            {vertices[0], center.chart(), 12},
            {vertices[1], center.chart(), 12},
            {vertices[2], center.chart(), 12},
            {vertices[3], center.chart(), 12},
            {vertices[4], center.chart(), 12},
            {vertices[5], center.chart(), 12}
        };

        // before interpolating, set a reference tolerance
        M.configuration.curve.propagation.tolerance.absolute = 1e-10;
        M.configuration.curve.propagation.tolerance.relative = 1e-15;

        auto numerical_interpolant = barycentric_interpolation(center, vertex_data,
                BarycentricInterpolationMethod::numerical);

        auto approx_interpolant = barycentric_interpolation(center, vertex_data,
                BarycentricInterpolationMethod::order_2);

        const double tol = 1e-10;
        Tensor<SpaceType> reference {center, center.chart(), 12};

        for (size_t i = 0; i < 2; i++) {
            CHECK_CLOSE(reference.point().internal_coordinates()(i),
                    numerical_interpolant.point().coordinates(reference.point().chart())(i),
                    tol);
        }

        CHECK_CLOSE(reference.internal_components()(0),
                    numerical_interpolant.components(reference.chart())(0),
                    tol);

        const double approx_tol = tol;

        for (size_t i = 0; i < 2; i++) {
            CHECK_CLOSE(reference.point().internal_coordinates()(i),
                    approx_interpolant.point().coordinates(reference.point().chart())(i),
                    approx_tol);

        }
        CHECK_CLOSE(reference.internal_components()(0),
                approx_interpolant.components(reference.chart())(0),
                approx_tol);
    }

    // test constrained interpolation
#if 1
    TEST(velocity_interpolation_mink) {
        using SpaceType = minkowski::MinkowskiSpacetime;

        SpaceType M;

        auto center = make_point(M.cartesian, {0, 0.5, 0.5, 0});

        aligned_vector<ManifoldPoint<SpaceType>> vertices = {
            make_point(M.cartesian, {0, 0, 0, 0}),
            make_point(M.cartesian, {0, 1, 0, 0}),
            make_point(M.cartesian, {0, 1, 1, 0}),
            make_point(M.cartesian, {0, 0, 1, 0}),
        };

        aligned_vector<Tensor<SpaceType, Cnt>> vertex_data  = {
            {vertices[0], center.chart(), {std::sqrt(2), 1, 0, 0}},
            {vertices[1], center.chart(), {std::sqrt(2), 1, 0, 0}},
            {vertices[2], center.chart(), {std::sqrt(5), 2, 0, 0}},
            {vertices[3], center.chart(), {std::sqrt(5), 2, 0, 0}}
        };

        auto interpolant = barycentric_interpolation_velocity(
                M.cartesian, center, vertex_data.begin(), vertex_data.end(),
                BarycentricInterpolationMethod::numerical
                );

        EigenRk1Tensor<4> icomps = interpolant.components(M.cartesian);
        EigenRk1Tensor<4> ref;
        ref.setValues({1.75532, 1.44262, 0, 0});

        CHECK_ARRAY_CLOSE(ref.data(), icomps.data(), 4, 1e-5);
    }
#endif

#if 1
    TEST(velocity_interpolation_kerr) {
        using SpaceType = kerr::KerrSpacetime;

        SpaceType M(1.0, 0.9375);
        M.use_fixed_chart(M.kerr_schild_out);

        auto center = make_point(M.kerr_schild_out, {0, 10.5, 10.5, 5});

        aligned_vector<ManifoldPoint<SpaceType>> vertices = {
            make_point(M.kerr_schild_out, {0, 10, 10, 5}),
            make_point(M.kerr_schild_out, {0, 11, 10, 5}),
            make_point(M.kerr_schild_out, {0, 11, 11, 5}),
            make_point(M.kerr_schild_out, {0, 10, 11, 5}),
        };

        aligned_vector<TangentVector<SpaceType>> vertex_data  = {
            normalized(TangentVector<SpaceType>{vertices[0], center.chart(), {std::sqrt(2), 1, 0, 0}}),
            normalized(TangentVector<SpaceType>{vertices[1], center.chart(), {std::sqrt(2), 1, 0, 0}}),
            normalized(TangentVector<SpaceType>{vertices[2], center.chart(), {std::sqrt(5), 2, 0, 0}}),
            normalized(TangentVector<SpaceType>{vertices[3], center.chart(), {std::sqrt(5), 2, 0, 0}})
        };

        auto interpolant = barycentric_interpolation_velocity(
                M.kerr_schild_out, center, vertex_data.begin(), vertex_data.end(),
                BarycentricInterpolationMethod::numerical
                );

        EigenRk1Tensor<4> icomps = interpolant.components(M.kerr_schild_out);
        EigenRk1Tensor<4> ref;
        // TODO: reference values currently from implementation itself
        ref.setValues({ 1.84899, 1.52212, -0.00113844, -0.000515302 });

        CHECK_ARRAY_CLOSE(ref.data(), icomps.data(), 4, 1e-5);
    }
#endif

} // SUITE
