#include "test.h" // Brings in the UnitTest++ framework

#include <iostream>
#include <random>
#include <functional>
#include <limits>

#include <arcmancer/general_utils.h>
#include <arcmancer/geometry.h>
#include <arcmancer/other_spaces/euclidean_space.h>

using namespace arcmancer;
using namespace arcmancer::euclidean;

double eval_root(double a, double b, double c, double root) {
    return c + root * (b + root * a);
}

SUITE(utils) {

    TEST(make_point) {
        EuclideanSpace<3> M;

        // test initializer_list, lvalue and rvalue arguments for make_point
        EigenRk1Tensor<3> coords;
        coords.setValues({1, 2, 3});
        auto point_lv = make_point(M.cartesian, coords);
        auto point_rv = make_point(M.cartesian, [](){ EigenRk1Tensor<3> c; c.setValues({1,2,3}); return c; }() );
        auto point_initlist = make_point(M.cartesian, {1,2,3});

        CHECK_ARRAY_EQUAL(point_lv.coordinates(M.cartesian), point_rv.coordinates(M.cartesian), 3);
        CHECK_ARRAY_EQUAL(point_lv.coordinates(M.cartesian), point_initlist.coordinates(M.cartesian), 3);
    }

    TEST(solve_quadratic) {
        std::default_random_engine gen;
        std::uniform_real_distribution<double> dist(-10, 10);
        auto rng = std::bind(dist, gen);
        //const double eps = std::numeric_limits<double>::epsilon();

        for (size_t i = 0; i < 50; i++) {
            double a = rng(), b = rng(), c = rng();
            if (b*b < 4*a*c) {
                c = b * b / (8 * a);
            }
            const auto roots = solve_quadratic(a, b, c);
            const double r1 = eval_root(a, b, c, roots[0]);
            const double r2 = eval_root(a, b, c, roots[1]);

            CHECK_CLOSE(0, r1, 1e-10);
            CHECK_CLOSE(0, r2, 1e-10);

#if 0
            std::cout
                << "(a,b,c): " << a << ", " << b << ", " << c << "\n"
                << "roots: " << roots[0] << ", " << roots[1] << "\n"
                << "(r1,r2): " << r1 << ", " << r2 << "\n";
#endif
        }

    }
}
