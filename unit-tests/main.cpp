/*
 * This file includes the magic necessary in order to get your unit tests
 * that you create with UnitTest++ to automatically run. There should
 * never be a reason to modify this file. For an example unit test,
 * see "sanity_check.cpp". For a reference on creating tests, see "test.h".
 *
 */

#include <cstdio>
#include <iostream>
#include <iomanip>

#include "test.h"

#include <UnitTest++/TestReporterStdout.h>
#include <UnitTest++/TestDetails.h>

#include <arcmancer/log.h>

using namespace UnitTest;


class UNITTEST_LINKAGE TimedTestReporter : public TestReporter 
{
    private:
        virtual void ReportFailure(TestDetails const& details, char const* failure) override {
            using namespace std;
            char const* const errorFormat = "%s(%d): error: Failure in %s: %s\n";
            fprintf(stderr, errorFormat, details.filename, details.lineNumber, details.testName, failure);
        }

        virtual void ReportTestStart(TestDetails const& details) override {
            std::cout << "Test: " << std::left << std::setw(40) << details.testName
                << " Suite: " << std::left << std::setw(30) << details.suiteName
                //<< " in file " << details.filename
                << "... " << std::flush;
        }

        virtual void ReportTestFinish(TestDetails const& /*details*/, float runTime) override {
            std::cout << "OK [took " << runTime << " seconds]" << std::endl;
        }

        virtual void ReportSummary(int const totalTestCount, int const failedTestCount,
                int const failureCount, float const secondsElapsed) override {
            using namespace std;

            if (failureCount > 0)
                printf("FAILURE: %d out of %d tests failed (%d failures).\n", failedTestCount, totalTestCount, failureCount);
            else
                printf("Success: %d tests passed.\n", totalTestCount);

            printf("Test time: %.2f seconds.\n", secondsElapsed);
        }
};

int main(int /*argc*/, char** /*argv[]*/)
{
    // Disable console logging during testing to avoid clutter from
    // log messages.
    using namespace arcmancer;
    Log::set_level(Log::level::off);

    // The standard test runner doesn't report what tests are running
    // and when
    //return RunAllTests();


    // XXX: UnitTest++ documentation promises that this overload exists,
    // but it doesn't
    //return RunAllTests(reporter, Test::GetTestList(), NULL, 0);

    TimedTestReporter reporter;
    TestRunner runner(reporter);
    return runner.RunTestsIf(Test::GetTestList(), NULL, True(), 0);
}
