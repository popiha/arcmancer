
#include "test.h" // Brings in the UnitTest++ framework

#include "arcmancer/geometry.h"
#include "arcmancer/spacetimes/minkowski.h"
using namespace arcmancer;
using namespace arcmancer::minkowski;

class XYPlane : public Surface<MinkowskiSpacetime> {
    double z_;

public:
    XYPlane(const MinkowskiSpacetime &M, double z)
    : Surface<MinkowskiSpacetime>(M), z_(z) {}

    double value(const PointType &x) const override {
        return x.coordinates(M().cartesian)(3) - z_;
    }

    CotangentVector<MinkowskiSpacetime> gradient(
        const PointType &x) const override {
        return {x, M().cartesian, {0, 0, 0, 1}};
    }

    TangentVector<MinkowskiSpacetime> observer(
        const PointType &x) const override {
        return {x, M().cartesian, {1, 0, 0, 0}};
    }
};

SUITE(surfaces) {

TEST(multi_surface_intersection) {

    MinkowskiSpacetime M;
    M.configuration.curve.propagation.minimum_stepsize = .5;
    M.configuration.curve.propagation.enforce_minimum_stepsize = true;

    auto x0 = M.create_point(M.cartesian, {0, 0, 0, 0});

    const double offset = 1e-10;

    // all these surfaces are crossed within a single step, but S1 is the one
    // that should be hit
    XYPlane S0(M, 1 + offset);
    XYPlane S1(M, 1);
    XYPlane S2(M, 1 + 2 * offset);

    Geodesic<MinkowskiSpacetime> geo({x0, M.cartesian, {1, 0, 0, 1}});

    geo.compute(10, {&S0, &S1, &S2});

    CHECK(geo.back_termination().hit_surface);

    CHECK_EQUAL(1, geo.back_termination().surface_index);
}

} // SUITE(surfaces)
