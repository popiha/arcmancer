#include "test.h" // Brings in the UnitTest++ framework

#include <arcmancer/geometry.h>
#include <arcmancer/spacetimes/kerr.h>

using namespace arcmancer;
using namespace arcmancer::kerr;
using KS = KerrSpacetime;

SUITE(kerr) {

// Mostly tests for consistency of the different charts and transitions,
// should maybe test also actual values of the functions at some point.
// 
// Setup a spacetime and some points
class KerrFixture {
public:
    KS M;
    std::vector<const Chart<KS>*> chart_pointers;
    std::vector<ManifoldPoint<KS>, aligned_allocator<ManifoldPoint<KS>>> points;
    double der_tol = 1e-6;
    double converted_tol = 1e-12;
    DifferenceOrder der_order = DifferenceOrder::order_6;

    KerrFixture()
    : M(1, .99)
    {
        chart_pointers = {&M.boyer_lindquist, &M.kerr_schild_in,
                          &M.kerr_schild_out};
        // Note that the points should be fairly near the hole
        // so that the metric components aren't too large compared to
        // the tolerances
        points = {{M.boyer_lindquist, {1, 5, 1, 2}},
                  {M.kerr_schild_out, {-100, -1, 2, .1}},
                  {M.kerr_schild_in, {1e4, 1, 2.5, -5}},
                  {M.boyer_lindquist, {1, 10, 0.1, 0}},
                  {M.boyer_lindquist, {1, 2, 1.5, 3}}
                  };
    }

    // check that the metric derivatives match the numeric ones
    void derivatives_num_test(const Chart<KS> &c) {

        for (auto &x : points) {

            auto der = M.metric_derivatives(x, c);
            auto der_num = M.metric_derivatives_num(x, c, der_order);

            CHECK_ARRAY_CLOSE(der_num.data(), der.data(), 4*4*4, der_tol);
        }
    }

    void converted_metric_components_test(const ManifoldPoint<KS> &x,
                                          const Chart<KS> &c) {

        Tensor<KS, Cov, Cov> g(x, c, M.metric_components(x, c));

        for (auto c2 : chart_pointers) {
            auto comp_actual = M.metric_components(x, *c2);
            auto g_comp = g.components(*c2);
            CHECK_ARRAY_CLOSE(comp_actual.data(), g_comp.data(), 16,
                              converted_tol);
        }
    }

};

TEST_FIXTURE(KerrFixture, BL_derivatives_num) {
    derivatives_num_test(M.boyer_lindquist); 
}

TEST_FIXTURE(KerrFixture, KSI_derivatives_num) {
    derivatives_num_test(M.kerr_schild_in); 
}

TEST_FIXTURE(KerrFixture, KSO_derivatives_num) {
    derivatives_num_test(M.kerr_schild_out); 
}

// check that the metrics match each other when converted between coord systems
// This also tests that point coordinates are converted correctly, at least
// as far as the metric depends on them.
TEST_FIXTURE(KerrFixture, converted_metric_components) {
    for (auto c : chart_pointers) {
        for (auto &x : points) {
            converted_metric_components_test(x, *c);
        }
    }
}

// Check that geodesic computations give the same result in all charts
TEST_FIXTURE(KerrFixture, geodesic_in_different_charts) {
    // Use extra tight tolerances
    M.configuration.curve.propagation.tolerance = {1e-14, 1e-14};
    M.configuration.curve.propagation.maximum_steps = 1e5;

    const double tol = 1e-8;

    TangentVector<KerrSpacetime> u{{M.boyer_lindquist, {0, 10, 1, 0}},
                                   M.boyer_lindquist,
                                   {1, 0, 0.01, 0.03}};
    const double u2 = dot_product(u,u);

    std::vector<ParametrizedPoint<KerrSpacetime>,
                aligned_allocator<ParametrizedPoint<KerrSpacetime>>> end_points;
    for (auto c: chart_pointers){
        M.use_fixed_chart(*c);

        Geodesic<KerrSpacetime> geo(u);

        auto res = geo.compute(1000);
        // The tight tolerance can cause overstepping also
        CHECK(res == decltype(res)::success || res == decltype(res)::overstep);

        end_points.push_back(geo.back());

        // check conservation of Hamiltonian
        CHECK_CLOSE(u2,
                    dot_product(geo.back().tangent(), geo.back().tangent()),
                    tol);
    }

    const auto ref_coords = end_points[0].components(M.kerr_schild_in);
    for (const auto &p : end_points) {

        CHECK_ARRAY_CLOSE(ref_coords, p.components(M.kerr_schild_in),
                          ParametrizedPoint<KerrSpacetime>::component_count,
                          tol);
    }
}


// The same for the extremal case
// XXX: just copy pasting the above, maybe there is some cleaner way?

class ExtremalKerrFixture {
public:
    KS M;
    std::vector<const Chart<KS>*> chart_pointers;
    std::vector<ManifoldPoint<KS>, aligned_allocator<ManifoldPoint<KS>>> points;
    double der_tol = 1e-6;
    double converted_tol = 1e-12;
    DifferenceOrder der_order = DifferenceOrder::order_6;

    ExtremalKerrFixture()
    : M(1, 1)
    {
        chart_pointers = {&M.boyer_lindquist, &M.kerr_schild_in,
                          &M.kerr_schild_out};
        points = {{M.boyer_lindquist, {1, 5, 1, 2}},
                  {M.kerr_schild_out, {-100, -1, 2, .1}},
                  {M.kerr_schild_in, {1e4, 1, 2.5, -5}},
                  {M.boyer_lindquist, {1, 10, 0.1, 0}},
                  {M.boyer_lindquist, {1, .5, 0.1, .1}},
                  {M.boyer_lindquist, {1, 2, 1.5, 3}}
                  };
    }

    // check that the metric derivatives match the numeric ones
    void derivatives_num_test(const Chart<KS> &c) {

        for (auto &x : points) {

            auto der = M.metric_derivatives(x, c);
            auto der_num = M.metric_derivatives_num(x, c, der_order);

            CHECK_ARRAY_CLOSE(der_num.data(), der.data(), 4*4*4, der_tol);
        }
    }

    void converted_metric_components_test(const ManifoldPoint<KS> &x,
                                          const Chart<KS> &c) {

        Tensor<KS, Cov, Cov> g(x, c, M.metric_components(x, c));

        for (auto c2 : chart_pointers) {
            auto comp_actual = M.metric_components(x, *c2);
            auto g_comp = g.components(*c2);
            CHECK_ARRAY_CLOSE(comp_actual.data(), g_comp.data(), 16,
                              converted_tol);
        }
    }

};


// check that the metrics match each other when converted between coord systems
// This also tests that point coordinates are converted correctly, at least
// as far as the metric depends on them.
TEST_FIXTURE(ExtremalKerrFixture, extremal_converted_metric_components) {
    for (auto c : chart_pointers) {
        for (auto &x : points) {
            converted_metric_components_test(x, *c);
        }
    }
}

// Check that geodesic computations give the same result in all charts
TEST_FIXTURE(ExtremalKerrFixture, extremal_geodesic_in_different_charts) {
    // Use extra tight tolerances
    M.configuration.curve.propagation.tolerance = {1e-14, 1e-14};
    M.configuration.curve.propagation.maximum_steps = 1e5;

    const double tol = 1e-8;

    TangentVector<KerrSpacetime> u{{M.boyer_lindquist, {0, 10, 1, 0}},
                                   M.boyer_lindquist,
                                   {1, 0, 0.01, 0.03}};
    const double u2 = dot_product(u,u);

    std::vector<ParametrizedPoint<KerrSpacetime>,
                aligned_allocator<ParametrizedPoint<KerrSpacetime>>> end_points;
    for (auto c: chart_pointers){
        M.use_fixed_chart(*c);

        Geodesic<KerrSpacetime> geo(u);

        auto res = geo.compute(1000);
        // The tight tolerance can cause overstepping also
        CHECK(res == decltype(res)::success || res == decltype(res)::overstep);

        end_points.push_back(geo.back());

        // check conservation of Hamiltonian
        CHECK_CLOSE(u2,
                    dot_product(geo.back().tangent(), geo.back().tangent()),
                    tol);
    }

    const auto ref_coords = end_points[0].components(M.kerr_schild_in);
    for (const auto &p : end_points) {

        CHECK_ARRAY_CLOSE(ref_coords, p.components(M.kerr_schild_in),
                          ParametrizedPoint<KerrSpacetime>::component_count,
                          tol);
    }
}

} // SUITE(kerr)
