#include "test.h" // Brings in the UnitTest++ framework

#include <array>
#include <iostream>
#include <vector>
#include <cmath>
#include <functional>

#include <H5Cpp.h>

#include <Eigen/Dense>
#include <unsupported/Eigen/CXX11/Tensor>

#include <arcmancer/interpolation/griddata.h>

SUITE(griddata) {
    using namespace arcmancer;
    using arcmancer::interpolation::GridData;
    using std::cos;
    using std::pow;

    // For testing custom data types
    class Foo {
        public:
        Eigen::Vector3d v;
        double s;

        // Should work without specific initialization
        Foo() = default;
        // also need initialization list compatible constructore
        Foo(Eigen::Vector3d vec, double scalar) : v(vec), s(scalar) {}

        // following algebraic operations needed:
        // - multiplication by scalar
        // - addition
        friend Foo operator*(Foo foo, double d) {
            return Foo { d * foo.v, d * foo.s };
        }

        Foo& operator+=(Foo other) {
            v += other.v;
            s += other.s;
            return *this;
        }
    };

    class GridFixture {

    public:
        Eigen::Tensor<double, 3> scalar_data;
        Eigen::Tensor<double, 3> linear_scalar_data;

        Eigen::Tensor<Eigen::Vector3d, 3> vector_data;
        Eigen::Tensor<Eigen::Vector3d, 3> linear_vector_data;

        Eigen::Tensor<Foo, 3> foo_data;

        // interpolate over 3d-array
        std::array<std::vector<double>, 3> grid = {{
            {-1.0, 1.0, 2.0, 4.0},
            {-1.0, 1.0, 2.0, 4.0},
            {-1.0, 1.0, 3.0, 4.0},
        }};

        std::function<double(double,double,double)> scalar_func;
        std::function<double(double,double,double)> linear_scalar_func;
        std::function<Eigen::Vector3d(double,double,double)> vector_func;
        std::function<Eigen::Vector3d(double,double,double)> linear_vector_func;
        std::function<Foo(double,double,double)> foo_func;

        GridFixture() {

            scalar_func = [](double x, double y, double z)->double {
                return cos( pow(x/5, 2) + pow(y/5,2) + pow(z/5,2) );
            };

            linear_scalar_func = [](double x, double y, double z) -> double {
                return 10 * x - 5 * y + z;
            };

            vector_func = [](double x, double y, double z)->Eigen::Vector3d {
                double r = std::sqrt(x*x + y*y + z*z);
                return Eigen::Vector3d( -y/(5*r), x/(5*r), sin(r/10.0) );
            };

            linear_vector_func = [](double x, double y,
                                    double z) -> Eigen::Vector3d {
                return Eigen::Vector3d( -y, 2*x + z, x - y + z );
            };

            foo_func = [&](double x, double y, double z)->Foo {
                return { vector_func(x, y, z), scalar_func(x, y, z) };
            };

            scalar_data = Eigen::Tensor<double, 3>(4, 4, 4);
            linear_scalar_data = Eigen::Tensor<double, 3>(4, 4, 4);
            vector_data = Eigen::Tensor<Eigen::Vector3d, 3>(4, 4, 4);
            linear_vector_data = Eigen::Tensor<Eigen::Vector3d, 3>(4, 4, 4);
            foo_data = Eigen::Tensor<Foo, 3>(4, 4, 4);

            for (int i = 0; i < 4; i++) {
                for (int j = 0; j < 4; j++) {
                    for (int k = 0; k < 4; k++) {
                        double x = grid[0][i];
                        double y = grid[1][j];
                        double z = grid[2][k];

                        scalar_data(i, j, k) = scalar_func(x,y,z);
                        linear_scalar_data(i, j, k) = linear_scalar_func(x,y,z);
                        vector_data(i, j, k) = vector_func(x,y,z);
                        linear_vector_data(i, j, k) = linear_vector_func(x,y,z);
                        foo_data(i, j, k) = Foo {
                            vector_data(i,j,k),
                            scalar_data(i,j,k)
                        };
                    }
                }
            }
        }
    };

TEST_FIXTURE(GridFixture, initialization) {
    GridData<3, double> scalar_gd(scalar_data, grid);

    GridData<3, Eigen::Vector3d> vector_gd(vector_data, grid);
}

TEST_FIXTURE(GridFixture, scalar_interpolation) {

    GridData<3, double> scalar_gd(scalar_data, grid);

    // fuzz the interpolator with random inputs
    std::vector<double> ip(10), r(10);
    for (int i = 0; i < 10; i++) {
        Eigen::Vector3d pt = (4-(-1)) * 0.5*(Eigen::Vector3d::Random() + Eigen::Vector3d::Ones()) + (-1)*Eigen::Vector3d::Ones();
        double ref = scalar_func(pt(0), pt(1), pt(2));
        double interpolant = scalar_gd.interpolate(pt).first;
        ip[i] = interpolant;
        r[i] = ref;
    }
    CHECK_ARRAY_CLOSE(ip, r, 10, 0.5);
}

TEST_FIXTURE(GridFixture, linear_scalar_interpolation) {

    GridData<3, double> scalar_gd(linear_scalar_data, grid);
    // the interpolation should be exact for linear functions, modulo rounding
    const double tol = 1e-14;

    // fuzz the interpolator with random inputs
    std::vector<double> ip(10), r(10);
    for (int i = 0; i < 10; i++) {
        Eigen::Vector3d pt = (4-(-1)) * 0.5*(Eigen::Vector3d::Random() + Eigen::Vector3d::Ones()) + (-1)*Eigen::Vector3d::Ones();
        double ref = linear_scalar_func(pt(0), pt(1), pt(2));
        double interpolant = scalar_gd.interpolate(pt).first;
        ip[i] = interpolant;
        r[i] = ref;
    }
    CHECK_ARRAY_CLOSE(ip, r, 10, tol);
}

TEST_FIXTURE(GridFixture, vector_interpolation) {

    GridData<3, Eigen::Vector3d> vector_gd(vector_data, grid);

    // fuzz the interpolator with random inputs
    std::vector<Eigen::Vector3d> ip(10), r(10);
    for (int i = 0; i < 10; i++) {
        Eigen::Vector3d pt = (4-(-1)) * 0.5*(Eigen::Vector3d::Random() + Eigen::Vector3d::Ones()) + (-1)*Eigen::Vector3d::Ones();
        auto ref = vector_func(pt(0), pt(1), pt(2));
        auto interpolant = vector_gd.interpolate(pt).first;
        ip[i] = interpolant;
        r[i] = ref;

        for (int j = 0; j < 3; j++) {
            CHECK_CLOSE(interpolant[j], ref[j], 0.5);
        }
    }
}

TEST_FIXTURE(GridFixture, linear_vector_interpolation) {

    GridData<3, Eigen::Vector3d> vector_gd(linear_vector_data, grid);
    // the interpolation should be exact for linear functions, modulo rounding
    const double tol = 1e-14;
    // fuzz the interpolator with random inputs
    std::vector<Eigen::Vector3d> ip(10), r(10);
    for (int i = 0; i < 10; i++) {
        Eigen::Vector3d pt = (4-(-1)) * 0.5*(Eigen::Vector3d::Random() + Eigen::Vector3d::Ones()) + (-1)*Eigen::Vector3d::Ones();
        auto ref = linear_vector_func(pt(0), pt(1), pt(2));
        auto interpolant = vector_gd.interpolate(pt).first;
        ip[i] = interpolant;
        r[i] = ref;

        for (int j = 0; j < 3; j++) {
            CHECK_CLOSE(interpolant[j], ref[j], tol);
        }
    }
}

TEST_FIXTURE(GridFixture, custom_interpolation) {
    GridData<3, Foo> foo_gd(foo_data, grid);

    // fuzz the interpolator with random inputs
    std::vector<Foo> ip(10), r(10);
    for (int i = 0; i < 10; i++) {
        Eigen::Vector3d pt = (4-(-1)) * 0.5*(Eigen::Vector3d::Random() + Eigen::Vector3d::Ones()) + (-1)*Eigen::Vector3d::Ones();
        auto ref = foo_func(pt(0), pt(1), pt(2));
        auto interpolant = foo_gd.interpolate(pt).first;
        ip[i] = interpolant;
        r[i] = ref;

        for (int j = 0; j < 3; j++) {
            CHECK_CLOSE(interpolant.v[j], ref.v[j], 0.5);
        }
        CHECK_CLOSE(interpolant.s, ref.s, 0.5);
    }

}

TEST_FIXTURE(GridFixture, scalar_boundaries) {
    // Check that only data from the data array is read, i.e. no out of bounds
    // reads.
    // Use a constant array with a flag value
    const double flag_val = 12345678910.11;
    const double tol = 1e-3;
    scalar_data.setConstant(flag_val);
    GridData<3, double> scalar_gd(scalar_data, grid);
    
    for (int i = -1; i<5; i+=5){
        for (int j = -1; j < 5; j += 5) {
            for (int k = -1; k < 5; k += 5) {

                CHECK_CLOSE(
                    flag_val,
                    scalar_gd.interpolate({i + 1e-2, j + 1e-2, k + 1e-2}).first, tol);
                CHECK_CLOSE(
                    flag_val,
                    scalar_gd.interpolate({i - 1e-2, j - 1e-2, k - 1e-2}).first, tol);
            }
        }
    }
}

TEST_FIXTURE(GridFixture, single_cell_dim) {
    // Check that dimensions with only a single data point work correctly
    Eigen::Tensor<double, 3> data(grid[0].size(), 1, grid[2].size());
    data.chip(0,1) = scalar_data.chip(0,1);
    grid[1] = {grid[1][0]};
    GridData<3, double> gd(data, grid);
    // Fuzzing

    std::vector<double> ip(10), r(10);
    for (int i = 0; i < 10; i++) {
        Eigen::Vector3d pt = (4-(-1)) * 0.5*(Eigen::Vector3d::Random() + Eigen::Vector3d::Ones()) + (-1)*Eigen::Vector3d::Ones();
        pt(1) = grid[1][0];
        double ref = scalar_func(pt(0), pt(1), pt(2));
        double interpolant = gd.interpolate(pt).first;
        ip[i] = interpolant;
        r[i] = ref;
    }
    CHECK_ARRAY_CLOSE(ip, r, 10, 0.5);
    
    // Memory bounds
    const double flag_val = 12345678910.11;
    const double tol = 1e-3;
    data.setConstant(flag_val);
    gd = {data, grid};

    for (int i = -1; i < 5; i += 5) {
        for (int k = -1; k < 5; k += 5) {

            CHECK_CLOSE(
                flag_val,
                gd.interpolate({i + 1e-2, grid[1][0] + 1, k + 1e-2}).first,
                tol);

            CHECK_CLOSE(
                flag_val,
                gd.interpolate({i + 1e-2, grid[1][0], k + 1e-2}).first,
                tol);

            CHECK_CLOSE(
                flag_val,
                gd.interpolate({i - 1e-2, grid[1][0], k - 1e-2}).first,
                tol);

            CHECK_CLOSE(
                flag_val,
                gd.interpolate({i - 1e-2, grid[1][0] - 1, k - 1e-2}).first,
                tol);
        }
    }
}


} // SUITE
