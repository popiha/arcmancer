#include "test.h" // Brings in the UnitTest++ framework

#include <iostream>
#include <vector>

#include <H5Cpp.h>
#include <Eigen/Dense>

#include <arcmancer/geometry.h>
#include <arcmancer/spacetimes/minkowski.h>
#include <arcmancer/image_plane.h>
#include <arcmancer/hdf5_utils.h>

SUITE(imaging) {

using namespace arcmancer;
using namespace arcmancer::minkowski;

TEST(parallel_plane){
    MinkowskiSpacetime M;
    auto x0 = M.create_point(M.cartesian, {0,0,0,0});
    using IP = ImagePlane<MinkowskiSpacetime, double> ;
    IP ip({10,10,10,10}, IP::Projection::ParallelPlane,
          {x0, M.cartesian, {1,0,0,0}},
          {x0, M.cartesian, {0,0,0,1}},
          {x0, M.cartesian, {0,1,0,0}}
          );

    ArcVector<4> e_t = {1,0,0,0};
    ArcVector<4> e_x = {0,1,0,0};
    ArcVector<4> e_y = {0,0,1,0};
    ArcVector<4> e_z = {0,0,0,1};
    double tol = 1e-10;

    //check that the rays are where they're expected to be and that the directions are parallel transported correctly
    for (auto &row : ip.points()) {
        for (auto &p : row) {
            CHECK_CLOSE(p.x, p.ray_tangent.point().coordinates(M.cartesian)(1),
                        tol);
            CHECK_CLOSE(p.y, p.ray_tangent.point().coordinates(M.cartesian)(2),
                        tol);
            CHECK_CLOSE(0, p.ray_tangent.point().coordinates(M.cartesian)(3),
                        tol);
            CHECK_CLOSE(0, p.ray_tangent.point().coordinates(M.cartesian)(0),
                        tol);

            CHECK_ARRAY_CLOSE(e_t, p.frame.e_t().components(M.cartesian), 4,
                              tol);
            CHECK_ARRAY_CLOSE(e_x, p.frame.e_x().components(M.cartesian), 4,
                              tol);
            CHECK_ARRAY_CLOSE(e_y, p.frame.e_y().components(M.cartesian), 4,
                              tol);
            CHECK_ARRAY_CLOSE(e_z, p.frame.e_z().components(M.cartesian), 4,
                              tol);
        }
    }
}

// Confirm that ImagePlane can be output to HDF5
TEST(hdf5_roundtrip) {
    const std::string filename = "test_data.h5";

    MinkowskiSpacetime M;
    auto x0 = M.create_point(M.cartesian, {10,10,10,10});
    using IP = ImagePlane<MinkowskiSpacetime, double> ;
    IP ip(IP::PlaneShape{5,5,10,10}, IP::Projection::ParallelPlane,
          {x0, M.cartesian, {1,0,0,0}},
          {x0, M.cartesian, {0,0,0,1}},
          {x0, M.cartesian, {0,1,0,0}}
          );

    ip.compute(
            [](const IP::ImagePoint &p) -> double {
                Geodesic<MinkowskiSpacetime> ray(p.ray_tangent);
                ray.compute(-10);

                return dot_product(ray.front().tangent(), ray.front().tangent());
            }
            );

    ip.save_data(filename, "Test data.");

    H5::H5File file(filename, H5F_ACC_RDONLY);

    Eigen::MatrixXd data_in;
    EigenHDF5::load(file, "/data", data_in);

    for (auto &row : ip.data_points()) {
        for (const auto &p : row) {
            CHECK_EQUAL(p.data, data_in(p.ix, p.iy));
        }
    }

    double value;

    EigenHDF5::load_scalar_attribute(file , "xbins"      , value); CHECK_EQUAL(ip.shape().xbins      , value);
    EigenHDF5::load_scalar_attribute(file , "ybins"      , value); CHECK_EQUAL(ip.shape().ybins      , value);
    EigenHDF5::load_scalar_attribute(file , "xspan"      , value); CHECK_EQUAL(ip.shape().xspan      , value);
    EigenHDF5::load_scalar_attribute(file , "yspan"      , value); CHECK_EQUAL(ip.shape().yspan      , value);
    EigenHDF5::load_scalar_attribute(file , "dx"         , value); CHECK_EQUAL(ip.shape().dx         , value);
    EigenHDF5::load_scalar_attribute(file , "dy"         , value); CHECK_EQUAL(ip.shape().dy         , value);
    EigenHDF5::load_scalar_attribute(file , "pixel_area" , value); CHECK_EQUAL(ip.shape().pixel_area , value);

    int intvalue;
    EigenHDF5::load_scalar_attribute(file, "projection", intvalue); 
    CHECK_EQUAL(static_cast<int>(ip.projection()), intvalue);
}

} // SUITE(imaging)
