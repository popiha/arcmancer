#include "test.h" // Brings in the UnitTest++ framework

#include <iostream>
#include <vector>

#include <Eigen/Dense>
#include <H5Cpp.h>

#include <arcmancer/interpolation/cubic_spline.h>

SUITE(cubic_spline) {

    using namespace arcmancer::interpolation;

    TEST(natural_boundary) {

        Eigen::Matrix<double, 4, 2> data;
        data << 0, 0, 1, 1, 2, 2, 3, 3;

        CubicSpline spline(data);

        double tol = 1e-10;

        CHECK_CLOSE(0, spline.evaluate(0)(1), tol);
        CHECK_CLOSE(0.5, spline.evaluate(0.5)(1), tol);
        CHECK_CLOSE(1.5, spline.evaluate(1.5)(1), tol);
        CHECK_CLOSE(2.2, spline.evaluate(2.2)(1), tol);
        CHECK_CLOSE(3, spline.evaluate(3)(1), tol);

        // Comparison data from scipy implementation
        data << 0, 1, 1, 3, 2, 1, 3, 5;

        CubicSpline spline2(data);

        CHECK_CLOSE(2.4314934016, spline2.evaluate(0.452)(1), tol);
        CHECK_CLOSE(3.0391442532, spline2.evaluate(0.831)(1), tol);
        CHECK_CLOSE(2.1074300000000004, spline2.evaluate(1.41)(1), tol);
        CHECK_CLOSE(2.8227287999999993, spline2.evaluate(2.61)(1), tol);
    }

    TEST(exact_boundary) {

        Eigen::Matrix<double, 4, 2> data;
        data << 0, 0, 1, 1, 2, 2, 3, 3;
        Eigen::Matrix<double, 1, 2> deriv;
        deriv << 1, 1;

        double tol = 1e-10;
        CubicSpline spline(data, deriv, deriv);

        CHECK_CLOSE(0, spline.evaluate(0)(1), tol);
        CHECK_CLOSE(0.5, spline.evaluate(0.5)(1), tol);
        CHECK_CLOSE(1.5, spline.evaluate(1.5)(1), tol);
        CHECK_CLOSE(2.2, spline.evaluate(2.2)(1), tol);
        CHECK_CLOSE(3, spline.evaluate(3)(1), tol);

        data << 0, 1, 1, 3, 2, 1, 3, 5;

        CubicSpline spline2(data, deriv, 2 * deriv);

        CHECK_CLOSE(2.051891025066667, spline2.evaluate(0.452)(1), tol);
        CHECK_CLOSE(2.9339179917999996, spline2.evaluate(0.831)(1), tol);
        CHECK_CLOSE(2.078563266666667, spline2.evaluate(1.41)(1), tol);
        CHECK_CLOSE(3.4642657999999997, spline2.evaluate(2.61)(1), tol);
    }

} // SUITE
