FILE (GLOB_RECURSE test_SRCS *.cpp *.cxx *.cc *.C *.c *.h *.hpp)
SET (test_BIN unit-tests)

# Use OpenMP for unit tests if available
OPTION(UnitTest_Use_OpenMP "Use OpenMP parallelization in unit tests" ON)
IF(UnitTest_Use_OpenMP)
    FIND_PACKAGE(OpenMP)
    
    IF(OPENMP_FOUND)
        message("OpenMP is enabled for unit tests")
    ELSE()
        message("No OpenMP support found, OpenMP is disabled for unit tests")
    ENDIF()
ENDIF()

# testing the library
IF (NOT CMAKE_CROSSCOMPILING)

    ADD_EXECUTABLE(${test_BIN} ${test_SRCS})
    
    target_include_directories(${test_BIN} SYSTEM PRIVATE ${UTPP_INCLUDE_DIRS})

    TARGET_LINK_LIBRARIES(${test_BIN}
        arcmancer
        UnitTest++
        ${OpenMP_CXX_FLAGS}
    )

    target_compile_options(${test_BIN} PRIVATE 
                           ${WARNING_FLAGS} 
                           ${OpenMP_CXX_FLAGS})
    
    ADD_CUSTOM_TARGET(check ALL "${MAINFOLDER}/bin/${test_BIN}" 
                      DEPENDS ${test_BIN} 
                      WORKING_DIRECTORY "${MAINFOLDER}/bin"
                      COMMENT "Executing unit tests..." 
                      VERBATIM SOURCES ${test_SRCS}
    )

ENDIF (NOT CMAKE_CROSSCOMPILING)
