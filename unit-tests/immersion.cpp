#include "test.h" // Brings in the UnitTest++ framework

#include <iostream>

#include <arcmancer/general_utils.h>
#include <arcmancer/geometry.h>
#include <arcmancer/spacetimes/minkowski.h>
#include <arcmancer/spacetimes/kerr.h>
#include <arcmancer/other_spaces/immersion.h>
#include <arcmancer/other_spaces/tensor_space.h>

using namespace arcmancer;
using namespace arcmancer::minkowski;
using namespace arcmancer::kerr;
using namespace arcmancer::immersion;
using namespace arcmancer::tensor_space;

SUITE(immersion) {

    TEST(minkowski_plane_immersion) {
        MinkowskiSpacetime Mamb;

        Immersion<MinkowskiSpacetime, 2, OtherSignature> M(
            Mamb.cartesian,
            [](const EigenRk1Tensor<2> &x) {
                EigenRk1Tensor<4> ret;
                ret.setZero();
                ret.setValues({0, x(0), x(1), 0});
                return ret;
            },
            [](const EigenRk1Tensor<2> &) {
                Eigen::TensorFixedSize<double, Eigen::Sizes<4, 2>> ret;
                ret.setZero();
                // clang-format off
                ret.setValues(
                    {
                        {0, 0},
                        {1, 0},
                        {0, 1},
                        {0, 0}
                    });
                // clang-format on
                return ret;
            },
            [](const EigenRk1Tensor<2> &) {
                Eigen::TensorFixedSize<double, Eigen::Sizes<4, 2, 2>> ret;
                ret.setZero();
                return ret;
            });

        auto test_point = make_point(M.canonical, {2, 3});

        auto g = M.metric_components(test_point, M.canonical);
        auto gder  =  M.metric_derivatives(test_point, M.canonical) ;

        EigenRk2Tensor<2> g_expected;
        g_expected.setValues( {{-1,0,}, {0, -1}} );

        EigenRk3Tensor<2> gder_expected;
        gder_expected.setZero();

        CHECK_ARRAY_EQUAL(g_expected.data(), g.data(), 2*2);
        CHECK_ARRAY_EQUAL(gder_expected.data(), gder.data(), 2*2*2);

#if 0
        std::cout << "M.metric_components at:\n" << test_point << "\nare:\n" << g << "\n";
        std::cout << "M.metric_derivatives at:\n" << test_point << "\nare:\n"<< gder << "\n";
#endif
    }

    TEST(minkowski_parabola_immersion) {
        MinkowskiSpacetime Mamb;

        Immersion<MinkowskiSpacetime, 2, OtherSignature> M(
            Mamb.cartesian,
            [](const EigenRk1Tensor<2> &x) {
                EigenRk1Tensor<4> ret;
                ret.setZero();
                ret.setValues({0, x(0), x(1), 0.5*(x(0)*x(0) + x(1)*x(1)) });
                return ret;
            },
            [](const EigenRk1Tensor<2> &x) {
                Eigen::TensorFixedSize<double, Eigen::Sizes<4, 2>> ret;
                ret.setZero();
                // clang-format off
                ret.setValues(
                    {
                        {0, 0},
                        {1, 0},
                        {0, 1},
                        {x(0), x(1)}
                    });
                // clang-format on
                return ret;
            },
            [](const EigenRk1Tensor<2> &) {
                Eigen::TensorFixedSize<double, Eigen::Sizes<4, 2, 2>> ret;
                ret.setZero();
                ret(3,0,0) = 1.0;
                ret(3,1,1) = 1.0;
                return ret;
            });

        auto test_point = make_point(M.canonical, {2, 3});

        auto g = M.metric_components(test_point, M.canonical);
        auto gder  =  M.metric_derivatives(test_point, M.canonical) ;

        // compare to test values from Mathematica
        EigenRk2Tensor<2> g_expected;
        g_expected.setValues( {{-5,-6,}, {-6, -10}} );

        EigenRk3Tensor<2> gder_expected;
        gder_expected.setValues({ { {-4, 0}, {-3, -2} }, { {-3, -2}, {0, -6} } });

        CHECK_ARRAY_EQUAL(g_expected.data(), g.data(), 2*2);
        CHECK_ARRAY_EQUAL(gder_expected.data(), gder.data(), 2*2*2);

#if 0
        EigenRk3Tensor<2> gder_expected2;
        EigenRk2Tensor<2> xder, yder;
        xder.setValues({ {-4, -3}, {-3, 0} });
        yder.setValues({ {0, -2}, {-2, -6} });
        gder_expected2.chip(0, 2) = xder;
        gder_expected2.chip(1, 2) = yder;

        std::cout
            << "metric:\n" << g << "\n"
            << "g_expected:\n" << g_expected << "\n"
            << "metric derivatives:\n" << gder << "\n"
            << "gder_expected:\n" << gder_expected << "\n"
            << "gder_expected2:\n" << gder_expected2 << "\n";
#endif
    }

    TEST(kerr_parabola_immersion) {
        KerrSpacetime Mamb(1.0, 0.9375);

        Immersion<KerrSpacetime, 2, OtherSignature> M(
            Mamb.kerr_schild_out,
            [](const EigenRk1Tensor<2> &x) {
                EigenRk1Tensor<4> ret;
                ret.setZero();
                ret.setValues({0, x(0), x(1), 0.5*(x(0)*x(0) + x(1)*x(1)) });
                return ret;
            },
            [](const EigenRk1Tensor<2> &x) {
                Eigen::TensorFixedSize<double, Eigen::Sizes<4, 2>> ret;
                ret.setZero();
                // clang-format off
                ret.setValues(
                    {
                        {0, 0},
                        {1, 0},
                        {0, 1},
                        {x(0), x(1)}
                    });
                // clang-format on
                return ret;
            },
            [](const EigenRk1Tensor<2> &) {
                Eigen::TensorFixedSize<double, Eigen::Sizes<4, 2, 2>> ret;
                ret.setValues(
                        {{{0, 0}, {0, 0}}, {{0, 0}, {0, 0}}, {{0, 0}, {0, 0}}, {{1, 0}, {0, 1}}}
                        );
                return ret;
            });

        auto test_point = make_point(M.canonical, {2, 3});

        auto g = M.metric_components(test_point, M.canonical);
        auto gder  =  M.metric_derivatives(test_point, M.canonical) ;

        // compare to test values from Mathematica

        auto test_pt = make_point(Mamb.kerr_schild_out, {0, 2, 3, 6.5});
        EigenRk2Tensor<4> g_amb = Mamb.metric_components(test_pt, Mamb.kerr_schild_out);
        EigenRk2Tensor<4> g_amb_expected;
        g_amb_expected.setValues(
                {{0.7336956376756289940, 0.0572662381999424553, 0.1149168599129192293, 0.2333081795890830328},
                {0.0572662381999424553, -1.01231456371555063408, -0.02471178547554870422, -0.0501707207111742852},
                {0.1149168599129192293, -0.02471178547554870422, -1.0495894418588611981, -0.1006781982704427796},
                {0.2333081795890830328, -0.0501707207111742852, -0.1006781982704427796, -1.2044003567499591738}}
                );

        EigenRk2Tensor<2> g_expected;
        g_expected.setValues(
                {{-6.030598873560084470, -7.602982484649712162}, 
                {-7.602982484649712162, -12.493261842231150440}}
            );

        EigenRk3Tensor<2> gder_expected;
        gder_expected.setValues(
                {{{-4.800256208268965013, 0.402598172246264642}, {-3.40032503170022688, -1.86915606321480733}},
                {{-3.40032503170022688, -1.86915606321480733}, {0.69068602574936396, -6.56695344602566623}}}
                );

        CHECK_ARRAY_CLOSE(g_amb_expected.data(), g_amb.data(), 4*4, 1e-14);
        CHECK_ARRAY_CLOSE(g_expected.data(), g.data(), 2*2, 1e-14);
        CHECK_ARRAY_CLOSE(gder_expected.data(), gder.data(), 2*2*2, 1e-14);

#if 0
        EigenRk3Tensor<2> gder_expected2;
        EigenRk2Tensor<2> xder, yder;
        xder.setValues({{-4.800256208268965013, -3.40032503170022688}, {-3.40032503170022688, 0.69068602574936396}});
        yder.setValues({{0.402598172246264642, -1.86915606321480733}, {-1.86915606321480733, -6.56695344602566623}});
        gder_expected2.chip(0, 2) = xder;
        gder_expected2.chip(1, 2) = yder;

        std::cout
            << "g_amb :\n" << g_amb << "\n"
            << "g_amb_expected:\n" << g_amb_expected << "\n"
            << "g:\n" << g << "\n"
            << "g_expected:\n" << g_expected << "\n"
            << "gder:\n" << gder << "\n"
            << "gder_expected:\n" << gder_expected << "\n"
            << "gder_expected2:\n" << gder_expected2 << "\n";
#endif
    }

    TEST(kerr_mixed_immersion) {
        KerrSpacetime Mamb(1.0, 0.9375);

        Immersion<KerrSpacetime, 2, OtherSignature> M(
            Mamb.kerr_schild_out,
            [](const EigenRk1Tensor<2> &x) {
                EigenRk1Tensor<4> ret;
                ret.setZero();
                ret.setValues({ x(0)*x(1), x(1)*x(1), x(0)*x(0), 0.5*(x(0)*x(0) + x(1)*x(1)) });
                return ret;
            },
            [](const EigenRk1Tensor<2> &x) {
                Eigen::TensorFixedSize<double, Eigen::Sizes<4, 2>> ret;
                ret.setZero();
                // clang-format off
                ret.setValues(
                    {
                        {x(1), x(0)},
                        {0, 2*x(1)},
                        {2*x(0), 0},
                        {x(0), x(1)}
                    });
                // clang-format on
                return ret;
            },
            [](const EigenRk1Tensor<2> &) {
                Eigen::TensorFixedSize<double, Eigen::Sizes<4, 2, 2>> ret;
                ret.setValues({{{0, 1}, {1, 0}}, {{0, 0}, {0, 2}}, {{2, 0}, {0, 0}}, {{1, 0}, {0, 1}}});
                return ret;
            });

        auto test_point = make_point(M.canonical, {2, 3});

        auto g = M.metric_components(test_point, M.canonical);
        auto gder  =  M.metric_derivatives(test_point, M.canonical) ;

        // compare to test values from Mathematica

        auto test_pt = make_point(Mamb.kerr_schild_out, {6, 9, 4, 6.5});
        EigenRk2Tensor<4> g_amb = Mamb.metric_components(test_pt, Mamb.kerr_schild_out);
        EigenRk2Tensor<4> g_amb_expected;
        g_amb_expected.setValues(
                {{0.8304689893270736002, 0.1242102785659329427, 0.0674823495149191711, 0.0935879414364025456},
                {0.1242102785659329427, -1.0910051396507748030, -0.0494422902232502341,  -0.0685690731747887249}, 
                {0.0674823495149191711,  -0.0494422902232502341, -1.02686156047780201686,  -0.03725297306566270127}, 
                {0.0935879414364025456,  -0.0685690731747887249, -0.03725297306566270127,  -1.05166431054434957992}}
                );

        EigenRk2Tensor<2> g_expected;
        g_expected.setValues(
                {{-11.01563718933428076, 0.20863560143230082}, {0.20863560143230082, -43.78367251649191765}}
            );

        EigenRk3Tensor<2> gder_expected;
        gder_expected.setValues(
                {{{-19.74678944758221960, 5.81508733600062749}, {-1.80745739336762394, 1.3189346474431758}}, 
                {{-1.80745739336762394, 1.3189346474431758}, {7.15559682440745668, -32.2777204618206915}}}
                );

        CHECK_ARRAY_CLOSE(g_amb_expected.data(), g_amb.data(), 4*4, 1e-14);
        CHECK_ARRAY_CLOSE(g_expected.data(), g.data(), 2*2, 1e-14);
        CHECK_ARRAY_CLOSE(gder_expected.data(), gder.data(), 2*2*2, 1e-14);

#if 0
        EigenRk3Tensor<2> gder_expected2;
        EigenRk2Tensor<2> xder, yder;
        xder.setValues({{-19.74678944758221960, -1.80745739336762394}, {-1.80745739336762394, 7.15559682440745668}});
        yder.setValues({{5.81508733600062749, 1.3189346474431758}, {1.3189346474431758, -32.2777204618206915}});
        gder_expected2.chip(0, 2) = xder;
        gder_expected2.chip(1, 2) = yder;

        std::cout
            << "g_amb :\n" << g_amb << "\n"
            << "g_amb_expected:\n" << g_amb_expected << "\n"
            << "g:\n" << g << "\n"
            << "g_expected:\n" << g_expected << "\n"
            << "gder:\n" << gder << "\n"
            << "gder_expected:\n" << gder_expected << "\n"
            << "gder_expected2:\n" << gder_expected2 << "\n";
#endif
    }
}
