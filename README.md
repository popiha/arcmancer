Arcmancer
=========

Arcmancer is a general purpose header-only C++14 library for computing 
geodesics and performing polarized radiative transfer in user specified 
spacetimes.
The library includes high-level constructs which allow quickly defining
relativistic ray-tracing computations and provides generic implementations 
of basic differential geometric objects that allow the implementation of
more complex procedures.
In addition to the C++ library, most of the main features are also available 
through the `pyarcmancer` Python module, which allows combining the convenience
of Python with the speed of the C++ code.

While the main focus of Arcmancer is on general relativistic polarized radiative
transfer, it also offers various features that are applicable in very general
contexts.
The main features of Arcmancer include:

* User defined (Pseudo-)Riemannian manifolds:
    - Arbitrary dimension, not limited to 4D spacetimes.
    - Multiple coordinate systems at once.
    - Support for both spacetime signature conventions, i.e. (+---) and (-+++)
    - Simple to define: only need to specify functions for the metric and its.
      derivatives, and potentially the coordinate system transitions.

* General tensors:
    - Automatic conversion of components between coordinate systems.
    - Type checked tensor index operations.
      Tensor index types (covariant/contravariant) are known at compile time
      and checked to ensure mathematical validity of operations.
    - Parallel transport along curves.

* Geodesics and Curves:
    - Accurate solution of the curve equation of motion using adaptive
      integration with configurable tolerances.
    - Support for curves with an arbitrary acceleration term.
    - Transport of objects along curves during curve computation.
      Includes e.g. the parallel transport of tensors, but custom objects are
      also supported.
    - Interpolation of points at arbitrary values of curve parameter.
    - Solution of differential equations along precomputed curves.

* Polarized Radiative Transfer:
    - Solution of the polarized radiative transfer equation along null geodesic
      rays. Multiple frequencies can be computed at once.
    - Simple interface for defining the fluid and radiation content: just
      implement two functions / functor objects.
    - Generic tools for generating ray initial conditions and performing
      parallel computation over the image.


For more details and results from some example computations performed using
Arcmancer see [the code paper][code-paper].


Getting Started
---------------

### Prerequisites

To use Arcmancer, you should have the following tools installed on your system:

* [CMake (v3.0+)](http://cmake.org/)

* [GNU Make](http://www.gnu.org/software/make/) or equivalent.

* [GCC (v5+)](http://gcc.gnu.org/) or [Clang (v5+)](http://clang.llvm.org/)  
  Other C++14 compilers may also work but not yet tested or supported.

* [Doxygen](http://www.doxygen.org/)  
  Required for API documentation.


Additionally, the following libraries are required:

* [Boost C++ Libraries v1.53+](http://www.boost.org/)  
  Only header-only components are used in the main library,
  the example programs additionally use `ProgramOptions`.

* [HDF5 C++ libraries](https://support.hdfgroup.org/HDF5/)  
  Known to work at least with versions 1.8.16 and 1.10.0.

* [UnitTest++ (a.k.a. unittest-cpp) v2.0+](https://github.com/unittest-cpp/unittest-cpp)  
  Optional, only required for running the tests.

* Python development headers  
  On Linux these are usually included in either `python-dev` or `python3-dev`,
  depending on the Python version used.

* [NumPy (v1.7.0+)](http://www.numpy.org/)

* [Matplotlib](https://matplotlib.org/)  
  Optional, only required for the example programs.

* [H5py](http://www.h5py.org/)   
  Optional, only required for the example programs.

### Cloning/Downloading

The best way of getting the library is to simply clone the git repository with

    git clone --recurse-submodules https://bitbucket.org/popiha/arcmancer.git

This ensures that the git submodule bringing in 
the [pybind11][pybind11] library is also initialized correctly. 
The most recently released stable version of Arcmancer is on the `master` 
branch, which is selected by default.

The Bitbucket page also allows downloading 
released versions of the library without fetching the whole history.
However, in this case you must manually add the correct version of pybind11
(the version the submodule points to)
under `pyarcmancer/pybind11` in order to compile the Python bindings.

### Configuring and Building

To configure and build the project run the following commands

    mkdir build
    cd build
    cmake .. -DCMAKE_BUILD_TYPE=Release
    make

This will build the `pyarcmancer` Python bindings for Arcmancer, 
the library API documentation and the unit tests,
as well as generating a `CMake` target file which can be used by other projects
to conveniently add a dependency on Arcmancer.

It may be necessary to manually specify paths to some external libraries.
In this case the error messages should contain the necessary details,
but also see below.

Additional compiler flags (e.g. `-march=native` for more efficient code) can
be specified using `CMAKE_CXX_FLAGS` variable 
(e.g. when calling `cmake` add `-DCMAKE_CXX_FLAGS="-march=native"` to the call).

You can also build only specific targets, in case not all of them are necessary.
For a list of all possible build targets, use the command `make help`.

It is also possible use the main C++ library without `CMake`,
in which case all the
necessary include paths need to be specified manually.
However, the recommended way is to use `CMake`.

#### CMake troubleshooting

* If multiple versions of Python are installed on the system, it is possible to
to build `pyarcmancer` against a specific one of these by adding the flag
`-DPYTHON_EXECUTABLE=path/to/python` to the `cmake` call.

* If Boost is not found, try setting `-DBOOST_ROOT=path/to/boost`.

* Similarly, for HDF5 libraries and header files, set
  `-DHDF5_ROOT=path/to/hdf5-libraries`. This is particularly necessary,
  if there are several HDF5 libraries installed on the system, e.g. with
  a Python distribution, as CMake can easily confuse these, which may
  break the build.

* If CMake complains about 'sysconfig' when processing the pybind11 submodule,
  the cause is likely missing Python distutils. For Ubuntu-type systems
  installing python3-distutils can solve this problem.

### Installing

At the moment we do not provide an install method integrated into the build
system, but using the library is still fairly straightforward.

The header library can be used in an external project with `CMake`,
see [examples/example\_project](examples/example_project)
and the other examples for details.

The python bindings can be used by adding the lib subdirectory to `PYTHONPATH`
or by copying `lib/pyarcmancer.*` to a directory on the python module search 
path.


Documentation and Examples
--------------------------

If `doxygen` is installed on the system, the main build process generates HTML
documentation for the library.
Open `docs/doxygen/html/index.html` to view this documentation.

The [examples](examples) directory contains various examples of using 
Arcmancer both as a C++ library as well as through the Python bindings.
These examples are built separately, see their respective READMEs for more 
details.

Note that most of the Python code in the examples
has been tested using Python 3, 
and may require small modifications to function correctly if used with 
Python 2.


License
-------

This project is licensed under the MIT License.
See the [LICENSE](LICENSE) file for details.

If work utilizing Arcmancer results in a scientific publication,
please cite the code paper [Pihajoki et al. (2018)][code-paper].


Contributing
---------------

If you detect bugs or have feature requests, please submit an issue on the 
project Bitbucket page.
You may also submit pull requests for code features you have implemented.


Third-Party Libraries
---------------------

The following third-party libraries are included with Arcmancer:

* [Eigen](http://eigen.tuxfamily.org/)
* [eigen3-hdf5](https://github.com/garrison/eigen3-hdf5)
* [spdlog](https://github.com/gabime/spdlog)
* [pybind11][pybind11]
* [CppOptimizationLibrary](https://github.com/PatWie/CppNumericalSolvers.git)

The respective licenses of these libraries are listed in the file
[thirdparty/LICENSES.md](thirdparty/LICENSES.md).



[code-paper]: https://arxiv.org/abs/1804.04670  "Pihajoki et al. (2018)"
[pybind11]: https://github.com/pybind/pybind11/ "pybind11"
