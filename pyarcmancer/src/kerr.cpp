
#include <arcmancer/spacetimes/kerr.h>
#include "pyarcmancer.h"

using namespace arcmancer::kerr;
void kerr(py::module &m){
    
    auto sub = metric_space_submodule<KerrSpacetime>(m, "kerr");
    py::class_<KerrSpacetime, 
               MetricSpace<KerrSpacetime,4>>(sub, "KerrSpacetime")
        .def(py::init<double, double>(), "mass"_a, "chi"_a)
        .def_readonly("boyer_lindquist", &KerrSpacetime::boyer_lindquist)
        .def_readonly("kerr_schild_in", &KerrSpacetime::kerr_schild_in)
        .def_readonly("kerr_schild_out", &KerrSpacetime::kerr_schild_out)
        .def("carters_constant", &KerrSpacetime::carters_constant,
             "Compute the value of Carter's constant for given four-momentum",
             "p"_a)
        .def_property_readonly("horizon_r", &KerrSpacetime::horizon_r,
            "Radius of the event horizon in Boyer-Lindquist coordinates")
        .def_property_readonly("mass", &KerrSpacetime::mass)
        .def_property_readonly("chi", &KerrSpacetime::chi);

    py::class_<KerrSphere, Surface<KerrSpacetime>>(sub, "KerrSphere")
        .def(py::init<const KerrSpacetime&, double>(), "M"_a, "r"_a);

    py::class_<OutgoingKerrSphere, Surface<KerrSpacetime>>
        (sub, "OutgoingKerrSphere")
        .def(py::init<const KerrSpacetime&, double>(), "M"_a, "r"_a);
    
    py::class_<IngoingKerrSphere, Surface<KerrSpacetime>>
        (sub, "IngoingKerrSphere")
        .def(py::init<const KerrSpacetime&, double>(), "M"_a, "r"_a);

    // Additional example surfaces

    py::class_<KerrNeutronStarSurface, Surface<KerrSpacetime>>(sub, "KerrNeutronStarSurface")
        .def(py::init<const KerrSpacetime&, double, double>());
}
