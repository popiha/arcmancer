
#include <functional>
#include <string>

#include <arcmancer/geometry/metric_space.h>
#include "pyarcmancer.h"

using namespace arcmancer;


using namespace std::literals::string_literals; // for ""s suffix operator

// A generic spacetime that takes the metric functions in a single chart as
// constructor arguments.

class GenericSpacetime : public MetricSpace<GenericSpacetime, 4> {

public:
    std::string name() const { return "Generic Spacetime"s; }

    const ChartType chart = {*this, "Generic coordinates"s};

    GenericSpacetime(
        std::function<EigenRk2Tensor<4>(const EigenRk1Tensor<4>&)> metric,
        std::function<EigenRk3Tensor<4>(const EigenRk1Tensor<4>&)>
            metric_derivatives) {
        add_chart(chart, metric, metric_derivatives);
    }
};


void generic_spacetime(py::module &m){
    
    auto sub 
        = metric_space_submodule<GenericSpacetime>(m, "generic_spacetime");
    py::class_<GenericSpacetime, 
               MetricSpace<GenericSpacetime,4>>(sub, "GenericSpacetime")
        .def(py::init<
            std::function<EigenRk2Tensor<4>(const EigenRk1Tensor<4>&)>,
            std::function<EigenRk3Tensor<4>(const EigenRk1Tensor<4>&)>>(),
            "Construct a spacetime with the given metric function",
            "metric"_a, "metric_derivatives"_a)
        .def_readonly("chart", &GenericSpacetime::chart);
}
