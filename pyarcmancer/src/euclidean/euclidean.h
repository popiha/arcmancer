#include <arcmancer/other_spaces/euclidean_space.h>
#include "../pyarcmancer.h"

using namespace arcmancer::euclidean;

// Helper function for generating Euclidean spaces of small dimensions
template <long dim>
void instantiate_euclidean(py::module &m) {
    std::string name = std::string("euclidean") + std::to_string(dim);

    auto sub = metric_space_submodule<EuclideanSpace<dim>>(m, name.c_str());

    name = std::string("EuclideanSpace_") + std::to_string(dim);
    py::class_<EuclideanSpace<dim>,
               MetricSpace<EuclideanSpace<dim>, dim, OtherSignature>>(
        sub, name.c_str())
        .def(py::init<>())
        .def_readonly("cartesian", &EuclideanSpace<dim>::cartesian);
}
