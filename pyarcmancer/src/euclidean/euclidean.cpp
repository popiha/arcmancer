#include <pybind11/pybind11.h>
namespace py = pybind11;

// Generate a bunch of euclidean spaces in all D = 2, 3 or 4 dimensions.
// Instantiations split into different translation units, since a single call
// can consume gigs of RAM.
// Here only calls, instantiations of templates in euclidean_i.cpp for i = 2,3,4
template<long>
void instantiate_euclidean(py::module &m);

void euclidean_instantiations(py::module &m) {
    instantiate_euclidean<2>(m);
    instantiate_euclidean<3>(m);
    instantiate_euclidean<4>(m);
}
