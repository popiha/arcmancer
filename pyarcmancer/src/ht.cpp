#include <arcmancer/spacetimes/ht.h>
#include "pyarcmancer.h"

using namespace arcmancer::ht;
void ht(py::module &m){

    auto ht_sub
        = metric_space_submodule<HartleThorneSpacetime>(m, "ht");

    py::class_<HartleThorneSpacetime,
               MetricSpace<HartleThorneSpacetime,4>> ht_spacetime(ht_sub, "HartleThorneSpacetime");

    py::enum_<HartleThorneSpacetime::Parametrization>(ht_spacetime, "Parametrization")
        .value("chi_eta", HartleThorneSpacetime::Parametrization::chi_eta)
        .value("r_omega", HartleThorneSpacetime::Parametrization::r_omega);
    ht_spacetime.def(py::init<double, double, double, HartleThorneSpacetime::Parametrization>());
    ht_spacetime.def_readonly("boyer_lindquist", &HartleThorneSpacetime::boyer_lindquist);

    py::class_<HTNeutronStarSurface, Surface<HartleThorneSpacetime>> ht_surf(ht_sub, "HTNeutronStarSurface");
    ht_surf.def(py::init<const HartleThorneSpacetime&, double, double>());


}
