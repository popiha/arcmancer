#pragma once
//Header with functions for defining the MetricSpace dependent portions of the
//bindings. Also brings in all the necessary includes etc.

#include <pybind11/pybind11.h>
#include <pybind11/eigen.h>
#include <pybind11/stl.h>
#include <pybind11/functional.h>
#include <pybind11/numpy.h>
#include <pybind11/operators.h>

namespace py = pybind11;
using namespace pybind11::literals; //brings in the _a literal for arguments

// Include only what's needed here to reduce unnecessary recompilations
#include <arcmancer/geometry.h>
#include <arcmancer/geometry/metric_space.h>
#include <arcmancer/image_plane.h>
#include <arcmancer/radiation/radiation_transfer.h>
#include <arcmancer/radiation/thermal_synchrotron.h>
#include <arcmancer/geometry/normal_coordinates.h>

using namespace arcmancer;

// Custom type caster for Eigen::TensorFixedSize <-> numpy.array
// http://pybind11.readthedocs.io/en/master/advanced/cast/custom.html
// Should be able to handle all TensorFixedSize, but arcmancer currently
// needs only double tensors with identical size dimensions.
namespace pybind11 {
namespace detail {

template <typename Scalar, long... Ds>
struct type_caster<Eigen::TensorFixedSize<Scalar, Eigen::Sizes<Ds...>>> {
    using TensorType = Eigen::TensorFixedSize<Scalar, Eigen::Sizes<Ds...>>;

    PYBIND11_TYPE_CASTER(
        TensorType,
        // Using the magic _() and concat function of pybind internals here to
        // generate the correct string to describe the accepted numpy array.
        // This is based on pybind internals.
        (_("numpy.ndarray[") + npy_format_descriptor<Scalar>::name + _("[") +
         concat(_<Ds>()...) + _("]") + _("]")));

    bool load(handle src, bool) {
        auto buf = array_t<Scalar>::ensure(src);
        if (!buf) return false;

        auto info = buf.request();
        // Check that the input has the correct shape
        if (info.ndim != rank) return false;

        for (size_t i = 0; i < rank; ++i) {
            if (info.shape[i] != dims()[i]) { return false; }
        }

        int flags = buf.flags();

        if (flags & npy_api::constants::NPY_ARRAY_C_CONTIGUOUS_) {
            // C-contiguous i.e. row-major, so need to shuffle the
            // result to the column-major order used by Eigen::Tensors
            value = Eigen::TensorMap<Eigen::Tensor<Scalar, rank>>(
                        const_cast<Scalar *>(buf.data()), Ds...)
                        .shuffle(type_caster::row_col_major_shuffle_inds());

        } else if (flags & npy_api::constants::NPY_ARRAY_F_CONTIGUOUS_) {
            // F-continuous i.e. column-major, no special handling needed
            value = Eigen::TensorMap<Eigen::Tensor<Scalar, rank>>(
                const_cast<Scalar *>(buf.data()), Ds...);
        } else {
            // Non-continuous, need to handle strides.
            // For now, just do with a loop over the storage.
            // Maybe there's a faster way, but this should not cause major
            // performance issues anyway.

            for (size_t i = 0; i < total_size; ++i) {
                value.data()[i] =
                    buf.data()[type_caster::non_cont_buffer_offset(
                        i, info.strides)];
            }
        }
        return true;
    }

    // Copies the data to a new array, probably...
    // May also return an array that's just a reference to the original
    // tensor, but this shouldn't cause any problems for us since
    // no mutable component containers are exposed.
    static handle cast(const TensorType &src, return_value_policy /*policy */,
                       handle /*parent*/) {
        array a({Ds...}, type_caster::tensor_strides(), src.data(), handle());

        return a.release();
    }

private:
    static constexpr size_t rank = sizeof...(Ds);
    static constexpr size_t total_size = TensorType::Dimensions::total_size;

    // use singleton stuff to avoid linking trouble with static constexpr
    // members
    static const std::array<long, rank> &dims() {
        static constexpr std::array<long, rank> d{Ds...};
        return d;
    }

    // Generate index arrays etc.
    // This could be made constexpr with C++17 constexpr array operations,
    // but using static values they get initialized only once anyway.
    static const std::array<int, rank> &row_col_major_shuffle_inds() {
        static std::array<int, rank> ret;
        static bool initialized = false;
        if (!initialized) {
            for (size_t i = 0; i < rank; ++i) {
                ret[i] = rank - 1 - i;
            }
            initialized = true;
        }
        return ret;
    }

    static const std::array<size_t, rank> &tensor_strides() {
        static std::array<size_t, rank> ret{};
        static bool initialized = false;
        if (!initialized) {
            ret[0] = sizeof(Scalar);
            for (size_t i = 1; i < rank; ++i) {
                ret[i] = ret[i - 1] * dims()[i - 1];
            }
            initialized = true;
        }
        return ret;
    }

    // Get the index to a strided buffer corresponding to the given offset to
    // tensor storage
    template <typename Container>
    static size_t non_cont_buffer_offset(size_t tensor_offset,
                                         const Container &buffer_strides) {

        size_t buffer_offset = 0;
        for (size_t j = rank; j > 0; --j) {
            size_t i = j - 1;
            size_t ind = tensor_offset / (tensor_strides()[i] / sizeof(Scalar));
            tensor_offset -= ind * (tensor_strides()[i] / sizeof(Scalar));

            buffer_offset += ind * buffer_strides[i];
        }
        return buffer_offset / sizeof(Scalar);
    }
};
}} //namespace pybind11::detail


// for overriding virtual classes in Python, ''trampoline'' classes are
// required

// surface.h
template<typename MetricSpaceType>
class PySurface : public Surface<MetricSpaceType> {
public:
    using Surface<MetricSpaceType>::Surface;

    double value(const ManifoldPoint<MetricSpaceType> &x) const override {
        PYBIND11_OVERLOAD_PURE(
                double,  // return type
                Surface<MetricSpaceType>, // parent
                value,   // name of this function
                x        // argument(s)
                );
    }

    CotangentVector<MetricSpaceType> 
    gradient(const ManifoldPoint<MetricSpaceType> &x) const override {
        PYBIND11_OVERLOAD_PURE(
                CotangentVector<MetricSpaceType>, // return type
                Surface<MetricSpaceType>,   // parent
                gradient,  // name of this function
                x          // argument(s)
                );
    }

    TangentVector<MetricSpaceType>
    observer(const ManifoldPoint<MetricSpaceType> &x) const override {
        PYBIND11_OVERLOAD_PURE(
                TangentVector<MetricSpaceType>, // return type
                Surface<MetricSpaceType>,      // parent
                observer,     // name of this function
                x     // argument(s)
                );
    }

    bool ignore_at(const ManifoldPoint<MetricSpaceType> &x) const override {
        PYBIND11_OVERLOAD(
                bool, // return type
                Surface<MetricSpaceType>,      // parent
                ignore_at,     // name of this function
                x     // argument(s)
                );
    }

};



template<typename TensorType, typename MetricSpaceType>
std::enable_if_t<MetricSpaceType::is_lorentzian, void>
lorentzian_specific_tensor_defs(py::class_<TensorType> &tensor){
    using ContainerType = typename TensorType::ContainerType;
    
    tensor    
    .def(py::init<const LorentzFrame<MetricSpaceType>&, const ContainerType&>(),
         "Construct from components given in the basis of frame", 
         "frame"_a, "components"_a)
    .def("components",(ContainerType (TensorType::*)
                       (const LorentzFrame<MetricSpaceType>&) const
                      ) &TensorType::components,
         "Components in the basis of frame", "frame"_a)
    ;    
}

template<typename TensorType, typename MetricSpaceType>
std::enable_if_t<!MetricSpaceType::is_lorentzian, void>
lorentzian_specific_tensor_defs(py::class_<TensorType>&){}

//tensor definitions that don't depend on the indices
template<typename TensorType, typename MetricSpaceType>
py::class_<TensorType> tensor_common(py::module &mod, const char *name){
    using ContainerType = typename TensorType::ContainerType;
    using ChartType = typename TensorType::ChartType;
    using PointType = typename TensorType::PointType;

    auto t = py::class_<TensorType>(mod, name)
    .def(py::init<const PointType&, const ChartType&, const ContainerType&>(),
         "Construct from components given in the basis of chart",
         "point"_a, "chart"_a, "components"_a)
    .def("components",(ContainerType (TensorType::*)
                       (const ChartType&) const) &TensorType::components,
         "Components in the basis of out_chart", "out_chart"_a)
    .def("internal_components",(ContainerType (TensorType::*)
                       () const) &TensorType::internal_components,
         "Components in the internally used coordinate basis")
    .def_property_readonly("point",&TensorType::point, 
        "Base point of this tensor on the manifold")
    .def_property_readonly("M", &TensorType::M, 
        "The manifold where this tensor is defined")
    .def_property_readonly("chart", &TensorType::chart, 
        "The chart this tensor uses internally")
    .def("transport_derivatives", &TensorType::transport_derivatives,
         "Parallel transport derivatives in the basis of chart."
         "contracted_Gamma = components of Gamma^a_bc v^c given in chart,"
         "with v the tangent vector of the curve.", 
         "chart"_a, "contracted_Gamma"_a)
    .def("__repr__", 
         [](const TensorType& tens){
                std::stringstream ss;
                ss << tens;
                return ss.str();
            })
    .def(py::self + py::self)
    .def(py::self += py::self)
    .def(py::self - py::self)
    .def(py::self -= py::self)
    .def(py::self * double())
    .def(double() * py::self)
    .def(py::self *= double())
    .def(py::self / double())
    .def(py::self /= double())
    .def(-py::self)
    .def(py::self == py::self)
    ;
    
    lorentzian_specific_tensor_defs<TensorType, MetricSpaceType>(t);
    return t;
}

// GCC doesn't deduce auto functions as template params correctly,
// and needs explicit casts to work. Seems to be caused by this bug: 
// https://gcc.gnu.org/bugzilla/show_bug.cgi?id=64194
// These helpers hide the repetitive cast.
template <size_t index, typename TensorType>
void raise_index_def(py::class_<TensorType> &t,
                     std::string method_name = "raise_index" +
                                               std::to_string(index),
                     std::string info = "raise index " +
                                        std::to_string(index)) {

    using F = decltype(&TensorType::template raise_index<index>);
    t.def(method_name.c_str(),
          static_cast<F>(&TensorType::template raise_index<index>),
          info.c_str());
}

template <size_t index, typename TensorType>
void lower_index_def(py::class_<TensorType> &t,
                     std::string method_name = "lower_index" +
                                               std::to_string(index),
                     std::string info = "lower index " +
                                        std::to_string(index)) {

    using F = decltype(&TensorType::template lower_index<index>);
    t.def(method_name.c_str(),
          static_cast<F>(&TensorType::template lower_index<index>),
          info.c_str());
}


// XXX: The next two functions have some duplication, should maybe refactor
template<typename MetricSpaceType, typename TransportType>
auto parametrized_point_w_transport(py::module &mod, const char *name, 
                              const char *doc){
    using AP = ParametrizedPoint<MetricSpaceType, TransportType>;
    py::class_<AP> ap(mod, name, doc);
    ap.def_property_readonly("curve_parameter", &AP::curve_parameter,
        "Curve parameter of this point")
        .def_property_readonly("point", &AP::point, 
            "The point on the MetricSpace")
        .def_property_readonly("tangent", &AP::tangent,
            "Tangent vector of this point")
        .def_property_readonly("M", &AP::M, 
            "The MetricSpace where this point is defined")
        .def("__repr__", 
             [](const AP& p){
                    std::stringstream ss;
                    ss << p;
                    return ss.str();
            })
        .def(py::init<double, const TangentVector<MetricSpaceType>&,
                      const TransportType&>(),
            "curve_parameter"_a, "tangent"_a, "transported"_a)
        .def_property_readonly("transported", 
            static_cast<const TransportType& (AP::*)() const>(&AP::transported),
            "The transported object");

    return ap;
}

template<typename MetricSpaceType>
auto parametrized_point(py::module &mod, const char *name, const char *doc){
    using AP=ParametrizedPoint<MetricSpaceType>;
    
    py::class_<AP> ap(mod, name, doc);
    ap.def_property_readonly("curve_parameter", &AP::curve_parameter,
        "Curve parameter of this point")
        .def_property_readonly("point", &AP::point, 
            "The point on the MetricSpace")
        .def_property_readonly("tangent", &AP::tangent, 
            "Tangent vector of this point")
        .def_property_readonly("M", &AP::M, 
            "The MetricSpace where this point is defined")
        .def("__repr__", 
             [](const AP& p){
                    std::stringstream ss;
                    ss << p;
                    return ss.str();
            })
        .def(py::init<double, const TangentVector<MetricSpaceType>&>(),
             "curve_parameter"_a, "tangent"_a);

    return ap;
}

// Need to have 0 or 1 constructor arg called "transported" depending on
// whether it is accepted by the ParametrizedCurve.
// Use these functions to do it since we don't have constexpr if.
template <typename MetricSpaceType>
void parametrized_curve_constructor_def(
    py::class_<ParametrizedCurve<MetricSpaceType>> &ac) {
    using ParametrizedPointType =
        ParametrizedPoint<MetricSpaceType>;

    ac.def(py::init<const TangentVector<MetricSpaceType> &,
                    typename ParametrizedPointType::ForceFunction>(),
           "tangent"_a, "force_function"_a = nullptr);
}

template <typename MetricSpaceType, typename TransportType>
void parametrized_curve_constructor_def(
    py::class_<ParametrizedCurve<MetricSpaceType, TransportType>> &ac) {
    using ParametrizedPointType =
        ParametrizedPoint<MetricSpaceType, TransportType>;

    ac.def(
        py::init<const TangentVector<MetricSpaceType> &, const TransportType &,
                 typename ParametrizedPointType::ForceFunction>(),
        "tangent"_a, "transported"_a, "force_function"_a = nullptr);
}

template<typename MetricSpaceType, typename... TransportTypes>
py::class_<ParametrizedCurve<MetricSpaceType, TransportTypes...>> 
parametrized_curve(py::module &mod, const char *name, const char *doc) {
    using AC = ParametrizedCurve<MetricSpaceType, TransportTypes...>;
    using ParametrizedPointType =
        ParametrizedPoint<MetricSpaceType, TransportTypes...>;
    using SurfaceType = typename AC::SurfaceType;
    
    py::class_<AC> ac(mod, name, doc);

    py::enum_<typename AC::ComputationResult>(ac, "ComputationResult")
        .value("success"     , AC::ComputationResult::success)
        .value("overstep"    , AC::ComputationResult::overstep)
        .value("step_limit"  , AC::ComputationResult::step_limit)
        .value("hit_surface" , AC::ComputationResult::hit_surface);

    ac.def(py::init<ParametrizedPointType,
                    typename ParametrizedPointType::ForceFunction>(),
           "initial_point"_a, "force_function"_a = nullptr)
        .def_property_readonly(
            "front", &AC::front,
            "The point with the most negative curve parameter")
        .def_property_readonly(
            "back", &AC::back,
            "The point with the most positive curve parameter")
        .def_property_readonly("extent_front", &AC::extent_front,
                               "Curve parameter of front")
        .def_property_readonly("extent_back", &AC::extent_back,
                               "Curve parameter of back")
        .def_property_readonly("back_termination", &AC::back_termination)
        .def_property_readonly("front_termination", &AC::front_termination)
        .def_property_readonly("num_points", &AC::num_points)
        .def_property_readonly("M", &AC::M,
                               "The MetricSpace where this curve is defined")
        .def_property(
            "configuration",
            py::overload_cast<>(&AC::configuration, py::const_),
            [](AC &ac,
               const decltype(std::declval<AC>().configuration()) &conf) {
                ac.configuration() = conf;
            },
            "The configuration parameters of this curve")
        .def("compute",
             (typename AC::ComputationResult(AC::*)(double)) & AC::compute,
             "Extend the curve to curve_parameter without surfaces",
             "curve_parameter"_a)
        .def("compute",
             (typename AC::ComputationResult(AC::*)(
                 double, const std::vector<SurfaceType *> &)) &
                 AC::compute,
             "Extend the curve to curve_parameter and check for collisions with"
             " surfaces",
             "curve_parameter"_a, "surfaces"_a)
        .def("interpolator",
             py::overload_cast<double, double, size_t>(&AC::interpolator,
                                                       py::const_),
             "Get an interpolator over the part of the curve between "
             "curve_parameter_start and curve_parameter_end",
             "curve_parameter_start"_a, "curve_parameter_end"_a,
             py::arg("min_window_points") = 6)
        .def("interpolator", py::overload_cast<>(&AC::interpolator, py::const_),
             "Get an interpolator over the whole curve")
        .def("interpolate", &AC::interpolate,
             "Interpolate the curve for the point at curve_parameter",
             py::arg("curve_parameter"))
        .def("__iter__",
             [](const AC &ac) {
                 return py::make_iterator(ac.begin(), ac.end());
             },
             py::keep_alive<0, 1>())
        .def("__getitem__",
             [](const AC &ac, int i) -> const ParametrizedPointType & {
                 if (i < 0) { i = ac.num_points() + i; }
                 return ac[i];
             },
             py::keep_alive<0, 1>())
        .def("__len__", [](const AC &ac) { return ac.num_points(); });

    parametrized_curve_constructor_def(ac);

    py::class_<typename AC::Termination>(ac, "Termination", 
        "Contains information about whether the curve has collided with "
        "a surface")
        .def_readonly("hit_surface", &AC::Termination::hit_surface,
                       "Has the curve hit a surface")
        .def_readonly("surface_index", &AC::Termination::surface_index,
                       "Index of the surface in the surfaces list")
        .def_readonly("observer_hit_angle",
                       &AC::Termination::observer_hit_angle,
                       "Angle of the curve with the surface as seen by "
                       "a comoving observer on the surface")
        .def_readonly("observer_dot_tangent",
                       &AC::Termination::observer_dot_tangent,
                       "u^a v_a where u is the curve tangent and v is the"
                       "four-velocity of a comoving observer on the surface")
        .def("__repr__", 
             [](const typename AC::Termination& t){
                    std::stringstream ss;
                    ss << t;
                    return ss.str();
            });


    return ac;
}


//Stuff that's only defined for lorentzian signatures
//need enable_if tricks to work around limitations on
//partial function template specialization
template <typename MetricSpaceType>
std::enable_if_t<MetricSpaceType::is_lorentzian, void>
lorentzian_specific_definitions(
    py::module &sub,
    py::class_<
        MetricSpace<MetricSpaceType, 4, typename MetricSpaceType::Signature>>
        &metric_space) {

    using ChartType = Chart<MetricSpaceType>;
    using PointType = ManifoldPoint<MetricSpaceType>;
    using Tangent = TangentVector<MetricSpaceType>;
    using Cotangent = CotangentVector<MetricSpaceType>;
    metric_space
        .def("levi_civita_tensor",
             (Tensor<MetricSpaceType, Cov, Cov, Cov, Cov> 
              (MetricSpaceType::MetricSpace::*)(const PointType&) const)
             &MetricSpaceType::levi_civita_tensor,
             "The Levi-Civita tensor at x", "x"_a);
    
   //geometry/utils.h
    sub.def("spatial_cross_product",
            (Tangent(*)(const Tangent &, const Tangent &, const Tangent &)) 
            &spatial_cross_product,
            "Compute the tangent vector corresponding to the cross product of"
            "the spacelike parts of a and b as seen by an observer with "
            "four-velocity u",
            "u"_a, "a"_a, "b"_a)
        .def("screen_inner_product", &screen_inner_product<MetricSpaceType>,
             "Dot product of a and b in the subspace orthogonal to u and k",
             "u"_a, "k"_a, "a"_a, "b"_a)
        .def("vector_angle", &vector_angle<MetricSpaceType>,
             "Spatial angle between a and b as seen by an observer with "
             "4-velocity"
             " u",
             "u"_a, "a"_a, "b"_a)
        .def("screen_project", &screen_project<MetricSpaceType>,
             "Project a to the subspace orthogonal to u and k", "u"_a, "k"_a,
             "a"_a)
        .def("vector_likeness",
             (VectorLikeness(*)(const Tangent &)) &vector_likeness,
             "Find the likeness of a vector to numerical tolerance")
        .def("vector_likeness",
             (VectorLikeness(*)(const Cotangent &)) &vector_likeness);

    //geometry/lorentz_frame.h
    {

    using Frame = LorentzFrame<MetricSpaceType>;
    py::class_<Frame>(sub, "LorentzFrame", 
            "A local frame with an orthonormal basis")
        .def(py::init<const Tangent&, const Tangent&, const Tangent&>(),
            "Construct from TangentVectors.\n"
            "obs_u: The four-velocity of the observer corresponding to this"
            "frame, or the direction of e_t\n"
            "approximate_z: The direction of e_z will be the part orthogonal to"
            " e_t\n"
            "approximate_x: The direction of e_x will be the part orthogonal to"
            "e_t and e_z",
            "obs_u"_a, "approximate_z"_a, "approximate_x"_a)
        .def(py::init<const PointType&, const ChartType&, 
                      const EigenRk1Tensor<4>&, const EigenRk1Tensor<4>&, 
                      const EigenRk1Tensor<4>&>(),
            "Construct from at point with components of vectors given in chart",
            "point"_a, "chart"_a, "obs_u"_a, "approximate_z"_a,
            "approximate_x"_a)
        .def("make_lightlike", 
             (Tangent (Frame::*)(const Tangent&) const) &Frame::make_lightlike,
             "Create a lightlike vector that points in the same spatial"
             " direction in this frame as v", "v"_a)
        .def("make_lightlike", 
             (Tangent (Frame::*)(const EigenRk1Tensor<4>&) const) 
             &Frame::make_lightlike, 
             "components are the components of v in this frame")
        .def_property_readonly("basis", &Frame::basis,
            "List of the basis vectors")
        .def_property_readonly("e_t", &Frame::e_t)
        .def_property_readonly("e_x", &Frame::e_x)
        .def_property_readonly("e_y", &Frame::e_y)
        .def_property_readonly("e_z", &Frame::e_z)
        .def("transformation_to_basis", &Frame::transformation_to_basis,
            "Transition matrix from chart to the basis of this frame",
            "chart"_a)
        .def_property_readonly("M", &Frame::M, 
            "The MetricSpace where this frame is defined")
        .def_property_readonly("point", &Frame::point, 
            "The point where this frame is defined")
        .def("__repr__", 
             [](const Frame& f){
                    std::stringstream ss;
                    ss << f;
                    return ss.str();
            });
    
    using FrameFW = LorentzFrameFW<MetricSpaceType>;
    py::class_<FrameFW, Frame>(sub, "LorentzFrameFW", 
            "A Fermi-Walker transported local frame with an orthonormal basis")
        .def(py::init<const Tangent&, const Tangent&, const Tangent&>(),
            "Construct from TangentVectors.\n"
            "obs_u: The four-velocity of the observer corresponding to this"
            "frame, or the direction of e_t\n"
            "approximate_z: The direction of e_z will be the part orthogonal to"
            " e_t\n"
            "approximate_x: The direction of e_x will be the part orthogonal to"
            "e_t and e_z",
            "obs_u"_a, "approximate_z"_a, "approximate_x"_a)
        .def(py::init<const PointType&, const ChartType&, 
                      const EigenRk1Tensor<4>&, const EigenRk1Tensor<4>&, 
                      const EigenRk1Tensor<4>&>(),
            "Construct from at point with components of vectors given in chart",
            "point"_a, "chart"_a, "obs_u"_a, "approximate_z"_a,
            "approximate_x"_a)
        .def("__repr__", 
             [](const FrameFW& f){
                    std::stringstream ss;
                    ss << f;
                    return ss.str();
            });
    }

    {
    using AP = ParametrizedPoint<MetricSpaceType,
                           LorentzFrame<MetricSpaceType>>;
    parametrized_point_w_transport<MetricSpaceType,
                             LorentzFrame<MetricSpaceType>>
    (sub, "ParametrizedPoint_LorentzFrame", 
     "Parametrized point parallel transporting a LorentzFrame");

    using Geo = Geodesic<MetricSpaceType, LorentzFrame<MetricSpaceType>>;
    auto ac =
        parametrized_curve<MetricSpaceType, LorentzFrame<MetricSpaceType>>(
            sub, "ParametrizedCurve_LorentzFrame",
            "Base ParametrizedCurve of Geodesic_LorentzFrame");
    py::class_<Geo>(sub, "Geodesic_LorentzFrame", ac)
        .def(py::init<const AP&>(),
            "Construct from the initial ParametrizedPoint", "initial_point"_a)
        .def(py::init<const Tangent&, const LorentzFrame<MetricSpaceType>&>(),
            "Construct from a TangentVector u and LorentzFrame frame."
            " The initial curve_parameter = 0",
            "u"_a, "frame"_a)
        .def_property_readonly("likeness", &Geo::likeness,
            "The likeness of the tangent vector of this geodesic");    
    
    parametrized_point_w_transport<MetricSpaceType,
                             LorentzFrameFW<MetricSpaceType>>
    (sub,"ParametrizedPoint_LorentzFrameFW", 
     "Parametrized point transporting a LorentzFrameFW");

    parametrized_curve<MetricSpaceType, LorentzFrameFW<MetricSpaceType>>
    (sub, "ParametrizedCurve_LorentzFrameFW",
     "Parametrized curve with a transported a LorentzFrameFW");
    }

    //
    //radiation/radiation_transfer.h and related
    {

    py::class_<PolarizationFrame<MetricSpaceType>>(sub, "PolarizationFrame",
        "Wraps the horizontal and vertical basis vectors for parallel"
        " transport and radiation transfer computation in RadiationGeodesic")
        .def(py::init<const Tangent&, const Tangent&>(), 
             "vertical"_a, "horizontal"_a)
        .def("polarization_angle", 
             &PolarizationFrame<MetricSpaceType>::polarization_angle,
             "Compute the rotation angle of the frame.",
             "obs_u"_a, "B"_a, "ray_tangent"_a)
        .def_property_readonly("vertical", 
            &PolarizationFrame<MetricSpaceType>::vertical)
        .def_property_readonly("horizontal", 
            &PolarizationFrame<MetricSpaceType>::horizontal)
        .def_property_readonly("point", 
            &PolarizationFrame<MetricSpaceType>::point)
        .def_property_readonly("M", 
            &PolarizationFrame<MetricSpaceType>::M)
        .def("__repr__", 
             [](const PolarizationFrame<MetricSpaceType>& f){
                    std::stringstream ss;
                    ss << f;
                    return ss.str();
            });

    using AP = ParametrizedPoint<MetricSpaceType,
                           PolarizationFrame<MetricSpaceType>>;
    parametrized_point_w_transport<MetricSpaceType,
                             PolarizationFrame<MetricSpaceType>>
    (sub, "ParametrizedPoint_PolarizationFrame", 
     "Parametrized point parallel transporting a PolarizationFrame");

    using Geo = Geodesic<MetricSpaceType, PolarizationFrame<MetricSpaceType>>;
    auto ac =
        parametrized_curve<MetricSpaceType, PolarizationFrame<MetricSpaceType>>(
            sub, "ParametrizedCurve_PolarizationFrame",
            "Base ParametrizedCurve of Geodesic_PolarizationFrame");
    py::class_<Geo>(sub, "Geodesic_PolarizationFrame", ac)
        .def(py::init<const AP&>(),
            "Construct from the initial ParametrizedPoint", "initial_point"_a)
        .def(py::init<const Tangent&, const PolarizationFrame<MetricSpaceType>&>(),
            "Construct from a TangentVector u and PolarizationFrame frame"
            " The initial curve_parameter = 0",
            "u"_a, "frame"_a)
        .def_property_readonly("likeness", &Geo::likeness,
            "The likeness of the tangent vector of this geodesic");

    py::class_<FluidData<MetricSpaceType>>(sub, "FluidData",
                                           "Basic fluid properties")
        .def(py::init<double, double, double, double, const Tangent &,
                      const Tangent &>(),
             "number_density"_a, "temperature"_a, "pressure"_a,
             "magnetic_field_strength"_a, "magnetic_field"_a,
             "fluid_velocity"_a)
        .def_readwrite("number_density",
                       &FluidData<MetricSpaceType>::number_density)
        .def_readwrite("temperature", &FluidData<MetricSpaceType>::temperature)
        .def_readwrite("pressure", &FluidData<MetricSpaceType>::pressure)
        .def_readwrite("magnetic_field_strength",
                       &FluidData<MetricSpaceType>::magnetic_field_strength)
        .def_readwrite("magnetic_field",
                       &FluidData<MetricSpaceType>::magnetic_field)
        .def_readwrite("fluid_velocity",
                       &FluidData<MetricSpaceType>::fluid_velocity);

    sub.def("radiation_transfer",
            static_cast<PolarizationSpectrum (*)(
                const Geodesic<MetricSpaceType,
                               PolarizationFrame<MetricSpaceType>> &,
                double, double, double, const PolarizationSpectrum &,
                const FluidFunction<MetricSpaceType, FluidData<MetricSpaceType>>
                    &,
                const RadiationFunction<FluidData<MetricSpaceType>> &)>(
                radiation_transfer),
            "Compute polarized radiative transfer over the specified parameter "
            "range of the ray",
            "ray"_a, "parameter_start"_a, "parameter_end"_a,
            "observer_dot_tangent"_a, "initial_values"_a, "fluid"_a,
            "radiation"_a)
        .def("radiation_transfer",
             static_cast<PolarizationSpectrum (*)(
                 const Geodesic<MetricSpaceType,
                                PolarizationFrame<MetricSpaceType>> &,
                 double, const PolarizationSpectrum &,
                 const FluidFunction<MetricSpaceType,
                                     FluidData<MetricSpaceType>> &,
                 const RadiationFunction<FluidData<MetricSpaceType>> &)>(
                 radiation_transfer),
             "Compute polarized radiative transfer over the whole ray",
             "ray"_a, "observer_dot_tangent"_a, "initial_values"_a, "fluid"_a,
             "radiation"_a);
    
    // Radiation models (which depend on MetricSpaceType)
    py::class_<ThermalSynchrotron<MetricSpaceType>>(sub, "ThermalSynchrotron")
        .def(py::init<double, double, bool>(), "length_unit"_a,
             "theta_e_min"_a=0.05,  "scalar_transport"_a=false)
        .def("__call__", &ThermalSynchrotron<MetricSpaceType>::operator(),
            "fluid"_a, "nu"_a, "th_B"_a);
    }




    //image_plane.h
    {
    using IP = ImagePlane<MetricSpaceType, py::object>;
    py::class_<IP> ip(sub, "ImagePlane", 
        "Compute initial conditions for lightlike geodesics on an image plane"
        " with different projections");
    ip.def(py::init<const typename IP::PlaneShape &, typename IP::Projection,
                    const LorentzFrame<MetricSpaceType> &>(),
           "Construct from the local frame corresponding to the observer\n"
           "shape: the dimensions of the image plane\n"
           "projection: the projection used. Possible values are\n"
           " Projection.ParallelPlane:\n"
           "  The image plane is formed by propagating geodesics in the"
           " x,y plane of the frame."
           " x,y are the physical distances from the"
           " central observer, and sizes in shape are the physical sizes"
           " of the plane. The incoming rays are parallel to e_z, and the"
           " local frame is parallel transported to each point.\n"
           "\n Projection.Perspective:\n"
           "  A simple perspective projection."
           "frame: the observer frame",
           "shape"_a, "projection"_a, "frame"_a)
        .def(py::init<const typename IP::PlaneShape &,
                      const LorentzFrame<MetricSpaceType> &>(),
             "Construct from the local frame corresponding to the observer\n"
             "using the default ParallelPlane projection."
             "shape: the dimensions of the image plane\n"
             "frame: the observer frame",
             "shape"_a, "frame"_a)
        .def(py::init<const typename IP::PlaneShape &, typename IP::Projection,
                      const Tangent &, const Tangent &, const Tangent &>(),
             "As above, but construct frame from the given vectors.\n"
             "The vectors match the LorentzFrame constructor.",
             "shape"_a, "projection"_a, "obs_u"_a, "approzimate_z"_a,
             "approximate_x"_a)
        .def(py::init<const typename IP::PlaneShape &, const Tangent &,
                      const Tangent &, const Tangent &>(),
             "shape"_a, "obs_u"_a, "approzimate_z"_a, "approximate_x"_a)
        .def_property_readonly(
            "shape", &IP::shape,
            "The dimensions of the plane."
            " The interpretation depends on the projection used.")
        .def_property_readonly("projection", &IP::projection,
                               "The projection used")
        .def_property_readonly("points", &IP::points, "The points of the plane")
        .def_property_readonly("data_points", &IP::data_points,
                               "The computed datapoints of the plane")
        .def("compute",
             &IP::template compute<
                 std::function<py::object(const typename IP::ImagePoint &)>>,
             "Run function f on all the points on the plane and return the"
             " results wrapped in ImageDataPoints",
             "f"_a);

    py::class_<typename IP::PlaneShape>(ip, "PlaneShape")
        .def(py::init<size_t, size_t, double, double>(),
             "xbins"_a, "ybins"_a, "xspan"_a, "yspan"_a)
        .def_readonly("xbins", &IP::PlaneShape::xbins)
        .def_readonly("ybins", &IP::PlaneShape::ybins)
        .def_readonly("xspan", &IP::PlaneShape::xspan)
        .def_readonly("yspan", &IP::PlaneShape::yspan)
        .def_readonly("dx", &IP::PlaneShape::dx)
        .def_readonly("dy", &IP::PlaneShape::dy)
        .def_readonly("pixel_area", &IP::PlaneShape::pixel_area);

    py::enum_<typename IP::Projection>(ip, "Projection")
        .value("ParallelPlane", IP::Projection::ParallelPlane)
        .value("Perspective", IP::Projection::Perspective);
    
    using IDP = typename IP::ImageDataPoint;
    py::class_<IDP>(ip, "ImageDataPoint")
        .def_readonly("ix", &IDP::ix, "Index ix of the corresponding ImagePoint")
        .def_readonly("iy", &IDP::iy, "Index iy of the corresponding ImagePoint")
        .def_readonly("x", &IDP::x, 
            "Coordinate of the corresponding ImagePoint")
        .def_readonly("y", &IDP::y, 
            "Coordinate of the corresponding ImagePoint")
        .def_readonly("data", &IDP::data, 
            "Data returned by the function used in compute");

    py::class_<typename IP::ImagePoint>(ip, "ImagePoint", 
        "A point on the image plane. Indexing starts from (x,y)=(-xspan,-xspan)")
        .def_readonly("ix", &IP::ImagePoint::ix, "Index in the x direction")
        .def_readonly("iy", &IP::ImagePoint::iy, "Index in the y direction")
        .def_readonly("x", &IP::ImagePoint::x, "Coordinate of the point")
        .def_readonly("y", &IP::ImagePoint::y, "Coordinate of the point")
        .def_readonly("ray_tangent", &IP::ImagePoint::ray_tangent,
            "Tangent vector of an incoming ray of light.")
        .def_readonly("frame", &IP::ImagePoint::frame, 
            "The local frame corresponding this point");
    }

}

// Empty function for non-lorentzian cases
template <typename MetricSpaceType>
std::enable_if_t<!MetricSpaceType::is_lorentzian, void>
lorentzian_specific_definitions(
    py::module &,
    py::class_<MetricSpace<MetricSpaceType, MetricSpaceType::D,
                           typename MetricSpaceType::Signature>> &) {}

//Function for defining a submodule for every different metric space
//Everything that is parametrized by MetricSpaceType should be defined here.
template<typename MetricSpaceType>
py::module metric_space_submodule(py::module &m, const char *submodule_name){
    py::module sub = m.def_submodule(submodule_name);

    constexpr long D = MetricSpaceType::D;

    // TODO: Add interpolation.h

    //geometry/types.h
    using ChartType = Chart<MetricSpaceType>;
    py::class_<ChartType>(sub, "Chart")
        //XXX: can't take pointer to reference, this gets around it
        .def_property_readonly("M",[](const ChartType& c)
                                   ->const MetricSpaceType&{return c.M;},
                                "The MetricSpace this chart belongs to")
        .def_readonly("info", &ChartType::info, "Description of this chart")
        .def("__repr__", 
             [](const ChartType& c){
                    std::stringstream ss;
                    ss << c;
                    return ss.str();
            });

    using PointType = ManifoldPoint<MetricSpaceType>;
    py::class_<PointType>(sub, "ManifoldPoint", "A point on a manifold")
        .def(py::init<const ChartType&, const EigenRk1Tensor<D>&>(),
            "Construct from coordinates in given chart",
            "chart"_a, "coordinates"_a)
        .def("coordinates", (EigenRk1Tensor<D> (PointType::*)(const ChartType &) const) &PointType::coordinates,
             "The coordinates of this point in out_chart", "chart"_a)
        .def("coordinates", (EigenRk1Tensor<D> (PointType::*)() const) &PointType::internal_coordinates,
             "The coordinates of this point in the internal chart")
        .def("on_chart", &PointType::on_chart,
             "Check if this point can be represented in the given chart",
             "chart"_a)
        .def_property_readonly("M", &PointType::M, 
            "The MetricSpace where this point is defined")
        .def_property_readonly("chart", &PointType::chart)
        .def("__repr__", 
             [](const PointType& p){
                    std::stringstream ss;
                    ss << p;
                    return ss.str();
            });

    //geometry/tensor.h
    using Tangent = TangentVector<MetricSpaceType>;
    using Cotangent = CotangentVector<MetricSpaceType>;
    //TODO: how to define the index operations between different tensors?
    {
    auto t = tensor_common<Tangent, MetricSpaceType>(sub, "TangentVector");
    lower_index_def<0>(
        t, "lower_index",
        "Lower the index to get the corresponding cotangent vector");
    }
    {
    auto t = tensor_common<Cotangent, MetricSpaceType>(sub, "CotangentVector");
    raise_index_def<0>(
        t, "raise_index",
        "Raise the index to get the corresponding tangent vector");
    }
    {
    auto t = tensor_common<Tensor<MetricSpaceType, Cov, Cov>, MetricSpaceType>
        (sub, "TensorCovCov");
    raise_index_def<0>(t);
    raise_index_def<1>(t);
    }
    {
    auto t = tensor_common<Tensor<MetricSpaceType, Cnt, Cov>, MetricSpaceType>
        (sub, "TensorCntCov");
    lower_index_def<0>(t);
    raise_index_def<1>(t);
    }
    {
    auto t = tensor_common<Tensor<MetricSpaceType, Cov, Cnt>, MetricSpaceType>
        (sub, "TensorCovCnt");
    lower_index_def<1>(t);
    raise_index_def<0>(t);
    }
    {
    auto t = tensor_common<Tensor<MetricSpaceType, Cnt, Cnt>, MetricSpaceType>
        (sub, "TensorCntCnt");
    lower_index_def<0>(t);
    lower_index_def<1>(t);
    }
    {
    tensor_common<Tensor<MetricSpaceType, Cov, Cov, Cov, Cov>, MetricSpaceType>
        (sub, "TensorCovCovCovCov");
    }



    //geometry/utils.h
    sub
    .def("dot_product", (double (*)(const Tangent&, const Tangent&))
                        &dot_product,
        "Compute the dot product of the vectors")
    .def("dot_product", (double (*)(const Tangent&, const Cotangent&))
                        &dot_product)
    .def("dot_product", (double (*)(const Cotangent&,const Tangent&))
                        &dot_product)
    .def("dot_product", (double (*)(const Cotangent&, const Cotangent&))
                        &dot_product)
    .def("normalized", (Tangent (*)(const Tangent&)) &normalized,
         "Return a copy of the vector normalized to square norm +-1.\n"
         "Doesn't work for lightlike vectors.")
    .def("normalized", (Cotangent (*)(const Cotangent&)) &normalized)
    .def("project_orthogonal", &project_orthogonal<MetricSpaceType>,
         "Project b onto the subspace orthogonal to a.\n"
         "The vector a can not be lightlike.", "a"_a, "b"_a)
    ;
    


    //geometry/surface.h
    //XXX: why did the old implementation have std::unique_ptr in the params?
    py::class_<Surface<MetricSpaceType>,
               PySurface<MetricSpaceType>>(sub, "Surface",
               "A surface defined by S(x)=0.\n"
               "New surfaces can defined by deriving from this class and"
               "defining the functions value, gradient and observer.\n"
               "Remember to call the __init__ of this class when implementing"
               " a custom __init__")
        .def(py::init<const MetricSpaceType&>(),
            "M is the MetricSpace where this surface is defined", "M"_a)
        .def("value", &Surface<MetricSpaceType>::value,
             "The value of S at point", "point"_a)
        .def("gradient", &Surface<MetricSpaceType>::gradient,
             "The gradient cotangent vector dS at point", "point"_a)
        .def("observer", &Surface<MetricSpaceType>::observer,
             "The four-velocity of an observer comoving with the surface",
             "point"_a)
        .def("ignore_at", &Surface<MetricSpaceType>::ignore_at,
             "Whether to terminate curves or to ignore the surface at this"
             " point", 
             "point"_a)
        .def("points_cross_surface",
             &Surface<MetricSpaceType>::points_cross_surface,
             "Check whether p1 and p2 are on different sides of the surface",
             "p1"_a, "p2"_a)
        .def_property_readonly("M", &Surface<MetricSpaceType>::M,
            "The MetricSpace where this surface is defined");
    
    //geometry/parametrized_point.h 
    //geometry/parametrized_curve.h
    //geometry/geodesic.h 
    {
    using AP = ParametrizedPoint<MetricSpaceType>;
    parametrized_point<MetricSpaceType>
    (sub, "ParametrizedPoint", 
    "Parametrized point with no transported objects");

    using Geo = Geodesic<MetricSpaceType>;
    auto ac = parametrized_curve<MetricSpaceType>
                  (sub, "ParametrizedCurve", 
                  "Base ParametrizedCurve of Geodesic");
    py::class_<Geo>(sub, "Geodesic", ac)
        .def(py::init<const AP&>(), 
            "Construct from the initial ParametrizedPoint", "initial_point"_a)
        .def(py::init<const Tangent&>(), 
            "Construct from a TangentVector u. The initial curve_parameter = 0",
            "u"_a)
        .def_property_readonly("likeness", &Geo::likeness, 
            "The likeness of the tangent vector of this geodesic");    
    
    }

    
    {
    using AP = ParametrizedPoint<MetricSpaceType, Tangent>;
    parametrized_point_w_transport<MetricSpaceType, Tangent>(
        sub, "ParametrizedPoint_TangentVector",
        "Parametrized point parallel transporting a tangent vector");

    using Geo = Geodesic<MetricSpaceType, Tangent>;
    auto ac = parametrized_curve<MetricSpaceType, Tangent>
                  (sub, "ParametrizedCurve_TangentVector",
                   "Base ParametrizedCurve of Geodesic_TangentVector");
    py::class_<Geo>(sub, "Geodesic_TangentVector", ac)
        .def(py::init<const AP&>(),
            "Construct from the initial ParametrizedPoint", "initial_point"_a)
        .def(py::init<const Tangent&, const Tangent&>(),
            "Construct from a TangentVector u and TangentVector"
            " transported_tangent."
            " The initial curve_parameter = 0",
            "u"_a, "transported_tangent"_a)
        .def_property_readonly("likeness", &Geo::likeness,
            "The likeness of the tangent vector of this geodesic");    
    }

    {
    using AP = ParametrizedPoint<MetricSpaceType,
                           Tensor<MetricSpaceType, Cov, Cov>>;
    parametrized_point_w_transport<MetricSpaceType,
                             Tensor<MetricSpaceType, Cov, Cov>>
    (sub, "ParametrizedPoint_TensorCovCov", 
     "Parametrized point parallel "
     "transporting a tensor with two covariant indices");

    using Geo = Geodesic<MetricSpaceType, Tensor<MetricSpaceType, Cov, Cov>>;
    auto ac =
        parametrized_curve<MetricSpaceType, Tensor<MetricSpaceType, Cov, Cov>>(
            sub, "ParametrizedCurve_TensorCovCov",
            "Base ParametrizedCurve of Geodesic_TensorCovCov");
    py::class_<Geo>(sub, "Geodesic_TensorCovCov", ac)
        .def(py::init<const AP&>(),
            "Construct from the initial ParametrizedPoint", "initial_point"_a)
        .def(py::init<const Tangent&, const Tensor<MetricSpaceType, Cov, Cov>&>(),
            "Construct from a TangentVector u and TensorCovCov"
            " transported_tensor."
            " The initial curve_parameter = 0",
            "u"_a, "transported_tensor"_a)
        .def_property_readonly("likeness", &Geo::likeness,
            "The likeness of the tangent vector of this geodesic");    
    
    }


    //
    //geometry/metric_space.h
    
    // get the base MetricSpace type
    using MS = typename MetricSpaceType::MetricSpace;
    auto metric_space = py::class_<MS>(sub, "MetricSpace");

    metric_space
        .def_readwrite("configuration", &MS::configuration,
                       "The configuration used in computations")
        .def_property_readonly("name", &MS::name)
        // cast to function pointer to resolve the overload
        .def("create_point",
             static_cast<PointType (MS::*)(const ChartType &,
                                           const EigenRk1Tensor<D> &) const>(
                 &MS::create_point),
             "Convenience method for constructing a point from given"
             " coordinates.",
             "chart"_a, "coordinates"_a)
        .def("use_fixed_chart", &MS::use_fixed_chart,
             "Set the fixed chart to be used instead of the best chart",
             "chart"_a)
        .def("use_best_chart", &MS::use_best_chart,
             "Go back to using the numerically best chart")
        .def_property_readonly("uses_multiple_charts",
                               &MS::uses_multiple_charts,
                               "Whether multiple charts are currently in use")
        .def("transition_function", &MS::transition_function,
             "Compute the coordinates in to_chart from coordinates given in "
             "from_chart",
             "from_chart"_a, "to_chart"_a, "coordinates"_a)
        .def("jacobian", &MS::jacobian,
             "The Jacobian matrix from from_chart to to_chart at x", "x"_a,
             "from_chart"_a, "to_chart"_a)
        .def("metric_components", &MS::metric_components,
             "Components of the metric tensor at x in given chart", "x"_a,
             "chart"_a)
        .def("inverse_metric_components", &MS::inverse_metric_components,
             "Components of the inverse metric tensor at x in given chart",
             "x"_a, "chart"_a)
        .def("metric_derivatives", &MS::metric_derivatives,
             "Components of g_ab,c at x in given chart", "x"_a, "chart"_a)
        .def("metric_derivatives_num", &MS::metric_derivatives_num,
             "metric_derivatives computed numerically from metric_components",
             "x"_a, "chart"_a, "order"_a)
        .def("metric_determinant", &MS::metric_determinant,
             "Determinant of the metric at x in given chart", "x"_a, "chart"_a)
        .def("metric_tensor", &MS::metric_tensor, "The metric tensor at x",
             "x"_a)
        .def("inverse_metric_tensor", &MS::inverse_metric_tensor,
             "The inverse metric tensor at x", "x"_a)
        .def("christoffel_symbols", &MS::christoffel_symbols,
             "The Cristoffel symbols of the second kind at x in given chart",
             "x"_a, "chart"_a)
        .def("christoffel_symbols_contracted", 
             &MS::christoffel_symbols_contracted,
             "The Cristoffel symbols of the second kind at x in given chart "
             "contracted with the tangent vector given as components",
             "x"_a, "chart"_a, "tangent_components"_a)
        .def("get_best_chart", &MS::get_best_chart,
             "Find the numerically most stable chart at x. If use_fixed_chart"
             " has been called, returns the fixed chart instead.",
             "x"_a, py::return_value_policy::reference_internal)
        .def("get_best_integration_chart", &MS::get_best_integration_chart,
             "Find the chart where geodesic integration has the best behaviour."
             " If use_fixed_chart"
             " has been called, returns the fixed chart instead.",
             "u"_a, py::return_value_policy::reference_internal)
        .def("choose_better_chart", &MS::choose_better_chart,
             "Selects the chart that is numerically more stable at x. "
             " If use_fixed_chart"
             " has been called, returns the fixed chart instead.",
             "x"_a, "chart1"_a, "chart2"_a)
        .def("__repr__", [](const MS &m) {
            std::stringstream ss;
            ss << m;
            return ss.str();
        });

    lorentzian_specific_definitions<MetricSpaceType>(sub, metric_space);


    // geometry/normal_coordinates.h
/*
 *    .def("normalized", (Tangent (*)(const Tangent&)) &normalized,
 *         "Return a copy of the vector normalized to square norm +-1.\n"
 *         "Doesn't work for lightlike vectors.")
 *
 *    .def("normalized", (Cotangent (*)(const Cotangent&)) &normalized)
 *
 *    .def("coordinates", (EigenRk1Tensor<D> (PointType::*)(const ChartType &) const) &PointType::coordinates,
 *         "The coordinates of this point in out_chart", "chart"_a)
 *    .def("coordinates", (EigenRk1Tensor<D> (PointType::*)() const) &PointType::internal_coordinates,
 *         "The coordinates of this point in the internal chart")
 */

    sub.def("riemann_normal_coordinates",
            (EigenRk1Tensor<D> (*)(const ChartType&,
                                   const PointType&,
                                   const PointType&,
                                   const NormalCoordinateMethod&)) &riemann_normal_coordinates<MetricSpaceType>,
            "Computes the Riemann normal coordinates of given point with "
            "respect to given origin",
            "chart"_a, "origin"_a, "point"_a, "method"_a);

    sub.def("riemann_normal_coordinates",
            (EigenRk1Tensor<D> (*)(const ChartType&,
                                   const PointType&,
                                   const PointType&,
                                   const NormalCoordinateMethod&,
                                   const EigenRk1Tensor<D>&)) &riemann_normal_coordinates<MetricSpaceType>,
            "Computes the Riemann normal coordinates of given point with "
            "respect to given origin",
            "chart"_a, "origin"_a, "point"_a, "method"_a, "guess"_a);

    return sub;
}
