
#include <arcmancer/other_spaces/two_sphere.h>
#include "pyarcmancer.h"

using namespace arcmancer::two_sphere;

// Function defining the two-sphere spacetime submodule
void two_sphere(py::module &m){

    auto sub 
        = metric_space_submodule<TwoSphere>(m, "two_sphere");
    py::class_<TwoSphere, 
               MetricSpace<TwoSphere, 2, OtherSignature>>(sub,
                                       "TwoSphere")
        .def(py::init<>())
        .def_readonly("spherical", &TwoSphere::spherical)
        .def_readonly("rotated_spherical", &TwoSphere::rotated_spherical);
}
