#include <arcmancer/spacetimes/agm.h>
#include "pyarcmancer.h"

using namespace arcmancer::agm;
void agm(py::module &m){

    auto sub = metric_space_submodule<AGMSpacetime>(m, "agm");
    py::class_<AGMSpacetime, 
               MetricSpace<AGMSpacetime,4>>(sub, "AGMSpacetime")
        .def(py::init<double, double, double>())
        .def_readonly("isotropic", &AGMSpacetime::isotropic);

    py::class_<AGMNeutronStarSurface, Surface<AGMSpacetime>>(sub, "AGMNeutronStarSurface")
        .def(py::init<const AGMSpacetime&>());

}
