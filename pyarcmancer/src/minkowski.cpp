#include <arcmancer/spacetimes/minkowski.h>
#include "pyarcmancer.h"

using namespace arcmancer::minkowski;
//Function defining the minkowski spacetime submodule
void minkowski(py::module &m){
    
    auto minkowski_sub 
        = metric_space_submodule<MinkowskiSpacetime>(m, "minkowski");
    py::class_<MinkowskiSpacetime, 
               MetricSpace<MinkowskiSpacetime,4>>(minkowski_sub,
                                                "MinkowskiSpacetime")
        .def(py::init<>())
        .def_readonly("cartesian", &MinkowskiSpacetime::cartesian)
        .def_readonly("spherical", &MinkowskiSpacetime::spherical);
}
