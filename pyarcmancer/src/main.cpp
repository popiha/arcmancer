#include "pyarcmancer.h"

#include <arcmancer/appinfo.h>
#include <arcmancer/log.h>
#include <arcmancer/units.h>
#include <arcmancer/configuration.h>
#include <arcmancer/radiation/radiation_utils.h>
#include <arcmancer/geometry/normal_coordinates.h>

//The different metric spaces are defined in their own files, forward declare
//the definition functions here.
void minkowski(py::module&);
void agm(py::module&);
void ht(py::module&);
void kerr(py::module&);
void two_sphere(py::module&);
void generic_spacetime(py::module&);
void euclidean_instantiations(py::module&);

PYBIND11_MODULE(pyarcmancer, m) {
    
    m.doc() = "Python bindings for the Arcmancer library.";
    
    m.attr("__version__") = ARCMANCER_VERSION_STRING;
    
    //Metric space independent parts
    //
    // log.h
    py::enum_<Log::level>(m, "LogLevel")
        .value("trace"    , Log::level::trace)
        .value("debug"    , Log::level::debug)
        .value("info"     , Log::level::info)
        .value("warn"     , Log::level::warn)
        .value("error"    , Log::level::err)
        .value("critical" , Log::level::critical)
        .value("off"      , Log::level::off);

    py::class_<spd::logger>(m, "Logger")
        .def("trace"     , (void (spd::logger::*)(const std::string&)) &spd::logger::trace<std::string>)
        .def("debug"     , (void (spd::logger::*)(const std::string&)) &spd::logger::debug<std::string>)
        .def("info"      , (void (spd::logger::*)(const std::string&)) &spd::logger::info<std::string>)
        .def("warn"      , (void (spd::logger::*)(const std::string&)) &spd::logger::warn<std::string>)
        .def("error"     , (void (spd::logger::*)(const std::string&)) &spd::logger::error<std::string>)
        .def("critical"  , (void (spd::logger::*)(const std::string&)) &spd::logger::critical<std::string>)
        .def("set_level" , (void (spd::logger::*)(const spd::level::level_enum)) &spd::logger::set_level)
        .def("level"     , (spd::level::level_enum (spd::logger::*)()) &spd::logger::level);

    py::class_<Log>(m, "Log")
        .def_static("set_level"   , [](const spd::level::level_enum &l) { Log::log()->set_level(l); })
        .def_static("level"       , []() { return Log::log()->level(); })
        .def_static("trace"       , [](const std::string &str) { Log::log()->trace(str); })
        .def_static("debug"       , [](const std::string &str) { Log::log()->debug(str); })
        .def_static("info"        , [](const std::string &str) { Log::log()->info(str); })
        .def_static("warn"        , [](const std::string &str) { Log::log()->warn(str); })
        .def_static("error"       , [](const std::string &str) { Log::log()->error(str); })
        .def_static("critical"    , [](const std::string &str) { Log::log()->critical(str); })
        .def_static("set_console" , []() { Log::set_default_logger(Log::console); })
        .def_static("set_file"    , []() { Log::set_default_logger(Log::file); })
        .def_static("set_combo"   , []() { Log::set_default_logger(Log::combo); })
        ;


    //
    // configuration.h
    //
    py::class_<configuration::Tolerance>(m, "Tolerance")
        .def(py::init<>())
        .def_readwrite("relative",
                      &configuration::Tolerance::relative)
        .def_readwrite("absolute",
                      &configuration::Tolerance::absolute);

    py::class_<configuration::HenonStepping>(m, "HenonStepping")
        .def(py::init<>())
        .def_readwrite("tolerance",
                      &configuration::HenonStepping::tolerance)
        .def_readwrite("maximum_steps",
                      &configuration::HenonStepping::maximum_steps);

    py::class_<configuration::Propagation>(m, "Propagation")
        .def(py::init<>())
        .def_readwrite("tolerance",
                      &configuration::Propagation::tolerance)
        .def_readwrite("enforce_sampling_interval",
                       &configuration::Propagation::enforce_sampling_interval)
        .def_readwrite("sampling_interval",
                       &configuration::Propagation::sampling_interval)
        .def_readwrite("enforce_maximum_stepsize",
                       &configuration::Propagation::enforce_maximum_stepsize)
        .def_readwrite("enforce_minimum_stepsize",
                       &configuration::Propagation::enforce_minimum_stepsize)
        .def_readwrite("maximum_stepsize",
                       &configuration::Propagation::maximum_stepsize)
        .def_readwrite("minimum_stepsize",
                       &configuration::Propagation::minimum_stepsize)
        .def_readwrite("enforce_maximum_steps",
                       &configuration::Propagation::enforce_maximum_steps)
        .def_readwrite("maximum_steps",
                       &configuration::Propagation::maximum_steps);


    py::class_<configuration::NumericalOptimization>(m, "NumericalOptimization")
        .def(py::init<>())
        .def_readwrite("iterations",
                      &configuration::NumericalOptimization::iterations)
        .def_readwrite("x_tol",
                      &configuration::NumericalOptimization::x_tol)
        .def_readwrite("f_tol",
                      &configuration::NumericalOptimization::f_tol)
        .def_readwrite("min_grad_norm",
                      &configuration::NumericalOptimization::min_grad_norm);

    py::class_<configuration::Configuration> conf(m, "Configuration");

    conf.def(py::init<>())
        .def_readwrite("chart_selection_cond_tol",
                       &configuration::Configuration::chart_selection_cond_tol)
        .def_readwrite("pointwise",
                       &configuration::Configuration::pointwise)
        .def_readwrite("curve",
                       &configuration::Configuration::curve)
        .def_readwrite("line_integral",
                       &configuration::Configuration::line_integral)
        .def_readwrite("barycentric_interpolation",
                       &configuration::Configuration::barycentric_interpolation)
        .def_readwrite("normal_coordinates",
                       &configuration::Configuration::normal_coordinates);

    // define nested structs within Configuration
    py::class_<configuration::Configuration::Pointwise>(conf, "Pointwise")
        .def(py::init<>())
        .def_readwrite("tolerance",
                       &configuration::Configuration::Pointwise::tolerance);

    py::class_<configuration::Configuration::Curve>(conf, "Curve")
        .def(py::init<>())
        .def_readwrite("propagation",
                       &configuration::Configuration::Curve::propagation)
        .def_readwrite("henon",
                       &configuration::Configuration::Curve::henon)
        .def_readwrite("enforce_surface_stepsize_reduction",
                       &configuration::Configuration::Curve::enforce_surface_stepsize_reduction)
        .def_readwrite("surface_stepsize_reduction_tolerance",
                       &configuration::Configuration::Curve::surface_stepsize_reduction_tolerance)
        .def_readwrite("failed_step_penalty",
                       &configuration::Configuration::Curve::failed_step_penalty)
        .def_readwrite("chart_switch_failed_step_score",
                       &configuration::Configuration::Curve::chart_switch_failed_step_score);

    py::class_<configuration::Configuration::LineIntegral>(conf, "LineIntegral")
        .def(py::init<>())
        .def_readwrite("propagation",
                       &configuration::Configuration::LineIntegral::propagation);

    py::class_<configuration::Configuration::BarycentricInterpolation>(conf, "BarycentricInterpolation")
        .def(py::init<>())
        .def_readwrite("optimization",
                       &configuration::Configuration::BarycentricInterpolation::optimization)
        .def_readwrite("tolerance",
                       &configuration::Configuration::BarycentricInterpolation::tolerance);

    py::class_<configuration::Configuration::NormalCoordinates>(conf, "NormalCoordinates")
        .def(py::init<>())
        .def_readwrite("optimization",
                       &configuration::Configuration::NormalCoordinates::optimization)
        .def_readwrite("tolerance",
                       &configuration::Configuration::NormalCoordinates::tolerance);

    //
    //geometry/types.h
    py::enum_<VectorLikeness>(m, "VectorLikeness")
        .value("LightLike", VectorLikeness::Lightlike)
        .value("Timelike", VectorLikeness::Timelike)
        .value("Spacelike", VectorLikeness::Spacelike)
        .value("Undefined", VectorLikeness::Undefined);

    //
    //geometry/utils.h
    py::enum_<DifferenceOrder>(m, "DifferenceOrder")
        .value("order_2", DifferenceOrder::order_2)
        .value("order_4", DifferenceOrder::order_4)
        .value("order_6", DifferenceOrder::order_6);

    //
    //geometry/normal_coordinates.h
    py::enum_<NormalCoordinateMethod>(m, "NormalCoordinateMethod")
        .value("numerical", NormalCoordinateMethod::numerical)
        .value("order_2", NormalCoordinateMethod::order_2);

    //
    //radiation/stokes.h

    py::class_<StokesVector>(m, "StokesVector")
        .def(py::init<double, Eigen::Vector4d>(),
             "frequency"_a, "invariant_IQUV"_a)
        .def_property_readonly("frequency", &StokesVector::frequency)
        .def_property_readonly("invariant_IQUV_vector",
                               &StokesVector::invariant_IQUV_vector)
        .def_property_readonly("rest_frame_IQUV_vector",
                               &StokesVector::rest_frame_IQUV_vector)
        .def_property_readonly("rest_frame_I", &StokesVector::rest_frame_I)
        .def_property_readonly("rest_frame_Q", &StokesVector::rest_frame_Q)
        .def_property_readonly("rest_frame_U", &StokesVector::rest_frame_U)
        .def_property_readonly("rest_frame_V", &StokesVector::rest_frame_V)
        .def("__repr__", [](StokesVector &s) {
            std::stringstream ss;
            ss << "StokesVector:\n"
               << "frequency " << s.frequency() << "\ninvariant IQUV "
               << s.invariant_IQUV_vector().transpose();
            return ss.str();
        });

    py::class_<PolarizationSpectrum>(m, "PolarizationSpectrum")
        .def(py::init<const std::vector<StokesVector> &>(), "stokes_vectors"_a)
        .def_property_readonly("stokes_vectors",
                               &PolarizationSpectrum::stokes_vectors)
        .def("__getitem__", [](const PolarizationSpectrum &p, size_t i) {
            if (i >= p.size()) { 
                throw py::index_error("Index out of range");
            }
            return p[i];
        });
    //
    //radiation/radiation_types.h

    py::class_<RadiationData>(m, "RadiationData",
                              "Emissivity and Mueller matrix components")
        .def(py::init<>())
        .def(py::init<double, double, double, double, double, double, double,
                      double, double, double, double, double>(),
             "j_I"_a, "j_Q"_a, "j_U"_a, "j_V"_a, "a_I"_a, "a_Q"_a, "a_U"_a,
             "a_V"_a, "r_Q"_a, "r_U"_a, "r_V"_a, "length_unit"_a)
        .def_readwrite("j_I", &RadiationData::j_I)
        .def_readwrite("j_Q", &RadiationData::j_Q)
        .def_readwrite("j_U", &RadiationData::j_U)
        .def_readwrite("j_V", &RadiationData::j_V)
        .def_readwrite("a_I", &RadiationData::a_I)
        .def_readwrite("a_Q", &RadiationData::a_Q)
        .def_readwrite("a_U", &RadiationData::a_U)
        .def_readwrite("a_V", &RadiationData::a_V)
        .def_readwrite("r_Q", &RadiationData::r_Q)
        .def_readwrite("r_U", &RadiationData::r_U)
        .def_readwrite("r_V", &RadiationData::r_V)
        .def_readwrite("length_unit", &RadiationData::length_unit);

    //
    // radiation_utils.h
    m.def("electron_scattering_atmosphere", &electron_scattering_atmosphere,
          "theta"_a);

    //
    // units.h
    {
    using namespace units;
    auto units_mod = m.def_submodule("units");
    units_mod.attr("fine_structure_constant") = fine_structure_constant;
    units_mod.attr("N_A") = N_A;

    units_mod.attr("c_SI") = c_SI;
    units_mod.attr("G_SI") = G_SI;
    units_mod.attr("k_SI") = k_SI;
    units_mod.attr("sigma_SB_SI") = sigma_SB_SI;
    units_mod.attr("h_SI") = h_SI;
    units_mod.attr("hbar_SI") = hbar_SI;
    units_mod.attr("epsilon0_SI") = epsilon0_SI;
    units_mod.attr("mu0_SI") = mu0_SI;
    units_mod.attr("e_SI") = e_SI;
    units_mod.attr("m_e_SI") = m_e_SI;
    units_mod.attr("m_p_SI") = m_p_SI;
    units_mod.attr("u_SI") = u_SI;
    units_mod.attr("eV_SI") = eV_SI;
    units_mod.attr("M_sun_SI") = M_sun_SI;
    units_mod.attr("R_sun_SI") = R_sun_SI;
    units_mod.attr("L_sun_SI") = L_sun_SI;
    units_mod.attr("T_sun_SI") = T_sun_SI;
    
    units_mod.attr("c_cgs") = c_cgs;
    units_mod.attr("G_cgs") = G_cgs;
    units_mod.attr("k_cgs") = k_cgs;
    units_mod.attr("sigma_SB_cgs") = sigma_SB_cgs;
    units_mod.attr("h_cgs") = h_cgs;
    units_mod.attr("hbar_cgs") = hbar_cgs;
    units_mod.attr("e_cgs") = e_cgs;
    units_mod.attr("m_e_cgs") = m_e_cgs;
    units_mod.attr("m_p_cgs") = m_p_cgs;
    units_mod.attr("u_cgs") = u_cgs;
    units_mod.attr("eV_cgs") = eV_cgs;
    units_mod.attr("M_sun_cgs") = M_sun_cgs;
    units_mod.attr("R_sun_cgs") = R_sun_cgs;
    units_mod.attr("L_sun_cgs") = L_sun_cgs;
    units_mod.attr("T_sun_cgs") = T_sun_cgs;
    }
    
    //
    // Call the metric space definition functions
    
    minkowski(m);
    agm(m);
    ht(m);
    kerr(m);
    two_sphere(m);
    generic_spacetime(m);
    
    euclidean_instantiations(m);
}

