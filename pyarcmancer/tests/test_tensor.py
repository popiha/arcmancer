import unittest

import numpy as np

from pyarcmancer.minkowski import *



class TestTensorArrayConversions(unittest.TestCase):
    """
    Tests for correctness of numpy array <-> Eigen::TensorFixedSize or Tensor 
    component storage conversions.
    """

    def setUp(self):
        self.M = MinkowskiSpacetime()
        self.x = ManifoldPoint(self.M.cartesian, [1,2,3,4])

# To make these tests properly test different parts separately,
# should define some test values on the C++ side.
# For now just test some known non-trivial outputs and round-trips.

# Output of some tensors defined on the C++ side
    def test_rk3_out(self):
        g_ders = self.M.metric_derivatives(self.x, self.M.spherical)
        g_ders_expected = np.zeros((4,4,4))
        _,r,th,_ = self.x.coordinates(self.M.spherical)
        g_ders_expected[2,2,1] = -2*r
        g_ders_expected[3,3,1] = -2*r*np.sin(th)**2
        g_ders_expected[3,3,2] = -2 * r**2 * np.sin(th) * np.cos(th)

        self.assertTrue(np.allclose(g_ders, g_ders_expected))

# C-continuous array input
    def test_rk1_c_cont_in(self):

        comp = np.array([1,2,3,4]).astype(float, order='C')
        A = TangentVector(self.x, self.M.cartesian, comp)
        self.assertTrue(np.all(comp == A.components(self.M.cartesian)))
# Check that a round trip works 
        A2 = TangentVector(self.x, self.M.cartesian, 
                           A.components(self.M.cartesian))
        self.assertTrue(np.all(comp == A2.components(self.M.cartesian)))

# Also check that the components are the same in the printed output.
# This depends on the exact output format, but is a good sanity check
# since printing is handled by the Eigen implementation.
        comp_parsed = list(map(float, repr(A).split('\n')[-2].split()[-4:]))
        self.assertTrue(np.all(comp == comp_parsed))


    def test_rk2_c_cont_in(self):
        comp = np.random.random((4,4)).astype(float, order='C')
        A = TensorCovCov(self.x, self.M.cartesian, comp)
        self.assertTrue(np.all(comp == A.components(self.M.cartesian)))
        A2 = TensorCovCov(self.x, self.M.cartesian, 
                          A.components(self.M.cartesian))
        self.assertTrue(np.all(comp == A2.components(self.M.cartesian)))
        
        comp_parsed = np.array(list(
                        map(lambda s: list(map(float,s.split())),
                            repr(A).split('\n')[3:])))
        self.assertTrue(np.allclose(comp, comp_parsed))
        
    def test_rk4_c_cont_in(self):
        comp = np.random.random((4,4,4,4)).astype(float, order='C')
        A = TensorCovCovCovCov(self.x, self.M.cartesian, comp)
        self.assertTrue(np.all(comp == A.components(self.M.cartesian)))
        A2 = TensorCovCovCovCov(self.x, self.M.cartesian, 
                                A.components(self.M.cartesian))
        self.assertTrue(np.all(comp == A2.components(self.M.cartesian)))
    
# F-continuous array input
    def test_rk1_f_cont_in(self):
        comp = np.random.random((4)).astype(float, order='F')
        A = TangentVector(self.x, self.M.cartesian, comp)
        self.assertTrue(np.all(comp == A.components(self.M.cartesian)))
        A2 = TangentVector(self.x, self.M.cartesian, 
                           A.components(self.M.cartesian))
        self.assertTrue(np.all(comp == A2.components(self.M.cartesian)))

    def test_rk2_f_cont_in(self):
        comp = np.random.random((4,4)).astype(float, order='F')
        A = TensorCovCov(self.x, self.M.cartesian, comp)
        self.assertTrue(np.all(comp == A.components(self.M.cartesian)))
        A2 = TensorCovCov(self.x, self.M.cartesian, 
                          A.components(self.M.cartesian))
        self.assertTrue(np.all(comp == A2.components(self.M.cartesian)))
        
    def test_rk4_f_cont_in(self):
        comp = np.random.random((4,4,4,4)).astype(float, order='F')
        A = TensorCovCovCovCov(self.x, self.M.cartesian, comp)
        self.assertTrue(np.all(comp == A.components(self.M.cartesian)))
        A2 = TensorCovCovCovCov(self.x, self.M.cartesian, 
                                A.components(self.M.cartesian))
        self.assertTrue(np.all(comp == A2.components(self.M.cartesian)))

# Slice input. 
# Just using some slices that are not trivial, could make more complete 
# but this seems fairly convincing
    def test_rk1_slice_in(self):
        comp = np.random.random((8,2))[::2,1]
        A = TangentVector(self.x, self.M.cartesian, comp)
        self.assertTrue(np.all(comp == A.components(self.M.cartesian)))

        A2 = TangentVector(self.x, self.M.cartesian, 
                           A.components(self.M.cartesian))
        self.assertTrue(np.all(comp == A2.components(self.M.cartesian)))

    def test_rk2_slice_in(self):
        comp = np.random.random((2,4,12))[0,:,::3]
        A = TensorCovCov(self.x, self.M.cartesian, comp)
        self.assertTrue(np.all(comp == A.components(self.M.cartesian)))
        
        A2 = TensorCovCov(self.x, self.M.cartesian, 
                          A.components(self.M.cartesian))
        self.assertTrue(np.all(comp == A2.components(self.M.cartesian)))
        
    def test_rk4_slice_in(self):
        comp = np.random.random((6,16,8,4,4))[1:5, 4:12:2, 2:6, 3]
        A = TensorCovCovCovCov(self.x, self.M.cartesian, comp)
        self.assertTrue(np.all(comp == A.components(self.M.cartesian)))
        
        A2 = TensorCovCovCovCov(self.x, self.M.cartesian, 
                                A.components(self.M.cartesian))
        self.assertTrue(np.all(comp == A2.components(self.M.cartesian)))
