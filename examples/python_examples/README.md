
This directory includes various examples of using Arcmancer through the
pyarcmancer Python module.
The examples are described in more detail below and in the comments in the
code.
They require that pyarcmancer has been built and added to `PYTHONPATH`.
Alternatively, you can add the following snippet at the beginning of the 
scripts:

    import sys
    sys.path.append('path/to/arcmancer/lib')

Additional module dependencies include numpy, scipy and matplotlib.


charged_particle.py
-------------------

This example shows how to calculate the trajectory of a charged particle in a
given external electromagnetic field in flat spacetime.
Additionally, a Fermi-Walker transported frame is attached to the particle.


extended_kerr.py
----------------

Computes and plots the path of an infalling particle through the extended Kerr
spacetime. The Kerr-Schild charts and the Boyer-Lindquist chart overlap both
outside the outer horizon as well as the inside the inner horizon with the 
current definitions, but the KS charts actually connect to different 
asymptotically flat external regions,
and the particle can pass through the hole to these regions.

The sections of the trajectory which are computed in the ingoing KS chart 
are shown in blue, while sections computed in the outgoing chart are shown
in orange.
These sections are shown in their respective charts, as no chart covering the
whole path is implemented.
The different pieces are rotated to align them, but as they are still
represented in different charts they do not join smoothly.


generic_spacetime.py
--------------------

This example illustrates defining spacetime geometries from Python using the
`GenericSpacetime` class.
`GenericSpacetime` allows defining the metric and its derivatives as Python 
functions. It has a single coordinate chart named `chart`.
This example  also shows how to compute non-geodesic curves.


kerr_shadow.py
--------------

A simple example of computing an image of the Kerr black hole shadow and a
coordinate grid on the sky.


norm_surface.py
---------------

A simple example of defining a custom surface and computing its image in
perspective projection. 


rad_transfer.py
---------------

A simple example of computing radiative transfer over a single ray.
Constructs simple fluid and radiation models and computes radiative transfer
for two different fluid velocities.


