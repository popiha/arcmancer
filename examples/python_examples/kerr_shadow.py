import numpy as np
import matplotlib.pyplot as plt

# Import the Arcmancer python module.
import pyarcmancer
from pyarcmancer.kerr import *

# Silence some logging we're not interested in.
pyarcmancer.Log.set_level(pyarcmancer.LogLevel.info)

# Setup the spacetime.
chi = .99
M = KerrSpacetime(1, chi)

# Use lower tolerances and disable automatic chart switching for a faster 
# calculation.
M.configuration.curve.propagation.tolerance.relative = 1e-6
M.use_fixed_chart(M.kerr_schild_out)

# Setup the observer and image plane
r0 = 5000 # image plane distance
inc = 90  # inclination in degrees

# Point at the centre of the image plane
x0 = ManifoldPoint(M.boyer_lindquist, [0, r0, np.radians(inc), 0])

# The local frame defining the image plane directions.
obs_frame = LorentzFrame(x0, # Location of the frame.
                         M.boyer_lindquist,
                         [1,0,0,0], # t basis vector components
                         [0,1,0,0], # z basis vector components
                         [0,0,0,1]  # x basis vector components
                        ) 
# Number of pixels
xbins= 200
ybins = xbins
# Half-widths of the image plane
xspan = 10
yspan = ybins/xbins*xspan # Ensure square pixels

# Construct the image plane
ip_shape = ImagePlane.PlaneShape(xbins, ybins, xspan, yspan)
image_plane = ImagePlane(ip_shape, obs_frame)

# Spherical surfaces representing the event horizon and the far away sky.
sky_sphere = OutgoingKerrSphere(M, 5*r0)
eh_sphere = OutgoingKerrSphere(M, M.horizon_r)

# The computation to perform for a single pixel.
# The function receives an object containing e.g. ray initial conditions as its
# only argument, and can perform arbitrary computations,
# for example radiative transfer.
def compute_func(ip):
# Construct the ray and extend it backwards in time for a long distance,
# or until it hits either of the surfaces.
    ray = Geodesic(ip.ray_tangent)
    ray.compute(-30*r0, [sky_sphere, eh_sphere])
    
# It is possible to return arbitrary python objects.
# Note however that returning ray points or their components may result in large
# memory consumption, as currently those objects are references to the objects
# stored by the curve and thus prevent deallocating the curve.

# Return hit point coordinates and whether the ray escaped, i.e. hit the sky.
    th, phi = ray.front.point.coordinates(M.boyer_lindquist)[2:]
    ret = {'hit_sky': ray.front_termination.hit_surface 
                      and ray.front_termination.surface_index == 0,
           'th' : th,
           'phi': phi}
    return ret

# Perform the computation.
image_plane.compute(compute_func)

# Extract the image data into arrays for plotting.
th      = np.zeros((ybins,xbins))
phi     = np.zeros((ybins,xbins))
hit_sky = np.zeros((ybins,xbins), dtype=bool)

for row in image_plane.data_points:
    for val in row:
        th[val.iy,val.ix] = val.data['th']
        phi[val.iy,val.ix] = val.data['phi']
        hit_sky[val.iy, val.ix] = val.data['hit_sky']

# Show a simple coordinate grid on the sky.
grid = .5 + .3 * np.round((1 + np.cos(5 * th) * np.sin(5 * phi)) / 2)
img = np.zeros((ybins, xbins))
img[hit_sky] = grid[hit_sky]

# Draw image and save.
plt.imshow(img, origin='lower', cmap='gray', interpolation='none',
           extent=(-xspan, xspan, -yspan, yspan))
plt.xlabel("$x/M$")
plt.ylabel("$y/M$")

plt.show()
