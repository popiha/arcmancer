import numpy as np
import matplotlib.pyplot as plt

import pyarcmancer
# Import the relevant parts of the Arcmancer Python module
from pyarcmancer import StokesVector, PolarizationSpectrum, RadiationData
# Geometric objects are templated based on the spacetime, so each spacetime
# has its own Python submodule.
from pyarcmancer.minkowski import *

# Display trace level logging to see information about the curve computation
# and radiation transfer. Comment out to hide the output.
pyarcmancer.Log.set_level(pyarcmancer.LogLevel.trace)

# Setup the spacetime.
M = MinkowskiSpacetime()

# Point at the end of the ray.
x0 = ManifoldPoint(M.cartesian, # Which coordinate system is used.
                   [0, 0, 0, 0] # The coordinates of the point.
                  )
# Ray tangent. The ray comes in along the positive x-axis.
k = TangentVector(x0, # The point where the vector is defined.
                  M.cartesian, # Coordinate system used for the components.
                  [1,-1,0,0] # Components of the vector.
                 )
# The polarization basis.
pol_frame = PolarizationFrame(
                TangentVector(x0, M.cartesian, [0,0,0,1]), # vertical
                TangentVector(x0, M.cartesian, [0,0,-1,0]) # horizontal
            )
# Construct the ray. 
# Geodesic is a version of ParametrizedCurve with external forces disallowed.
# The name of the type mirrors the C++ template syntax 
# Geodesic<MinkowskiSpacetime, PolarizationFrame>,
# which defines the transported object type.
ray = Geodesic_PolarizationFrame(k, pol_frame)
# Extend the ray backwards in time.
ray.compute(-100)

# Define the fluid and radiation models.
# Constructs the fluid model for given temperature and velocity.
def fluid(T, v):
# Returns a function that satisfies the fluid model interface,
# i.e. computes the properties of the fluid at a point X.
    return lambda X: \
        FluidData(1, # Number density, ignored in this example.
                  T,
                  0, 0, # Pressure and magnetic field strength, also ignored.
# The direction of the magnetic field / polarization reference direction.
                  TangentVector(X, M.cartesian, [0,0,0,1]),
# The four-velocity of the fluid.
                  normalized(TangentVector(X, M.cartesian, [1,v,0,0]))
                 )

# Radiation model.
# Arguments: fluid data, rest frame frequency of radiation nu,
# angle between ray and the reference direction th.
def radiation(fluid_data, nu, th):
# Black-body emission
    B_nu = 4 * np.pi * nu**3 / np.expm1(2 * np.pi * nu/fluid_data.temperature) 
    j_I = B_nu
    j_Q = .5*j_I
    
# Constant absorption coefficients
    a_I = 1
    a_Q = .1 # Does not follow Kirchoff's law to get net polarization.

# Other emissivities and Mueller matrix components are set to vanish.
    j_U = j_V = a_U = a_V = r_Q = r_U = r_V = 0
    return RadiationData(j_I, j_Q, j_U, j_V, 
                         a_I, a_Q, a_U, a_V, 
                         r_Q, r_U, r_V,
                         1 # Unit system scaling factor.
                        )

# Initial spectrum of invariant Stokes vectors, all vanish.
# This determines also the observed frequencies.
frequencies = np.linspace(.1, 20, 100)
initial_vals = PolarizationSpectrum(
                    [StokesVector(nu, [0,0,0,0]) for nu in frequencies])

# Compute radiation transfer for two fluid velocities.
res1 = radiation_transfer(ray, # Compute transfer over this ray.
                          k.components(M.cartesian)[0], # Normalization factor.
                          initial_vals,
                          fluid(10, 0),
                          radiation)

res2 = radiation_transfer(ray, k.components(M.cartesian)[0], initial_vals,
                          fluid(10, .5), radiation)

# Extract the observed Stokes vectors.
I1 = np.array([sv.rest_frame_IQUV_vector for sv in res1.stokes_vectors])
I2 = np.array([sv.rest_frame_IQUV_vector for sv in res2.stokes_vectors])

# Plot the results.
plt.plot(frequencies, I1[:,0], label='$I_1$')
plt.plot(frequencies, I1[:,1], label='$Q_1$')
plt.plot(frequencies, I2[:,0], label='$I_2$')
plt.plot(frequencies, I2[:,1], label='$Q_2$')

plt.xlim(.1,20)
plt.xlabel(r'$\nu$')
plt.ylabel(r'$I,Q$')
plt.legend()

plt.show()
