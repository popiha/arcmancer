import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

import pyarcmancer as pyac
from pyarcmancer.kerr import *


m,chi = 1, .9
M = KerrSpacetime(m,chi)

# Display trace level logging to see information about the curve computation
# and chart switches. Comment out to hide the output.
pyac.Log.set_level(pyac.LogLevel.trace)

# These options control chart switching, but the default values seem towork fine
# for the initial configuration here.
# If failed_step_penalty > chart_switch_failed_step_score, 
# switching is attempted on every rejected step, 
# otherwise several rejected steps are required.
# Increasing this makes the chart switching logic trigger less easily:
#M.configuration.chart_switch_failed_step_score = 15
# Increasing this triggers the switch more easily: 
#M.configuration.failed_step_penalty = 10

# Compute an infalling timelike geodesic
x0 = ManifoldPoint(M.kerr_schild_in, [0,1,1,3])
u = normalized(TangentVector(x0, M.boyer_lindquist, [1,0,0,0]))

geo = Geodesic(u)
res = geo.compute(50)
print(res)

outer_eh_r = M.horizon_r
inner_eh_r = 2-M.horizon_r
#extract coordinates and conserved quantities
lamda = [p.curve_parameter for p in geo]
carter = np.array([M.carters_constant(p.tangent.lower_index()) 
                   for p in geo])
p_t, p_phi = np.array([p.tangent.lower_index()
                        .components(M.boyer_lindquist)[[0,3]] 
                       for p in geo]).T
carter0 = M.carters_constant(u.lower_index())
p_phi0 = p_phi[0]
p_t0 = p_t[0]
print("C=", carter0)
print("p_phi=", p_phi0)

# Extract coordinates in Boyer-Lindquist

t,r,th,phi = np.array([p.point.coordinates(M.boyer_lindquist) for p in geo]).T

# Figure out which chart was used in the computation and where chart switching 
# occured, ignore the B-L chart. 
charts = np.array([p.tangent.chart for p in geo])
charts[np.where(charts == M.boyer_lindquist)] = M.kerr_schild_in
chart_switch_indices = np.where(charts[1:] != charts[:-1])[0]+1

# separate each section between chart switches
_, x, y, z = np.array([p.point.coordinates(c) 
                       for p,c in zip(geo, charts)]).T
chart_used = np.append([charts[0]], charts[chart_switch_indices])

xs = list(np.split(x, chart_switch_indices))
ys = list(np.split(y, chart_switch_indices))
zs = list(np.split(z, chart_switch_indices))

# Rotate the different pieces to align them. 
# This corresponds to a different choice of an integration constant for the 
# coordinate system definition. Possibly inconsistent in some cases, 
# but results in a signifificantly clearer plot.
for i in range(1, len(xs)):
    angle = np.arctan2(ys[i][0], xs[i][0])-np.arctan2(ys[i-1][-1], xs[i-1][-1])
    c = np.cos(angle)
    s = np.sin(angle)
    xs[i], ys[i] = c*xs[i] +s*ys[i], c*ys[i] -s*xs[i]
    
# Plot the pieces, blue solid line for ingoing Kerr-Schild coordinates,
# orange dashed for outgoing.
f, ax = plt.subplots(1,1, subplot_kw=dict(projection='3d'))
for x,y,z,c in zip(xs,ys,zs,chart_used):
    color = 'tab:blue' if c==M.kerr_schild_in else 'tab:orange' 
    style = '-' if c==M.kerr_schild_in else '--'
    ax.plot(x,y,z,style, color=color)

# Endpoint with a star
ax.plot([x[-1]], [y[-1]], [z[-1]], '*', color=color)

# Plot surfaces for the outer and inner horizon
eh_ths = np.linspace(0, np.pi, 40)
eh_phis = np.linspace(0,2*np.pi, 60)
eh_ths, eh_phis = np.meshgrid(eh_ths, eh_phis)
eh_points = np.vectorize(
    lambda th, phi: ManifoldPoint(M.boyer_lindquist,
                                  [0, outer_eh_r * 1.001, th, phi]
                                 ).coordinates(M.kerr_schild_in)[1:],
    signature='(),()->(3)'
    )(eh_ths, eh_phis)

ax.plot_surface(eh_points[:,:,0], eh_points[:,:,1],
                eh_points[:,:,2], color='k',alpha=.5)

eh_points = np.vectorize(lambda th, phi: M.create_point(M.boyer_lindquist,
                    [0,inner_eh_r*.999,th,phi]
                    ).coordinates(M.kerr_schild_in)[1:], signature='(),()->(3)'
                    )(eh_ths, eh_phis)

ax.plot_surface(eh_points[:,:,0], eh_points[:,:,1],
                eh_points[:,:,2], color='k',alpha=.3)
ax.set_xlim(-4,4)
ax.set_ylim(-4,4)
ax.set_zlim(-4,4)

# Plot conserved quantities and Boyer-Lindquist r, theta, t coordinates
f, axes = plt.subplots(7,1,sharex='col')
axes[0].plot(lamda, carter-carter0)
axes[0].set_ylabel(r"$\Delta C$")

axes[1].plot(lamda, p_phi-p_phi0)
axes[1].set_ylabel(r"$\Delta p_\phi$")
axes[2].plot(lamda, p_t-p_t0)
axes[2].set_ylabel(r"$\Delta p_t$")

axes[3].plot(lamda, [dot_product(p.tangent,p.tangent)-1 for p in geo])
axes[3].set_ylabel(r"$\Delta(u^\alpha u_\alpha)$")

axes[4].plot(lamda, r)
# horizons
axes[4].axhline(outer_eh_r, color='k')
axes[4].axhline(inner_eh_r, color='k')
axes[4].set_ylabel("$r$")

axes[5].plot(lamda, th)
axes[5].set_ylabel(r"$\theta$")

axes[6].plot(lamda, t)
axes[6].set_ylabel(r"$t$")
axes[6].set_xlabel(r"$\lambda$")
plt.tight_layout()

plt.show()
