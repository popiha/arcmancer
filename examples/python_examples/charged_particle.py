
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

import pyarcmancer as pyac
from pyarcmancer.minkowski import *

pyac.Log.set_level(pyac.LogLevel.warn)

M = MinkowskiSpacetime()

# The initial conditions used here run into the corner case described in the
# tolerance documentation, so set a looser absolute tolerance.
# Changing the initial velocity z component to be non-zero would also work.
M.configuration.curve.propagation.tolerance.absolute = 1e-25

# Define the EM field using the components of F^mn
def F_components(point):
    _, x, y, z = point.coordinates(M.cartesian)
    r = np.sqrt(x*x + y*y + z*z)
    
    # A point charge + magnetic dipole
    # The strength of the field is controlled by k and m
    k = -1e-2
    Ex = k * x/r**3
    Ey = k * y/r**3
    Ez = k * z/r**3

    m = 3e-3
    Bx = 3*m*z*x/r**5
    By = 3*m*z*y/r**5
    Bz = 3*m*z*z/r**5 - m/r**3

    
    return np.array([[0., -Ex, -Ey, -Ez],
                     [Ex,  0., -Bz,  By],
                     [Ey,  Bz,  0., -Bx],
                     [Ez, -By,  Bx,  0.]])

def force(tangent):
    # Need to operate directly with components since the Tensor bindings are
    # currently incomplete.
    u_comp = tangent.lower_index().components(M.cartesian)
    # Lorentz force with unit charce
    f_comp = np.einsum("ab,b", F_components(tangent.point), u_comp)
    return TangentVector(tangent.point, M.cartesian, f_comp)

# Set up a particle and compute its path
x0 = ManifoldPoint(M.cartesian, [0,2,0,1])
u = normalized(TangentVector(x0, M.cartesian, [1,.03,.04, 0]))
# Also add a Fermi-Walker transported frame to give the particle's rest frame
frame = LorentzFrameFW(u, 
                       TangentVector(x0, M.cartesian, [0,0,0,1]),
                       TangentVector(x0, M.cartesian, [0,1,0,0]))

c = ParametrizedCurve_LorentzFrameFW(u, frame, force)

# compute until proper time tau_max
tau_max = 1000

c.compute(tau_max)

print("Initial curve point", c.front)
print("Final curve point", c.back)

print("Initial tangent components in transported frame =", 
      c.front.tangent.components(c.front.transported))
print("Final tangent components in transported frame =", 
      c.back.tangent.components(c.back.transported))

# extract coordinates
N = int(tau_max) # how many points to use for the curve plot
t,x,y,z = np.array([c.interpolate(tau).point.coordinates(M.cartesian) 
                    for tau in np.linspace(0, tau_max, N)]).T
    
# plot the path
f, ax = plt.subplots(1,1, subplot_kw=dict(projection='3d'))
ax.plot(x,y,z, '-', color='tab:blue')

# Endpoint with a point
ax.plot([x[-1]], [y[-1]], [z[-1]], '.', color='tab:blue')

# A star for the origin where the field source is
plt.plot([0],[0],[0], '*', color='k')

# Set the axis limits
ax.set_xlim(-3,3)
ax.set_ylim(-3,3)
ax.set_zlim(-3,3)

plt.show()
