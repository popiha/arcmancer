"""
This example illustrates defining spacetime geometries from Python using the
GenericSpacetime class.
GenericSpacetime allows defining the metric and its derivatives as Python 
functions. It has a single coordinate chart named `chart`.
This example  also shows how to compute non-geodesic curves.
"""

import numpy as np
import matplotlib.pyplot as plt

from pyarcmancer.generic_spacetime import *

a = 1e-3
def g(X):
    """
    Definition of the metric.
    X contains the set of four generic coordinates which are given meaning by 
    the metric.
    Here the metric is defined to give a constant acceleration of a in the -z
    direction in the low velocity limit.
    The definition is valid for z>-1/(2a).
    """
    t,x,y,z = X
    
# The metric components can be given as nested lists or a numpy array.
# Generic spacetime uses the timelike (+---) signature
    return [[1+2*a*z,  0,  0,  0],
            [0      , -1,  0,  0],
            [0      ,  0, -1,  0],
            [0      ,  0,  0, -1]]

def g_der(X):
    """
    Metric derivatives
    """
    t,x,y,z = X
    ret = np.zeros((4,4,4))
    ret[0,0,3] = 2*a # the only non-zero component

    return ret

# Now the spacetime can be defined

M = GenericSpacetime(g, g_der)

# Compute a geodesic starting from x to affine_parameter l_max
l_max = 100

x = ManifoldPoint(M.chart, [0,0,0,0])
u = TangentVector(x, M.chart, [1, 0, 0, 0])

geo = Geodesic(u)
geo.compute(l_max)

# Compute a curve with the same initial conditions but a constant acceleration
# of a in the +z direction applied, which causes the curve to remain straight.
def accel(u):
    """
    The acceleration of a curve is simply given as a function taking the tangent
    of the curve and returning a corresponding acceleration vector.
    """
    return TangentVector(u.point, u.M.chart, [0,0,0,a])

curve = ParametrizedCurve(u, accel)
curve.compute(l_max)

# plot interpolated points
ls = np.linspace(0,l_max, 200)
geo_coords = np.array([geo.interpolate(l).point.coordinates(M.chart) 
                       for l in ls]).T

plt.plot(geo_coords[0], geo_coords[3], label='Geodesic', lw=2)

# plot also a parabola approximately corresponding to the geodesic 
# for comparison
plt.plot(ls, -.5*a*ls**2, '--', label='Approximate analytic solution')

# plot the curve
curve_coords = np.array([curve.interpolate(l).point.coordinates(M.chart) 
                         for l in ls]).T
plt.plot(curve_coords[0], curve_coords[3], label='Accelerated curve')

plt.xlabel('$t$')
plt.ylabel('$z$')
plt.legend()

plt.show()
