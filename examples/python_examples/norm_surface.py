"""
A simple example of using custom surfaces
"""

import numpy as np
import matplotlib.pyplot as plt

# Import the Arcmancer python module.
import pyarcmancer
from pyarcmancer.minkowski import *


# Define a custom surface 
# Surface defined by unit distance wrt. a norm of the specified order
class UnitNormSurface(Surface): #inherits from minkowski.Surface
    
    def __init__(self, M, order):
        Surface.__init__(self, M) # need to explicitly call the base class init
        self.order = order

# Methods are overloaded in a natural way.
# These 3 methods have to be defined for all surfaces
    def value(self, X):
        _, x, y, z  = X.coordinates(self.M.cartesian)
        
        # surface is located at the point where the norm of (x,y,z) of the 
        # given order == 1
        return x**self.order + y**self.order + z**self.order -1

    def gradient(self, X):
        _, x, y, z = X.coordinates(self.M.cartesian)
        
        return CotangentVector(X, self.M.cartesian, 
               [0, x**(self.order-1)*self.order, 
                   y**(self.order-1)*self.order,
                   z**(self.order-1)*self.order ])

    def observer(self, x):
        return TangentVector(x, self.M.cartesian, [1,0,0,0])

# It would be also possible to add a ignore_at method to cut holes into the
# surface. It should return a bool or a type convertible to bool.


## Image computation
# see kerr_shadow.py for further detail 

# configuration
order = 8 # order of the surface norm, should be even
r0 = 5 # observer distance
inc = 70  # inclination in degrees
phi = 40 # azimuthal angle in degrees
# the observer velocity has interesting effects. 
# try e.g. v=.8 with camera_rotation=-50 
v = .0 # observer velocity
# camera rotation angle from the line towards origin in degrees
camera_rotation = -0
# horizontal angle covered by the image in perspective projection
img_angle = 40
# Number of pixels
xbins= 150
ybins = xbins

# The current version of perspective projection is not very polished, 
# so need to work around the rough edges
xspan = np.tan(np.radians(img_angle)/2)
yspan = ybins/xbins*xspan # Ensure square pixels

pyarcmancer.Log.set_level(pyarcmancer.LogLevel.info)

M = MinkowskiSpacetime()

M.configuration.curve.propagation.tolerance.relative = 1e-6

S = UnitNormSurface(M, order)

x0 = ManifoldPoint(M.spherical, [0, r0, np.radians(inc), np.radians(phi)])

# observer frame
# the vector components are normalized internally 
obs_frame = LorentzFrame(x0, 
                         M.spherical,
                         [1,0,0,v/r0], 
                         [0,1,0,0],
                         [0,0,0,1] 
                        )
# rotated camera frame
camera_frame = LorentzFrame(obs_frame.e_t, 
                            np.cos(np.radians(camera_rotation))*obs_frame.e_z
                            +np.sin(np.radians(camera_rotation))*obs_frame.e_x, 
                            -np.sin(np.radians(camera_rotation))*obs_frame.e_z
                            +np.cos(np.radians(camera_rotation))*obs_frame.e_x)

ip_shape = ImagePlane.PlaneShape(xbins, ybins, xspan, yspan)
image_plane = ImagePlane(ip_shape, ImagePlane.Projection.Perspective, 
                         camera_frame)


def compute_func(ip):
    ray = Geodesic(ip.ray_tangent)
    ray.compute(-(r0 +3) , [S])
    
    ret = {'hit_surface': ray.front_termination.hit_surface,
           'hit_angle' : ray.front_termination.observer_hit_angle
           }
    return ret

# Perform the computation.
image_plane.compute(compute_func)

hit_angle = np.zeros((ybins,xbins))
hit_surface = np.zeros((ybins,xbins), dtype=bool)

for row in image_plane.data_points:
    for val in row:
        hit_angle[val.iy,val.ix] = val.data['hit_angle']
        hit_surface[val.iy, val.ix] = val.data['hit_surface']

# plot the impact angle to give a shaded appearance
img = np.ma.masked_where(~hit_surface, hit_angle)

# Draw image and save.
plt.imshow(img, origin='lower', cmap='gray', interpolation='none')

plt.show()
