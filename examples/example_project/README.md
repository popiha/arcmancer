Example `CMake` Project
=======================

This is a minimal working example of adding Arcmancer to a project with `CMake`.
The `CMakeList.txt` file contains comments explaining the necessary steps,
while the included program serves simply to show that everything works
correctly.

Building & Running
------------------

To build and run the example after Arcmancer has been configured,
execute the following commands
    
    mkdir build
    cd build
    cmake .. && make && ./example

The program outputs the current Arcmancer version and whether `OpenMP` is 
enabled.
