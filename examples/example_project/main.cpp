#include<arcmancer/arcmancer.h> //bring in the Arcmancer main library

int main(){
    arcmancer::Log::console()->info("Using Arcmancer version {}",
                                    ARCMANCER_VERSION_STRING);
#ifdef _OPENMP 
    arcmancer::Log::console()->info("OpenMP parallelization is enabled");
#else
    arcmancer::Log::console()->info("OpenMP parallelization is not enabled");
#endif
    return 0;
}
