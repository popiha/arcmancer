#include <iostream>

#include <arcmancer/arcmancer.h>
#include <arcmancer/spacetimes/minkowski.h>

using namespace arcmancer;

int main(){
    
    using MS = minkowski::MinkowskiSpacetime;

    // Basic setup:
    // Instantiate the spacetime
    MS M;
    // Create two different points by giving the coordinates in the cartesian
    // chart.
    ManifoldPoint<MS> x1(M.cartesian, {0,1,0,0});
    ManifoldPoint<MS> x2(M.cartesian, {0,0,1,0});
    // Create also a LorentzFrame for a boosted observer.
    // This is like the tensor constructors below, but takes the components of 
    // 3 vectors (~e_t, ~e_z, ~e_x) defining the frame.
    LorentzFrame<MS> frame(x1, M.cartesian, {1,.7,0,0}, {0,0,0,1}, {0,1,0,0});

    //
    // Initializing tensors:
    // Tensors are initialized by giving the base point and the components.

    // A tensor with indices T_a^b
    Tensor<MS, Cov, Cnt> T1(x1, // base point
                           M.cartesian, // chart used for the coordinates
                           // The components can be given as nested initializer
                           // lists.
                           {{1 , 2 , 3 , 4}, 
                            {5 , 6 , 7 , 8},
                            {9 , 10, 11, 12},
                            {13, 14, 15, 16}});

    std::cout << "Tensor T1 is:\n" << T1 << "\n\n";
    
    // The components can also be given as an Eigen::Tensor, which is useful
    // especially with larger tensors.
    EigenRk2Tensor<4> T2_comp;
    T2_comp.setConstant(2);
    Tensor<MS, Cov, Cnt> T2(x2, M.spherical, T2_comp);

    std::cout << "Tensor T2 is:\n" << T2 << "\n\n";

    // It is also possible to give the components with respect to a LorentzFrame
    Tensor<MS, Cnt, Cov> T3(frame, 
                            {{1 , 2 , 3 , 4}, 
                             {5 , 6 , 7 , 8},
                             {9 , 10, 11, 12},
                             {13, 14, 15, 16}});
    
    std::cout << "Tensor T3 is:\n" << T3 << "\n\n";

    //
    // Reading tensor components:
    // Tensor components can be read in any chart:
    std::cout << "Tensor T1 components in spherical coordinates:\n"
              << T1.components(M.spherical) << "\n\n";
    // or in a given LorentzFrame
    std::cout << "Tensor T1 components in LorentzFrame frame:\n"
              << T1.components(frame) << "\n\n";

    //
    // Index operations:
    // 
    // Tensor indices can be raised and lowered with the corresponding methods.

    auto T1_lowered = T1.lower_index<1>();
    std::cout << "Tensor T1_lowered is:\n" << T1_lowered << "\n\n";
    auto T1_raised = T1.raise_index<0>();
    std::cout << "Tensor T1_raised is:\n" << T1_raised << "\n\n";

    // Trying to raise or lower an index that is already raised/lowered is a
    // compile-time error (enable by replacing 0 with 1):
#if 0
    auto T1_lowered0 = T1.lower_index<0>();
    std::cout << "Tensor T1_lowered0 is:\n" << T1_lowered0 << "\n\n";
#elif 0
    auto T1_raised1 = T1.raise_index<1>();
    std::cout << "Tensor T1_raised1 is:\n" << T1_raised1 << "\n\n";
#endif

    // Tensor indices can be contracted within a single tensor:
    auto T1_contracted = T1.contract_indices<0,1>();
    std::cout << "Tensor T1_contracted is:\n" << T1_contracted << "\n\n";
    // Note that the result is a Tensor<MS>, i.e. a scalar.
    // It can be implicitly converted to double
    double T1_contracted_val = T1_contracted;
    std::cout << "T1_contracted_val is: " << T1_contracted_val << "\n\n";

    // It is possible to compute the tensor product between tensors:
    auto T1T3 = T1.tprod(T3);
    std::cout << "Tensor T1T3 is:\n" << T1T3 << "\n\n";
    // Trying to compute any operations between tensors at different points
    // is a runtime error:
    try{
        auto T1T2 = T1.tprod(T2);
        std::cout << "Tensor T1T2 is:\n" << T1T2 << "\n\n";

    }
    catch (const std::domain_error &e) {
        std::cout << "Computing T1T2 threw the exception:\n" 
                  << e.what() << "\n\n"; 
    }

    // Tensor product and contraction of indices between the two tensors
    // are combined in contract_prod.
    // This computes T1_a^b T2^a_c
    auto T1T3_contract_prod = T1.contract_prod<0,0>(T3);
    std::cout << "Tensor T1T3_contract_prod is:\n"
              << T1T3_contract_prod << "\n\n";
    
    // Again operations between wrong types of indices are a compile-time error:
#if 0
    auto T1T3_contract_prod2 = T1.contract_prod<0,1>(T3);
    std::cout << "Tensor T1T3_contract_prod2 is:\n"
              << T1T3_contract_prod2 << "\n\n";
#endif

    //
    // Vector types:
    // (Co)TangentVectors are just tensors of a certain type,
    // but there are some additional convenience aliases and methods provided:

    // Alias for Tensor<MS, Cnt>
    TangentVector<MS> V(x1, M.spherical, {1,.1,0,0});
    std::cout << "Tensor V is:\n" << V << "\n\n";
    // Alias for Tensor<MS, Cov>
    CotangentVector<MS> W(x1, M.cartesian, {1,1,2,0});
        std::cout << "Tensor W is:\n" << W << "\n\n";
    
    // LorentzFrame offers a helper for creating null vectors along a particular
    // spatial direction
    TangentVector<MS> k = frame.make_lightlike({0,1,1,0});
    std::cout << "Tensor k is:\n" << k << "\n\n";

    // One important helper function is dot_product, which is overloaded
    // for different pairs of vector types
    std::cout << "dot_product(V, W) = " << dot_product(V, W) << "\n\n";
    std::cout << "dot_product(V, V) = " << dot_product(V, V) << "\n\n";
    std::cout << "dot_product(W, W) = " << dot_product(W, W) << "\n\n";
    
    // The same with tensor operations, a bit more verbose and slightly slower.
    // Note also that the return type is still a tensor type
    std::cout << "dot_product(V, W) equivalent contraction = " 
              << V.contract_prod<0,0>(W) << "\n\n";
    std::cout << "dot_product(V, V) equivalent contraction = " 
              << V.lower_index<0>().contract_prod<0,0>(V) << "\n\n";
    std::cout << "dot_product(W, W) equivalent contraction = " 
              << W.raise_index<0>().contract_prod<0,0>(W) << "\n\n";


    //
    // Arithmetic operations
    //
    // The standard arithmetic operations are also implemented
    std::cout << "3*V + k/2 =\n" << 3*V + k/2 << "\n\n"; 

    return 0;    
}
