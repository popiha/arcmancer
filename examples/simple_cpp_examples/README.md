Simple C++ Examples
===================

This directory collects simple examples of using the Arcmancer C++ API
into a single project for convenience.

Building and Running
--------------------
To build and run the example after Arcmancer has been configured,
execute the following commands to build the examples
    
    mkdir build
    cd build
    cmake .. && make

Each example will produce an executable with the same base name as the `.cpp`
file, e.g. `tensor_operations.cpp` compiles to `tensor_operations`, which can
then be executed simply as `./tensor_operations`.

Example Descriptions
====================

Tensor Operations (`tensor_operations.cpp`)
-------------------------------------------

This example shows how the various tensor operations work.

Several code blocks are disabled by default with #if 0, these contain
code that will cause an intentional compiler error.
These can be enabled to see the compiler error messages generated in these
cases.


Fermi-Walker Transport (`fermi_walker.cpp`)
-------------------------------------------

This example shows how to define a simple custom type transportable along 
curves, as well as computing accelerated curves.
The custom type is a tangent vector transported according to Fermi-Walker
transport, which maintains the orientation of a vector relative to the curve
tangent constant also on accelerated curves.

The example is kept simple by only defining a custom `transport_derivatives`
method, with all other necessary functionality being inherited from the Tensor
implementation.
More complicated types require some additional definitions described in the 
`ParametrizedPoint` documentation.

Two accelerated curves are computed to compare parallel transport and
Fermi-Walker transported vectors along otherwise identical curves.
The results show that Fermi-Walker transport maintains the dot product with the
curve tangent as opposed to parallel transport.




