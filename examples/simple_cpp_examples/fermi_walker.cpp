#include <iostream>

#include <arcmancer/geometry.h>
#include <arcmancer/spacetimes/minkowski.h>

using namespace arcmancer;
using namespace minkowski;

// Define a Fermi-Walker transported tangent vector.
// This can be simply done by inheriting from TangentVector and redefining
// the transport_derivatives method.
//
// The class is templated for generality, altough here only MinkowskiSpacetime
// is used.
//
template <typename MetricSpaceType>
class FermiWalkerVector : public TangentVector<MetricSpaceType> {
public:
    // Inherit the default tensor constructors.
    // Reminder: TangentVector<MST> is just an alias to Tensor<MST, Cnt>.
    //using TangentVector<MetricSpaceType>::Tensor;
    // XXX: The above doesn't work with clang, see
    // <https://bugs.llvm.org/show_bug.cgi?id=23107#c1>
    using TangentVector<MetricSpaceType>::TangentVector;

    // Define the transport_derivatives method accepting also the tangent vector
    // and acceleration of the curve.
    typename TangentVector<MetricSpaceType>::ContainerType
    transport_derivatives(
        const Chart<MetricSpaceType> &chart,
        const EigenRk2Tensor<MetricSpaceType::D> &contracted_Gamma,
        const TangentVector<MetricSpaceType> &tangent,
        const TangentVector<MetricSpaceType> &acceleration) const {

        // Right hand side of the FW transport equation.
        // Note the metric sign factor.
        auto rhs = MetricSpaceType::Signature::timelike_sign *
                   (acceleration * dot_product(tangent, *this) -
                    tangent * dot_product(acceleration, *this));

        // Add rhs component to the base parallel transport.
        // The base method can be accessed by specifying also the base class.
        return TangentVector<MetricSpaceType>::transport_derivatives(
                   chart, contracted_Gamma) +
               rhs.components(chart);
    }

    // Define also custom printing
    friend std::ostream& operator<<(std::ostream &os, const FermiWalkerVector &fw) {
        os << "Fermi-Walker transported "
           << static_cast<const TangentVector<MetricSpaceType> &>(fw);
        return os;
    }
};


int main() {
    
    // set up the spacetime and initial conditions for the curve
    using MS = MinkowskiSpacetime;
    
    MS M;
    ManifoldPoint<MS> x0{M.cartesian, {0,1,0,0}};
    TangentVector<MS> u {x0, M.cartesian, {1,0,.1,0}};
    u = normalized(u);
    
    // Transported vector.
    // One transported using the standard parallel transport and the other using
    // Fermi-Walker transport
    FermiWalkerVector<MS> w_fermi {x0, M.cartesian, {0,0,1,1}};
    TangentVector<MS> w_parallel = w_fermi;



    // Define the acceleration function, which corresponds to a constant force
    // in the y direction
    auto acceleration = [](const TangentVector<MS> &u) -> TangentVector<MS> {
                auto u_comp = u.components(u.M().cartesian);
                const double f = -0.1; // magnitude of force along y-axis
                return {u.point(), u.M().cartesian, 
                        {f * u_comp(2), 0, u_comp(0) * f, 0}};
            };

    // compute the curves 
    ParametrizedCurve<MS, TangentVector<MS>> curve_parallel{u, w_parallel, 
                                                            acceleration};
    curve_parallel.compute(100);

    ParametrizedCurve<MS, FermiWalkerVector<MS>> curve_fermi{u, w_fermi, 
                                                         acceleration};
    
    curve_fermi.compute(100);

    std::cout << "Dot products with curve tangent at the beginning and the end "
                 "of the curve\n"
              << "Parallel transport: "
              << dot_product(curve_parallel.front().tangent(),
                             curve_parallel.front().transported())
              << ", "
              << dot_product(curve_parallel.back().tangent(),
                             curve_parallel.back().transported())
              << "\n"
              << "Fermi-Walker transport: "
              << dot_product(curve_fermi.front().tangent(),
                             curve_fermi.front().transported())
              << ", "
              << dot_product(curve_fermi.back().tangent(),
                             curve_fermi.back().transported()) << std::endl;


        return 0;
}
