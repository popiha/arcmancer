
#include <arcmancer/arcmancer.h>
#include <arcmancer/geometry/metric_space.h>


using namespace arcmancer;

// A superposition of two Schwarzschild metrics in outgoing cartesian KS
// coordinates.
// The central hole is stationary at the origin and has mass m1,
// while the secondary holes motion is given by the functions x2, dx2dt.
// The motion is assumed to be non-relativistic.

class BinarySchwarzschild : public MetricSpace<BinarySchwarzschild, 4> {

public:
    std::string name() const { return "Binary Schwarzschild"; }

    // only chart is cartesian Kerr-Schild like coordinates
    const ChartType KS{*this, "KS cartesian coordinates"};

    // Constructor takes the masses of the holes and the position and velocity 
    // functions of the secondary hole.
    BinarySchwarzschild(double m1, double m2,
                        std::function<EigenRk1Tensor<4>(double /*t*/)> x2,
                        std::function<EigenRk1Tensor<4>(double)> dx2dt)
    : m1_(m1), m2_(m2), x2_(x2), dx2dt_(dx2dt) {

        add_chart(KS, &BinarySchwarzschild::metric,
                  &BinarySchwarzschild::metric_derivatives);

        zero.setZero();
    }

    double m1() const { return m1_; }

private:
    double m1_, m2_;
    std::function<EigenRk1Tensor<4>(double)> x2_, dx2dt_;
    EigenRk1Tensor<4> zero;

    EigenRk2Tensor<4> metric(const EigenRk1Tensor<4> &x) const {
        EigenRk2Tensor<4> eta;
        eta.setZero();
        eta(0, 0) = 1;
        eta(1, 1) = -1;
        eta(2, 2) = -1;
        eta(3, 3) = -1;

        // Just add the difference from flat spacetime for both holes
        return eta + offset_schwarzschild_metric(m1_, zero, x) +
               offset_schwarzschild_metric(m2_, x2_(x(0)), x);
    };

    EigenRk3Tensor<4> metric_derivatives(const EigenRk1Tensor<4> &x) const {
        return offset_schwarzschild_metric_derivatives(m1_, zero, zero, x) +
               offset_schwarzschild_metric_derivatives(m2_, x2_(x(0)),
                                                       dx2dt_(x(0)), x);
    };

    EigenRk2Tensor<4> offset_schwarzschild_metric(
        double m, const EigenRk1Tensor<4> &x0,
        const EigenRk1Tensor<4> &x) const {

        using namespace std;
        const EigenRk1Tensor<4> x_shifted = x - x0;

        const double X = x_shifted(1), Y = x_shifted(2), Z = x_shifted(3);

        const double r = sqrt(X * X + Y * Y + Z * Z);
        const double F = -2 * m / r;

        EigenRk2Tensor<4> ret;
        const double /*l_t = -1,*/ l_x = X / r, l_y = Y / r, l_z = Z / r;
        // clang-format off
        ret.setValues({{F   , -F*l_x     , -F*l_y     , -F*l_z     },
                       {-F*l_x, F*l_x*l_x, F*l_x*l_y  , F*l_x*l_z  },
                       {-F*l_y, F*l_x*l_y  , F*l_y*l_y, F*l_y*l_z  },
                       {-F*l_z, F*l_x*l_z  , F*l_y*l_z  , F*l_z*l_z}});
        // clang-format on
        return ret;
    }

    EigenRk3Tensor<4> offset_schwarzschild_metric_derivatives(
        double m, const EigenRk1Tensor<4> &x0, const EigenRk1Tensor<4> &dx0dt,
        const EigenRk1Tensor<4> &x) const {

        using namespace std;
        const EigenRk1Tensor<4> x_shifted = x - x0;

        const double X = x_shifted(1), Y = x_shifted(2), Z = x_shifted(3);

        const double r2 = X * X + Y * Y + Z * Z;
        const double r = sqrt(r2);

        const double F = -2 * m / r;
        const double F_r = 2 * m / r2;

        const double /*l_t = -1,*/ l_x = X / r, l_y = Y / r, l_z = Z / r;
        EigenRk2Tensor<4> l2; // l_a l_b
        // clang-format off
        l2.setValues({{1   , -l_x   , -l_y   , -l_z   },
                      {-l_x, l_x*l_x, l_x*l_y, l_x*l_z},
                      {-l_y, l_x*l_y, l_y*l_y, l_y*l_z},
                      {-l_z, l_x*l_z, l_y*l_z, l_z*l_z}});
        // clang-format on

        EigenRk3Tensor<4> ret;
        ret.setZero();

        // x
        {
            // derivatives of the functions
            const double r_x = X / r;
            const double F_x = F_r * r_x;

            const double /*l_tx = 0,*/
                l_xx = 1 / r - X / r2 * r_x,
                l_yx = -Y / r2 * r_x, l_zx = -Z / r2 * r_x;
            EigenRk2Tensor<4> l2x; // l_a l_b,x
            // clang-format off
            l2x.setValues({{0, -l_xx   , -l_yx   , -l_zx   },
                           {0, l_x*l_xx, l_x*l_yx, l_x*l_zx},
                           {0, l_y*l_xx, l_y*l_yx, l_y*l_zx},
                           {0, l_z*l_xx, l_z*l_yx, l_z*l_zx}});
            // clang-format on

            ret.chip(1, 2) =
                F_x * l2 + F * (l2x + l2x.shuffle(Eigen::array<int, 2>{1, 0}));
        }
        // y
        {
            const double r_y = Y / r;
            const double F_y = F_r * r_y;

            const double /*l_ty = 0,*/
                l_xy = -X / r2 * r_y,
                l_yy = 1 / r - Y / r2 * r_y, l_zy = -Z / r2 * r_y;
            EigenRk2Tensor<4> l2y;
            // clang-format off
            l2y.setValues({{0, -l_xy   , -l_yy   , -l_zy   },
                           {0, l_x*l_xy, l_x*l_yy, l_x*l_zy},
                           {0, l_y*l_xy, l_y*l_yy, l_y*l_zy},
                           {0, l_z*l_xy, l_z*l_yy, l_z*l_zy}});
            // clang-format on

            ret.chip(2, 2) =
                F_y * l2 + F * (l2y + l2y.shuffle(Eigen::array<int, 2>{1, 0}));
        }
        // z
        {
            const double r_z = Z / r;
            const double F_z = F_r * r_z;

            const double /*l_tz = 0,*/
                l_xz = -X / r2 * r_z,
                l_yz = -Y / r2 * r_z, l_zz = 1 / r - Z / r2 * r_z;
            EigenRk2Tensor<4> l2z;
            // clang-format off
            l2z.setValues({{0, -l_xz   , -l_yz   , -l_zz   },
                           {0, l_x*l_xz, l_x*l_yz, l_x*l_zz},
                           {0, l_y*l_xz, l_y*l_yz, l_y*l_zz},
                           {0, l_z*l_xz, l_z*l_yz, l_z*l_zz}});
            // clang-format on

            ret.chip(3, 2) =
                F_z * l2 + F * (l2z + l2z.shuffle(Eigen::array<int, 2>{1, 0}));
        }

        // t
        ret.chip(0, 2) = -dx0dt(1) * ret.chip(1, 2) -
                         dx0dt(2) * ret.chip(2, 2) - dx0dt(3) * ret.chip(3, 2);

        return ret;
    }
};

// A sphere in coordinate space with a given center and velocity
class Sphere : public Surface<BinarySchwarzschild> {

    double r2_;
    std::function<EigenRk1Tensor<4>(double)> x0_, dx0dt_;

public:
    Sphere(const BinarySchwarzschild &M, double r,
           std::function<EigenRk1Tensor<4>(double)> x0,
           std::function<EigenRk1Tensor<4>(double)> dx0dt)
    : Surface<BinarySchwarzschild>(M), r2_(r * r), x0_(x0), dx0dt_(dx0dt) {}

    double value(const PointType &x) const override {
        EigenRk1Tensor<4> X = x.coordinates(M().KS);
        X -= x0_(X(0));
        return X(1) * X(1) + X(2) * X(2) + X(3) * X(3) - r2_;
    }

    CotangentVector<BinarySchwarzschild> gradient(
        const PointType &x) const override {
        const auto coords = x.coordinates(M().KS);
        const auto x0 = x0_(coords(0));

        EigenRk1Tensor<4> X = 2 * (coords - x0);

        const auto dx0dt = dx0dt_(coords(0));
        X(0) = -X(1) * dx0dt(1) - X(2) * dx0dt(2) - X(3) * dx0dt(3);

        return CotangentVector<BinarySchwarzschild>(x, M().KS, X);
    }

    TangentVector<BinarySchwarzschild> observer(
        const PointType &position) const override {
        return normalized(CotangentVector<BinarySchwarzschild>{
                              position, M().KS, {1, 0, 0, 0}})
            .raise_index<0>();
    }
};

// Flat disk on the z==0 plane
class Disk : public Surface<BinarySchwarzschild> {
    double min_stable_radius_;

public:
    Disk(const BinarySchwarzschild &M) : Surface<BinarySchwarzschild>(M) {
        min_stable_radius_ = 6 * M.m1();
    }

    double value(const PointType &x) const override {
        return x.coordinates(M().KS)(3);
    }

    CotangentVector<BinarySchwarzschild> gradient(
        const PointType &x) const override {
        return CotangentVector<BinarySchwarzschild>(x, M().KS, {0, 0, 0, 1});
    }

    // Keplerian rotation
    TangentVector<BinarySchwarzschild> observer(
        const PointType &position) const override {
        const auto X = position.coordinates(M().KS);
        const double r = std::sqrt(X(1) * X(1) + X(2) * X(2));

        if (r < min_stable_radius_) {
            return normalized(CotangentVector<BinarySchwarzschild>{
                                  position, M().KS, {1, 0, 0, 0}})
                .raise_index<0>();
        }

        const double omega = std::sqrt(M().m1()) * std::pow(r, -3. / 2.);

        return normalized(TangentVector<BinarySchwarzschild>{
            position, M().KS, {1, -X(2) * omega, X(1) * omega, 0}});
    }

    // Cut a hole into the disk below the stable orbit radius
    bool ignore_at(const PointType &p) const override {
        const auto X = p.coordinates(M().KS);
        const double r = std::sqrt(X(1) * X(1) + X(2) * X(2));
        
        // TODO: Could also ignore very far away to allow for lensed images
        // coming from under the disk when the observer is near the equator.
        return r < min_stable_radius_;      
    }
};
