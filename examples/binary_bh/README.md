Binary Black Hole Lensing
=========================

This example computes images and light curves of a
simple approximation of a system of two Schwarzschild black holes.

The primary black hole is stationary at the origin, while a secondary hole
passes in front of it on a linear trajectory.
The trajectory of the secondary hole can be rotated and moved in the plane
orthogonal to the line connecting the primary and the observer.
The observer sees the holes nearest each other at t=0.

A thin accretion disk, approximated as the z=0 plane with, is placed around the
primary hole. 
The rays can pass through the disk inside the minimum stable circular orbit,
and emission from the disk is modelled as black-body radiation through an
electron scattering atmosphere.


The program is divided into files as follows:

- *main.cpp*: Definition of the computation and code related to the command line
  interface and saving results.

- *binary_schwarzschild.h*: Definitions of the spacetime and surfaces.

- *post_process.py*: Post-processing and visualization of the computation
  results.


Building
--------

After Arcmancer has been configured and `pyarcmancer` has been built,
run the following commands in this directory:

    mkdir build
    cd build
    cmake .. -DCMAKE_BUILD_TYPE=RELEASE
    make


Running
-------

The build process produces the executable `binary_bh` in the build directory.
Running (in the build directory)
    
    ./binary_bh

will perform a low resolution computation with aligned black holes.
The computation results are stored in the file `img.h5`.
To post process and plot these results, run (in the build directory)


    python3 ../post_process.py img.h5

To run, `post_process.py` needs `pyarcmancer` to be on `PYTHONPATH`.
Alternatively, you can modify `post_process.py` as described in the 
[Python examples README](../python_examples/README.md).


Both the main `binary_bh` application are configurable with command line
options.
Run 

    ./binary_bh --help
    python3 ../post_process.py --help

for a list of available options.

In particular, it is possible to compute an animation of multiple frames which
also allows computing a light curve.
