#include <iostream>

#include <boost/program_options.hpp>

#include <arcmancer/arcmancer.h>
#include <arcmancer/geometry/metric_space.h>
#include <arcmancer/radiation/radiation_transfer.h>

#include "binary_schwarzschild.h"

using namespace arcmancer;
namespace po = boost::program_options;


// Data saved from the calculation
struct Data {
    int surface_index;
    double r_eq; // distance from origin in the xy-plane
    double observer_hit_angle;
    double redshift;
    double pol_angle;
};

struct Options {
    double obs_r0; // observer distance
    double inclination; // inclination in degrees
    double t1; // time of the first frame
    double t2; // time of the last frame
    size_t frames; // number of frames
    size_t xbins; // resolution
    size_t ybins;
    double xspan; // size of the image plane
    double yspan;

    double primary_mass;

    double secondary_m; // mass of the smaller hole
    double secondary_r; // distance of the hole from the origin
    double secondary_v; // velocity of the hole
    double secondary_angle; // angle of motion from the x-axis in the image
    double secondary_offset; // offset from the primary-observer line
    double integration_tolerance;

    int log_level;
};


void write_options(const Options& opt, H5::H5File& out_file) {

    EigenHDF5::save_scalar_attribute(out_file, "obs_r0", opt.obs_r0);
    EigenHDF5::save_scalar_attribute(out_file, "inclination", opt.inclination);
    EigenHDF5::save_scalar_attribute(out_file, "t1", opt.t1);
    EigenHDF5::save_scalar_attribute(out_file, "t2", opt.t2);
    EigenHDF5::save_scalar_attribute(out_file, "frames", opt.frames);
    EigenHDF5::save_scalar_attribute(out_file, "xbins", opt.xbins);
    EigenHDF5::save_scalar_attribute(out_file, "ybins", opt.ybins);
    EigenHDF5::save_scalar_attribute(out_file, "xspan", opt.xspan);
    EigenHDF5::save_scalar_attribute(out_file, "yspan", opt.yspan);
    EigenHDF5::save_scalar_attribute(out_file, "primary_mass",
                                     opt.primary_mass);
    EigenHDF5::save_scalar_attribute(out_file, "secondary_mass",
                                     opt.secondary_m);
    EigenHDF5::save_scalar_attribute(out_file, "secondary_r",
                                     opt.secondary_r);
    EigenHDF5::save_scalar_attribute(out_file, "secondary_v",
                                     opt.secondary_v);
    EigenHDF5::save_scalar_attribute(out_file, "secondary_angle",
                                     opt.secondary_angle);
    EigenHDF5::save_scalar_attribute(out_file, "secondary_offset",
                                     opt.secondary_offset);
    EigenHDF5::save_scalar_attribute(out_file, "integration_tolerance",
                                     opt.integration_tolerance);
    EigenHDF5::save_scalar_attribute(out_file, "log_level",
                                     opt.log_level);
}


void compute_frame(double t0, const Options &opt, H5::Group &frame_group) {

    // observer position, on the xz-plane
    const double r0 = opt.obs_r0;
    const double inclination = opt.inclination * cnst::pi / 180;

    // image plane properties
    double xspan = opt.xspan, yspan = opt.yspan;
    size_t xbins = opt.xbins, ybins = opt.ybins;

    // mass and position of first hole
    const double m1 = opt.primary_mass;
    EigenRk1Tensor<4> x1;
    x1.setZero();

    // second hole
    const double m2 = opt.secondary_m;
    const double v2 = opt.secondary_v;
    const double angle = opt.secondary_angle * cnst::pi / 180;
    const double offset = opt.secondary_offset;

    const double v2x = -std::cos(inclination) * std::sin(angle) * v2;
    const double v2y = std::cos(angle) * v2;
    const double v2z = std::sin(inclination) * std::sin(angle) * v2;

    // make the distance from the primary be secondary_r at the nearest point,
    // even with offset
    const double R = std::sqrt(std::pow(opt.secondary_r, 2)
                               - std::pow(offset, 2));

    const double light_delay = std::sqrt(std::pow(r0 - R, 2)
                                         + std::pow(offset, 2));

    const double x20 = R * std::sin(inclination)
                       - offset * std::cos(inclination) * std::cos(angle)
                       + light_delay * v2x;

    const double y20 = - std::sin(angle) * offset + light_delay * v2y;

    const double z20 = R * std::cos(inclination)
                       + offset * std::sin(inclination) * std::cos(angle)
                       + light_delay * v2z;

    // position and velocity as functions of time
    auto x2_fun = [x20, y20, z20, v2x, v2y, v2z](double t) {
        EigenRk1Tensor<4> x2;
        x2.setValues({t, v2x * t + x20, v2y * t + y20, v2z * t + z20});
        return x2;
    };

    auto dx2dt_fun = [v2x, v2y, v2z](double) {
        EigenRk1Tensor<4> ret;
        ret.setValues({1, v2x, v2y, v2z});
        return ret;
    };

    auto zero_fun = [](double) {
        EigenRk1Tensor<4> ret;
        ret.setZero();
        return ret;
    };

    // Setup the spacetime and surfaces
    BinarySchwarzschild M(m1, m2, x2_fun, dx2dt_fun);
    Sphere eh1(M, 2.1 * m1, zero_fun, zero_fun);
    Sphere eh2(M, 2.1 * m2, x2_fun, dx2dt_fun);
    Disk disk(M);

    std::vector<Surface<BinarySchwarzschild> *> surfaces{&eh1, &eh2, &disk};

    // Setup the image plane
    ManifoldPoint<BinarySchwarzschild> x0(
        M.KS, {t0, r0 * std::sin(inclination), 0, r0 * std::cos(inclination)});

    using IP = ImagePlane<BinarySchwarzschild, Data>;

    IP ip({xbins, ybins, xspan, yspan}, IP::Projection::ParallelPlane,
          {x0, M.KS, {1, 0, 0, 0}},
          {x0, M.KS, {0, std::sin(inclination), 0, std::cos(inclination)}},
          {x0, M.KS, {0, 0, 1, 0}});

    M.configuration.curve.propagation.tolerance.relative =
        opt.integration_tolerance;

    // Compute the image
    ip.compute([&surfaces, &r0](auto &p) -> Data {
        Data data;

        Geodesic<BinarySchwarzschild, PolarizationFrame<BinarySchwarzschild>>
            geo(p.ray_tangent, {p.frame.e_y(), p.frame.e_x()});

        // Ignoring the return status here,
        // since the termination gives the relevant info anyway, and any
        // failures seem unlikely.
        geo.compute(-2 * r0, surfaces);
        const auto &term = geo.front_termination();

        if (term.hit_surface) {
            data.surface_index = term.surface_index;
        } else {
            data.surface_index = -1;
            return data;
        }

        if (term.surface_index == 2) {
            // Hit the disk
            const auto X = geo.front().point().coordinates(geo.M().KS);
            data.r_eq = std::sqrt(X(1) * X(1) + X(2) * X(2));
            data.observer_hit_angle = term.observer_hit_angle;
            data.redshift = dot_product(p.ray_tangent, p.frame.e_t()) /
                            term.observer_dot_tangent;

            const auto &geo_frame = geo.front().transported();
            const auto u_surf = surfaces[2]->observer(geo.front().point());
            const auto reference_dir =
                surfaces[2]->gradient(geo.front().point()).raise_index<0>();

            data.pol_angle =
                geo_frame.polarization_angle(u_surf, reference_dir,
                                             geo.front().tangent());
        }

        return data;
    });

    // Save data
    EigenHDF5::save(frame_group, "surface_index",
        ip.scalar_data_array([](auto &dp){
            return dp.data.surface_index;
        }));

    EigenHDF5::save(frame_group, "r_eq",
        ip.scalar_data_array([](auto &dp){
            return dp.data.r_eq;
        }));

    EigenHDF5::save(frame_group, "observer_hit_angle",
        ip.scalar_data_array([](auto &dp){
            return dp.data.observer_hit_angle;
        }));

    EigenHDF5::save(frame_group, "redshift",
        ip.scalar_data_array([](auto &dp){
            return dp.data.redshift;
        }));

    EigenHDF5::save(frame_group, "pol_angle",
        ip.scalar_data_array([](auto &dp){
            return dp.data.pol_angle;
        }));
}


int main(int argc, char** argv) {
    // Configure logging, info is the default level
    Log::log()->set_level(Log::level::info);

    // Setup command line options
    Options opt;

    po::options_description desc("Options");

    // clang-format off
    desc.add_options()
        ("help", "Print help messages")

        ("output_file",
         po::value<std::string>()->default_value("img.h5"),
         "Name of the output file")

        ("obs_r0", po::value<double>(&opt.obs_r0)->default_value(1e7),
         "Distance of the image plane from the origin")

        ("inclination", po::value<double>(&opt.inclination)->default_value(60),
         "Inclination of the image plane in degrees")

        ("t1", po::value<double>(&opt.t1)->default_value(0),
         "Time of the first frame, or time of the frame if frames=1")

        ("t2", po::value<double>(&opt.t2)->default_value(0),
         "Time of the last frame")

        ("frames", po::value<size_t>(&opt.frames)->default_value(1),
         "Number of frames")

        ("xbins", po::value<size_t>(&opt.xbins)->default_value(100),
         "Number of pixels in the horizontal direction."
         "\nThe number of pixels in the vertical direction is determined"
         "automatically to make the pixels square.")

        ("xspan", po::value<double>(&opt.xspan)->default_value(40),
         "Half the width of the image plane.\n"
         "The x coordinate goes from -xpan to xspan.")

        ("yspan", po::value<double>(&opt.yspan)->default_value(40),
         "Half the height of the image plane.\n"
         "The y coordinate goes from -ypan to yspan.")

        ("primary_mass", po::value<double>(&opt.primary_mass)->default_value(1),
         "Mass of the primary black hole at the origin")

        ("secondary_mass",
         po::value<double>(&opt.secondary_m)->default_value(1e-1),
         "Mass of the secondary black hole")

        ("secondary_r",
         po::value<double>(&opt.secondary_r)->default_value(1000),
         "Coordinate distance between the black holes on nearest approach")

        ("secondary_v",
         po::value<double>(&opt.secondary_v)->default_value(1e-2),
         "Velocity of the secondary black hole. Should be << 1.")

        ("secondary_angle",
         po::value<double>(&opt.secondary_angle)->default_value(0),
         "Angle of the secondary hole's path with the image plane x-axis")

        ("secondary_offset",
         po::value<double>(&opt.secondary_offset)->default_value(0),
         "Offset from the line connecting the observer and the primary hole."
         " Measured perpendicular to the line when the secondary is nearest to"
         " the primary. Must be < secondary_r.")

        ("integration_tolerance",
         po::value<double>(&opt.integration_tolerance)->default_value(1e-6),
         "Tolerance used for ray integration (relative).")

        ("log_level",
         po::value<int>(&opt.log_level)->default_value(2),
         "Logging level. From 0-6, corresponding to 'trace' - 'off'.")

        ("log_to_file",
         po::bool_switch()->default_value(false),
         "Log to file instead of stdout.")
    ;

    // clang-format on
    po::variables_map vm;

    try {
        po::store(po::parse_command_line(argc, argv, desc), vm);

        if (vm.count("help")) {
            std::cout << "Binary black hole lensing" << std::endl
                      << desc << std::endl;

            return 0;
        }

        po::notify(vm);

        opt.ybins = opt.xbins * (opt.yspan / opt.xspan);

    } catch(po::error &e) {
        std::cerr << "ERROR: " << e.what() << std::endl << std::endl;
        std::cerr << desc << std::endl;
        return 1;
    }

    // Setup logging
    if (vm["log_to_file"].as<bool>()) {
        Log::set_default_logger(Log::file);
    }
    Log::set_level(opt.log_level);

    // Output file
    std::string fname = vm["output_file"].as<std::string>();
    H5::H5File out_file(fname, H5F_ACC_TRUNC);

    write_options(opt, out_file);

    // Compute the frames
    for(size_t i = 0; i<opt.frames; ++i) {
        double t = opt.t1 + (opt.t2-opt.t1)*i/std::max(opt.frames-1, 1lu);
        auto group = out_file.createGroup("/frame_" + std::to_string(i));

        EigenHDF5::save_scalar_attribute(group, "t", t);

        compute_frame(t, opt, group);
        std::cout << "Computed frame " << i+1 << "/" << opt.frames << "\n";
    }

    std::cout << "Saved image data as " << fname << std::endl;

    return 0;
}
