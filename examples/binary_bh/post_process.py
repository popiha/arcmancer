"""
A simple script to plot the output of the binary_bh program.
Models the radiation from the disk as black-body radiation based on the temperature of a Novikov-Thorne disk, with polarization computed from an semi-infinite 
electron scattering athmosphere model.
"""

import argparse

import h5py

import numpy as np
import scipy.constants as constants

import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.animation as animation

from pyarcmancer import electron_scattering_atmosphere


kB_eV = constants.value('Boltzmann constant in eV/K')


## Functions related to the Novikov-Thorne disk emission 

# black-body specific intensity in SI units
def bb_intensity(nu, T):
    h = constants.h
    return 2 * h * nu**3 /constants.c**2 /np.expm1( h*nu/(constants.k*T) )

# disk inner radius for NT model
def ms_radius(mass, chi):
    Z1 = 1 + (1-chi**2)**(1./3) * ( (1+chi)**(1./3) + (1-chi)**(1./3) )
    Z2 = (3*chi**2 + Z1**2)**(1./2)
    x0 = 3 + Z2 - np.sign(chi) * ( (3 - Z1)*(3 + Z1 + 2*Z2) )**(1./2)
    return x0*mass

# Novikov-Thorne disk
# - mass in solar masses
# - mdot in Eddington accretion rates
def novikov_thorne(r, mass, mdot, alpha):
    chi = 0
    rs = r / mass
    y = np.sqrt(rs)

    # radius of marginally stable orbit (in masses)
    x0 = ms_radius(mass, chi) / mass
    #print("x0", x0)

    # novikov-thorne quantities 
    # (c.f. Page & Thorne 1974, Abramowicz & Fragile 2013, Penna et al. 2012)
    A = 1 + chi**2 * y**-4 + 2*chi**2*y**-6
    B = 1 + chi*y**-3
    C = 1 - 3*y**-2 + 2*chi*y**-3
    D = 1 - 2*y**-2 + chi**2 * y**-4
    E = 1 + 4*chi**2*y**-4 - 4*chi**2 * y**-6 + 3*chi**4 * y**-8
    L0 = B / (y * np.sqrt(C))

    y0 = np.sqrt(x0)
    #print("y0", y0)
    y1 = 2 * np.cos( (np.arccos(chi) - np.pi)/3 )
    y2 = 2 * np.cos( (np.arccos(chi) + np.pi)/3 )
    y3 = -2 * np.cos( np.arccos(chi)/3 )

    L = L0 * (
            y - y0 - 3./2*chi * np.log(y/y0) \
          - 3*(y1-chi)**2 / ( y1*(y1-y2)*(y1-y3) ) * np.log((y-y1)/(y0-y1)) \
          - 3*(y2-chi)**2 / ( y2*(y2-y1)*(y2-y3) ) * np.log((y-y2)/(y0-y2)) \
          - 3*(y3-chi)**2 / ( y3*(y3-y1)*(y3-y2) ) * np.log((y-y3)/(y0-y3)))

    # compute disk properties in outer, middle and inner regions
    # fluxes are in units of erg cm^-2 s^-1
    # semiheights in cm
    # temperatures in K
    flux = 7e26 * mass**-1 * mdot * rs**-3 * B**-1 * C**(-1/2) * L
    outer = {
        'flux' : flux,
        'temperature' : 2e8 * alpha**(-1./5) * mass**(-1./5) * mdot**(3./10) * rs**(-3./4)  * \
            A**(-1./10) * B**(-1./5) * D**(-3./20) * E**(1./20) * L**(3./10),
        'pressure_ratio': 3 * alpha**(-1./10) * mass**(-1./10) * mdot**(-7./20) * rs**(3./8) * \
            A**(-11./20) * B**(9./10) * D**(7./40) * E**(11./40) * L**(-7./20),
        'opacity_ratio' : 2e-3 * mdot**(-1./2) * rs**(3./4) * \
            A**(-1./2) * B**(2./5) * D**(1./4) * E**(1./4) * L**(-1./2)
    }
    middle = {
        'flux' : flux,
        'temperature' : 7e8 * alpha**(-1./5) * mass**(-1./5) * mdot**(2./5) * rs**(-9./10) * \
            B**(-2./5) * D**(-1./5) * L**(2./5),
        'pressure_ratio': 7e-3 * alpha**(-1./10) * mass**(-1./10) * mdot**(-4./5) * rs**(21./20) * \
            A**(-1) * B**(9./5) * D**(2./5) * E**(1./2) * L**(-4./5),
        'opacity_ratio' : 2e-6 * mdot**(-1) * rs**(3./2) * \
            A**(-1) * B**(2) * D**(1./2) * E**(1./2) * L**(-1.)
    }
    inner = {
        'flux' : flux,
        'temperature' : 5e7 * alpha**(-1./4) * mass**(-1./4) * rs**(-3./8) * \
            A**(-1./2) * B**(1./2) * E**(1./4),
        'pressure_ratio': 4e-6 * alpha**(-1./4) * mass**(-1./4) * mdot**(-2) * rs**(21./8) * \
            A**(-5./2) * B**(9./2) * D * E**(5./4) * L**(-2),
    }

    regions = {
        'outer' : outer,
        'middle': middle,
        'inner' : inner
    }

    # works for all inputs
    outside_isco = rs >= x0
    outer_idx  = (middle['opacity_ratio'] >= 1) & outside_isco
    # XXX: the constants are only accurate to 1 digit, and since we compute the
    # flux from temperature, we need the temperature to match, not necessarily
    # the pressure ratio
    #inner_idx  = (inner['pressure_ratio'] <= 1) & outside_isco
    inner_idx  = (inner['temperature'] < middle['temperature']) & outside_isco
    middle_idx = np.logical_not(outer_idx | inner_idx) & outside_isco

    output_keys = ['flux', 'temperature', 'pressure_ratio']
    output = {}
    for key in output_keys:
        output[key] = np.zeros_like(rs)
        output[key][inner_idx]  = inner[key][inner_idx]
        output[key][middle_idx] = middle[key][middle_idx]
        output[key][outer_idx]  = outer[key][outer_idx]

    return (output, regions)


def compute_frame_data(frame_group, header, args):
    
    accretion_rate = args.accretion_rate
    alpha = args.alpha
    mass_scale = args.mass_scale
    
    mass = header["primary_mass"]*mass_scale
    x_bins = header["xbins"]
    y_bins = header["ybins"]

    t = frame_group.attrs["t"]#* mass_scale
    
    xbins = header["xbins"]
    ybins = header["ybins"]
    
    
    radii          = np.array(frame_group["r_eq"])*mass_scale
    obs_hit_angle  = np.array(frame_group["observer_hit_angle"])
    redshift       = np.array(frame_group["redshift"])
    polangle       = np.array(frame_group["pol_angle"])
    surface_index  = np.array(frame_group["surface_index"], dtype=int)
    

    ntdisk, ntdisk_regions = novikov_thorne(radii, mass, 
                                            accretion_rate, alpha)

    xspan = header["xspan"]
    dx = xspan/xbins # distance from edge of pixel to center
    yspan = header["yspan"]
    dy = yspan/ybins
    xs, ys = np.meshgrid(np.linspace(-xspan + dx, xspan - dx, xbins),
                        np.linspace(-yspan + dy, yspan - dy, ybins))

    hit_disk = surface_index == 2
    

    scattering_coeffs = np.vectorize(electron_scattering_atmosphere,
                                     signature='()->(3)')(obs_hit_angle)

    # polarization fraction
    polfrac = scattering_coeffs[:,:,2]

    # angle dependent correction to intensity from scattering
    correction_factor = scattering_coeffs[...,1]

    
    T = ntdisk['temperature']
    G = redshift 
    nu = args.frequency
    
    I = G**3 * bb_intensity(nu/G, T) * correction_factor
    Q = np.cos(2*polangle)*I*polfrac
    U = np.sin(2*polangle)*I*polfrac

    # zero data outside disk
    I[~hit_disk] = Q[~hit_disk] = U[~hit_disk] = 0
    polfrac[~hit_disk] = polangle[~hit_disk] = 0
    redshift[~hit_disk] = 0

    return {
            "t"  : t,
            "I" : I,
            "Q" : Q,
            "U" : U,
            "polfrac" : polfrac,
            "polangle" : polangle,
            "redshift" : redshift,
            "hit_disk" : hit_disk,
            "xs" : xs,
            "ys" : ys
           }


def load_data(args):
    f = h5py.File(args.data_file, 'r')

    header = dict(f.attrs)

    frames = [compute_frame_data(f["frame_"+str(i)], header, args)
              for i in range(header["frames"])]
    
    return header, frames



def plot_single_frame(header, frame):

    extent = ( -header["xspan"], header["xspan"], 
               -header["yspan"], header["yspan"])
    interpolation = 'none'
    
    plt.imshow(frame["I"],
               interpolation=interpolation,
               extent=extent,
               origin='lower',
               cmap='afmhot')
    bar = plt.colorbar()
    bar.formatter.set_useOffset(False)
    bar.update_ticks()
    bar.set_label(r"$I_\nu (\mathrm{W}/\mathrm{m}^2\mathrm{Hzsr}) $")
    plt.xlabel(r'$x/M$')
    plt.ylabel(r'$y/M$')
    
    # add polarization
    xs, ys = frame["xs"], frame["ys"]
    polfrac, polangle = frame["polfrac"], frame["polangle"]
    us = polfrac * np.cos(polangle)
    vs = polfrac * np.sin(polangle)
    skip = int(header["xbins"]/30)

    plt.quiver(xs[::skip,::skip], ys[::skip,::skip],
               us[::skip,::skip], vs[::skip,::skip], 
               units='xy', angles='xy', scale_units="width",
               scale=1, headwidth=0)
    
    plt.show()


def animate_frames(header, frames):
    
    extent = ( -header["xspan"], header["xspan"], 
               -header["yspan"], header["yspan"])
    
    gridkw = {
              "height_ratios" : [3,1]
        }
    fig, axarr = plt.subplots(2, 1, gridspec_kw=gridkw)

    # total flux, polarization fraction
    total_flux = np.array([np.sum(f["I"]) for f in frames])
    P = np.sqrt(np.array([np.sum(f["Q"])**2 + np.sum(f["U"])**2 
                          for f in frames]))/total_flux

    max_total_flux = np.max(total_flux)



    t = np.array([f["t"] for f in frames])
    
    vmax = np.max([f["I"] for f in frames])

    img = axarr[0].imshow(frames[0]["I"], origin="lower",
                          cmap="afmhot",
                          extent=extent, vmin=0, vmax=vmax,
                          interpolation='none',
                          animated=True)
    bar = fig.colorbar(img, ax=axarr[0])
    bar.set_label(r"$I_\nu (\mathrm{W}/\mathrm{m}^2\mathrm{Hzsr}) $")
    axarr[0].set_xlabel(r"$x/M$")
    axarr[0].set_ylabel(r"$y/M$")
    
    # polarization bars
    xs, ys = frames[0]["xs"], frames[0]["ys"]
    skip = int(header["xbins"]/30)
    polfrac, polangle = frames[0]["polfrac"], frames[0]["polangle"]
    us = polfrac * np.cos(polangle)
    vs = polfrac * np.sin(polangle)
    
    quiver_kwargs = dict(units='xy', angles='xy', 
             headwidth=1, headlength=0, headaxislength=0, 
             scale=2e-2, scale_units='xy', pivot='mid', color='gray',
             linewidth=0, edgecolor='k', 
             alpha=1)

    quiver = axarr[0].quiver(xs[::skip,::skip], ys[::skip,::skip],
                             us[::skip,::skip], vs[::skip,::skip], 
                             **quiver_kwargs)

   
    # plot light curve
    axarr[1].plot(t, total_flux/max_total_flux, color='tab:blue')
    axarr[1].set_xlabel(r"$t/M$")
    axarr[1].set_ylabel("Total flux (normalized)", color='tab:blue')
    axarr[1].set_xlim(frames[0]["t"], frames[-1]["t"])
    # polarization fraction
    Pax = axarr[1].twinx()
    Pax.plot(t, P, color='tab:orange')
    Pax.set_ylabel("Polarization fraction", color='tab:orange')
    
    vline = axarr[1].axvline(frames[0]["t"], color="red")

    fig.tight_layout()

    def update_fig(frame):
        img.set_array(frame["I"])
        polfrac, polangle = frame["polfrac"], frame["polangle"]
        us = polfrac * np.cos(polangle)
        vs = polfrac * np.sin(polangle)
        quiver.set_UVC(us[::skip,::skip],vs[::skip,::skip])
        vline.set_xdata([frame["t"]])

        return img, quiver, vline

    return animation.FuncAnimation(fig, update_fig, frames=frames, 
                                   repeat=True, blit=True)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("data_file", 
                        help="the file containing the binary_bh output")

    parser.add_argument("--accretion_rate", type=float,
                        help="accretion rate as fraction of Eddington limit",
                        default=0.2)

    parser.add_argument("--alpha", type=float, default=0.1,
                        help="the alpha viscosity parameter of the Novikov-"
                            + "Thorne model")

    parser.add_argument("--mass_scale", type=float, default=5e6,
                        help="Unit of mass in solar masses")

    parser.add_argument("--frequency", type=float, default=5e16,
                        help="The frequency of observations") 

    parser.add_argument("--save_animation", 
                        help="Save the animation if there are multiple frames",
                        action="store_true")

    args = parser.parse_args() 
    header, frames = load_data(args)
    if len(frames) == 1:
        plot_single_frame(header, frames[0])
    else:
        print(len(frames))
        ani = animate_frames(header, frames)
        
        if args.save_animation:
            ani_fname = args.data_file + ".mp4"
            print("Saving animation as ", ani_fname)
            #TODO improve output quality with options
            ani.save(ani_fname, fps=25)

        
        plt.show()
