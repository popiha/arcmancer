#pragma once

#include <iostream>

#include <boost/math/special_functions/gamma.hpp>
#include <boost/math/special_functions/bessel.hpp>

#include <arcmancer/radiation/radiation_types.h>
#include <arcmancer/units.h>
#include <arcmancer/spacetimes/kerr/kerr_spacetime.h>

// In any larger application this should not be done at file scope in headers,
// but here it keeps things concise.
using namespace arcmancer;

// Ultrarelativistic thermal synchrotron emission from Dexter 2016.
// A modified version of the one included in the main library to include
// additional compatibility settings.
class ThermalSynchrotron {

    double length_unit_; // conversion factor between CGS and the internal 
                         // geometric units to pass to RadiationData
    double theta_e_min_; // lower cutoff for electron temperature, model only
                         // valid for theta_e >~ 1. 
                         // Only used when not in grtrans mode
    bool grtrans_compatibility_; // use grtrans compatible bessel function approximations
    bool scalar_transport_; // disregard polarization effects
public:
    ThermalSynchrotron(double length_unit,
                       double theta_e_min, 
                       bool grtrans_compatibility = false,
                       bool scalar_transport = false)
    : length_unit_(length_unit),
      theta_e_min_(theta_e_min),
      grtrans_compatibility_(grtrans_compatibility),
      scalar_transport_(scalar_transport) {
        std::cout << "grtrans compatibility mode is "
                  << (grtrans_compatibility_ ? "ON" : "OFF") << std::endl;
        std::cout << "scalar only transport mode is "
                  << (scalar_transport_ ? "ON" : "OFF") << std::endl;
    }

    double planck_flux(double nu, double T) const {
        using namespace std;

        const double x = units::h_cgs * nu / (units::k_cgs * T);

        // for numerical reasons, case of small x needs to handled with
        // the Rayleigh-Jeans approximation
        double expm1 = 1;
        if (fabs(x)<= std::sqrt(numeric_limits<double>::epsilon())) {
            expm1 = x + x*x/2;
        }
        else {
            expm1 = exp(x) - 1;
        }

        return 2 * units::h_cgs * pow(nu, 3) / pow(units::c_cgs, 2) 
            * 1 / expm1;
    }

    // modified bessel functions of the second kind. placeholders here,
    // to achieve results equal to grtrans, which uses 1st order
    // approximations that go severely wrong
    double bessel_k(int order, double x)  const {
        if (grtrans_compatibility_) {
            switch (order) {
                case 0:
                    return -std::log(x/2) - 0.5772;
                case 1:
                    return 1./x;
                case 2:
                    return 2./(x*x);
                default:
                    return boost::math::cyl_bessel_k(order, x);
            }
        }

        return boost::math::cyl_bessel_k(order, x);
    }

    // fitting functions f(X) and g(X) from Shcherbakov (2008)
    double f_function(double X)  const {
        using namespace std;

        return 2.011 * exp(-pow(X, 1.035)/4.7)
            - cos(X/2) * exp(-pow(X, 1.2)/2.73) 
            - 0.011 * exp(-X/47.2);
    }

    double g_function(double X)  const {
        using namespace std;

        // The formula is g = 1 - 0.11 * log(1 + 0.035 * X),
        // so use log1p for stability, as X is often tiny
        return 1 - 0.11 * log1p(0.035 * X);
    }

    // modified fitting functions in Dexter (2016)
    double f_function_D16(double X)  const { using namespace std;

        return f_function(X) + (0.011 * exp(-X/47.2) 
            - pow(2,-1./3) / pow(3,23./6) * 1e4 * cnst::pi * pow(X, -8./3) )
            * 0.5 * (1 + tanh(10 * log(X/120)) );
    }

    double g_function_D16(double X)  const {
        using namespace std;

        return 0.43793091 * log(1+0.00185777 * pow(X, 1.50316886));
    }

    // emissivity functions from Dexter (2016), originally from Huang et
    // al (2009 and Madhevan (1996)
    double I_Q(double x)  const {
        using namespace std;

        return 2.5651 * (
                1 + 0.93193 * pow(x, -1./3) + 0.499873 * pow(x, -2./3) 
                ) * exp(-1.8899 * pow(x, 1./3));
    }

    double I_V(double x)  const {
        using namespace std;

        return (1.81384/x + 3.42319 * pow(x, -2./3) + 0.0292545 / sqrt(x)
                + 2.03773 * pow(x, -1./3) ) * exp(-1.8899 * pow(x, 1./3));
    }

    double I_I(double x)  const {
        using namespace std;

        return 2.5651 * (1 + 1.92 * pow(x, -1./3) + 0.9977 * pow(x, -2./3))
            * exp(-1.8899 * pow(x, 1./3));
    }

    // This method is the only thing required to be usable as a radiation
    // model.
    RadiationData operator()(const FluidData<KerrSpacetime> &fluid, 
        double nu, double th_B) const {

        using namespace std;

        double n = fluid.number_density;
        double B = fluid.magnetic_field_strength;
        double T = fluid.temperature;


        // DEBUG: fix input
#if 0
        n           =  7079.13;
        B           =  0.791674;
        th_B        =  1.34757;
        T           =  4.74452e+09;
        nu          =  2.46556e+11;
#endif

        // grtrans enforces minimum values for theta and nu_c.
        // In grtrans, this is enforced via addition and hardcoded values.
        const double theta_e_min =
            grtrans_compatibility_ ? 1e-10 : theta_e_min_;
        const double nu_c_min = 1;

        // shorthand for e^2, which comes up all the time
        constexpr double e2 = units::e_cgs * units::e_cgs;

        // dimensionless electron temperature
        double theta_e;
        if (grtrans_compatibility_) {
            // Dexter 2016 compatibility. minimum value is _added_
            theta_e = units::k_cgs * T / (units::m_e_cgs * pow(units::c_cgs, 2))
                + theta_e_min;
        }
        else {
            theta_e = max(units::k_cgs * T / (units::m_e_cgs * pow(units::c_cgs, 2)),
                theta_e_min);
        }

        const double nu_B = units::e_cgs * B /
            (2 * cnst::pi * units::m_e_cgs * units::c_cgs);

        double nu_c;
        if (grtrans_compatibility_) {
            nu_c = 3./2 * nu_B * sin(th_B) * pow(theta_e, 2)
                + nu_c_min;
        }
        else {
            nu_c = max(3./2 * nu_B * sin(th_B) * pow(theta_e, 2),
                nu_c_min);
        }

        const double x = nu / nu_c;

        // This is given in Dexter (2016)
        double j_I = 
            n * e2 * nu / (2 * sqrt(3) * units::c_cgs * pow(theta_e,2) ) * I_I(x);
        double j_Q =
            n * e2 * nu / (2 * sqrt(3) * units::c_cgs * pow(theta_e,2) ) * I_Q(x);
        double j_U = 0;
        double j_V =
            2 * n * e2 * nu * (1/tan(th_B)) / (3 * sqrt(3) * units::c_cgs * pow(theta_e,3) ) * I_V(x);

        const double B_nu = planck_flux(nu, T);

        double a_I = j_I / B_nu;
        double a_Q = j_Q / B_nu;
        double a_U = j_U / B_nu;
        double a_V = j_V / B_nu;

        // Faraday cofficients come originally from Shcherbakov (2008)
        // and Huang & Shcherbakov (2011), but use formulae modified by
        // Dexter. There is also apparently sign confusion issues, as
        // documented by Dexter.
        // TODO: This should be checked out, but Dexter seems confident
        // enough in having figured out the sign right.

        // Due to Stokes Q being aligned with magnetic field, U
        // components should be zero throughout
        double r_U = 0.0;

        // the angular frequency we are considering
        const double w = 2 * cnst::pi * nu;

        // square of the plasma angular frequency
        const double w_p2 = 4 * cnst::pi * n * e2 / units::m_e_cgs;

        // cyclotron angular frequency
        const double Omega_0 = 2 * cnst::pi * nu_B;
            //units::e_cgs * B / (units::m_e_cgs * units::c_cgs);

        // Shcherbakov (2008) X-parameter, eq. (27)
        const double xpar = theta_e * sqrt(
                sqrt(2) * sin(th_B) * 1e3 * Omega_0/w );

        // difference of diagonal elements of dielectric tensor:
        // eps^1_1 - eps^2_2
        const double diagonal_D16 = 
            f_function_D16(xpar) * w_p2 * pow(Omega_0, 2)/pow(w,4) * (
                bessel_k(1, 1/theta_e) / bessel_k(2, 1/theta_e) + 6 * theta_e
            ) * pow(sin(th_B), 2);

        // the non-diagonal component
        const double non_diagonal_D16 =
            w_p2 * Omega_0 / pow(w,3) * (
                    bessel_k(0, 1/theta_e) - g_function_D16(xpar)
                )/ bessel_k(2, 1/theta_e) * cos(th_B);

        // The Shcherbakov (2008) versions
        // XXX: according to Dexter, the sign in r_Q here is likely
        // erroneous, so it's been changed
        const double r_Q_D16 = w / (2*units::c_cgs) * diagonal_D16;
        const double r_V_D16 = w / units::c_cgs * non_diagonal_D16;

        // use D16 versions
        double r_Q = r_Q_D16;
        double r_V = r_V_D16;

        // DEBUG: no absorption and/or faraday rotation
        //a_I = 0.0;
        //a_Q = 0.0;
        //a_U = 0.0;
        //a_V = 0.0;
        //r_Q = 0.0;
        //r_U = 0.0;
        //r_V = 0.0;


        // DEBUG: print everything
#if 0
        //double n, double B, double th_B, double nu, double T) const {
        std::cout << "Polarized thermal synchrotron:" << "\n"
            << " pi              " << cnst::pi                 << "\n"
            << " m_e             " << units::m_e_cgs           << "\n"
            << " c               " << units::c_cgs             << "\n"
            << " n               " << n                        << "\n"
            << " B               " << B                        << "\n"
            << " th_B            " << th_B                     << "\n"
            << " T               " << T                        << "\n"
            << " nu              " << nu                       << "\n"
            << " nu_c            " << nu_c                     << "\n"
            << " nu_B            " << nu_B                     << "\n"
            << " x = nu / nu_c   " << x                        << "\n"
            << " nu / nu_B       " << nu / nu_B                << "\n"
            << " theta_e         " << theta_e                  << "\n"
            << " I_I(x)          " << I_I(x)                   << "\n"
            << " I_Q(x)          " << I_Q(x)                   << "\n"
            << " I_V(x)          " << I_V(x)                   << "\n"
            << " SH08 Xpar       " << xpar                     << "\n"
            //<< " D16 Xpar        " << xpar_D16                 << "\n"
            << " SH08 f(X)       " << f_function(xpar)         << "\n"
            << " SH08 g(X)       " << g_function(xpar)         << "\n"
            << " D08 f(X)        " << f_function_D16(xpar/*_D16*/) << "\n"
            << " D08 g(X)        " << g_function_D16(xpar/*_D16*/) << "\n"
            //<< " SH08 eps1122    " << diagonal_SH08            << "\n"
            //<< " SH08 eps12      " << non_diagonal_SH08        << "\n"
            << " D16 eps1122     " << diagonal_D16             << "\n"
            << " D16 eps12       " << non_diagonal_D16         << "\n"
            //<< " SH08 r_Q        " << r_Q_SH08                 << "\n"
            //<< " SH08 r_V        " << r_V_SH08                 << "\n"
            << " D16 r_Q         " << r_Q_D16                  << "\n"
            << " D16 r_V         " << r_V_D16                  << "\n"
            << " j_I             " << j_I                      << "\n"
            << " j_Q             " << j_Q                      << "\n"
            << " j_U             " << j_U                      << "\n"
            << " j_V             " << j_V                      << "\n"
            << " B_nu            " << B_nu                     << "\n"
            << " a_I             " << a_I                      << "\n"
            << " a_Q             " << a_Q                      << "\n"
            << " a_U             " << a_U                      << "\n"
            << " a_V             " << a_V                      << "\n"
            << " r_Q             " << r_Q                      << "\n"
            << " r_U             " << r_U                      << "\n"
            << " r_V             " << r_V                      << "\n"
            << " bes_k_0(1/th_e) " << bessel_k(0, 1/theta_e)   << "\n"
            << " bes_k_1(1/th_e) " << bessel_k(1, 1/theta_e)   << "\n"
            << " bes_k_2(1/th_e) " << bessel_k(2, 1/theta_e)   << "\n"
            ;

        exit(0);
#endif
        if (scalar_transport_) {
            // leave out all polarization effects
            j_Q = j_U = j_V = 0;
            a_Q = a_U = a_V = 0;
            r_Q = r_U = r_V = 0;
        }

        // filter possible nans from 0/0 limits
        // if anything is nan, then everything should vanish
        if(std::isnan(j_I) || std::isnan(j_Q) || std::isnan(j_U) || std::isnan(j_V)
           || std::isnan(a_I) || std::isnan(a_Q) || std::isnan(a_U) || std::isnan(a_V)
           || std::isnan(r_Q) || std::isnan(r_U) || std::isnan(r_V)) {

            return {0,0,0,0,
                    0,0,0,0,
                    0,0,0,
                    length_unit_};
        }

        if (grtrans_compatibility_) {
            // invert sign of r_U, since we are using a different
            // convention for the Müller matrix than grtrans
            r_U = -r_U;
        }

        return {j_I, j_Q, j_U, j_V, 
                a_I, a_Q, a_U, a_V, 
                r_Q, r_U, r_V, 
                length_unit_};
    }
};
