#pragma once

#include <fstream>
#include <stdexcept>
#include <string>
#include <utility>

#include <arcmancer/general_utils.h>
#include <arcmancer/geometry.h>
#include <arcmancer/spacetimes/kerr.h>
#include <arcmancer/log.h>
#include <arcmancer/interpolation/nlinear.h>
#include <arcmancer/units.h>

// In any larger application this should not be done at file scope in headers,
// but here it keeps things concise.
using namespace arcmancer;

// For converting between HARM units and CGS units
struct HarmToCGS {
    // grtrans versions
    const double gr_h_cgs = 6.626e-27;
    const double gr_k_cgs = 1.38e-16;
    const double gr_e_cgs = 4.8032e-10;
    const double gr_G_cgs = 6.67e-8;
    const double gr_M_sun_cgs = 1.998e33;
    const double gr_sigma_SB_cgs = 5.6704e-5;

    // what version to use
    const double G_cgs_;
    const double M_sun_cgs_;
    const double k_cgs_;

    const double length_to_cgs;
    const double time_to_cgs;
    const double density_to_cgs;
    const double pressure_to_cgs;
    const double Bfield_to_cgs;

    HarmToCGS(double bh_mass_in_Msun, double accretion_rate_cgs,
              double accretion_rate_code, bool grtrans_compatibility = false)
    : G_cgs_(grtrans_compatibility ? gr_G_cgs : units::G_cgs),
      M_sun_cgs_(grtrans_compatibility ? gr_M_sun_cgs : units::M_sun_cgs),
      k_cgs_(grtrans_compatibility ? gr_k_cgs : units::k_cgs),
      length_to_cgs(G_cgs_ * bh_mass_in_Msun * M_sun_cgs_ /
                    pow(units::c_cgs, 2)),
      time_to_cgs(length_to_cgs / units::c_cgs),
      density_to_cgs(accretion_rate_cgs / accretion_rate_code * time_to_cgs /
                     pow(length_to_cgs, 3)),
      pressure_to_cgs(density_to_cgs * pow(units::c_cgs, 2)),
      // Use the Gaussian convention (instead of Lorentz-Heaviside)
      Bfield_to_cgs(std::sqrt(4 * cnst::pi * density_to_cgs) * units::c_cgs) {}
};

// For reading in HARM 1.0 data dumps
// Specialized to one particular output format, but should be fairly simple
// to adapt to other formats as well.
// Various things could probably be optimized for better performance.

class HarmData {
public:
    using PointType = ManifoldPoint<kerr::KerrSpacetime>;

    // The useful data contained in the header row
    struct HarmHeader {
        double t;
        size_t N1, N2;
        double startx[2];
        double dx[2];
        double a, gam, hslope, R0;

        friend std::ostream& operator<<(std::ostream &os, const HarmHeader &h) {
            return os
                << "HarmHeader: " << std::endl
                << "         t: " << h.t << std::endl
                << "     N1,N2: " << h.N1 << ", " << h.N2 << std::endl
                << "    startx: (" << h.startx[0] << ", " << h.startx[1] << ")" << std::endl
                << "        dx: (" << h.dx[0] << ", " << h.dx[1] << ")" << std::endl
                << "         a: " << h.a << std::endl
                << "       gam: " << h.gam << std::endl
                << "    hslope: " << h.hslope << std::endl
                << "        R0: " << h.R0 << std::endl;
        }
    };

    // public interface for reading a header
    static HarmHeader read_header(const std::string &fname) {
        std::ifstream instream(fname);
        if (!instream.is_open()) {
            throw std::runtime_error("Could not open file " + fname);
        }

        return read_header(instream);
    }

    HarmData(const std::vector<std::string> &fnames,
             const kerr::KerrSpacetime &M)
    : M_(&M) {
        std::ifstream instream(fnames[0]);
        if (!instream.is_open()) {
            throw std::runtime_error("Could not open file " + fnames[0]);
        }
        header_ = read_header(instream);
        // HARM data assumes mass=1, a=chi
        if (M_->mass() != 1) {
            throw std::runtime_error("HARM data assumes mass=1, now M.mass()=" +
                                     std::to_string(M_->mass()));
        }
        if (M_->chi() != header_.a) {
            throw std::runtime_error("Inconsistent chi, data chi=" +
                                     std::to_string(header_.a) + ",  M.chi()=" +
                                     std::to_string(M_->chi()));
        }

        size_t time_cells = fnames.size();

        single_frame_ = time_cells == 1;

        // Setup storage.
        // Note that time increases along the last index.
        // This should in principle improve performance as data is stored in
        // column major order and the time index should change the slowest along
        // a ray. However, the actual effect has not been measured.

        is_invalid_cell_ =
            Eigen::Tensor<bool, 3>(header_.N1, header_.N2, time_cells);

        rho_ = Eigen::Tensor<double, 3>(header_.N1, header_.N2, time_cells);
        
        internal_energy_ =
            Eigen::Tensor<double, 3>(header_.N1, header_.N2, time_cells);

        ucon_ = Eigen::Tensor<EigenRk1Tensor<4>, 3>(header_.N1, header_.N2,
                                                    time_cells);

        bcon_ = Eigen::Tensor<EigenRk1Tensor<4>, 3>(header_.N1, header_.N2,
                                                    time_cells);

        grid_[0].resize(header_.N1, 0);
        grid_[1].resize(header_.N2, 0);
        grid_[2].resize(time_cells, 0);

        t_span_.first = header_.t;

        // read the data files
        for (size_t i = 0; i < fnames.size(); ++i) {
            std::ifstream instream2(fnames[i]);
            if (!instream2.is_open()) {
                throw std::runtime_error("Could not open file " + fnames[i]);
            }
            auto header = read_header(instream2);
            // Basic sanity checks
            if (header.N1 != header_.N1 || header.N2 != header_.N2 ||
                header.startx[0] != header_.startx[0] ||
                header.startx[1] != header_.startx[1] ||
                header.dx[0] != header_.dx[0] ||
                header.dx[1] != header_.dx[1] || header.a != header_.a ||
                header.gam != header_.gam || header.R0 != header_.R0 ||
                header.hslope != header_.hslope) {

                std::stringstream ss;
                ss << "Inconsistent file headers!\n"
                   << "File " << fnames[0] << " header:\n"
                   << header_ << "\n file " << fnames[i] << " header:\n"
                   << header;
                throw std::runtime_error(ss.str());
            }

            read_data(instream2, header.t, i);
            t_span_.second = header.t;
        }
        
    }

    // The interpolated data
    struct Data {
        double density;
        double internal_energy;
        double pressure;
        TangentVector<kerr::KerrSpacetime> u; // fluid 4-velocity
        TangentVector<kerr::KerrSpacetime> B; // magnetic field 4-vector
    };
    
    // Interpolate data at a point
    // XXX: in some cases linear interpolation doesn't produce valid vectors,
    // e.g. spacelike 4-velocity. These cases need to checked by the caller.
    Data data(const PointType &point) const {
        auto grid_coords = grid_coords_from_point(point);
        // cap the time coordinate to the data limits, so that time stops when
        // the data ends.
        cap_value(grid_coords(2), t_span_);

        auto out_coords = point.coordinates(M_->kerr_schild_out);
       
        // Check for masked cells and extrapolation
        bool masked, extrapolating;
        std::tie(masked, extrapolating) =
            interpolate_scalar(grid_coords, is_invalid_cell_);

        if (masked || extrapolating){
            // The point is outside the grid or invalid masked cells would be
            // used for interpolation.
            // Extrapolation usually doesn't work so
            // just return 0's / some reasonable values
            return {0,0,0, 
                    normalized(CotangentVector<kerr::KerrSpacetime>{
                              point, M_->kerr_schild_out, {1, 0, 0, 0}})
                        .raise_index<0>(),
                    {point, M_->kerr_schild_out, {0, 0, 0, 1}}
                   };
        }
        auto u = interpolate_vector(out_coords, grid_coords, ucon_).first;
        auto B = interpolate_vector(out_coords, grid_coords, bcon_).first;
        auto rho = interpolate_scalar(grid_coords, rho_).first;
        auto internal_energy = 
            interpolate_scalar(grid_coords, internal_energy_).first;
        double pressure = (header_.gam - 1) * internal_energy;
        
        return { rho, internal_energy, pressure,
                {point, M_->kerr_schild_out, u},
                {point, M_->kerr_schild_out, B},
               };
    }
    
    // radius of the sphere containing the data in BL/spherical KS
    double r_max() const { return r_max_; }
    
    // timespan of the data
    const std::pair<double, double>& t_span() const { return t_span_; }

    // get the header of the first file
    const HarmHeader& header() const { return header_; }

    double t_offset = 0; // offset for time coordinate

private:
    const kerr::KerrSpacetime *M_;
    double r_max_ = 0;
    std::pair<double, double> t_span_;
    bool single_frame_;
    HarmHeader header_;
    // The data is stored in outgoing KS, since that's the best chart for the
    // ray computations.
    // Use Eigen Tensors for storage since need dynamic multidimensional arrays
    Eigen::Tensor<double, 3> rho_; // density
    Eigen::Tensor<double, 3> internal_energy_;
    Eigen::Tensor<EigenRk1Tensor<4>, 3> ucon_; // fluid 4-velocity components
    Eigen::Tensor<EigenRk1Tensor<4>, 3> bcon_; // magnetic field components
    // Mask for cells that produce invalid data.
    // Even though there is no explicit support for masked data in the 
    // interpolation routines, booleans can still be interpolated.
    // The result is true if any of the cells used for interpolation are true.
    Eigen::Tensor<bool, 3> is_invalid_cell_; 

    // Need the coordinate grid for Arcmancer's generic interpolation
    // routines
    interpolation::GridSpec<3> grid_;

    // header format from HARM/dump.c
    static HarmHeader read_header(std::ifstream &instream) {
        HarmHeader h;

        instream >> h.t >> h.N1 >> h.N2 >> h.startx[0] >> h.startx[1] >>
            h.dx[0] >> h.dx[1];
        skip<2, double>(instream) >> h.a >> h.gam;
        skip<13, double>(instream) >> h.hslope >> h.R0;

        Log::log()->debug("Read in HARM header:\n{}", h);

        return h;
    }

    // Storage indices from the HARM internal representation coordinates
    // for the spatial slice.
    // Should only be called for coordinates that are actually on the grid
    std::pair<size_t, size_t> indices_from_coords(double X1, double X2) const {

        // the coords are given at the center of the grid cell,
        // and apparently startx at the edge
        double index1 = (X1 - header_.startx[0]) / header_.dx[0] - .5;
        double index2 = (X2 - header_.startx[1]) / header_.dx[1] - .5;

        return {static_cast<size_t>(std::lround(index1)),
                static_cast<size_t>(std::lround(index2))};
    }

    void read_data(std::ifstream &instream, double t, size_t frame_index) {
        // after reading the header the instream is at the beginning of the
        // first row
        for (size_t i = 0; i < header_.N1 * header_.N2; ++i) {
            read_row(instream, t, frame_index);
        }
    }

    // read a row of the data dump
    void read_row(std::ifstream &instream, double t, size_t frame_index) {
        using namespace std;

        double X1, X2;
        instream >> X1 >> X2;

        size_t i1, i2;
        std::tie(i1, i2) = indices_from_coords(X1, X2);

        double r, th;
        instream >> r >> th;
        
        grid_[0][i1] = r;
        grid_[1][i2] = th;
        grid_[2][frame_index] = t;

        is_invalid_cell_(i1, i2, frame_index) = false;


        instream  >> rho_(i1, i2, frame_index)  >>
            internal_energy_(i1, i2, frame_index);
        if (r > r_max_) { r_max_ = r; }

        // 6 other internal variables and divb
        skip<7, double>(instream);
        // ucon
        // convert from modified ingoing spherical KS
        // to ingoing cartesian KS and then to outgoing KS.
        // The stored vector is in the y=0 plane.
        // th = pi*X2 + .5*(1-hslope)*sin(2*pi*X2)
        // r = exp(X1) + R0
        ArcMatrix<4> jac_int_to_sks;
        // clang-format off
        jac_int_to_sks << 1, 0, 0, 0,
                          0, r - header_.R0, 0, 0,
                          0, 0,
            cnst::pi * (1 + (1 - header_.hslope) * cos(2 * cnst::pi * X2)), 0,
            0, 0, 0, 1;
        // clang-format on

        // Convert to ingoing cartesian KS with phi=0
        const double a = header_.a;
        ArcMatrix<4> jac_sks_to_cks;
        // clang-format off
        jac_sks_to_cks << 1, 0, 0, 0,
                          0, sin(th), r * cos(th), -a * sin(th),
                          0, 0, a * cos(th), r * sin(th),
                          0, cos(th), -r * sin(th), 0;
        // clang-format on

        ArcMatrix<4> jac = jac_sks_to_cks * jac_int_to_sks;

        double v0, v1, v2, v3;
        instream >> v0 >> v1 >> v2 >> v3;

        ArcVector<4> v_;
        v_ << v0, v1, v2, v3;
        v_ = jac * v_;

        PointType point(M_->kerr_schild_in,
                        {0, r * sin(th), a * sin(th), r * cos(th)});

        TangentVector<kerr::KerrSpacetime> u(point, M_->kerr_schild_in,
                                             {v_(0), v_(1), v_(2), v_(3)});

        auto point_coord = point.coordinates(M_->kerr_schild_out);
        double point_x = point_coord(1);
        double point_y = point_coord(2);
        const double r_xy = sqrt(point_x * point_x + point_y * point_y);

        if (!std::isfinite(point_x)) {
            // Coordinate conversion fails, we're likely inside the horizon.
            // These cells can be safely ignored since they can't affect the
            // ray, except maybe in the interpolation
            is_invalid_cell_(i1, i2, frame_index) = true;
            Log::log()->debug("HarmData: Ignoring invalid cell X1={} X2={}"
                             "\nr={} th={}", X1, X2, r, th);
        } else {

            // Get components in outgoing KS
            auto u_comp = u.components(M_->kerr_schild_out);
            // rotate to y=0 plane
            EigenRk1Tensor<4> rotated;
            rotated.setValues({
                    u_comp(0),
                    (point_x * u_comp(1) + point_y * u_comp(2)) / r_xy,
                    (-point_y * u_comp(1) + point_x * u_comp(2)) / r_xy,
                    u_comp(3)
                });
            ucon_(i1, i2, frame_index) = rotated;
        }
        // ucov
        skip<4, double>(instream);

        // bcon
        instream >> v0 >> v1 >> v2 >> v3;
        v_ << v0, v1, v2, v3;
        v_ = jac * v_;

        TangentVector<kerr::KerrSpacetime> b(point, M_->kerr_schild_in,
                                             {v_(0), v_(1), v_(2), v_(3)});

        auto b_comp = b.components(M_->kerr_schild_out);

        // rotate to y=0 plane
        EigenRk1Tensor<4> rotated;
        rotated.setValues({
                b_comp(0),
                (point_x * b_comp(1) + point_y * b_comp(2)) / r_xy,
                (-point_y * b_comp(1) + point_x * b_comp(2)) / r_xy,
                b_comp(3)
            });
        bcon_(i1, i2, frame_index) = rotated;

        // bcov, vmin/vmax, g
        skip<9, double>(instream);
    }

    Eigen::Vector3d grid_coords_from_point(const PointType &point) const {
        auto coords = point.coordinates(M_->boyer_lindquist);
        const double r = coords(1);
        const double th = coords(2);

        // Time is the ingoing KS time with offset
        // optimize the single frame case
        double t = t_span_.first;
        if (!single_frame_) {
            t = point.coordinates(M_->kerr_schild_in)(0) + t_offset;
        }

        return {r, th, t};
    }

    // Interpolate a scalar value on the grid,
    template<typename Scalar> 
    std::pair<Scalar, bool> interpolate_scalar(
        const Eigen::Vector3d &grid_coords, 
        const Eigen::Tensor<Scalar, 3> &data) const {

        double res;
        bool extrapolating;
        std::tie(res, extrapolating) = interpolation::interpolate<3,Scalar>(
                grid_, data, grid_coords);

        return {res, extrapolating};
    }

    // Interpolate a vector value on the grid
    std::pair<EigenRk1Tensor<4>, bool> interpolate_vector(
        const EigenRk1Tensor<4> &point_ks_coords,
        const Eigen::Vector3d &grid_coords,
        const Eigen::Tensor<EigenRk1Tensor<4>, 3> &data) const {

        EigenRk1Tensor<4> ret;
        bool extrapolating;
        std::tie(ret, extrapolating) =
            interpolation::interpolate<3, EigenRk1Tensor<4>>(grid_, data,
                                                             grid_coords);

        // rotate out of y=0 plane which is used for internal storage
        const double out_x = point_ks_coords(1), out_y = point_ks_coords(2);
        const double r_xy = std::sqrt(out_x * out_x + out_y * out_y);
        const double x_c = (out_x * ret(1) - out_y * ret(2)) / r_xy;
        const double y_c = (out_y * ret(1) + out_x * ret(2)) / r_xy;

        ret(1) = x_c;
        ret(2) = y_c;

        return {ret, extrapolating};
    }
};

