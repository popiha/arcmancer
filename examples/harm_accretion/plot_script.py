"""
A script to plot images and spectra from the output of the main executable.
"""

import argparse
import h5py

import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable
import matplotlib.animation as animation


def plot_freq_image(f, frame_num, freq_num, interpolation):    
    """
    Plot images of the Stokes parameters
    """

    key = "frame_{:d}/freq_{:d}".format(frame_num, freq_num)

    w,h = f.attrs["xspan"], f.attrs["yspan"]
    freq = f[key].attrs["frequency"]
    I = np.array(f[key]["I"])
    Q = np.array(f[key]["Q"])
    U = np.array(f[key]["U"])
    V = np.array(f[key]["V"])


    fig, axarr = plt.subplots(2,2, sharex='all', sharey='all')
    fig.suptitle(r"$\nu = {:1.4g} \,\mathrm{{Hz}}$".format(freq))
    
    img = axarr[0,0].imshow(I, extent=(-w,w,-h,h),  origin='lower', 
               interpolation=interpolation,
               cmap="afmhot")
    
    axarr[0,0].set_title("$I$")
    
    Qmax = np.max(abs(Q))
    img = axarr[0,1].imshow(Q, extent=(-w,w,-h,h),  origin='lower', 
               interpolation=interpolation,
               cmap="RdBu",
               vmin=-Qmax, vmax=Qmax)
    
    axarr[0,1].set_title("$Q$")
    
    Umax = np.max(abs(U))
    img = axarr[1,0].imshow(U, extent=(-w,w,-h,h),  origin='lower', 
               interpolation=interpolation,
               cmap="RdBu",
               vmin=-Umax, vmax=Umax)
    
    axarr[1,0].set_title("$U$")

    Vmax = np.max(abs(V))
    img = axarr[1,1].imshow(V, extent=(-w,w,-h,h),  origin='lower', 
               interpolation=interpolation,
               cmap="RdBu",
               vmin=-Vmax, vmax=Vmax)
    
    axarr[1,1].set_title("$V$")

    axarr[1,0].set_xlabel("$x/M$")
    axarr[1,1].set_xlabel("$x/M$")
    axarr[0,0].set_ylabel("$y/M$")
    axarr[1,0].set_ylabel("$y/M$")
    
    for ax in axarr.flatten():
        # add colorbars
        divider = make_axes_locatable(ax)
        cax = divider.append_axes("right", size="5%", pad=0.05)
        fig.colorbar(ax.images[0], cax=cax)

    
    plt.tight_layout()


def animate_freq_images(f, freq_num, interpolation, interval):    
    """
    Animation of the above
    """

    keys = ["frame_{:d}/freq_{:d}".format(frame_num, freq_num) 
            for frame_num in range(len(f.keys()))]

    w,h = f.attrs["xspan"], f.attrs["yspan"]
    freq = f[keys[0]].attrs["frequency"]
    
    ts = [f["frame_{:d}".format(frame_num)].attrs["t_offset"] - f.attrs["r_obs"]
          for frame_num in range(len(f.keys()))]

    Is = [np.array(f[key]["I"]) for key in keys]
    Qs = [np.array(f[key]["Q"]) for key in keys]
    Us = [np.array(f[key]["U"]) for key in keys]
    Vs = [np.array(f[key]["V"]) for key in keys]


    fig, axarr = plt.subplots(2,2, sharex='all', sharey='all')
    fig.suptitle(r"$\nu = {:1.4g} \,\mathrm{{Hz}}$".format(freq))
    
    I_img = axarr[0,0].imshow(Is[0], extent=(-w,w,-h,h),  origin='lower', 
               interpolation=interpolation, 
               vmax=np.max(Is),
               animated=True,
               cmap="afmhot")
    
    axarr[0,0].set_title("$I$")
    
    Qmax = np.max(np.abs(Qs))
    Q_img = axarr[0,1].imshow(Qs[0], extent=(-w,w,-h,h),  origin='lower', 
               interpolation=interpolation,
               cmap="RdBu",
               animated=True,
               vmin=-Qmax, vmax=Qmax)
    
    axarr[0,1].set_title("$Q$")
    
    Umax = np.max(np.abs(Us))
    U_img = axarr[1,0].imshow(Us[0], extent=(-w,w,-h,h),  origin='lower', 
               interpolation=interpolation,
               cmap="RdBu",
               animated=True,
               vmin=-Umax, vmax=Umax)
    
    axarr[1,0].set_title("$U$")

    Vmax = np.max(np.abs(Vs))
    V_img = axarr[1,1].imshow(Vs[0], extent=(-w,w,-h,h),  origin='lower', 
               interpolation=interpolation,
               cmap="RdBu",
               animated=True,
               vmin=-Vmax, vmax=Vmax)
    
    axarr[1,1].set_title("$V$")

    axarr[1,0].set_xlabel("$x/M$")
    axarr[1,1].set_xlabel("$x/M$")
    axarr[0,0].set_ylabel("$y/M$")
    axarr[1,0].set_ylabel("$y/M$")
    
    for ax in axarr.flatten():
        # add colorbars
        divider = make_axes_locatable(ax)
        cax = divider.append_axes("right", size="5%", pad=0.05)
        fig.colorbar(ax.images[0], cax=cax)

    
    plt.tight_layout()

    # put the time into an axes text to allow blitting, 
    t_text = axarr[0,0].text(0.05, 0.9, "$t={:1.4g}M$".format(ts[0]),
                             color='white',
                             transform=axarr[0,0].transAxes)

    def update_fig(frame_num):
        I_img.set_array(Is[frame_num])
        Q_img.set_array(Qs[frame_num])
        U_img.set_array(Us[frame_num])
        V_img.set_array(Vs[frame_num])

        t_text.set_text("$t/M={:1.4g}$".format(ts[frame_num]))

        return I_img, Q_img, U_img, V_img, t_text

    return animation.FuncAnimation(fig, update_fig, frames=list(range(len(ts))), 
                                   repeat=True, blit=True, interval=interval)



def plot_spectrum(data):
    """
    Plot spectrums integrated over the image in arbitrary units.
    Needs to be passed a single frame
    """

    freqs = []
    Is = []
    Qs = []
    Us = []
    Vs = []
    Ps = []
    polangles = []

    # compute the data
    for key in data.keys():
        freq = data[key].attrs["frequency"]
        I = np.sum(data[key]["I"])
        Q = np.sum(data[key]["Q"])
        U = np.sum(data[key]["U"])
        V = np.sum(data[key]["V"])

        polfrac = np.sqrt(Q**2+U**2+V**2)/I

        freqs.append(freq)
        Is.append(I)
        Qs.append(Q)
        Us.append(U)
        Vs.append(V)
        Ps.append(polfrac)
        polangles.append(.5*np.arctan2(U,Q))

    # sort in ascending frequency order
    inds = np.argsort(freqs)

    freqs = np.array(freqs)[inds]
    Is = np.array(Is)[inds]
    Qs = np.array(Qs)[inds]
    Us = np.array(Us)[inds]
    Vs = np.array(Vs)[inds]
    Ps = np.array(Ps)[inds]
    polangles = np.array(polangles)[inds]


    fig, (ax1,ax2) = plt.subplots(2,1, sharex='col')

    ax1.plot(freqs, Is, '.-', label="$I$", color='tab:blue')
    ax1.plot(freqs, Qs, '.-', label="$Q$", color='tab:orange')
    
    ax1.plot(freqs, Us, '.-', label="$U$", color='tab:green')
    ax1.plot(freqs, Vs, '.-', label="$V$", color='tab:red')
   
    ax1.set_ylabel(r"$IQUV$ (arb. units)")


    # polarization degree and angle
    ax2.plot(freqs, Ps, '.-', label='$P$', color='tab:olive')
    ax2.set_ylabel("$P$")

    ax2_ = ax2.twinx()
    ax2_.plot(freqs, np.degrees(polangles%np.pi), '.-',
              label=r"$\psi$", color='tab:purple')
    ax2_.set_ylabel(r"$\psi (^\circ)$")
    

    ax2.set_xlim(freqs[0], freqs[-1])

    ax2.set_xlabel(r"$\nu (\mathrm{Hz})$")
    
    h1,l1 = ax1.get_legend_handles_labels()
    h2,l2 = ax2.get_legend_handles_labels()
    h2_,l2_ = ax2_.get_legend_handles_labels()

    fig.legend(h1+h2+h2_, l1+l2+l2_)
    
def plot_time_series(f, freq_num):
    """
    Plot spectrums integrated over the image in arbitrary units.
    Needs to be passed a single frame
    """

    Is = []
    Qs = []
    Us = []
    Vs = []
    Ps = []
    polangles = []

    keys = ["frame_{:d}/freq_{:d}".format(frame_num, freq_num) 
            for frame_num in range(len(f.keys()))]
    # compute the data
    for key in keys:
        freq = data[key].attrs["frequency"]
        I = np.sum(data[key]["I"])
        Q = np.sum(data[key]["Q"])
        U = np.sum(data[key]["U"])
        V = np.sum(data[key]["V"])

        polfrac = np.sqrt(Q**2+U**2+V**2)/I

        Is.append(I)
        Qs.append(Q)
        Us.append(U)
        Vs.append(V)
        Ps.append(polfrac)
        polangles.append(.5*np.arctan2(U,Q))
    
    ts = [f["frame_{:d}".format(frame_num)].attrs["t_offset"] - f.attrs["r_obs"]
          for frame_num in range(len(f.keys()))]


    Is = np.array(Is)
    Qs = np.array(Qs)
    Us = np.array(Us)
    Vs = np.array(Vs)
    Ps = np.array(Ps)
    polangles = np.array(polangles)


    fig, (ax1,ax2) = plt.subplots(2,1, sharex='col')

    ax1.plot(ts, Is, '.-', label="$I$", color='tab:blue')
    ax1.plot(ts, Qs, '.-', label="$Q$", color='tab:orange')
    
    ax1.plot(ts, Us, '.-', label="$U$", color='tab:green')
    ax1.plot(ts, Vs, '.-', label="$V$", color='tab:red')
   
    ax1.set_ylabel(r"$IQUV$ (arb. units)")


    # polarization degree and angle
    ax2.plot(ts, Ps, '.-', label='$P$', color='tab:olive')
    ax2.set_ylabel("$P$")

    ax2_ = ax2.twinx()
    ax2_.plot(ts, np.degrees(polangles%np.pi), '.-',
              label=r"$\psi$", color='tab:purple')
    ax2_.set_ylabel(r"$\psi (^\circ)$")
    

    ax2.set_xlim(ts[0], ts[-1])

    ax2.set_xlabel(r"$t (M)$")
    
    h1,l1 = ax1.get_legend_handles_labels()
    h2,l2 = ax2.get_legend_handles_labels()
    h2_,l2_ = ax2_.get_legend_handles_labels()

    fig.legend(h1+h2+h2_, l1+l2+l2_)


if __name__ == '__main__':
    
    parser = argparse.ArgumentParser()
    parser.add_argument("data_file", 
                        help="The file containing the harm_accretion output")
    
    parser.add_argument("--freq_index",
                        help="Index of the frequency to plot as images",
                        type=int,
                        default=0)
    
    parser.add_argument("--frame_index",
                        help="Index of the frame to plot as images. No effect if animate specified.",
                        type=int,
                        default=0)

    parser.add_argument("--interpolation",
                        help="Interpolation option for pyplot imshow",
                        default='none')

    parser.add_argument("--spectrum",
                        help="Also plot a spectrum of the parameters",
                        action="store_true")
    
    parser.add_argument("--time_series",
                        help="Also plot a time series of the parameters",
                        action="store_true")
    
    parser.add_argument("--animate",
                        help="Animate the images",
                        action="store_true")

    parser.add_argument("--animation_interval",
                        help="Interval between frames in ms",
                        type=float,
                        default=25)
    
    parser.add_argument("--print_settings",
                        help="Also print the settings properties stored in the"
                             "data file.",
                        action="store_true")
    
    args = parser.parse_args()
    data = h5py.File(args.data_file, 'r')

    if args.animate:
        ani = animate_freq_images(data, args.freq_index, args.interpolation,
                                  args.animation_interval)
    else:
        plot_freq_image(data, args.frame_index, args.freq_index, args.interpolation)

    if args.spectrum:
        plot_spectrum(data['frame_{:d}'.format(args.frame_index)])

    if args.time_series:
        plot_time_series(data, args.freq_index)

    if args.print_settings:
        for key in data.attrs.keys():
            print(key, ":", data.attrs[key])

    plt.show()
