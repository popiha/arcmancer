#pragma once

#include <arcmancer/geometry.h>
#include <arcmancer/radiation/radiation_types.h>
#include <arcmancer/spacetimes/kerr.h>

#include "harm_data.h"

// In any larger application this should not be done at file scope in headers,
// but here it keeps things concise.
using namespace arcmancer;
using namespace arcmancer::kerr;

// A functor for computing FluidData from HARM data.
// Works directly as the fluid model in the radiation_transfer function.
class HarmFluid {
public:
    // Store pointers to the needed objects
    HarmFluid(const KerrSpacetime &M, const HarmData &harm,
              const HarmToCGS &harm_conv)
    : M_(&M), harm_(&harm), harm_conv_(&harm_conv) {}

    // Computing the fluid data
    FluidData<KerrSpacetime> operator()(
        const ManifoldPoint<KerrSpacetime> &x) const {
        auto data = harm_->data(x);
        
        // convert the quantities
        const double rho = data.density * harm_conv_->density_to_cgs;
        const double n = rho/units::m_p_cgs;
        const double p = data.pressure * harm_conv_->pressure_to_cgs;

        // for temperature, Schnittman uses radiation dominated fluid,
        // Dexter uses ideal gas equation of state.
        // XXX: use radiation dominated fluid, since it's relevant for
        // inner parts ( <~ 100 M_sch) of the accretion disk
        //const double T = pow(3 * p / units::a_rad_SI, 1./4);
        // or use ideal gas law as Dexter does

        double T = p / (n * harm_conv_->k_cgs_);

        // The grtrans computations in the original paper use T_e = T_p,
        // so use that here as well, and don't scale the temperature.

        // for magnetic field, assume that HARM uses Lorentz-Heaviside
        // units internally, as implied by Dexter's Grtrans code.
        const auto B_vec = data.B;
        const double B2 = dot_product(B_vec, B_vec);
        const double bmag = std::sqrt(-B2);
        const double B = bmag * harm_conv_->Bfield_to_cgs;

        auto u = data.u;

        // handle error cases
        if (dot_product(u, u) < 0) {
            Log::log()->debug("interpolated u spacelike at x :\n{}", x);
            // use zero spatial momentum instead of an unphysical, spacelike u
            u = normalized(CotangentVector<KerrSpacetime>{
                               x, M_->kerr_schild_out, {1, 0, 0, 0}})
                    .raise_index<0>();
        }

        if (B2 > 0) {
            Log::log()->debug("interpolated B timelike at x:\n{}", x);
            // just return zero density and magnetic field strength,
            // can't really handle this properly
            return {0, T, p, 0, B_vec, u};
        }

        return {n, T, p, B, B_vec, u};
    }

private:
    
    const KerrSpacetime *M_;
    const HarmData *harm_;
    const HarmToCGS *harm_conv_;
};
