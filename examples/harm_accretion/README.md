HARM Accretion Flow Post Processing
===================================

This example is a simple program to post process GRMHD plasma data around a Kerr
black hole produced using the [HARM 1.0][harm] 
([Gammie et al. 2003][harm-paper1], [Noble et al. 2006][harm-paper2]) 2D code.

The example program supports computing polarized images and spectra of
using single or multiple snapshots of HARM output.
If multiple snapshots are used, the time dependence of the plasma is taken into
account for the span of the data, otherwise the plasma is assumed to not change
in time.
It is also possible to compute multiple frames to produce animations.

The program has been confirmed to work at least with the output from HARM in
its default configuration, as well as with the `dump040` file available in the
[grtrans github][grtrans] repository.

The radiation from the plasma is modelled using the 
thermal synchrotron radiation model of [Dexter (2016)][grtrans-paper].
The fluid data is scaled using the accretion rate and black hole mass which 
can be specified as command line parameters.
Note that the radiation model only works for hot enough plasma, and the
temperature is clipped at a configurable lower limit to avoid numerical issues.

This example employs the main radiative transfer features of Arcmancer,
including the definition of fluid and radiation models as well as the
computation of images and spectra.
The code used in this example should also be fairly easy to adapt to other
similar computations, including fully 3D plasma data and more complicated
radiation models. 

The program is divided into files as follows:

- *main.cpp*: Definition of the computation and code related to the command line
  interface and saving results.

- *harm_data.h*: Reading and interpolation of HARM data dumps

- *harm_fluid.h*: Definition of the fluid model using HARM data

- *thermal_synchrotron.h*: A modified version of the thermal synchrotron
  radiation model included in the main library.

- *plot_script.py*: Visualization of computed data.


Building
--------

After Arcmancer has been configured,
run the following commands in this directory:

    mkdir build
    cd build
    cmake .. -DCMAKE_BUILD_TYPE=RELEASE
    make

Running
-------

The build process produces the executable `harm_accretion`
in the build directory.
Assuming that the `dump040` file has been moved to the build directory, 
run (in the build directory)

    ./harm_accretion dump040

This computes a low resolution image at 230 GHz with a black hole mass
corresponding to Sgr A\*.
The computation results are stored in the HDF5 file `img.h5`.
To plot these results, run (in the build directory)

    python3 ../plot_script.py img.h5

For the complete list of available options, run

    ./harm_accretion --help
    python3 ../plot_script.py --help

The options include an option for using various tweaks to
match the computations done by [grtrans][grtrans] (at commit `f8c3007`).

Note that the convergence of the radiative transfer can be controlled with two
types of parameters: the sampling interval and the tolerances.
The parameter `radiation_tolerance_rel` controls the allowed relative error per
step, and is the main parameter that should be changed for balance between speed
and accuracy.
The parameter `radiation_tolerance_abs` acts as a floor for error control in
regions where the radiation intensities are low.
The default value is appropriate for the default parameters and the `dump040`
data set, but other cases may require altering this value to ensure correct
results and to avoid the integrator getting stuck in low intensity regions.
See also the section on tolerances in the main documentation.
The sampling interval setting can be
used to force a small enough stepsize if for some reason the adaptive
integration does not behave well.


[grtrans-paper]: http://adsabs.harvard.edu/abs/2016ascl.soft05013D "Dexter 2016"
[grtrans]: https://github.com/jadexter/grtrans "GRTRANS github"
[harm]: http://rainman.astro.illinois.edu/codelib/ "HARM distribution site"
[harm-paper1]: https://doi.org/10.1086/374594 "Gammie et al. 2003"
[harm-paper2]: https://doi.org/10.1086/500349 "Noble et al. 2006"
