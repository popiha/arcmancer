#include <iostream>
#include <string>

#include <boost/program_options.hpp>

#include <arcmancer/arcmancer.h>
#include <arcmancer/spacetimes/kerr.h>

#include "harm_fluid.h"
#include "thermal_synchrotron.h"

using namespace arcmancer;
using namespace arcmancer::kerr;
namespace po = boost::program_options;

// for storing program options
struct Options {
    double inclination; // inclination in degrees
    double r_obs;       // distance of the image plane
    double t_offset;    // time offset of the start of the observations

    double t_span; // time span of the animation
    size_t frames; // how many frames for animation

    double f1;    // first frequency
    double f2;    // last frequency
    size_t freqs; // number of frequencies
    size_t xbins; // resolution
    size_t ybins;
    double xspan; // size of the image plane
    double yspan;

    double bh_mass;             // central mass in solar masses
    double accretion_rate_cgs;  // accretion rate in CGS units
    double accretion_rate_code; // accretion rate in HARM units

    double ray_tolerance_abs; // integration tolerances
    double ray_tolerance_rel; // integration tolerances
    double radiation_tolerance_abs;
    double radiation_tolerance_rel;
    double radiation_sampling;

    double theta_e_min;         // cutoff for normalized electron temperature
    bool grtrans_compatibility; // grtrans compatibility
    bool scalar_transport;      // disable polarization effects

    std::string output_file;
    std::vector<std::string> input_files;
};

// main computation function
void compute(const Options &opt) {

    // Read the HARM dump and set up the corresponding spacetime

    // It's possible that this throws an exception, e.g. if the input file
    // doesn't exist. These are left unhandled for simplicity.
    auto harm_header = HarmData::read_header(opt.input_files[0]);
    KerrSpacetime M(1, harm_header.a);

    HarmToCGS harm_conv(opt.bh_mass, opt.accretion_rate_cgs,
                        opt.accretion_rate_code, opt.grtrans_compatibility);

    std::cout << "Loading data from " << opt.input_files.size()
              << " file(s)... " << std::endl;
    HarmData harm(opt.input_files, M);

    std::cout << "Spacetime " << M << std::endl
              << "HARM maximum r " << harm.r_max() << std::endl
              << "HARM time span [" << harm.t_span().first << ", "
              << harm.t_span().second << "]" << std::endl
              << harm.header() << std::endl;

    // set tolerances
    M.configuration.curve.propagation.tolerance.absolute = opt.ray_tolerance_abs;
    M.configuration.curve.propagation.tolerance.relative = opt.ray_tolerance_rel;
    M.configuration.line_integral.propagation.tolerance.absolute =
        opt.radiation_tolerance_abs;
    M.configuration.line_integral.propagation.tolerance.relative =
        opt.radiation_tolerance_rel;

    M.configuration.line_integral.propagation.sampling_interval =
        opt.radiation_sampling;
    M.configuration.line_integral.propagation.enforce_sampling_interval = true;

    // using a fixed chart makes computation significantly faster, since chart
    // selection in dot_products etc doesn't take time.
    // Using the same chart as HarnmDta also avoids doing excess coordinate
    // transformations.
    // Leaving everything on auto increases runtime by ~50%
    M.use_fixed_chart(M.kerr_schild_out);

    // Set up the radiation and fluid models
    // Note that the ThermalSynchrotron class used here is a modified version
    // of the one included in the main library.
    ThermalSynchrotron tsynch(harm_conv.length_to_cgs, opt.theta_e_min,
                              opt.grtrans_compatibility, opt.scalar_transport);

    HarmFluid harm_fluid(M, harm, harm_conv);

    // Initial intensities (zeros) for all the frequencies that are computed
    std::vector<StokesVector> init_intensities_vec;

    for (size_t i = 0; i < opt.freqs; ++i) {
        double freq =
            opt.f1 + (opt.f2 - opt.f1) * i / std::max(opt.freqs - 1, 1lu);
        init_intensities_vec.emplace_back(StokesVector({freq, 0, 0, 0, 0}));
    }

    PolarizationSpectrum initial_intensities(init_intensities_vec);

    // Construct surfaces necessary for the computation.

    // Event horizon.
    // To avoid zones where HARM data interpolation produces garbage, set the
    // horizon limit a bit outwards.
    // The ray integration can handle passing the horizon fine in
    // the outgoing KS chart, so this can be changed if working with extremal
    // holes where it could have an effect on the result, altough the HARM data
    // interpolation will still likely fail.
    OutgoingKerrSphere eh(M, M.horizon_r() * 1.05);

    // Surfaces limiting the radiating region.
    // The inner surface needs to be within the data range to avoid
    // weird effects from the sudden transition to empty space. The outer
    // surface doesn't suffer from this since the intensity is 0 there anyway
    double inner_surf_r = harm.r_max() * (1 - 2e-2),
           outer_surf_r = harm.r_max() * (1 - 1e-2);

    // grtrans only goes up to u = 0.04 -> r = 2.5
    if (opt.grtrans_compatibility) {
        inner_surf_r = 25;
        outer_surf_r = 25.01;
    }
    OutgoingKerrSphere inner_rad_surf(M, inner_surf_r);
    OutgoingKerrSphere outer_rad_surf(M, outer_surf_r);

    double t_span = opt.t_span;
    if (t_span < 0) { t_span = harm.t_span().second - harm.t_span().first; }

    // Central point of the image plane
    auto p = M.create_point(
        M.boyer_lindquist, {0, opt.r_obs, opt.inclination * cnst::pi / 180, 0});

    // Construct the image plane
    // Using BL coordinates for constructing the image plane is simple,
    // but can fail for small inclinations. This could be fixed by using
    // either of the cartesian Kerr-Schild coordinates
    ImagePlane<KerrSpacetime, PolarizationSpectrum> ip(
        {opt.xbins, opt.ybins, opt.xspan, opt.yspan},
        {p, M.boyer_lindquist, {1, 0, 0, 0}},
        {p, M.boyer_lindquist, {0, 1, 0, 0}},
        {p, M.boyer_lindquist, {0, 0, 0, 1}});

    // Data is saved into a hdf5 file
    H5::H5File h5file(opt.output_file, H5F_ACC_TRUNC);
    std::cout << "Writing data in HDF5 file " + opt.output_file << std::endl;
    // First the settings
    ip.write_plane_attributes(h5file);

    EigenHDF5::save_scalar_attribute(h5file, "inclination", opt.inclination);
    EigenHDF5::save_scalar_attribute(h5file, "r_obs", opt.r_obs);
    EigenHDF5::save_scalar_attribute(h5file, "t_offset", opt.t_offset);
    EigenHDF5::save_scalar_attribute(h5file, "bh_mass", opt.bh_mass);
    EigenHDF5::save_scalar_attribute(h5file, "bh_chi", M.chi());
    EigenHDF5::save_scalar_attribute(h5file, "accretion_rate_code",
                                     opt.accretion_rate_code);
    EigenHDF5::save_scalar_attribute(h5file, "accretion_rate_cgs",
                                     opt.accretion_rate_cgs);
    EigenHDF5::save_scalar_attribute(h5file, "ray_tolerance_abs",
                                     opt.ray_tolerance_abs);
    EigenHDF5::save_scalar_attribute(h5file, "ray_tolerance_rel",
                                     opt.ray_tolerance_rel);
    EigenHDF5::save_scalar_attribute(h5file, "radiation_tolerance_abs",
                                     opt.radiation_tolerance_abs);
    EigenHDF5::save_scalar_attribute(h5file, "radiation_tolerance_rel",
                                     opt.radiation_tolerance_rel);
    EigenHDF5::save_scalar_attribute(h5file, "radiation_sampling",
                                     opt.radiation_sampling);

    // XXX: h5py doesn't read bools saved using this function correctly,
    // so using ints for now. Maybe a bug on our side.
    if (opt.grtrans_compatibility) {
        EigenHDF5::save_scalar_attribute(h5file, "GRTRANS_COMPATIBILITY", 1);
    } else {
        EigenHDF5::save_scalar_attribute(h5file, "GRTRANS_COMPATIBILITY", 0);
    }

    // list of input files as a string
    std::stringstream input_files_ss;
    for (auto &f : opt.input_files) {
        input_files_ss << f << " ";
    }
    EigenHDF5::save_scalar_attribute(h5file, "input_files",
                                     input_files_ss.str());

    std::cout << "Computing images...\n";
    // Loop over the frames and compute
    for (size_t frame = 0; frame < opt.frames; ++frame) {

        std::cout << "Computing frame " << frame + 1 << "/" << opt.frames
                  << std::endl;

        // Set the time offset for the frame
        // Shifting the time of the data is better since we don't have to
        // generate a new image plane for every frame
        double dt = opt.frames > 1 ? t_span / (opt.frames - 1) : 0;
        harm.t_offset =
            opt.r_obs + harm.t_span().first + opt.t_offset + dt * frame;

        // Perform the computation. The calculations to be performed on each
        // pixel are conveniently defined as a lambda. Possible errors in the
        // ray computations could be caught inside the compute function to
        // avoid a single failing pixel ruining the whole image. This is not
        // done here, as usually such failures are a result of some error
        // somewhere else.
        ip.compute([&eh, &outer_rad_surf, &inner_rad_surf, &harm_fluid, &tsynch,
                    &initial_intensities,
                    r_obs = opt.r_obs](auto &p) -> PolarizationSpectrum {
            const double l = -5 * r_obs;
            auto h = p.frame.e_x(), v = p.frame.e_y();

            using Geo =
                Geodesic<KerrSpacetime, PolarizationFrame<KerrSpacetime>>;
            Geo ray(p.ray_tangent, {v, h});

            // First compute to a surface enclosing the radiating
            // volume. This way we don't need to do radiation transfer
            // for long distances in vacuum.
            Geo::ComputationResult status = ray.compute(l, {&inner_rad_surf});

            // The ray should have terminated on the surface around the
            // radiating region
            if (status == Geo::ComputationResult::hit_surface) {

                // compute the ray inside the radiating area
                Geo ray2(ray.front());
                status = ray2.compute(l, {&eh, &outer_rad_surf});

                // The ray should hit either the outer surface -> escape
                // to infinity or the event horizon. Other results mean
                // that the ray terminates in the middle of the plasma
                if (status == Geo::ComputationResult::hit_surface) {
                    // normalization of the ray tangent
                    double E0 = dot_product(p.frame.e_t(), p.ray_tangent);

                    // perform radiative transfer
                    auto res = radiation_transfer(ray2, E0, initial_intensities,
                                                  harm_fluid, tsynch);

                    return res;
                } else {
                    // Something went wrong, but this should be rare /
                    // not happen at all, so just a basic warning
                    Log::log()->warn(
                        "Ray inside the radiating region returned "
                        "unexpected "
                        "status {} (expected "
                        "ComputationResult::hit_surface), "
                        "pixel at ix={}, iy={} may be erroneous.",
                        status, p.ix, p.iy);
                    return initial_intensities;
                }
            }

            // The ray missed the radiating region.
            return initial_intensities;
        });

        // The data is saved as a group for each frame and a group for each
        // frequency with the path being `/frame_i/freq_j` for some i,j.
        // These groups then contain the arrays of observed Stokes intensities.
        auto frame_group =
            h5file.createGroup("/frame_" + std::to_string(frame));

        EigenHDF5::save_scalar_attribute(frame_group, "t_offset",
                                         harm.t_offset);

        for (size_t i = 0; i < opt.freqs; ++i) {

            auto group = frame_group.createGroup("freq_" + std::to_string(i));

            EigenHDF5::save_scalar_attribute(
                group, "frequency", initial_intensities[i].frequency());

            EigenHDF5::save(group, "I", ip.scalar_data_array([i](auto &dp) {
                return dp.data[i].rest_frame_I();
            }));

            EigenHDF5::save(group, "Q", ip.scalar_data_array([i](auto &dp) {
                return dp.data[i].rest_frame_Q();
            }));

            EigenHDF5::save(group, "U", ip.scalar_data_array([i](auto &dp) {
                return dp.data[i].rest_frame_U();
            }));

            EigenHDF5::save(group, "V", ip.scalar_data_array([i](auto &dp) {
                return dp.data[i].rest_frame_V();
            }));
        }
        // Flush the file after each frame so that the already computed data is 
        // not lost in case there is an error or the program is terminated.
        h5file.flush(H5F_SCOPE_GLOBAL);
    }
}

int main(int argc, char **argv) {
    // configure logging, info is the default level
    Log::log()->set_level(Log::level::info);

    // Command line options
    Options opt;

    po::options_description desc("Options");

    // clang-format off
    desc.add_options()
        ("help", "Print help messages")

        ("input_files", po::value<std::vector<std::string>>(&opt.input_files),
         "Names of the input HARM dump files in ascending order. "
         "Works also by specifying the files as the last arguments")

        ("output_file",
         po::value<std::string>(&opt.output_file)->default_value("img.h5"),
         "Name of the output file")

        ("inclination",
         po::value<double>(&opt.inclination)->default_value(50),
         "Inclination of the image plane in degrees")

        ("f1", po::value<double>(&opt.f1)->default_value(230e9),
         "The first frequency in Hz, or the only frequency if "
         "freqs=1")

        ("f2", po::value<double>(&opt.f2)->default_value(330e9),
         "The last frequency in Hz")

        ("freqs", po::value<size_t>(&opt.freqs)->default_value(1),
         "Number of frequncies evenly spaced between f1 "
         "and f2")

        ("r_obs", po::value<double>(&opt.r_obs)->default_value(1e4),
         "Distance of the image plane from the origin")

        ("t_offset", po::value<double>(&opt.t_offset)->default_value(0),
         "Starting time coordinate of the observations"
         " = t_offset + r_obs + time of first data file")

        ("t_span",po::value<double>(&opt.t_span)->default_value(-1),
         "Total time span of observations when computing multiple frames."
         "Negative values will give the total data time span.")

        ("frames", po::value<size_t>(&opt.frames)->default_value(1),
         "Number of image frames to compute")

        ("xbins",po::value<size_t>(&opt.xbins)->default_value(100),
         "Number of pixels in the horizontal direction.\n"
         "The number of pixels in the vertical direction is "
         "determined automatically to make the pixels square.")

        ("xspan", po::value<double>(&opt.xspan)->default_value(20),
         "Half the width of the image plane.\n"
         "The x coordinate goes from -xpan to xspan.")

        ("yspan",
         po::value<double>(&opt.yspan)->default_value(20),
         "Half the height of the image plane.\n"
         "The y coordinate goes from -ypan to yspan.")

        ("bh_mass", po::value<double>(&opt.bh_mass)->default_value(4e6),
         "Mass of the black hole in solar masses")

        // the following default accretion rates is used by grtrans for HARM
        // test
        ("accretion_rate_cgs",
         po::value<double>(&opt.accretion_rate_cgs)->default_value(1.57e15),
         "Accretion rate in cgs units")

        ("accretion_rate_code",
         po::value<double>(&opt.accretion_rate_code)->default_value(3e-3),
         "Accretion rate in HARM internal units")

        ("ray_tolerance_abs",
         po::value<double>(&opt.ray_tolerance_abs)
            ->default_value(std::numeric_limits<double>::min()),
         "Absolute tolerance used for ray integration")

        ("ray_tolerance_rel",
         po::value<double>(&opt.ray_tolerance_rel)->default_value(1e-9),
         "Relative tolerance used for ray integration")

        ("radiation_tolerance_abs",
         po::value<double>(&opt.radiation_tolerance_abs)
            ->default_value(1e-70),
         "Absolute tolerance used for radiation transfer integration")

        ("radiation_tolerance_rel",
         po::value<double>(&opt.radiation_tolerance_rel)
             ->default_value(1e-6),
         "Relative tolerance used for radiation transfer "
         "integration")

        ("radiation_sampling_interval",
         po::value<double>(&opt.radiation_sampling)
             ->default_value(2e-2),
         "Maximum relative stepsize for radiation transfer integration")

        ("theta_e_min",
         po::value<double>(&opt.theta_e_min)
             ->default_value(5e-2),
         "Minimum cutoff for dimensionless electron "
         "temperature when not in grtrans mode")

        ("grtrans_mode",
         po::value<bool>(&opt.grtrans_compatibility)->default_value(false),
         "Enforce compatibility with the published "
         "version of GRTRANS (Dexter 2016)")

        ("scalar_transport",
         po::value<bool>(&opt.scalar_transport)->default_value(false),
         "Only propagate intensity, disregard polarization effects.")
        ;
    // clang-format on

    po::positional_options_description pos;
    pos.add("input_files", -1);

    po::variables_map vm;

    try {
        po::store(po::command_line_parser(argc, argv)
                      .options(desc)
                      .positional(pos)
                      .run(),
                  vm);

        if (vm.count("help")) {
            std::cout << "HARM accretion disk post processing" << std::endl
                      << desc << std::endl;

            return 0;
        }

        po::notify(vm);

        if (vm.count("input_files") == 0) {
            std::cout << "Specify an input file!" << std::endl
                      << desc << std::endl;

            return 1;
        }

        opt.ybins = opt.xbins * (opt.yspan / opt.xspan);

    } catch (po::error &e) {
        std::cerr << "ERROR: " << e.what() << std::endl << std::endl;
        std::cerr << desc << std::endl;
        return 1;
    }

    compute(opt);

    return 0;
}
