Change Log
==========

v0.3.1
------

This minor update fixes a number of issues and minor bugs. An non-exhaustive
list of changes:

* Removed the cppoptlib optimization library used in barycentric interpolation.
  It was replaced by the optimization features provided with the
  Eigen-unsupported library provided with Eigen.

* The bundled version of Eigen was updated to version 3.3.7.

* Removed calls to broken or deprecated Matplotlib functions in the Python
  scripts.

* Fixed bugs in velocity space interpolation.


v0.3.0
------

The new version provides a major restructuring of how the configuration
parameters are set. This change breaks backward compatibility.

New features:

* Modular configuration of different subcomponents of the library. The
  old Configuration structure has been disassembled into several
  orthogonal subcomponents.

* Interpolation for unstructured (i.e. not on a grid) data on any
  Riemannian or semi-Riemannian manifold. This is achieved using an
  extension of barycentric interpolation, see Pihajoki, Mannerkoski &
  Johansson (2019, in prep.).

* Euclidean spaces of dimensions 2-4. These are purely Riemannian and
  can be used instead of using e.g. a 3-dimensional subspace of a
  Minkowski space.

* Tensor spaces, which represent the space of tensors at a given point
  of a MetricSpace. These work just like a MetricSpace does.

* Immersion, which is again a MetricSpace, representing an immersed
  subspace of a MetricSpace. Immersion automatically computes the
  pullback metric and its derivatives.

* Functionality for constructing the spaces of velocity vectors at a
  point using the Immersion class.

* Adjusted logging levels in the library to provide a more balanced
  tradeoff between too much output and information on the progress of
  long computations. The default level `info` should now provide a
  decent experience for the most common use case.

Other changes:

* General performance improvements, especially in the radiative transfer
  computations and the `harm_accretion` example program.

* Fixed issues with rank 0 tensors (scalars).

* Fixed miscellaneous small bugs indicated by ASAN and scan-build.

* Some other minor backward incompatible changes to the interface, including
  the removal of `ParametrizedCurve::points()` and changes to the template
  parameters of `line_integral`.

v0.2.0
------

First public release
