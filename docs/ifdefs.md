##Compiler Defines


There are a few preprocessor conditional compilation switches in Arcmancer.
These are mainly useful for debugging and testing, and should not be needed in 
normal use.
These switches can be enabled by defining the name during code compilation.

---

#### `ARCMANCER_DEBUG_CUBIC_SPLINE`
enables addional debug logging in `CubicSpline`

---

#### `ARCMANCER_NO_LOG_INNER_LOOP`
disables some debug logging calls in innermost integration loops. 
May have a minor impact on performance.

---
